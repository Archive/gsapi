
/*  $Header: /cvs/archive/GSAPI/NEWS,v 1.18 2003/10/08 17:40:13 richb Exp $
 *
 *  Copyright (c) 2002-2003 Sun Microsystems, Inc.
 *  All Rights Reserved.
 */

Overview of changes in gnome-speech 0.6.17

* Started fixing up the warning and errors output when generating the
  XML/HTML from the gtk-doc documentation. The XML/HTML documentation
  no longer generates any errors at build time.

* In speech_recognition_rule_alternatives.[c,h], adjusted:

      SpeechStatus
      speech_recognition_rule_alternatives_new_with_subrules_and_weights(
                                              SpeechRecognitionRule *rules[],
                                              float weights[])

      to be:

      SpeechRecognitionRuleAlternatives *
      speech_recognition_rule_alternatives_new_with_subrules_and_weights(
                                              SpeechRecognitionRule *rules[],
                                              float weights[])

* In speech_recognition_rule_name.[c,h], adjusted:

      SpeechStatus
      speech_recognition_rule_alternatives_new_with_subrules_and_weights(
                                              SpeechRecognitionRule *rules[],
                                              float weights[])

      to be:

      SpeechRecognitionRuleName *
      speech_recognition_rule_alternatives_new_with_subrules_and_weights(
                                              SpeechRecognitionRule *rules[],
                                              float weights[])

----

Overview of changes in gnome-speech 0.6.16

* Continued working the comments into gtk-doc format.

----

Overview of changes in gnome-speech 0.6.15

* Continued working the comments into gtk-doc format.

----

Overview of changes in gnome-speech 0.6.14

* Continued working the comments into gtk-doc format.

* speech_recognition_final_dictation_result_getAlternativeTokens()
  should have been:
  speech_recognition_final_dictation_result_get_alternative_tokens()
  in speech_recognition_final_dictation_result.[c,h]

----

Overview of changes in gnome-speech 0.6.13

* Continued working the comments into gtk-doc format.

----

Overview of changes in gnome-speech 0.6.12

* Continued working the comments into gtk-doc format.
* Added a:

    gchar *
    speech_recognition_rule_token_to_string(
                                SpeechRecognitionRuleToken *rule_token);

    function to speech_recognition_rule_token.[c,h]
* Added a:

    gchar *
    speech_recognition_rule_tag_to_string(
                                SpeechRecognitionRuleTag *rule_tag);

    function to speech_recognition_rule_tag.[c,h]

----

Overview of changes in gnome-speech 0.6.11

* Continued working the comments into gtk-doc format.

----

Overview of changes in gnome-speech 0.6.10

* Continued working the comments into gtk-doc format.

----

Overview of changes in gnome-speech 0.6.9

* Continued working the comments into gtk-doc format.
* speech_synthesis_synthesizer_event_get_top_of_queue_changed() in
  speech_synthesis_synthesizer_event.c should return a gboolean not a gchar *.

----

Overview of changes in gnome-speech 0.6.8

* Continued working the comments into gtk-doc format.
* Adjusted the names of various defintions in
  speech_synthesis_synthesizer.h for consistency:
    SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_JSML
    SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_SSML
    SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY
    SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY

----

Overview of changes in gnome-speech 0.6.7

* Fixed up the various ERROR messages generated by gtkdoc-mkdb and several 
  of the WARNING messages.

----

Overview of changes in gnome-speech 0.6.6

* Continued working the comments into gtk-doc format.
* Added in the infrastructure to generate gtk-doc style documentation.
* Changed version number of the three generated libraries from 1.0 to 0.6.

----

Overview of changes in gnome-speech 0.6.5

* Continued working the comments into gtk-doc format.

----

Overview of changes in gnome-speech 0.6.4

* Started working the comments into gtk-doc format (from JavaDoc).

----

Overview of changes in gnome-speech 0.6.3

* Copied in JavaDoc style comments for most files/routines from the
  equivalent IDL calls.
* Fixed up some routine names in speech_engine_mode_desc.[c,h]
* Fixed up some routine names in sppech_synthesis_synthesizer.[c,h]

----

Overview of changes in gnome-speech 0.6.2

* Most of the _new() routines needed to return the proper object reference 
  (not void).
* Needed to add a SPEECH_RESULT_OKAY=0, to the beginning of the SpeechStatus 
  enum in speech_types.h

----

Overview of changes in gnome-speech 0.6.1

* Created a first cut at a C-style API to the speech API. At this point 
  there are header files and .c stubs for all the routines and they compile.
  No real comments added to the files yet though.

----

Overview of changes in gnome-speech 0.6.0

* Preparation for first official release to the GNOME community.

----

Overview of changes in gnome-speech 0.5.28

* Added in numerous HREF links (similar to those found in the JSAPI
  JavaDoc comments.

----

Overview of changes in gnome-speech 0.5.27

* Changes needed to generate the first version of the HTML documentation.

----

Overview of changes in gnome-speech 0.5.26

* Added in the following definitions to GNOME_Speech.idl and adjusted 
  the code in the three IDL files to use them.
 
  interface SpeechObject    : ::Bonobo::Unknown  {};
  interface EventListener : ::Bonobo::Listener {};
  interface InputStream : ::Bonobo::Stream {};
  interface OutputStream : ::Bonobo::Stream {};
 
  typedef Bonobo::BadArg     IllegalArgumentException;
  typedef Bonobo::Control    ControlComponent;
  typedef Bonobo::IOError    IOException;
  typedef CosNaming::NamingContextExt::InvalidAddress MalformedURLException;

----

Overview of changes in gnome-speech 0.5.25

* Adjusted GNOME_Speech_Recognition.idl to have JavaDoc style documentation.

----

Overview of changes in gnome-speech 0.5.24

* Adjusted GNOME_Speech.idl and GNOME_Speech_Synthesis.idl to have JavaDoc
  style documentation.

----

Overview of changes in gnome-speech 0.5.23

* Added in various files used to generate the HTML documentation (via the
  Synopsis documentation tool).

----

Overview of changes in gnome-speech 0.5.22

* "grammarType" changed to "type" to avoid conflict with GrammarType typedef.
* Boolean changed to Tristate to avoid conflict with the keyword "boolean".
* The ::GNOME::Speech::Synthesis::Synthesizer getQueueItem operation
  has been adjusted to return a SythesizerQueueItem.
* The MarkupType typedef has been changed from a long to a string. 
  The two predefined markup types have been adjusted to string definitions.
* The GrammarType typedef has been changed from a long to a string. 
  The two predefined grammar types have been adjusted to string definitions.

----

Overview of changes in gnome-speech 0.5.21

* Speech properties are now Gconf resources.
* Replaced all occurances of "Unicode" with "UTF8".
* Added getNumberOfQueueItems and getQueueItem methods to the Synthesizer
  interface.
* Adjusted loadJSGF methods in the Recognizer interface to be grammar 
  agnostic. Adjusted comments accordingly. Added isGrammarTypeSupported
  method.
* Removed all specific references to JSGF.
* The ruleForJSGF method in the RuleGrammar interface becomes ruleForGrammar.
* Adjusted speakJSML methods in the Synthesizer interface to be markup 
  agnostic. Adjusted comments accordingly. Added isMarkupTypeSupported method.
* Adjusted JSAPI style method names of the type #name(...) in the comments to
  the unique GSAPI equivalents.
* Fixed up all Exception: names in the various comments.

----

Overview of changes in gnome-speech 0.5.20

* Added an initWithRecognizerAndState method to the
  ::GNOME::Speech::Recognition::RecognizerEvent interface.
* Added an initWithSynthesizerAndState method to the
  ::GNOME::Speech::Synthesis::SynthesizerEvent interface.
* Central interface comments now mention gconf resources in:
  ~/.gconf/desktop/gnome/speech/%gconf.xml and
  .../etc/gconf/gconf.xml.defaults/schemas/desktop/gnome/speech/%gconf.xml
* Adjusted the comments for the getStartTime and getEndTime methods in
  ::GNOME::Speech::Recognition::ResultToken interface to change
  "System.currentTimeMillis" to be "the current system time (in milliseconds)".
* Adjusted various "equals" methods to use the specific object they are doing
  an equality test on rather than Bonobo::Unknown.

----

Overview of changes in gnome-speech 0.5.19

* Reinstated the PropertyVetoException for various property set methods in
  ::GNOME::Speech::Recognition and ::GNOME::Speech::Synthesis.
* Adjusted ::GNOME::Speech::EngineProperties::getControlComponent to return 
  a Control rather than a ControlFrame.  Same change for 
  ::GNOME::Speech::Recognition::SpeakerManager::getControlComponent
* Added interface definitions for InputSteam and OutputStream to
  GNOME_Speech_Recognition.idl. and adjusted various methods to use them:
* Created "typedef string GrammarURL" in GNOME_Speech_Recognition.idl
* Created "typedef string SpeechURL" in GNOME_Speech_Synthesis.idl
* Replaced occurances of CosNaming::NamingContextExt::URLString with
  GrammarURL or SpeechURL (as appropriate).
* Removed spurious </A> and </CODE> tags from comments in the IDL files.
* Changed ::GNOME::Speech::Recognition::Recognizer
  loadJSGFFromReader(in Bonobo::Stream JSGF) to:
  loadJSGFFromStream(in InputStream JSGF)

----

Overview of changes in gnome-speech 0.5.18

* Adjusted ChangeLog to the preferred Gnu style.
* Added in various missing files.
* "make" now does enough to generate the .c and .h files from the IDL
  files and compile them.
* The NEW_LINE definition in GNOME_Speech_Recognition.idl needed to be
  "\\n" not "\n" (the latter generated code which didn't compile).

----

Overview of changes in gnome-speech 0.5.17

* Added in gtk-doc style comments for all the interfaces in
  GNOME_Speech_Recognition.idl.
* Name change for first parameter in the loadJSGFFromURL and 
  loadJSGFFromURLWithImports methods.

----

Overview of changes in gnome-speech 0.5.16

* Added in gtk-doc style comments for all the interfaces in
  GNOME_Speech_Synthesis.idl.
* Added in ::GNOME::Speech::Synthesis::SynthesizerQueueItem
  initWithSourceAndText method.
* Changed ::GNOME::Speech::Synthesis::Synthesizer speakText method to 
  speakJSMLText.
* Added in ::GNOME::Speech::Recognition::GrammarSyntaxDetail init method.
* Added in ::GNOME::Speech::Recognition::RuleName isLegalRuleNameFromName
  method.

----

Overview of changes in gnome-speech 0.5.15

* Added in gtk-doc style comments for all the interfaces in GNOME_Speech.idl.
* Added in ::GNOME::Speech::Word initWithDetails method.

----

Overview of changes in gnome-speech 0.5.14

* Added in ::GNOME::Speech::ExceptionValue and ::GNOME::Speech::ErrorValue
  getMessage methods.
* Adjusted numerous methods to correctly raise various exceptions.
* Added NullPointerException and SecurityException to GNOME_Speech.idl.
* Occurances of IllegalArgumentException changed to Bonobo::BadArg.
* GNOME::Speech::AudioListener and GNOME::Speech::EngineListener now extend 
  Bonobo::Listener rather than Bonobo::Unknown
* Changed ::GNOME::Speech::Recognition::setRuleNameWithComponent to
  setRuleNameWithComponents.
* Changed ::GNOME::Speech::Recognition::initFromPackage to
  initFromComponents for consistency.

----

Overview of changes in gnome-speech 0.5.13

* Added in all the methods that the "rmic -idl" conversion didn't
  automatically include.
* Changed ::GNOME::Speech::Recognition::RuleGrammar 
  isEnabled__CORBA_WStringValue method to isEnabledWithRuleName.
* Changed ::GNOME::Speech::Recognition::RuleName setRuleName method to
  setRuleNameWithComponent.

----

Overview of changes in gnome-speech 0.5.12

* Adjusted ::GNOME::Speech::Recognition::GrammarEvent initWithState method.
* Adjusted use of ::java::awt::Component to Bonobo::ControlFrame.
* Adjusted use of ::java::beans::PropertyChangeListener to Bonobo::Listener.
* Adjusted use of ::java::beans::PropertyVetoException to Bonobo::BadArg.
* Adjusted use of ::java::io::InputStream to Bonobo::Stream.
* Adjusted use of ::java::io::OutputStream to Bonobo::Stream.
* Adjusted use of ::java::io::Reader to Bonobo::Stream.
* Adjusted use of ::java::io::IOException to Bonobo::IOError.
* Adjusted use of ::java::lang::InterruptedException to InterruptedException
  (new exception in GNOME_Speech.idl).
* Fixed up use of ::java::util::EventObject.
* Adjusted use of ::java::net::URL to CosNaming::NamingContextExt::URLString
  and ::java::net::MalformedURLException to
  CosNaming::NamingContextExt::InvalidAddress

----

Overview of changes in gnome-speech 0.5.11

* Adjusted all occurances of ::java::lang::Object to Bonobo::Unknown.
* Added a read-only string attribute called "message" to the
  GNOME::Speech::ExceptionValue, GNOME::Speech::ErrorValue and
  GNOME::Speech::Recognition::GrammarSyntaxDetail interfaces.
* Removed the SpeechPermission interface in GNOME_Speech.idl.
* Removed the enumerateQueue method in the Synthesizer interface in 
  GNOME_Speech_Synthesis.idl.
* Added an AudioClip interface to the GNOME::Speech::Recognition module with
  the loop, play and stop methods.
* Added a Boolean enum to the GNOME::Speech module, with three values:
  IS_TRUE, IS_FALSE, DONT_CARE
  Adjusted all occurrances of java.lang.Boolean to use this enum.
* Adjusted the EngineErrorEvent interface in the GNOME::Speech module to
  have an attribute called "problem", rather than a java.lang.Throwable.
  Added an initWithError method to the interface to allow this value to 
  be initially set.
* Adjusted occurances of java::util::Locale in GNOME::Speech to use a
  string to hold the locale value.
* Added the following typedef to the GNOME::Speech module:
    typedef sequence<::GNOME::Speech::EngineModeDesc> EngineListArray;
* Adjusted the EngineList interface to be derived from Bonobo::Unknown
  and added a new getEngineList method to return the Engine list.
* Added the following typedef to the GNOME::Speech module:
    typedef sequence<::GNOME::Speech::Recognition::Grammar> GrammarArray;
* Various changes to the IDL to get it to compile.

----

Overview of changes in gnome-speech 0.5.10

* Added a "Proposed Solution" section for most of the Java specific classes
  that still remain.

----

Overview of changes in gnome-speech 0.5.9

* Adjusted the form of the base exception definition.
* Fleshed out the TODO file with a list of all places where Java specific
  classes are still used.

----

Overview of changes in gnome-speech 0.5.8

* Removed all methods that were Java constructors with no parameters.
* Fixed up occurances of method over-loading using unique method names.
* For interfaces that already have create methods in the interface that they
  are subclassed from, those create methods are removed.
* The "factory" keyword has been replaced with "void".
* All "private" declarations have been removed.
* All "abstract" keywords have been removed.
* Occurances of the typedef "String" will be replaced by the basic type
  "string".
* Removed all Java style Adapters. They don't make sense in IDL.

----

Overview of changes in gnome-speech 0.5.7

* Added much more detail to the items in the TODO file. Forwarded to the
  gnome-speech-dev@sun.com alias for comments.

----

Overview of changes in gnome-speech 0.5.6

* The four GNOME Speech IDL files can now be successfully parsed with the
  GNOME IDL compiler.

----

Overview of changes in gnome-speech 0.5.5

* Created a GNOME_Speech_Defs.idl file containing all the other definitions
  used by the three main GNOME Speech IDL files.
* Created various typedefs in GNOME_Speech.idl, GNOME_Speech_Recognition.idl
  and GNOME_Speech_Synthesis.idl and adjusted the code accordingly.
* Changed "valuetype" to "interface" as appropriate in all IDL files.
* Adjusted the various Java exceptions to be GNOME IDL style exceptions.

----

Overview of changes in gnome-speech 0.5.4

* Merged the 83 .idl files into three:
    GNOME_Speech.idl
    GNOME_Speech_Recognition.idl
    GNOME_Speech_Synthesis.idl

----

Overview of changes in gnome-speech 0.5.3

* Replaced all occurances of ::CORBA::WStringValue with just "string".
* Adjusted the base object for all .idl files to be Bonobo::Unknown

----

Overview of changes in gnome-speech 0.5.2

* Adjusted the .idl files to separate each method signature, and generally
  make things more readable.

----

Overview of changes in gnome-speech 0.5.1

* Starting fixing up the generated .idl files to me GNOME-like.
  Changes include:
  - adjusting the #ifndef / #define lines.
  - adjusting the #include lines for the GNOME_Speech_... includes.
  - adjusting the ::javax::speech::... declarations.
  - removed the #pragma ID lines.
  - replaced arg0/arg1/... arguments with the equivalent names to the
    Java code.

----

Overview of changes in gnome-speech 0.5.0

* Using "rmic -idl <classnames>", converted the JSAPI implementation
  that comes with FreeTTS (.../FreeTTS/lib/jsapi.jar) to IDL files.

----
