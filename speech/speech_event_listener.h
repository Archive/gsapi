
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Speech event listener object that all other listeners are derived from. */

#ifndef __SPEECH_EVENT_LISTENER_H__
#define __SPEECH_EVENT_LISTENER_H__

#include <glib-object.h>
#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_EVENT_LISTENER              (speech_event_listener_get_type())
#define SPEECH_EVENT_LISTENER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_EVENT_LISTENER, SpeechEventListener))
#define SPEECH_EVENT_LISTENER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_EVENT_LISTENER, SpeechEventListenerClass))
#define SPEECH_IS_EVENT_LISTENER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_EVENT_LISTENER))
#define SPEECH_IS_EVENT_LISTENER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_EVENT_LISTENER))
#define SPEECH_EVENT_LISTENER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_EVENT_LISTENER, SpeechEventListenerClass))

typedef struct _SpeechEventListener            SpeechEventListener;
typedef struct _SpeechEventListenerClass       SpeechEventListenerClass;

struct _SpeechEventListener {
    GObject object;
};

struct _SpeechEventListenerClass {
    GObjectClass parent_class;
};


GType 
speech_event_listener_get_type(void);

SpeechEventListener * 
speech_event_listener_new(void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_EVENT_LISTENER_H__ */
