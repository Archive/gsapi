
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_TYPES_H__
#define __SPEECH_TYPES_H__

#include <speech/speech_object.h>

typedef enum {
    SPEECH_IS_TRUE,
    SPEECH_IS_FALSE,
    SPEECH_DONT_CARE
} SpeechTristate;

typedef enum {
    SPEECH_RESULT_OKAY = 0,
    SPEECH_AUDIO_EXCEPTION,
    SPEECH_ENGINE_EXCEPTION,
    SPEECH_ENGINE_STATE_ERROR,
    SPEECH_ILLEGAL_ARGUMENT_EXCEPTION,
    SPEECH_INTERRUPTED_EXCEPTION,
    SPEECH_IO_EXCEPTION,
    SPEECH_MALFORMED_URL_EXCEPTION,
    SPEECH_NULL_POINTER_EXCEPTION,
    SPEECH_PROPERTY_VETO_EXCEPTION,
    SPEECH_RECOGNITION_GRAMMAR_EXCEPTION,
    SPEECH_RECOGNITION_RESULT_STATE_ERROR,
    SPEECH_SECURITY_EXCEPTION,
    SPEECH_SYNTHESIS_MARKUP_EXCEPTION,
    SPEECH_VENDOR_DATA_EXCEPTION
} SpeechStatus;


/* Forward declarations of commonly used types. */

typedef struct _SpeechEngine         SpeechEngine;
typedef struct _SpeechEngineListener SpeechEngineListener;


struct _SpeechEvent {
    GObject event;
};

struct _SpeechEventClass {
    GObjectClass parent_class;
};

typedef struct _SpeechEvent		SpeechEvent;
typedef struct _SpeechEventClass	SpeechEventClass;


struct _SpeechEngineEvent {
    SpeechEvent event;
};

struct _SpeechEngineEventClass {
    SpeechEventClass parent_class;
};

typedef struct _SpeechEngineEvent      SpeechEngineEvent;
typedef struct _SpeechEngineEventClass SpeechEngineEventClass;


struct _SpeechEngineErrorEvent {
    SpeechEngineEvent event;
};

struct _SpeechEngineErrorEventClass {
    SpeechEngineEventClass parent_class;
};

typedef struct _SpeechEngineErrorEvent      SpeechEngineErrorEvent;
typedef struct _SpeechEngineErrorEventClass SpeechEngineErrorEventClass;


#endif /* __SPEECH_TYPES_H__ */
