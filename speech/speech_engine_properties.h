
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Defines the set of run-time properties of an Engine. */

#ifndef __SPEECH_ENGINE_PROPERTIES_H__
#define __SPEECH_ENGINE_PROPERTIES_H__

#include <speech/speech_types.h>
#include <speech/speech_control_component.h>
#include <speech/speech_event_listener.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_PROPERTIES              (speech_engine_properties_get_type())
#define SPEECH_ENGINE_PROPERTIES(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_PROPERTIES, SpeechEngineProperties))
#define SPEECH_ENGINE_PROPERTIES_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_PROPERTIES, SpeechEnginePropertiesClass))
#define SPEECH_IS_ENGINE_PROPERTIES(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_PROPERTIES))
#define SPEECH_IS_ENGINE_PROPERTIES_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_PROPERTIES))
#define SPEECH_ENGINE_PROPERTIES_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_PROPERTIES, SpeechEnginePropertiesClass))

typedef struct _SpeechEngineProperties            SpeechEngineProperties;
typedef struct _SpeechEnginePropertiesClass       SpeechEnginePropertiesClass;

struct _SpeechEngineProperties {
    SpeechObject object;
};

struct _SpeechEnginePropertiesClass {
    SpeechObjectClass parent_class;
};


GType 
speech_engine_properties_get_type(void);

SpeechEngineProperties * 
speech_engine_properties_new(void);

void 
speech_engine_properties_add_property_change_listener(
                                           SpeechEngineProperties *properties,
                                           SpeechEventListener *listener);

SpeechControlComponent * 
speech_engine_properties_get_control_component(
                                           SpeechEngineProperties *properties);

void 
speech_engine_properties_remove_property_change_listener(
                                           SpeechEngineProperties *properties,
                                           SpeechEventListener *listener);

void 
speech_engine_properties_reset(SpeechEngineProperties *properties);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_PROPERTIES_H__ */
