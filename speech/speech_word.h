
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Provides a standard representation of speakable words for speech engines. */

#ifndef __SPEECH_WORD_H__
#define __SPEECH_WORD_H__

#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_WORD              (speech_word_get_type())
#define SPEECH_WORD(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_WORD, SpeechWord))
#define SPEECH_WORD_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_WORD, SpeechWordClass))
#define SPEECH_IS_WORD(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_WORD))
#define SPEECH_IS_WORD_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_WORD))
#define SPEECH_WORD_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_WORD, SpeechWordClass))

typedef struct _SpeechWord            SpeechWord;
typedef struct _SpeechWordClass       SpeechWordClass;

struct _SpeechWord {
    SpeechObject object;
};

struct _SpeechWordClass {
    SpeechObjectClass parent_class;
};


/*
 * Grammatical category of word is unknown.
 * The value is zero and implies that no other category flag is set.
 */

static long SPEECH_WORD_UNKNOWN = 0;

/*
 * Grammatical category of word doesn't matter.
 */

static long SPEECH_WORD_DONT_CARE = 1L << 0;

/*
 * Other grammatical category of word not specified elsewhere in
 * this class.
 */

static long SPEECH_WORD_OTHER = 1L << 1;

/*
 * Grammatical category of word is noun.
 * English examples: "car", "house", "elephant".
 */

static long SPEECH_WORD_NOUN = 1L << 2;

/*
 * Grammatical category of word is proper noun.
 * English examples: "Yellowstone", "Singapore".
 */

static long SPEECH_WORD_PROPER_NOUN = 1L << 3;

/*
 * Grammatical category of word is pronoun.
 * English examples: "me", "I", "they".
 */

static long SPEECH_WORD_PRONOUN = 1L << 4;

/*
 * Grammatical category of word is verb.
 * English examples: "run", "debug", "integrate".
 */

static long SPEECH_WORD_VERB = 1L << 5;

/*
 * Grammatical category of word is adverb.
 * English examples: "slowly", "loudly", "barely", "very", "never".
 */

static long SPEECH_WORD_ADVERB = 1L << 6;

/*
 * Grammatical category of word is adjective.
 * English examples: "red", "mighty", "very", "first", "eighteenth".
 */

static long SPEECH_WORD_ADJECTIVE = 1L << 7;

/*
 * Grammatical category of word is proper adjective.
 * English examples: "British", "Brazilian".
 */

static long SPEECH_WORD_PROPER_ADJECTIVE = 1L << 8;

/*
 * Grammatical category of word is auxiliary.
 * English examples: "have", "do", "is", "shall", "must", "cannot".
 */

static long SPEECH_WORD_AUXILIARY = 1L << 9;

/*
 * Grammatical category of word is determiner.
 * English examples: "the", "a", "some", "many", "his", "her".
 */

static long SPEECH_WORD_DETERMINER = 1L << 10;

/*
 * Grammatical category of word is cardinal.
 * English examples: "one", "two", "million".
 */

static long SPEECH_WORD_CARDINAL = 1L << 11;

/*
 * Grammatical category of word is conjunction.
 * English examples: "and", "or", "since", "if".
 */

static long SPEECH_WORD_CONJUNCTION = 1L << 12;

/*
 * Grammatical category of word is preposition.
 * English examples: "of", "for".
 */

static long SPEECH_WORD_PREPOSITION = 1L << 13;

/*
 * Grammatical category is contraction.
 * English examples: "don't", "can't".
 */

static long SPEECH_WORD_CONTRACTION = 1L << 14;

/*
 * Word is an abbreviation or acronynm.
 * English examples: "Mr.", "USA".
 */

static long SPEECH_WORD_ABBREVIATION = 1L << 15;


GType 
speech_word_get_type(void);

SpeechWord * 
speech_word_new(gchar *written_form,
                gchar *spoken_form,
                gchar *pron[],
                long cat);

long 
speech_word_get_categories(SpeechWord *word);

gchar ** 
speech_word_get_pronunciations(SpeechWord *word);

gchar * 
speech_word_get_spoken_form(SpeechWord *word);

gchar * 
speech_word_get_written_form(SpeechWord *word);

void 
speech_word_set_categories(SpeechWord *word,
                           long cat);

void 
speech_word_set_pronunciations(SpeechWord *word,
                               gchar **pron);

void 
speech_word_set_spoken_form(SpeechWord *word,
                            gchar *text);

void 
speech_word_set_written_form(SpeechWord *word,
                             gchar *text);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_WORD_H__ */
