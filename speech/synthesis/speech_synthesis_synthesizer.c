
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The #SpeechSynthesisSynthesizer object provides primary access to speech
 * synthesis capabilities. It extends the #SpeechEngine object. Thus, any 
 * #SpeechSynthesisSynthesizer implements basic speech engine capabilities 
 * plus the specialized capabilities required for speech synthesis.
 *
 * The primary functions provided by the #SpeechSynthesisSynthesizer object
 * are the ability to speak text, speak Java Speech Markup Language text,
 * and control an output queue of objects to be spoken.
 *
 * <title>Creating a Synthesizer</title>
 *
 * Typically, a #SpeechSynthesisSynthesizer is created by a call to the
 * ispeech_central_create_synthesizer() function. The procedures for
 * locating, selecting, creating and initializing a 
 * #SpeechSynthesisSynthesizer are described in the documentation for
 * the #SpeechCentral object.
 *
 * <title>Synthesis Package: Inherited and Extended Capabilities</title>
 *
 * A synthesis package inherits many of its important capabilities from the 
 * #SpeechEngine object and its related support objects. The synthesis package
 * adds specialized functionality for performing speech synthesis.
 *
 * <itemizedlist>
 *   <listitem>Inherits location mechanism by the
 *	 speech_central_available_synthesizers() function and
 *       #SpeechEngineModeDesc.</listitem>
 *
 *   <listitem>Extends #SpeechEngineModeDesc as 
 *       #SpeechSynthesisSynthesizerModeDesc.</listitem>
 *
 *   <listitem>Inherits speech_engine_allocate() and
 *	 speech_engine_deallocate() from the #SpeechEngine object.</listitem>
 *
 *   <listitem>Inherits speech_engine_pause() and speech_engine_resume()
 * 	 functions from the #SpeechEngine object.</listitem>
 *
 *   <listitem>Inherits
 *	 speech_engine_get_engine_state(), speech_engine_wait_engine_state()
 *       and speech_engine_test_engine_state() functions from the
 *	 #SpeechEngine object.</listitem>
 *
 *   <listitem>Inherits the
 *	 #SPEECH_ENGINE_DEALLOCATED,
 *	 #SPEECH_ENGINE_ALLOCATED,
 *	 #SPEECH_ENGINE_ALLOCATING_RESOURCES,
 *	 #SPEECH_ENGINE_DEALLOCATING_RESOURCES,
 *	 #SPEECH_ENGINE_PAUSED and
 *	 #SPEECH_ENGINE_RESUMED states from the
 *	 #SpeechEngine object.</listitem>
 *
 *   <listitem>Adds
 *	 #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_EMPTY and
 *	 #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY sub-states to the
 *	 #SPEECH_ENGINE_ALLOCATED state.</listitem>
 *
 *   <listitem>Inherits audio management:
 *	 see speech_engine_get_audio_manager()
 *	 and #SpeechAudioManager.</listitem>
 *
 *   <listitem>Inherits vocabulary management:
 *	 see speech_engine_get_vocab_manager()
 *	 and #SpeechVocabManager.</listitem>
 *
 *   <listitem>Inherits speech_engine_add_engine_listener() and 
 *	 speech_engine_remove_engine_listener() functions and uses the
 *	 #SpeechEngineListener object.</listitem>
 *
 *   <listitem>Extends #SpeechEngineListener object to
 *	 #SpeechSynthesisSynthesizerListener.</listitem>
 *
 *   <listitem>Adds
 *	 speech_synthesis_synthesizer_speak_markup_text(),
 *	 speech_synthesis_synthesizer_speak_text_from_url(),
 *	 speech_synthesis_synthesizer_speak_speakable() and
 *	 speech_synthesis_synthesizer_speak_plain_text() functions
 *	 to place text on the output queue of the synthesizer.</listitem>
 *
 *   <listitem>Adds the speech_synthesis_synthesizer_phoneme()
 *	 function that converts text to phonemes.</listitem>
 *
 *   <listitem>Adds
 *	 speech_synthesis_synthesizer_cancel(),
 *	 speech_synthesis_synthesizer_cancel_item() and
 *	 speech_synthesis_synthesizer_cancel_all()
 *	 functions for management of output queue.</listitem>
 * </itemizedlist>
 *
 *
 * <title>Speaking Text</title>
 *
 * The basic function of a #SpeechSynthesisSynthesizer is to speak text
 * provided to it by an application. This text can be plain UTF8 text in
 * a string or can be marked up using a markup language such as the
 * Java Speech Markup Language (JSML) or the Speech Synthesis Markup 
 * Language (SSML).
 *
 * Plain text is spoken using the 
 * speech_synthesis_synthesizer_speak_plain_text() function.
 * Marked up text is spoken using one of the 
 * speech_synthesis_synthesizer_speak_speakable(),
 * speech_synthesis_synthesizer_speak_text_from_url() or
 * speech_synthesis_synthesizer_speak_markup_text() functions.
 * These functions obtain the marked up text for a #SpeechSynthesisSpeakable
 * object, from a #SpeechMarkupURL or from a string.
 *
 * [Note: JSML text provided programmatically (by a #SpeechSynthesisSpeakable
 * object or a string) does not require the full XML header. JSML text
 * obtained from a URL requires the full XML header.]
 *
 * A synthesizer is mono-lingual (it speaks a single language) so the text
 * should contain only the single language of the synthesizer. An application
 * requiring output of more than one language needs to create multiple
 * #SpeechSynthesisSynthesizer objects through #SpeechCentral. The
 * language of the #SpeechSynthesisSynthesizer should be selected at the time
 * at which it is created. The language for a created 
 * #SpeechSynthesisSynthesizer can be checked through the locale of 
 * its #SpeechEngineModeDesc (see speech_engine_get_engine_mode_desc() ).
 *
 * Each object provided to a synthesizer is spoken independently.
 * Sentences, phrases and other structures should not span multiple call to
 * the various speak functions.
 *
 * <title>Synthesizer State System</title>
 *
 * #SpeechSynthesisSynthesizer extends the state system of the generic
 * #SpeechEngine object. It inherits the four basic allocation
 * states, plus the #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED states.
 *
 * #SpeechSynthesisSynthesizer adds a pair of sub-states to the
 * #SPEECH_ENGINE_ALLOCATED state to represent the state of the speech output
 * queue (queuing is described in more detail below). For an
 * #SPEECH_ENGINE_ALLOCATED #SpeechSynthesisSynthesizer, the speech output 
 * queue is either empty or not empty: represented by the states
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY and 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY.
 *
 * The queue status is independent of the pause/resume status. Pausing or
 * resuming a synthesizer does not effect the queue. Adding or removing
 * objects from the queue does not effect the pause/resume status. The only
 * form of interaction between these state systems is that the
 * #SpeechSynthesisSynthesizer only speaks in the #SPEECH_ENGINE_RESUMED state,
 * and therefore, a transition from #SPEECH_ENGINE_QUEUE_NOT_EMPTY to
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY because of completion of 
 * speaking an object is only possible in the #SPEECH_ENGINE_RESUMED state.
 * (A transition from #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY to 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY is possible
 * in the #SPEECH_ENGINE_PAUSED state only through a call to one of the
 * cancel functions.)
 *
 * <title>Speech Output Queue</title>
 *
 * A synthesizer implements a queue of items provided to it through the
 * various speak functions. The queue is "first-in, first-out (FIFO)" -- 
 * the objects are spoken in exactly the order in which they are received. 
 * The object at the top of the queue is the object that is currently being 
 * spoken or about to be spoken.
 *
 * The #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY and 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY states of
 * a #SpeechSynthesisSynthesizer indicate the current state of the speech
 * output queue. The state handling functions inherited from the
 * #SpeechEngine object (speech_engine_get_engine_state(),
 * speech_engine_wait_engine_state() and speech_engine_test_engine_state()
 * can be used to test the queue state.
 *
 * The cancel functions allows an application to:
 * <itemizedlist>
 *   <listitem>stop the output of item currently at the top of the speaking 
 *       queue,</listitem>
 *   <listitem>remove an arbitrary item from the queue, or</listitem>
 *   <listitem>remove all items from the output queue.</listitem>
 * </itemizedlist>
 *
 * Applications requiring more complex queuing mechanisms (e.g. a prioritized
 * queue) can implement their own queuing objects that control the synthesizer.
 *
 * <title>Pause and Resume</title>
 *
 * The speech_engine_pause() and speech_engine_resume() functions (inherited 
 * from the #SpeechEngine object) have behavior like a "tape player". 
 * speech_engine_pause() stops audio output as soon as possible. 
 * speech_engine_resume() restarts audio output from the point of the
 * pause. Pause and resume may occur within words, phrases or unnatural points
 * in the speech output.
 *
 * Pause and resume do not affect the speech output queue.
 *
 * In addition to the #SPEECH_ENGINE_EVENT_PAUSED and
 * #SPEECH_ENGINE_EVENT_RESUMED events issued to the
 * #SpeechEngineListener (or #SpeechSynthesisSynthesizerListener),
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_PAUSED and 
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_RESUMED events
 * are issued to appropriate #SpeechSynthesisSpeakableListener's for the
 * #SpeechSynthesisSpeakable object at the top of the speaking queue. (The
 * #SpeechSynthesisSpeakableEvent is first issued to any
 * #SpeechSynthesisSpeakableListener provided with the speak
 * function, then to each #SpeechSynthesisSpeakableListener attached to the
 * #SpeechSynthesisSynthesizer. Finally, the #SpeechEngineEvent is
 * issued to each #SpeechSynthesisSynthesizerListener and
 * #SpeechEngineListener attached to the #SpeechSynthesisSynthesizer.)
 *
 * Applications can determine the approximate point at which a pause occurs by
 * monitoring the #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED events.
 *
 * See: #SpeechCentral
 * See: #SpeechSynthesisSpeakable
 * See: #SpeechSynthesisSpeakableListener
 * See: #SpeechEngineListener
 * See: #SpeechSynthesisSynthesizerListener
 */

#include <speech/synthesis/speech_synthesis_synthesizer.h>


GType
speech_synthesis_synthesizer_get_type(void)
{
    printf("speech_synthesis_synthesizer_get_type called.\n");
}


SpeechSynthesisSynthesizer *
speech_synthesis_synthesizer_new(void)
{
    printf("speech_synthesis_synthesizer_new called.\n");
}


/**
 * speech_synthesis_synthesizer_add_speakable_listener:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @listener: the listener that will receive #SpeechSynthesisSpeakableEvent's.
 *
 * Request notifications of all #SpeechSynthesisSpeakableEvent's for
 * all speech output objects for this #SpeechSynthesisSynthesizer. An
 * application can attach multiple #SpeechSynthesisSpeakableListener's
 * to a #SpeechSynthesisSynthesizer. A single listener can be attached
 * to multiple synthesizers.
 *
 * When an event effects more than one item in the speech output
 * queue (e.g. speech_synthesis_synthesizer_cancel_all() ), the
 * #SpeechSynthesisSpeakableEvent's are issued in the order of the items
 * in the queue starting with the top of the queue.
 *
 * A #SpeechSynthesisSpeakableListener can also provided for an
 * individual speech output item by providing it as a parameter to
 * one of the speak functions.
 *
 * A #SpeechSynthesisSpeakableListener can be attached or removed in
 * any #SpeechEngine state.
 *
 * See: speech_synthesis_synthesizer_remove_speakable_listener()
 */
void
speech_synthesis_synthesizer_add_speakable_listener(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechSynthesisSpeakableListener *listener)
{
    printf("speech_synthesis_synthesizer_add_speakable_listener called.\n");
}


/**
 * speech_synthesis_synthesizer_remove_speakable_listener:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @listener: the listener to remove.
 *
 * Remove a #SpeechSynthesisSpeakableListener from this
 * #SpeechSynthesisSynthesizer.
 *
 * A #SpeechSynthesisSpeakableListener can be attached or removed in
 * any #SpeechEngine state.
 *
 * See: speech_synthesis_synthesizer_add_speakable_listener()
 */

void
speech_synthesis_synthesizer_remove_speakable_listener(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechSynthesisSpeakableListener *listener)
{
    printf("speech_synthesis_synthesizer_remove_speakable_listener called.\n");
}


/**
 * speech_synthesis_synthesizer_get_number_of_queue_items:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @n: return the number of items currently on the speech output queue.
 *
 * Return the number of items currently on the speech output queue.
 *
 * The speech_synthesis_synthesizer_get_number_of_queue_items() function 
 * works in the #SPEECH_ENGINE_ALLOCATED state. The call blocks if the
 * #SpeechSynthesisSynthesizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the
 * #SPEECH_ENGINE_ALLOCATED state. An error is returned for synthesizers
 * in the #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * states.
 *
 * See: speech_synthesis_synthesizer_get_queue_item()
 * See: #SpeechSynthesisSynthesizerQueueItem
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 * See: speech_engine_add_engine_listener()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *		#SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus 
speech_synthesis_synthesizer_get_number_of_queue_items(
                             SpeechSynthesisSynthesizer *synthesizer,
                             int n)
{
    printf("speech_synthesis_synthesizer_get_number_of_queue_items called.\n");
}


/**
 * speech_synthesis_synthesizer_get_queue_item:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @n: index of the item on the speech queue to be returned.
 * @item: return the n'th item currently on the speech output queue.
 *
 * Return the n'th item currently on the speech output queue.
 *
 * A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED event is issued to each
 * #SpeechSynthesisSynthesizerListener whenever the speech output
 * queue changes. A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED event 
 * is issued whenever the queue the emptied.
 *
 * This function returns only the items on the speech queue placed
 * there by the current application. For security reasons, it is
 * not possible to inspect items placed by other applications.
 *
 * The speech_synthesis_synthesizer_get_queue_item() function works in the
 * #SPEECH_ENGINE_ALLOCATED state.  The call blocks if the
 * #SpeechSynthesisSynthesizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the
 * #SPEECH_ENGINE_ALLOCATED state. An error is thrown for synthesizers
 * in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: #SpeechSynthesisSynthesizerQueueItem
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 * See: speech_engine_add_engine_listener()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_get_queue_item(
                             SpeechSynthesisSynthesizer *synthesizer,
                             int n,
                             SpeechSynthesisSynthesizerQueueItem *item)
{
}


/**
 * speech_synthesis_synthesizer_cancel:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 *
 * Cancel output of the current object at the top of the output queue.
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED event is 
 * issued to appropriate #SpeechSynthesisSpeakableListeners.
 *
 * If there is another object in the speaking queue, it is moved to
 * top of queue and receives the #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE
 * event. If the #SpeechSynthesisSynthesizer is not paused, speech output
 * continues with that object. To prevent speech output continuing
 * with the next object in the queue, call speech_engine_pause()
 * before calling speech_synthesis_synthesizer_cancel().
 *
 * A <#SpeechSynthesisSynthesizerEvent is issued to indicate
 * #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED (if objects remain on 
 * the queue) or #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED 
 * (if the cancel leaves the queue empty).
 *
 * It is not an exception to call cancel if the speech output queue
 * is empty.
 *
 * The cancel functions work in the #SPEECH_ENGINE_ALLOCATED
 * state. The calls blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and complete when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is thrown
 * for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: speech_synthesis_synthesizer_cancel_item()
 * See: speech_synthesis_synthesizer_cancel_all()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *		#SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

void
speech_synthesis_synthesizer_cancel(SpeechSynthesisSynthesizer *synthesizer)
{
    printf("speech_synthesis_synthesizer_cancel called.\n");
}


/**
 * speech_synthesis_synthesizer_cancel_item:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @source: object to be removed from the speech output queue.
 *
 * Remove a specified item from the speech output queue. The source object 
 * must be one of the items passed to a speak function. A 
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED event is issued to
 * appropriate #SpeechSynthesisSpeakableListener's.
 *
 * If the source object is the top item in the queue, the behavior
 * is the same as the speech_synthesis_synthesizer_cancel() function.
 *
 * If the source object is not at the top of the queue, it is removed
 * from the queue without affecting the current top-of-queue speech
 * output. A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED is then 
 * issued to #SpeechSynthesisSynthesizerListener's.
 *
 * If the source object appears multiple times in the queue, only
 * the first instance is cancelled.
 *
 * Warning: cancelling an object just after the synthesizer has
 * completed speaking it and has removed the object from the queue
 * will return an exception. In this instance, the exception can
 * be ignored.
 *
 * The cancel functions work in the #SPEECH_ENGINE_ALLOCATED
 * state. The calls blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and complete when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is thrown
 * for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: speech_synthesis_synthesizer_cancel()
 * See: speech_synthesis_synthesizer_cancel_all()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the source object is not
 *		found in the speech output queue.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_cancel_item(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechObject *source)
{
    printf("speech_synthesis_synthesizer_cancelItem called.\n");
}


/**
 * speech_synthesis_synthesizer_cancel_all:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 *
 * Cancel all objects in the synthesizer speech output queue and stop
 * speaking the current top-of-queue object.
 *
 * The #SpeechSynthesisSpeakableListener's of each cancelled item on the
 * queue receive a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED
 * event. A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED event is 
 * issued to attached #SpeechSynthesisSynthesizerListener's.
 *
 * A speech_synthesis_synthesizer_cancel_all() is implictly performed 
 * before a #SpeechSynthesisSynthesizer is deallocated.
 *
 * The cancel functions work in the #SPEECH_ENGINE_ALLOCATED
 * state. The calls blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and complete when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is returned
 * for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: speech_synthesis_synthesizer_cancel()
 * See: speech_synthesis_synthesizer_cancel_item()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_cancel_all(SpeechSynthesisSynthesizer *synthesizer)
{
    printf("speech_synthesis_synthesizer_cancelAll called.\n");
}


/**
 * speech_synthesis_synthesizer_get_synthesizer_properties:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 *
 * Return the #SpeechSynthesisSynthesizerProperties object. The function
 * returns exactly the same object as the
 * speech_engine_get_engine_properties() function in the #SpeechEngine
 * object. However, with the 
 * speech_synthesis_synthesizer_get_synthesizer_properties() function,
 * an application does not need to cast the return value.
 *
 * The #SpeechSynthesisSynthesizerProperties are available in any state
 * of a #SpeechEngine. However, changes only take effect once an engine 
 * reaches the #SPEECH_ENGINE_ALLOCATED state.
 *
 * Returns: the #SpeechSynthesisSynthesizerProperties object for this engine.
 *
 * See: speech_engine_get_engine_properties()
 */

SpeechSynthesisSynthesizerProperties *
speech_synthesis_synthesizer_get_synthesizer_properties(
                             SpeechSynthesisSynthesizer *synthesizer)
{
    printf("speech_synthesis_synthesizer_getSynthesizerProperties called.\n");
}


/**
 * speech_synthesis_synthesizer_phoneme:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @text: plain text to be converted to phonemes.
 * @phoneme: return the phonemic representation of @text or null.
 *
 * Returns the phoneme string for a text string.  The return string
 * uses the International Phonetic Alphabet subset of UTF8.
 * The input string is expected to be simple text (for example,
 * a word or phrase in English).  The text is not expected to
 * contain punctuation or markup.
 *
 * If the #SppechSynthesisSynthesizer does not support text-to-phoneme
 * conversion or cannot process the input text it will return null.
 *
 * If the text has multiple pronunciations, there is no way to
 * indicate which pronunciation is preferred.
 *
 * The speech_synthesis_synthesizer_phoneme() function operates as defined 
 * only when a #SpeechSynthesisSynthesizer is in the #SPEECH_ENGINE_ALLOCATED
 *  state. The call blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and completes when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state.  An error is
 * returned for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_phoneme(SpeechSynthesisSynthesizer *synthesizer,
                                     gchar *text,
                                     gchar *phoneme)
{
    printf("speech_synthesis_synthesizer_phoneme called.\n");
}


/**
 * speech_synthesis_synthesizer_is_markup_type_supported:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @type: the markup type being checked.
 *
 * Return an indication of whether this markup type is supported.
 *
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_JSML
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_SSML
 *
 * Returns: an indication of whether this #SpeechSynthesisSynthesizer
 *	   supports this markup type.
 */

gboolean
speech_synthesis_synthesizer_is_markup_type_supported(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechMarkupType type)
{
    printf("speech_synthesis_synthesizer_isMarkupTypeSupported called.\n");
}


/**
 * speech_synthesis_synthesizer_speak_markup_text:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @markup_text: string contains marked up text to be spoken.
 * @listener: receives notification of events as synthesis output proceeds.
 *
 * Speak a string containing markup formatted text. The marked up text is 
 * checked for formatting errors and a #SPEECH_SYNTHESIS_MARKUP_EXCEPTION
 * is thrown if any are found. If legal, the text is placed at the end of 
 * the speaking queue and will be spoken once it reaches the top of the 
 * queue and the synthesizer is in the #SPEECH_ENGINE_RESUMED state. 
 * In all other respects is it identical to the speak function that accepts
 * a #SpeechSynthesisSpeakable object.
 *
 * The source of a #SpeechSynthesisSpeakableEvent issued to the
 * #SpeechSynthesisSpeakableListener is the string.
 *
 * The speak functions operate as defined only when a 
 * #SpeechSynthesisSynthesizer is in the #SPEECH_ENGINE_ALLOCATED state.
 * The call blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and completes when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is
 * thrown for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_speak_text_from_url()
 * See: speech_synthesis_synthesizer_speak_plain_text()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SYNTHESIS_MARKUP_EXCEPTION if any syntax errors are 
 *       encountered in speech_synthesis_synthesizer_speak_markup_text().
 *       </listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_speak_markup_text(
                             SpeechSynthesisSynthesizer *synthesizer,
                             gchar *markup_text,
                             SpeechSynthesisSpeakableListener *listener)
{
    printf("speech_synthesis_synthesizer_speakMarkupText called.\n");
}


/**
 * speech_synthesis_synthesizer_speak_text_from_url:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @markup_url: URL containing marked up text to be spoken.
 * @listener: receives notification of events as synthesis output proceeds.
 *
 * Speak marked up text from a URL. The text is obtained from the
 * URL, checked for legal markup formatting, and placed at the end
 * of the speaking queue. It is spoken once it reaches the top of
 * the queue and the synthesizer is in the #SPEECH_ENGINE_RESUMED state.
 * In other respects is it identical to the speak function that accepts 
 * a #SpeechSynthesisSpeakable object.
 *
 * The source of a #SpeechSynthesisSpeakableEvent issued to the
 * #SpeechSynthesisSpeakableListener is the #SpeechMarkupURL.
 *
 * Because of the need to check markup syntax, this speak
 * function returns only once the complete URL is loaded, or until a
 * syntax error is detected in the URL stream. Network delays will
 * cause the function to return slowly.
 *
 * Note: the full XML header is required in JSML formatted text
 * provided in the URL. The header is optional on programmatically
 * generated JSML (ie. with the 
 * speech_synthesis_synthesizer_speak_markup_text() and
 * speech_synthesis_synthesizer_speak_speakable() functions.
 *
 * The speak functions operate as defined only when a
 * #SppechSynthesisSynthesizer is in the #SPEECH_ENGINE_ALLOCATED state.
 * The call blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and completes when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is
 * returned for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_speak_markup_text()
 * See: speech_synthesis_synthesizer_speak_plain_text()
 * See: #SpeechSynthesisSpeakableEvent
 * See: speech_synthesis_synthesizer_add_speakable_listener()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SYNTHESIS_MARKUP_EXCEPTION if any syntax errors are 
 *       encountered in the markup text.</listitem>
 *   <listitem>#SPEECH_MALFORMED_URL_EXCEPTION if errors are encountered with 
 *       @markup_url</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if errors are encountered with 
 *       @markup_url</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_speak_text_from_url(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechMarkupURL markup_url,
                             SpeechSynthesisSpeakableListener *listener)
{
    printf("speech_synthesis_synthesizer_speakTextFromURL called.\n");
}


/**
 * speech_synthesis_synthesizer_speak_speakable:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @markup_text: object implementing the Speakable object that
 *		provides marked up text to be spoken.
 * @listener: receives notification of events as synthesis output proceeds.
 *
 * Speak an object that implements the #SpeechSynthesisSpeakable
 * object and provides marked up text. The #SpeechSynthesisSpeakable
 * object is added to the end of the speaking queue and will be
 * spoken once it reaches the top of the queue and the synthesizer
 * is in the #SPEECH_ENGINE_RESUMED state.
 *
 * The synthesizer first requests the text of the #SpeechSynthesisSpeakable 
 * by calling its speech_synthesis_speakable_get_markup_text() function. 
 * It then checks the syntax of the markup and returns a
 * #SPEECH_SYNTHESIS_MARKUP_EXCEPTION if any problems are found. If the
 * marked up text is legal, the text is placed on the speech output queue.
 *
 * When the speech output queue is updated, a 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * event is issued to #SpeechSynthesisSynthesizerListeners.
 *
 * Events associated with the #SpeechSynthesisSpeakable object are
 * issued to the #SpeechSynthesisSpeakableListener object. The listener
 * may be null. A listener attached with this function cannot be
 * removed with a subsequent remove call. The source for the
 * #SpeechSynthesisSpeakableEvent's is the @markup_text object.
 *
 * #SpeechSynthesisSpeakableEvent's can also be received by attaching a
 * #SppechSynthesisSpeakableListener to the #SpeechSynthesisSynthesizer
 * with the speech_synthesis_synthesizer_add_speakable_listener() function.
 * A #SpeechSynthesisSpeakableListener attached to the
 * #SpeechSynthesisSynthesizer receives all #SpeechSynthesisSpeakableEvent's
 * for all speech output items of the synthesizer (rather than for
 * a single #SpeechSynthesisSpeakable).
 *
 * The speak call is asynchronous: it returns once the text for the
 * #SpeechSynthesisSpeakable has been obtained, checked for syntax, and
 * placed on the synthesizer's speech output queue. An application
 * needing to know when the #SpeechSynthesisSpeakable has been spoken
 * should wait for the #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED
 * event to be issued to the #SpeechSynthesisSpeakableListener object. The
 * speech_engine_get_engine_state() and speech_engine_wait_engine_state()
 * functions can be used to determine the speech output queue status.
 *
 * An object placed on the speech output queue can be removed with
 * one of the cancel functions.
 *
 * The speak functions operate as defined only when a
 * #SpeechSynthesisSynthesizer is in the #SPEECH_ENGINE_ALLOCATED state.
 * The call blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and completes when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is
 * returned for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: speech_synthesis_synthesizer_speak_markup_text()
 * See: speech_synthesis_synthesizer_speak_text_from_url()
 * See: speech_synthesis_synthesizer_speak_plain_text()
 * See: #SpeechSynthesisSpeakableEvent
 * See: speech_synthesis_synthesizer_add_speakable_listener()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SYNTHESIS_MARKUP_EXCEPTION if any syntax errors are 
 *       encountered in the markup text.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_speak_speakable(
                                   SpeechSynthesisSynthesizer *synthesizer,
                                   SpeechSynthesisSpeakable *markup_text,
                                   SpeechSynthesisSpeakableListener *listener)
{
    printf("speech_synthesis_synthesizer_speakSpeakable called.\n");
}


/**
 * speech_synthesis_synthesizer_speak_plain_text:
 * @synthesizer: the #SpeechSynthesisSynthesizer object that this operation
 *               will be applied to.
 * @plain_text: string containing plain text to be spoken
 * @listener: receives notification of events as synthesis output proceeds.
 *
 * Speak a plain text string. The text is not interpreted as
 * containing a markup language so markup elements are ignored.
 * The text is placed at the end of the speaking queue and will
 * be spoken once it reaches the top of the queue and the
 * synthesizer is in the #SPEECH_ENGINE_RESUMED state. In other
 * respects it is similar to the speak function that accepts a 
 * #SpeechSynthesisSpeakable object.
 *
 * The source of a #SpeechSynthesisSpeakableEvent issued to the
 * #SpeechSynthesisSpeakableListener is a string.
 *
 * The speak functions operate as defined only when a
 * #SpeechSynthesisSynthesizer is in the #SPEECH_ENGINE_ALLOCATED state.
 * The call blocks if the #SpeechSynthesisSynthesizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and completes when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is
 * returned for synthesizers in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_speak_text_from_url()
 * See: speech_synthesis_synthesizer_speak_markup_text()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a synthesizer in the
 *              #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_speak_plain_text(
                             SpeechSynthesisSynthesizer *synthesizer,
                             gchar *plain_text,
                             SpeechSynthesisSpeakableListener *listener)
{
    printf("speech_synthesis_synthesizer_speakPlainText called.\n");
}
