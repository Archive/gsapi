
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * An extension to the #SpeechEngineListener object for receiving
 * notification of events associated with a #SpeechSynthesisSynthesizer.
 * #SpeechSynthesisSynthesizerListener objects are attached to and removed
 * from a #SpeechSynthesisSynthesizer by calling the
 * speech_engine_add_engine_listener() and
 * speech_engine_remove_engine_listener() functions 
 * (which #SpeechSynthesisSynthesizer inherits from the
 * #SpeechEngine object).
 *
 * The source for all #SpeechSynthesisSynthesizerEvents provided to a
 * #SpeechSynthesisSynthesizerListener is the #SpeechSynthesisSynthesizer.
 *
 * See: #SpeechSynthesisSynthesizerListener
 * See: #SpeechSynthesisSynthesizer
 * See: #SpeechEngine
 * See: speech_engine_add_engine_listener()
 * See: speech_engine_remove_engine_listener()
 */

#include <speech/synthesis/speech_synthesis_synthesizer_listener.h>


GType
speech_synthesis_synthesizer_listener_get_type(void)
{
    printf("speech_synthesis_synthesizer_listener_get_type called.\n");
}


SpeechSynthesisSynthesizerListener *
speech_synthesis_synthesizer_listener_new(void)
{
    printf("speech_synthesis_synthesizer_listener_new called.\n");
}


/**
 * speech_synthesis_synthesizer_listener_queue_emptied:
 * @listener: the #SpeechSynthesisSynthesizerListener object that this
 *            operation will be applied to.
 * @e: the synthesizer event.
 *
 * A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED event has occurred 
 * indicating that the text output queue of the #SpeechSynthesisSynthesizer
 * has emptied. The #SpeechSynthesisSynthesizer is in the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY state.
 *
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 */

void
speech_synthesis_synthesizer_listener_queue_emptied(
                                   SpeechSynthesisSynthesizerListener *listener,
                                   SpeechSynthesisSynthesizerEvent *e)
{
    printf("speech_synthesis_synthesizer_listener_queue_emptied called.\n");
}


/**
 * speech_synthesis_synthesizer_listener_queue_updated:
 * @listener: the #SpeechSynthesisSynthesizerListener object that this
 *            operation will be applied to.
 * @e: the synthesizer event.
 *
 * A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED event has occurred 
 * indicating that the speaking queue has changed. The 
 * #SpeechSynthesisSynthesizer is in the 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY state.
 *
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 */

void
speech_synthesis_synthesizer_listener_queue_updated(
                                   SpeechSynthesisSynthesizerListener *listener,
                                   SpeechSynthesisSynthesizerEvent *e)
{
    printf("speech_synthesis_synthesizer_listener_queue_updated called.\n");
}
