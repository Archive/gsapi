
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Properties that are specific to speech synthesizers. */


#ifndef __SPEECH_SYNTHESIS_SYNTHESIZER_MODE_DESC_H__
#define __SPEECH_SYNTHESIS_SYNTHESIZER_MODE_DESC_H__

#include <speech/speech_engine_mode_desc.h>
#include <speech/synthesis/speech_synthesis_types.h>
#include <speech/synthesis/speech_synthesis_voice.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_MODE_DESC              (speech_synthesis_synthesizer_mode_desc_get_type())
#define SPEECH_SYNTHESIS_SYNTHESIZER_MODE_DESC(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_MODE_DESC, SpeechSynthesisSynthesizerModeDesc))
#define SPEECH_SYNTHESIS_SYNTHESIZER_MODE_DESC_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_MODE_DESC, SpeechSynthesisSynthesizerModeDescClass))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_MODE_DESC(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_MODE_DESC))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_MODE_DESC_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_MODE_DESC))
#define SPEECH_SYNTHESIS_SYNTHESIZER_MODE_DESC_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_MODE_DESC, SpeechSynthesisSynthesizerModeDescClass))

typedef struct _SpeechSynthesisSynthesizerModeDesc            SpeechSynthesisSynthesizerModeDesc;
typedef struct _SpeechSynthesisSynthesizerModeDescClass       SpeechSynthesisSynthesizerModeDescClass;

struct _SpeechSynthesisSynthesizerModeDesc {
    SpeechEngineModeDesc mode_desc;
};

struct _SpeechSynthesisSynthesizerModeDescClass {
    SpeechEngineModeDescClass parent_class;
};


GType 
speech_synthesis_synthesizer_mode_desc_get_type(void);

SpeechSynthesisSynthesizerModeDesc * 
speech_synthesis_synthesizer_mode_desc_new(void);

SpeechSynthesisSynthesizerModeDesc * 
speech_synthesis_synthesizer_mode_desc_new_with_locale(gchar *locale);

SpeechSynthesisSynthesizerModeDesc * 
speech_synthesis_synthesizer_mode_desc_new_with_name_and_state_and_voices(
                                             gchar *engine_name,
                                             gchar * mode_name,
                                             gchar * locale,
                                             SpeechTristate running,
                                             SpeechSynthesisVoice *voices[]);

void 
speech_synthesis_synthesizer_mode_desc_add_voice(
                             SpeechSynthesisSynthesizerModeDesc *mode_desc,
                             SpeechSynthesisVoice *voice);

SpeechSynthesisVoice ** 
speech_synthesis_synthesizer_mode_desc_get_voices(
                             SpeechSynthesisSynthesizerModeDesc *mode_desc);

void 
speech_synthesis_synthesizer_mode_desc_set_voices(
                             SpeechSynthesisSynthesizerModeDesc *mode_desc,
                             SpeechSynthesisVoice *voices[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SYNTHESIZER_MODE_DESC_H__ */
