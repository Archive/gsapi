
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechSynthesisSpeakable object that can be provided to one of the
 * speak functions (speech_synthesis_synthesizer_speak_markup_text() or
 * speech_synthesis_synthesizer_speak_text_from_url() or
 * speech_synthesis_synthesizer_speak_speakable() or
 * speech_synthesis_synthesizer_speak_plain_text() ) of a 
 * #SpeechSynthesisSynthesizer object  to be spoken.
 * The text is accessed through the 
 * speech_synthesis_speakable_get_markup_text() function.
 *
 * Applications can sub-class the #SpeechSynthesisSpeakable object and
 * combine it with other functionality. Examples might include graphical
 * objects or database entries.
 *
 * The speech_synthesis_speakable_get_markup_text() function returns text 
 * formatted for the supported markup language.
 *
 * Note that JSML allows structural information (paragraphs and sentences),
 * production information (pronunciations, emphasis, breaks, and prosody),
 * and other miscellaneous markup. Appropriate use of this markup improves
 * the quality and understandability of the synthesized speech.
 *
 * The JSML text is a string and is assumed to contain text of a single
 * language (the language of the #SpeechSynthesisSynthesizer). The text is
 * treated as independent of other text output on the synthesizer's text
 * output queue, so, a sentence or other important structure should be
 * contained within a single speakable object.
 *
 * The standard XML header is optional for software-created JSML documents.
 * Thus, the speech_synthesis_speakable_get_markup_text() function is not 
 * required to provide the header.
 *
 * A #SpeechSynthesisSpeakableListener can be attached to the
 * #SpeechSynthesisSynthesizer with the 
 * speech_synthesis_synthesizer_add_speakable_listener() function
 * to receive all #SpeechSynthesisSpeakableEvent's for all
 * #SpeechSynthesisSpeakable objects on the output queue.
 *
 * See: #SpeechSynthesisSpeakableListener
 * See: #SpeechSynthesisSpeakableEvent
 * See: #SpeechSynthesisSynthesizer
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_add_speakable_listener()
 */

#include <speech/synthesis/speech_synthesis_speakable.h>


GType
speech_synthesis_speakable_get_type(void)
{
    printf("speech_synthesis_speakable_get_type called.\n");
}


SpeechSynthesisSpeakable *
speech_synthesis_speakable_new(void)
{
    printf("speech_synthesis_speakable_new called.\n");
}


/**
 * speech_synthesis_speakable_get_markup_text:
 * @speakable: the #SpeechSynthesisSpeakable object that this operation will
 *             be applied to.
 *
 * Return text to be spoken formatted for the supported markup language.
 * This function is called immediately when a #SpeechSynthesisSpeakable 
 * object is passed to one of the speak functions of a 
 * #SpeechSynthesisSynthesizer.
 *
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_get_number_of_queue_items()
 * See: speech_synthesis_synthesizer_get_queue_item()
 * See: #SpeechSynthesisSynthesizerQueueItem
 *
 * Returns: a string containing marked up text.
 */

gchar *
speech_synthesis_speakable_get_markup_text(SpeechSynthesisSpeakable *speakable)
{
    printf("speech_synthesis_speakable_get_markup_text called.\n");
}
