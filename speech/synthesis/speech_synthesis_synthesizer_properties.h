
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Provides control of the run-time properties of a Synthesizer. */

#ifndef __SPEECH_SYNTHESIS_SYNTHESIZER_PROPERTIES_H__
#define __SPEECH_SYNTHESIS_SYNTHESIZER_PROPERTIES_H__

#include <speech/speech_engine_properties.h>
#include <speech/synthesis/speech_synthesis_types.h>
#include <speech/synthesis/speech_synthesis_voice.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_PROPERTIES              (speech_synthesis_synthesizer_properties_get_type())
#define SPEECH_SYNTHESIS_SYNTHESIZER_PROPERTIES(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_PROPERTIES, SpeechSynthesisSynthesizerProperties))
#define SPEECH_SYNTHESIS_SYNTHESIZER_PROPERTIES_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_PROPERTIES, SpeechSynthesisSynthesizerPropertiesClass))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_PROPERTIES(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_PROPERTIES))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_PROPERTIES_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_PROPERTIES))
#define SPEECH_SYNTHESIS_SYNTHESIZER_PROPERTIES_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_PROPERTIES, SpeechSynthesisSynthesizerPropertiesClass))

typedef struct _SpeechSynthesisSynthesizerProperties            SpeechSynthesisSynthesizerProperties;
typedef struct _SpeechSynthesisSynthesizerPropertiesClass       SpeechSynthesisSynthesizerPropertiesClass;

struct _SpeechSynthesisSynthesizerProperties {
    SpeechEngineProperties properties;
};

struct _SpeechSynthesisSynthesizerPropertiesClass {
    SpeechEnginePropertiesClass parent_class;
};


GType 
speech_synthesis_synthesizer_properties_get_type(void);

SpeechSynthesisSynthesizerProperties * 
speech_synthesis_synthesizer_properties_new(void);

float 
speech_synthesis_synthesizer_properties_get_pitch(
                              SpeechSynthesisSynthesizerProperties *properties);

float 
speech_synthesis_synthesizer_properties_get_pitch_range(
                              SpeechSynthesisSynthesizerProperties *properties);

float 
speech_synthesis_synthesizer_properties_get_speaking_rate(
                              SpeechSynthesisSynthesizerProperties *properties);

SpeechSynthesisVoice * 
speech_synthesis_synthesizer_properties_get_voice(
                              SpeechSynthesisSynthesizerProperties *properties);

float 
speech_synthesis_synthesizer_properties_get_volume(
                              SpeechSynthesisSynthesizerProperties *properties);

SpeechStatus 
speech_synthesis_synthesizer_properties_set_pitch(
                              SpeechSynthesisSynthesizerProperties *properties,
                              float hertz);

SpeechStatus 
speech_synthesis_synthesizer_properties_set_pitch_range(
                              SpeechSynthesisSynthesizerProperties *properties,
                              float hertz);

SpeechStatus 
speech_synthesis_synthesizer_properties_set_speaking_rate(
                              SpeechSynthesisSynthesizerProperties *properties,
                              float wpm);

SpeechStatus 
speech_synthesis_synthesizer_properties_set_voice(
                              SpeechSynthesisSynthesizerProperties *properties,
                              SpeechSynthesisVoice *voice);

SpeechStatus 
speech_synthesis_synthesizer_properties_set_volume(
                              SpeechSynthesisSynthesizerProperties *properties,
                              float volume);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SYNTHESIZER_PROPERTIES_H__ */
