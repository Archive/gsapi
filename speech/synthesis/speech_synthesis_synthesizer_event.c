
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Event issued by a #SpeechSynthesisSynthesizer to indicate a change in state
 * or other activity. A #SpeechSynthesisSynthesizerEvent is issued to each
 * #SpeechSynthesisSynthesizerListener attached to a 
 * #SpeechSynthesisSynthesizer using the 
 * speech_engine_add_engine_listener() function it inherits from the
 * #SpeechEngine object.
 *
 * The #SpeechSynthesisSynthesizerEvent object extends the #SpeechEngineEvent
 * object. Similarly, the #SpeechSynthesisSynthesizerListener object extends
 * the #SpeechEngineListener object.
 *
 * #SpeechSynthesisSynthesizerEvent extends #SpeechEngineEvent with
 * several events that are specialized for speech synthesis. It also
 * inherits several event types from #SpeechEngineEvent:
 *  #SPEECH_ENGINE_ENGINE_ALLOCATED,
 *  #SPEECH_ENGINE_ENGINE_DEALLOCATED,
 *  #SPEECH_ENGINE_ENGINE_ALLOCATING_RESOURCES,
 *  #SPEECH_ENGINE_ENGINE_DEALLOCATING_RESOURCES,
 *  #SPEECH_ENGINE_ENGINE_PAUSED,
 *  #SPEECH_ENGINE_ENGINE_RESUMED.
 *
 * See: #SpeechSynthesisSynthesizer
 * See: #SpeechSynthesisSynthesizerListener
 * See: speech_engine_add_engine_listener()
 * See: #SpeechEngineEvent
 */

#include <speech/synthesis/speech_synthesis_synthesizer_event.h>


GType
speech_synthesis_synthesizer_event_get_type(void)
{
    printf("speech_synthesis_synthesizer_event_get_type called.\n");
}


/**
 * speech_synthesis_synthesizer_event_new:
 * @source: the synthesizer that issued the event.
 * @id: the identifier for the event type.
 * @top_of_queue_changed: true if top item on speech output queue changed.
 * @old_engine_state: engine state prior to this event.
 * @new_engine_state: engine state following this event.
 *
 * Create a #SpeechSynthesisSynthesizerEvent with a specified event @id and 
 * @top_of_queue_changed flag.
 *
 * Returns: a new #SpeechSynthesisSynthesizerEvent object.
 */

SpeechSynthesisSynthesizerEvent *
speech_synthesis_synthesizer_event_new(SpeechEngine *source,
                                       int id,
                                       gboolean top_of_queue_changed,
                                       long old_engine_state,
                                       long new_engine_state)
{
    printf("speech_synthesis_synthesizer_event_new called.\n");
}


/**
 * speech_synthesis_synthesizer_event_get_top_of_queue_changed:
 * @event: the #SpeechSynthesisSynthesizerEvent object that this operation
 *         will be applied to.
 *
 * Return the #top_of_queue_changed value. The value is %TRUE for a 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED event when the top 
 * item in the speech output queue has changed.
 *
 * Returns: the #top_of_queue_changed value.
 *
 * See: #topOfQueueChanged
 */

gboolean
speech_synthesis_synthesizer_event_get_top_of_queue_changed(
                                      SpeechSynthesisSynthesizerEvent *event)
{
    printf("speech_synthesis_synthesizer_event_get_top_of_queue_changed called.\n");
}
