
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A description of one output voice of a speech synthesizer.
 * #SpeechVoice objects can be used in selection of synthesis
 * engines (through the #SpeechSynthesisSynthesizerModeDesc). The
 * current speaking voice of a #SpeechSynthesisSynthesizer can be 
 * changed during operation with the 
 * speech_synthesis_synthesizer_properties_set_voice() function of the
 * #SpeechSynthesisSynthesizerProperties object.
 *
 * See: #SpeechSynthesisSynthesizerModeDesc
 * See: speech_synthesis_synthesizer_properties_set_voice()
 * See: #SpeechSynthesisSynthesizer
 */

#include <speech/synthesis/speech_synthesis_voice.h>


GType
speech_synthesis_voice_get_type(void)
{
    printf("speech_synthesis_voice_get_type called.\n");
}


SpeechSynthesisVoice *
speech_synthesis_voice_new(void)
{
    printf("speech_synthesis_voice_new called.\n");
}


/**
 * speech_synthesis_voice_new_with_voice_info:
 * @name: the voice name.
 * @gender: the voice gender.
 * @age: the age of the voice.
 * @style: the style of the voice.
 *
 * Initializer provided with voice name, gender, age and style.
 *
 * Returns:
 */

SpeechSynthesisVoice *
speech_synthesis_voice_new_with_voice_info(gchar *name,
                                           int gender,
                                           int age,
                                           gchar *style)
{
    printf("speech_synthesis_voice_new_with_voice_info called.\n");
}


/**
 * speech_synthesis_voice_clone:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 *
 * Create a copy of this #SpeechVoice.
 *
 * Returns: a copy of this #SpeechVoice.
 */

SpeechObject *
speech_synthesis_voice_clone(SpeechSynthesisVoice *voice)
{
    printf("speech_synthesis_voice_clone called.\n");
}


/**
 * speech_synthesis_voice_equals:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 * @an_object: the voice to compare against.
 *
 * Returns %TRUE if and only if the parameter is not %NULL and is a 
 * #SpeechVoice with equal values of name, age, gender, and style.
 *
 * Returns: a boolean indication of whether the voices are equal.
 */

gboolean
speech_synthesis_voice_equals(SpeechSynthesisVoice *voice,
                              SpeechSynthesisVoice *an_object)
{
    printf("speech_synthesis_voice_equals called.\n");
}


/**
 * speech_synthesis_voice_get_age:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 *
 * Get the voice age. Age values are OR'able.
 *
 * Returns: the voice age.
 *
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_CHILD
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_TEENAGER
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_YOUNGER_ADULT
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_MIDDLE_ADULT
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_OLDER_ADULT
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_NEUTRAL
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_DONT_CARE
 */

int
speech_synthesis_voice_get_age(SpeechSynthesisVoice *voice)
{
    printf("speech_synthesis_voice_get_age called.\n");
}


/**
 * speech_synthesis_voice_get_gender:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 *
 * Get the voice gender. Gender values are OR'able.
 *
 * Returns: the voice gender.
 *
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_FEMALE
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_MALE
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_NEUTRAL
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_DONT_CARE
 */

int
speech_synthesis_voice_get_gender(SpeechSynthesisVoice *voice)
{
    printf("speech_synthesis_voice_get_gender called.\n");
}


/**
 * speech_synthesis_voice_get_name:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 *
 * Get the voice name. May return %NULL.
 *
 * Returns: the voice name.
 */

gchar *
speech_synthesis_voice_get_name(SpeechSynthesisVoice *voice)
{
    printf("speech_synthesis_voice_get_name called.\n");
}


/**
 * speech_synthesis_voice_get_style:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 *
 * Get the voice style. This parameter is designed for human
 * interpretation. Values might include "business", "casual",
 * "robotic", "breathy".
 *
 * Returns: the voice style.
 */

gchar *
speech_synthesis_voice_get_style(SpeechSynthesisVoice *voice)
{
    printf("speech_synthesis_voice_get_style called.\n");
}


/**
 * speech_synthesis_voice_match:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 * @require: the #SpeechVoice to match against.
 *
 * Determine whether a #SpeechVoice has all the features defined in the 
 * @require object. Strings in @require which are either %NULL or 
 * zero-length ("") are ignored. All string comparisons are exact matches 
 * (case-sensitive).
 *
 * #SPEECH_SYNTHESIS_VOICE_GENDER_DONT_CARE and 
 * #SPEECH_SYNTHESIS_VOICE_AGE_DONT_CARE values in the @require object 
 * are ignored. The age and gender parameters are OR'ed: e.g. the 
 * required age can be 
 * #SPEECH_SYNTHESIS_VOICE_AGE_TEENAGER | #SPEECH_SYNTHESIS_VOICE_AGE_CHILD.
 *
 * Returns: a boolean indication of whether these two voices match.
 */

gboolean
speech_synthesis_voice_match(SpeechSynthesisVoice *voice,
                             SpeechSynthesisVoice *require)
{
    printf("speech_synthesis_voice_match called.\n");
}


/**
 * speech_synthesis_voice_set_age:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 * @age: the new voice age.
 *
 * Set the voice age.
 *
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_CHILD
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_TEENAGER
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_YOUNGER_ADULT
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_MIDDLE_ADULT
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_OLDER_ADULT
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_NEUTRAL
 * See: #SPEECH_SYNTHESIS_VOICE_AGE_DONT_CARE
 */

void
speech_synthesis_voice_set_age(SpeechSynthesisVoice *voice,
                               int age)
{
    printf("speech_synthesis_voice_set_age called.\n");
}


/**
 * speech_synthesis_voice_set_gender:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 * @gender: the new voice gender.
 *
 * Set the voice gender.
 *
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_FEMALE
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_MALE
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_NEUTRAL
 * See: #SPEECH_SYNTHESIS_VOICE_GENDER_DONT_CARE
 */

void
speech_synthesis_voice_set_gender(SpeechSynthesisVoice *voice,
                                  int gender)
{
    printf("speech_synthesis_voice_set_gender called.\n");
}


/**
 * speech_synthesis_voice_set_name:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 * @name: the new voice name.
 *
 * Set the voice name. A %NULL or "" string in voice match means "don't care".
 */

void
speech_synthesis_voice_set_name(SpeechSynthesisVoice *voice,
                                gchar *name)
{
    printf("speech_synthesis_voice_set_name called.\n");
}


/**
 * speech_synthesis_voice_set_style:
 * @voice: the #SpeechSynthesisVoice object that this operation will be
 *         applied to.
 * @style: the new voice style.
 *
 * Set the voice style.
 */

void
speech_synthesis_voice_set_style(SpeechSynthesisVoice *voice,
                                 gchar *style)
{
    printf("speech_synthesis_voice_set_style called.\n");
}
