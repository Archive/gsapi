
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Provides primary access to speech synthesis capabilities. */

#ifndef __SPEECH_SYNTHESIS_SYNTHESIZER_H__
#define __SPEECH_SYNTHESIS_SYNTHESIZER_H__

#include <speech/speech_engine.h>
#include <speech/synthesis/speech_synthesis_types.h>
#include <speech/synthesis/speech_synthesis_speakable.h>
#include <speech/synthesis/speech_synthesis_speakable_listener.h>
#include <speech/synthesis/speech_synthesis_synthesizer_listener.h>
#include <speech/synthesis/speech_synthesis_synthesizer_queue_item.h>
#include <speech/synthesis/speech_synthesis_synthesizer_properties.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SYNTHESIZER              (speech_synthesis_synthesizer_get_type())
#define SPEECH_SYNTHESIS_SYNTHESIZER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER, SpeechSynthesisSynthesizer))
#define SPEECH_SYNTHESIS_SYNTHESIZER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER, SpeechSynthesisSynthesizerClass))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER))
#define SPEECH_SYNTHESIS_SYNTHESIZER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER, SpeechSynthesisSynthesizerClass))

typedef struct _SpeechSynthesisSynthesizer            SpeechSynthesisSynthesizer;
typedef struct _SpeechSynthesisSynthesizerClass       SpeechSynthesisSynthesizerClass;

struct _SpeechSynthesisSynthesizer {
    SpeechEngine engine;
};

struct _SpeechSynthesisSynthesizerClass {
    SpeechEngineClass parent_class;
};


/*
 * #SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_JSML is the Java Speech Markup 
 * Language markup type. It is one of the valid values for a 
 * #SpeechMarkupType, and can be used in conjunction with the
 * speech_synthesis_synthesizer_speak_markup_text(), 
 * speech_synthesis_synthesizer_speak_text_from_url() and
 * speech_synthesis_synthesizer_speak_speakable() functions, to speak 
 * text with this #SpeechSynthesisSynthesizer.
 *
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_SSML
 * See: speech_synthesis_synthesizer_speak_markup_text()
 * See: speech_synthesis_synthesizer_speak_text_from_url()
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_is_markup_type_supported()
 */

static gchar *SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_JSML = "JSML_MARKUP";

/*
 * #SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_SSML is the Speech Synthesis Markup 
 * Language markup type. It is one of the valid values for a 
 * #SpeechMarkupType, and can be used in conjunction with the 
 * speech_synthesis_synthesizer_speak_markup_text(), 
 * speech_synthesis_synthesizer_speak_text_from_url() and
 * speech_synthesis_synthesizer_speak_speakable() functions, to speak text 
 * with this #SpeechSynthesisSynthesizer.
 *
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_JSML
 * See: speech_synthesis_synthesizer_speak_markup_text()
 * See: speech_synthesis_synthesizer_speak_text_from_url()
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_is_markup_type_supported()
 */

static gchar *SPEECH_SYNTHESIS_SYNTHESIZER_MARKUP_SSML = "SSML_MARKUP";


/*
 * Bit of state that is set when the speech output queue of a
 * #SpeechSynthesisSynthesizer is empty. The 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state is a sub-state of 
 * #SPEECH_ENGINE_ALLOCATED state. An allocated #SpeechSynthesisSynthesizer
 * is always in either the #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 * or #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state.
 *
 * A #SpeechSynthesisSynthesizer is always allocated in the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state. The 
 * #SpeechSynthesisSynthesizer transitions from the 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state to the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY state when a call 
 * to one of the speak functions places an object on the speech output
 * queue. A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED event is 
 * issued to indicate this change in state.
 *
 * A #SpeechSynthesisSynthesizer returns from the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY state to the 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state once the queue is 
 * emptied because of completion of speaking all objects or because 
 * of a call to a cancel function.
 *
 * The queue status can be tested with the speech_engine_wait_engine_state(),
 * speech_engine_get_engine_state() and speech_engine_test_engine_state()
 * functions. To block a thread until the queue is empty:
 *
 *	SpeechSynthesisSynthesizer synth = ...;
 *	speech_synthesis_synthesizer_wait_engine_state(synth, QUEUE_EMPTY);
 *
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 * See: speech_engine_test_engine_state()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 */

static long SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY = 65536;

/*
 * Bit of state that is set when the speech output queue of a
 * #SpeechSynthesisSynthesizer is not empty. The
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY state is a 
 * sub-state of the #SPEECH_ENGINE_ALLOCATED state. An allocated
 * #SpeechSynthesisSynthesizer is always in either the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY or 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state.
 *
 * A #SpeechSynthesisSynthesizer enters the 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY state
 * from the #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state when one of the
 * speak functions is called to place an object on the speech output queue.
 * A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED event is issued
 * to mark this change in state.
 *
 * A #SpeechSynthesisSynthesizer returns from the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY state to the 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state once the queue is 
 * emptied because of completion of speaking all objects or because of 
 * a call to a cancel function.
 *
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 * See: speech_engine_test_engine_state()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 */

static long SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY = 131072;


GType 
speech_synthesis_synthesizer_get_type(void);

SpeechSynthesisSynthesizer * 
speech_synthesis_synthesizer_new(void);

void 
speech_synthesis_synthesizer_add_speakable_listener(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechSynthesisSpeakableListener *listener);

void 
speech_synthesis_synthesizer_remove_speakable_listener(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechSynthesisSpeakableListener *listener);

SpeechStatus 
speech_synthesis_synthesizer_get_number_of_queue_items(
                             SpeechSynthesisSynthesizer *synthesizer,
                             int n);

SpeechStatus 
speech_synthesis_synthesizer_get_queue_item(
                             SpeechSynthesisSynthesizer *synthesizer,
                             int n,
                             SpeechSynthesisSynthesizerQueueItem *item);

void 
speech_synthesis_synthesizer_cancel(
                             SpeechSynthesisSynthesizer *synthesizer);

SpeechStatus 
speech_synthesis_synthesizer_cancel_item(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechObject *source);

SpeechStatus 
speech_synthesis_synthesizer_cancel_all(
                             SpeechSynthesisSynthesizer *synthesizer);

SpeechSynthesisSynthesizerProperties * 
speech_synthesis_synthesizer_get_synthesizer_properties(
                             SpeechSynthesisSynthesizer *synthesizer);

SpeechStatus 
speech_synthesis_synthesizer_phoneme(
                             SpeechSynthesisSynthesizer *synthesizer,
                             gchar *text,
                             gchar *phoneme);

gboolean 
speech_synthesis_synthesizer_is_markup_type_supported(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechMarkupType type);

SpeechStatus 
speech_synthesis_synthesizer_speak_markup_text(
                             SpeechSynthesisSynthesizer *synthesizer,
                             gchar *markup_text,
                             SpeechSynthesisSpeakableListener *listener);

SpeechStatus 
speech_synthesis_synthesizer_speak_text_from_url(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechMarkupURL markup_url,
                             SpeechSynthesisSpeakableListener *listener);

SpeechStatus 
speech_synthesis_synthesizer_speak_speakable(
                             SpeechSynthesisSynthesizer *synthesizer,
                             SpeechSynthesisSpeakable *markup_text,
                             SpeechSynthesisSpeakableListener *listener);

SpeechStatus 
speech_synthesis_synthesizer_speak_plain_text(
                             SpeechSynthesisSynthesizer *synthesizer,
                             gchar *plain_text,
                             SpeechSynthesisSpeakableListener *listener);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SYNTHESIZER_H__ */
