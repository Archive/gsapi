
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Event issued by SpeechSynthesisSynthesizer to indicate a change in state 
 * or other activity.
 */

#ifndef __SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_H__
#define __SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_H__

#include <speech/speech_engine_event.h>
#include <speech/synthesis/speech_synthesis_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_EVENT              (speech_synthesis_synthesizer_event_get_type())
#define SPEECH_SYNTHESIS_SYNTHESIZER_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_EVENT, SpeechSynthesisSynthesizerEvent))
#define SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_EVENT, SpeechSynthesisSynthesizerEventClass))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_EVENT))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_EVENT))
#define SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_EVENT, SpeechSynthesisSynthesizerEventClass))

typedef struct _SpeechSynthesisSynthesizerEvent            SpeechSynthesisSynthesizerEvent;
typedef struct _SpeechSynthesisSynthesizerEventClass       SpeechSynthesisSynthesizerEventClass;

struct _SpeechSynthesisSynthesizerEvent {
    SpeechEngineEvent event;
};

struct _SpeechSynthesisSynthesizerEventClass {
    SpeechEngineEventClass parent_class;
};


/*
 * The speaking queue of the #SpeechSynthesisSynthesizer has emptied
 * and the #SpeechSynthesisSynthesizer has changed to the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state. The queue may 
 * become empty because speech output of all items in the queue is 
 * completed, or because the items have been cancelled.
 *
 * The #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED event follows the
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED or
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED event that 
 * removed the last item from the speaking queue.
 *
 * See: speech_synthesis_synthesizer_listener_queue_emptied()
 * See: speech_synthesis_synthesizer_cancel()
 * See: speech_synthesis_synthesizer_cancel_item()
 * See: speech_synthesis_synthesizer_cancel_all()
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY
 */

static int SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED = 700;

/*
 * The speech output queue has changed.	 This event may indicate a
 * change in state of the #SpeechSynthesisSynthesizer from
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY to 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY. The
 * event may also occur in the #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 * state without changing state.
 *
 * The speech output queue changes when:
 * <itemizedlist>
 *   <listitem>a new item is placed on the queue with a call to one of the
 *	 speak functions,</listitem>
 *   <listitem>when an item is removed from the queue with one of the
 *	 cancel functions (without emptying the queue), or</listitem>
 *   <listitem>when output of the top item of the queue is completed (again,
 *	 without leaving an empty queue).</listitem>
 * </itemizedlist>
 *
 * The #top_of_queue_changed boolean parameter is set to
 * %TRUE if the top item on the queue has changed.
 *
 * See: speech_synthesis_synthesizer_listener_queue_updated()
 * See: speech_synthesis_synthesizer_event_get_top_of_queue_changed()
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 */

static int SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED = 701;


GType 
speech_synthesis_synthesizer_event_get_type(void);

SpeechSynthesisSynthesizerEvent * 
speech_synthesis_synthesizer_event_new(SpeechEngine *source,
                                       int id,
                                       gboolean top_of_queue_changed,
                                       long old_engine_state,
                                       long new_engine_state);

gboolean 
speech_synthesis_synthesizer_event_get_top_of_queue_changed(
                                   SpeechSynthesisSynthesizerEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_H__ */
