
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Event issued during spoken output of text.
 *
 * #SpeechSynthesisSpeakableEvent's are issued to 
 * #SpeechSynthesisSpeakableListener's. A single 
 * #SpeechSynthesisSpeakableListener can be provided to any of the
 * speak functions of a #SpeechSynthesisSynthesizer to monitor progress of 
 * a single item on the speech output queue. Any number of 
 * #SpeechSynthesisSpeakableListener objects can be attached to a 
 * #SpeechSynthesisSynthesizer with the
 * speech_synthesis_synthesizer_add_speakable_listener() function. These 
 * listeners receive every #SpeechSynthesisSpeakableEvent for every item 
 * on the speech output queue of the #SpeechSynthesisSynthesizer. The 
 * #SpeechSynthesisSpeakableListener attached to an individual item on the 
 * speech output queue is notified before the
 * #SpeechSynthesisSpeakableListener's attached to the 
 * #SpeechSynthesisSynthesizer.
 *
 * The source for a #SpeechSynthesisSpeakableEvent is the object from which 
 * the marked up text was obtained: a #SpeechSynthesisSpeakable object, a
 * #SpeechMarkupURL, or a string.
 *
 * The normal sequence of events during output of the item of the
 * top of the synthesizer's speech output is:
 *
 * <itemizedlist>
 *   <listitem>#SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE</listitem>
 *   <listitem>#SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_STARTED</listitem>
 *   <listitem>Any number of #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED
 *       and #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED events.</listitem>
 *   <listitem>#SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED</listitem>
 * </itemizedlist>
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_PAUSED may occur any time 
 * after the #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE but before the 
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED event.
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_PAUSED event can only be
 * followed by a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_RESUMED or
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED event can occur 
 * at any time before a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED
 * (including before a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE
 * event). No other events can follow the 
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED event since the 
 * item has been removed from the speech output queue.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED event can be 
 * issued for items that are not at the top of the speech output queue. 
 * The other events are only issued for the top-of-queue item.
 *
 * See: #SpeechSynthesisSpeakableListener
 * See: speech_synthesis_synthesizer_add_speakable_listener()
 * See: #SpeechSynthesisSpeakable
 * See: speech_synthesis_synthesizer_speak_markup_text()
 * See: speech_synthesis_synthesizer_speak_text_from_url()
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_speak_plain_text()
 */

#include <speech/synthesis/speech_synthesis_speakable_event.h>


GType
speech_synthesis_speakable_event_get_type(void)
{
    printf("speech_synthesis_speakable_event_get_type called.\n");
}


SpeechSynthesisSpeakableEvent *
speech_synthesis_speakable_event_new(SpeechObject *source, int id)
{
    printf("speech_synthesis_speakable_event_new called.\n");
}


/**
 * speech_synthesis_speakable_event_new_with_marker_type:
 * @source: the specified source.
 * @id: the identifier.
 * @text: the text.
 * @marker_type: the marker type (used for a
 *               #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED event).
 *
 * Initialises a #SpeechSynthesisSpeakableEvent with a specified @source,
 * @id, @text and @marker type (used for a
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED event).
 *
 * Returns:
 */

SpeechSynthesisSpeakableEvent *
speech_synthesis_speakable_event_new_with_marker_type(SpeechObject *source,
                                                      int id,
                                                      gchar *text,
                                                      int marker_type)
{
    printf("speech_synthesis_speakable_event_new_with_marker_type called.\n");
}


/**
 * speech_synthesis_speakable_event_new_with_word_end:
 * @source: the specified source.
 * @id: the identifier.
 * @text: the text.
 * @word_start: the word start (called for a 
 *              #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED event).
 * @word_end: the word end.
 *
 * Initializer for a specified @source, @id, @text, @word_start
 * and @word_end (called for a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED
 * event).
 *
 * Returns:
 */

SpeechSynthesisSpeakableEvent *
speech_synthesis_speakable_event_new_with_word_end(SpeechObject *source,
                                                   int id,
                                                   gchar *text,
                                                   int word_start,
                                                   int word_end)
{
    printf("speech_synthesis_speakable_event_new_with_word_end called.\n");
}


/**
 * speech_synthesis_speakable_event_get_marker_type:
 * @event: the #SpeechSynthesisSpeakableEvent object that this operation
 *         will be applied to.
 *
 * Get the marker type.
 *
 * Returns: the type of a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED
 *          event.
 *
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_OPEN
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_CLOSE
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_EMPTY
 */

int
speech_synthesis_speakable_event_get_marker_type(
                                     SpeechSynthesisSpeakableEvent *event)
{
    printf("speech_synthesis_speakable_event_get_marker_type called.\n");
}


/**
 * speech_synthesis_speakable_event_get_text:
 * @event: the #SpeechSynthesisSpeakableEvent object that this operation
 *         will be applied to.
 *
 * Get the text associated with the event.
 *
 * For #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED_WORD_STARTED, the 
 * text is the next word to be spoken. This text may differ from the text 
 * between the #word_start and #word_end points is the original marked up text.
 *
 * For #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED, the text is the 
 * #MARK attribute in the marked up text.
 *
 * Returns: the text associated with the event.
 *
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED
 */

gchar *
speech_synthesis_speakable_event_get_text(SpeechSynthesisSpeakableEvent *event)
{
    printf("speech_synthesis_speakable_event_get_text called.\n");
}


/**
 * speech_synthesis_speakable_event_get_word_end:
 * @event: the #SpeechSynthesisSpeakableEvent object that this operation
 *         will be applied to.
 *
 * For a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED event, return the 
 * index of the last character of the word in the marked up text.
 *
 * Returns: the index of the last character of the word in the
 * marked up text.
 */

int
speech_synthesis_speakable_event_get_word_end(
                                     SpeechSynthesisSpeakableEvent *event)
{
    printf("speech_synthesis_speakable_event_get_word_end called.\n");
}


/**
 * speech_synthesis_speakable_event_get_word_start:
 * @event: the #SpeechSynthesisSpeakableEvent object that this operation
 *         will be applied to.
 *
 * For a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED event, return the 
 * index of the first character of the word in the marked up text.
 *
 * Returns: the index of the first character of the word in the
 * marked up text.
 */

int
speech_synthesis_speakable_event_get_word_start(
                                     SpeechSynthesisSpeakableEvent *event)
{
    printf("speech_synthesis_speakable_event_get_word_start called.\n");
}
