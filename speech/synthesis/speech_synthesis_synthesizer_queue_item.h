
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Represents an object on the speech output queue of a 
 * SpeechSynthesisSynthesizer.
 */

#ifndef __SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM_H__
#define __SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM_H__

#include <speech/synthesis/speech_synthesis_types.h>
#include <speech/synthesis/speech_synthesis_speakable_listener.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM              (speech_synthesis_synthesizer_queue_item_get_type())
#define SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM, SpeechSynthesisSynthesizerQueueItem))
#define SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM, SpeechSynthesisSynthesizerQueueItemClass))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM))
#define SPEECH_IS_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM))
#define SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM, SpeechSynthesisSynthesizerQueueItemClass))

typedef struct _SpeechSynthesisSynthesizerQueueItem            SpeechSynthesisSynthesizerQueueItem;
typedef struct _SpeechSynthesisSynthesizerQueueItemClass       SpeechSynthesisSynthesizerQueueItemClass;

struct _SpeechSynthesisSynthesizerQueueItem {
    SpeechObject object;
};

struct _SpeechSynthesisSynthesizerQueueItemClass {
    SpeechObjectClass parent_class;
};


GType 
speech_synthesis_synthesizer_queue_item_get_type(void);

SpeechSynthesisSynthesizerQueueItem * 
speech_synthesis_synthesizer_queue_item_new(
                                   SpeechObject *source,
                                   gchar *text,
                                   gboolean plain_text,
                                   SpeechSynthesisSpeakableListener *listener);

SpeechObject * 
speech_synthesis_synthesizer_queue_item_get_source(
                         SpeechSynthesisSynthesizerQueueItem *item);

SpeechSynthesisSpeakableListener * 
speech_synthesis_synthesizer_queue_item_get_speakable_listener(
                         SpeechSynthesisSynthesizerQueueItem *item);

gchar * 
speech_synthesis_synthesizer_queue_item_get_text(
                         SpeechSynthesisSynthesizerQueueItem *item);

gboolean 
speech_synthesis_synthesizer_queue_item_is_plain_text(
                         SpeechSynthesisSynthesizerQueueItem *item);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_ITEM_H__ */
