
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Description of one output voice of a speech synthesizer. */

#ifndef __SPEECH_SYNTHESIS_VOICE_H__
#define __SPEECH_SYNTHESIS_VOICE_H__

#include <speech/synthesis/speech_synthesis_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_VOICE              (speech_synthesis_voice_get_type())
#define SPEECH_SYNTHESIS_VOICE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_VOICE, SpeechSynthesisVoice))
#define SPEECH_SYNTHESIS_VOICE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_VOICE, SpeechSynthesisVoiceClass))
#define SPEECH_IS_SYNTHESIS_VOICE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_VOICE))
#define SPEECH_IS_SYNTHESIS_VOICE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_VOICE))
#define SPEECH_SYNTHESIS_VOICE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_VOICE, SpeechSynthesisVoiceClass))

typedef struct _SpeechSynthesisVoice            SpeechSynthesisVoice;
typedef struct _SpeechSynthesisVoiceClass       SpeechSynthesisVoiceClass;

struct _SpeechSynthesisVoice {
    SpeechObject object;
};

struct _SpeechSynthesisVoiceClass {
    SpeechObjectClass parent_class;
};


/*
 * Ignore gender when performing a match of voices.
 * Synthesizers never provide a voice with
 * #SPEECH_SYNTHESIS_VOICE_GENDER_DONT_CARE
 *
 * See: speech_synthesis_voice_get_gender()
 * See: speech_synthesis_voice_set_gender()
 */

static int SPEECH_SYNTHESIS_VOICE_GENDER_DONT_CARE = 0xFFFF;

/*
 * Female voice.
 *
 * See: speech_synthesis_voice_get_gender()
 * See: speech_synthesis_voice_set_gender()
 */

static int SPEECH_SYNTHESIS_VOICE_GENDER_FEMALE = 1;

/*
 * Male voice.
 *
 * See: speech_synthesis_voice_get_gender()
 * See: speech_synthesis_voice_set_gender()
 */

static int SPEECH_SYNTHESIS_VOICE_GENDER_MALE = 2;

/*
 * Neutral voice that is neither male or female
 * (for example, artificial voices, robotic voices).
 *
 * See: speech_synthesis_voice_get_gender()
 * See: speech_synthesis_voice_set_gender()
 */

static int SPEECH_SYNTHESIS_VOICE_GENDER_NEUTRAL = 4;


/*
 * Ignore age when performing a match.
 * Synthesizers never provide a voice with 
 * #SPEECH_SYNTHESIS_VOICE_AGE_DONT_CARE.
 *
 * See: speech_synthesis_voice_get_age()
 * See: speech_synthesis_voice_set_age()
 */

static int SPEECH_SYNTHESIS_VOICE_AGE_DONT_CARE = 0xFFFF;

/*
 * Age roughly up to 12 years.
 *
 * See: speech_synthesis_voice_get_age()
 * See: speech_synthesis_voice_set_age()
 */

static int SPEECH_SYNTHESIS_VOICE_AGE_CHILD = 0x0001 << 0;

/*
 * Age roughly 13 to 19 years.
 *
 * See: speech_synthesis_voice_get_age()
 * See: speech_synthesis_voice_set_age()
 */

static int SPEECH_SYNTHESIS_VOICE_AGE_TEENAGER = 0x0001 << 1;

/*
 * Age roughly 20 to 40 years.
 *
 * See: speech_synthesis_voice_get_age()
 * See: speech_synthesis_voice_set_age()
 */

static int SPEECH_SYNTHESIS_VOICE_AGE_YOUNGER_ADULT = 0x0001 << 2;

/*
 * Age roughly 40 to 60 years.
 *
 * See: speech_synthesis_voice_get_age()
 * See: speech_synthesis_voice_set_age()
 */

static int SPEECH_SYNTHESIS_VOICE_AGE_MIDDLE_ADULT = 0x0001 << 3;

/*
 * Age roughly 60 years and up.
 *
 * See: speech_synthesis_voice_get_age()
 * See: speech_synthesis_voice_set_age()
 */

static int SPEECH_SYNTHESIS_VOICE_AGE_OLDER_ADULT = 0x0001 << 4;

/*
 * Voice with age that is indeterminate.
 * For example, artificial voices, robotic voices.
 *
 * See: speech_synthesis_voice_get_age()
 * See: speech_synthesis_voice_set_age()
 */

static int SPEECH_SYNTHESIS_VOICE_AGE_NEUTRAL = 0x0001 << 5;


GType 
speech_synthesis_voice_get_type(void);

SpeechSynthesisVoice * 
speech_synthesis_voice_new(void);

SpeechSynthesisVoice * 
speech_synthesis_voice_new_with_voice_info(gchar *name,
                                           int gender,
                                           int age,
                                           gchar *style);

SpeechObject * 
speech_synthesis_voice_clone(SpeechSynthesisVoice *voice);

gboolean 
speech_synthesis_voice_equals(SpeechSynthesisVoice *voice,
                              SpeechSynthesisVoice *an_object);

int 
speech_synthesis_voice_get_age(SpeechSynthesisVoice *voice);

int 
speech_synthesis_voice_get_gender(SpeechSynthesisVoice *voice);

gchar * 
speech_synthesis_voice_get_name(SpeechSynthesisVoice *voice);

gchar * 
speech_synthesis_voice_get_style(SpeechSynthesisVoice *voice);

gboolean 
speech_synthesis_voice_match(SpeechSynthesisVoice *voice,
                             SpeechSynthesisVoice *require);

void 
speech_synthesis_voice_set_age(SpeechSynthesisVoice *voice,
                               int age);

void 
speech_synthesis_voice_set_gender(SpeechSynthesisVoice *voice,
                                  int gender);

void 
speech_synthesis_voice_set_name(SpeechSynthesisVoice *voice,
                                gchar *name);

void 
speech_synthesis_voice_set_style(SpeechSynthesisVoice *voice,
                                 gchar *style);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_VOICE_H__ */
