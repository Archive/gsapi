
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Provides control of the run-time properties of a 
 * #SpeechSynthesisSynthesizer. The #SpeechSynthesisSynthesizerProperties 
 * object is obtained by calling the speech_engine_get_engine_properties()
 * function of the #SpeechSynthesizer (inherited from the #SpeechEngine
 * object).
 *
 * Because #SpeechSynthesisSynthesizerProperties extends the
 * #SpeechEngineProperties object to provide synthesizer-specific
 * properties. It also inherits the following properties and conventions
 * from the #SpeechEngineProperties object.
 *
 * <itemizedlist>
 *   <listitem>Each property has a get and set function.</listitem>
 *   <listitem>Engines may ignore calls to change properties, for example by
 *	 applying maximum and minimum setttings.</listitem>
 *   <listitem>Calls to set functions may be asynchronous (they may return 
 *       before the property change takes effect). The #SpeechEngine will 
 *       apply a change as soon as possible. A #SpeechPropertyChangeEvent is
 *	 issued when the change takes effect. For example, a change in the
 *	 speaking rate might take place immediately, or at the end of the
 *	 current word, sentence or paragraph.</listitem>
 *   <listitem>The get functions return the current setting - not a pending 
 *       value.</listitem>
 *   <listitem>A #SpeechPropertyChangeListener may be attached to receive
 *	 property change events.</listitem>
 *   <listitem>Where appropriate, property settings are persistent across 
 *       sessions.</listitem>
 * </itemizedlist>
 *
 * The properties of a synthesizer are:
 *
 * <itemizedlist>
 *   <listitem>Speaking voice,</listitem>
 *   <listitem>Baseline pitch,</listitem>
 *   <listitem>Pitch range,</listitem>
 *   <listitem>Speaking rate,</listitem>
 *   <listitem>Volume.</listitem>
 * </itemizedlist>
 *
 * Setting these properties should be considered as a <emphasis>hint</emphasis>
 * to the synthesizer. A synthesizer may choose to ignore out-of-range
 * values. A synthesizer may have some properties that are unchangeable
 * (e.g. a single voice synthesizer). Reasonable values for baseline pitch,
 * pitch range and speaking rate may vary between synthesizers, between
 * languages and or between voices.
 *
 * A change in voice may lead to change in other properties. For example,
 * female and young voices typically have higher pitches than male voices.
 * When a change in voice leads to changes in other properties, a separate
 * #SpeechPropertyChangeEvent is issued for each property changed.
 *
 * Whenever possible, property changes should be persistent for a voice.
 * For instance, after changing from voice A to voice B and back, the previous
 * property settings for voice A should return.
 *
 * Changes in pitch, speaking rate and so on in the Java Speech Markup Language
 * text provided to the synthesizer affect the get values but do not lead to
 * a property change event.  Applications needing an event at the time these
 * changes should include a #MARK property with the appropriate
 * marked up element.
 *
 * See: #SpeechSynthesisSynthesizer
 * See: speech_engine_get_engine_properties()
 */

#include <speech/synthesis/speech_synthesis_synthesizer_properties.h>


GType
speech_synthesis_synthesizer_properties_get_type(void)
{
    printf("speech_synthesis_synthesizer_properties_get_type called.\n");
}


SpeechSynthesisSynthesizerProperties *
speech_synthesis_synthesizer_properties_new(void)
{
    printf("speech_synthesis_synthesizer_properties_new called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_get_pitch:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 *
 * Get the baseline pitch for synthesis.
 *
 * Returns: the baseline pitch for synthesis.
 *
 * See: speech_synthesis_synthesizer_properties_set_pitch()
 */

float
speech_synthesis_synthesizer_properties_get_pitch(
                           SpeechSynthesisSynthesizerProperties *properties)
{
    printf("speech_synthesis_synthesizer_properties_get_pitch called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_get_pitch_range:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 * 
 * Get the pitch range for synthesis.
 *
 * Returns: the pitch range for synthesis.
 *
 * See: speech_synthesis_synthesizer_properties_set_pitch_range()
 */

float
speech_synthesis_synthesizer_properties_get_pitch_range(
                           SpeechSynthesisSynthesizerProperties *properties)
{
    printf("speech_synthesis_synthesizer_properties_get_pitch_range called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_get_speaking_rate:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 *
 * Get the current target speaking rate.
 *
 * Returns: the current target speaking rate.
 *
 * See: speech_synthesis_synthesizer_properties_set_speaking_rate()
 */

float
speech_synthesis_synthesizer_properties_get_speaking_rate(
                           SpeechSynthesisSynthesizerProperties *properties)
{
    printf("speech_synthesis_synthesizer_properties_get_speaking_rate called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_get_voice:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 *
 * Get the current synthesizer voice.  Modifications to the returned
 * voice do not affect the #SpeechSynthesisSynthesizer voice - a call
 * to speech_synthesis_synthesizer_properties_set_voice() is required 
 * for a change to take effect.
 *
 * See: speech_synthesis_synthesizer_properties_set_voice()
 */

SpeechSynthesisVoice *
speech_synthesis_synthesizer_properties_get_voice(
                           SpeechSynthesisSynthesizerProperties *properties)
{
    printf("speech_synthesis_synthesizer_properties_get_voice called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_get_volume:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 *
 * Get the current volume.
 *
 * Returns: the current volume.
 *
 * See: ispeech_synthesis_synthesizer_properties_set_volume()
 */

float
speech_synthesis_synthesizer_properties_get_volume(
                           SpeechSynthesisSynthesizerProperties *properties)
{
    printf("speech_synthesis_synthesizer_properties_get_volume called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_set_pitch:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 * @hertz: the new baseline pitch for the current synthesis voice.
 *
 * Set the baseline pitch for the current synthesis voice. Out-of-range
 * values may be ignored or restricted to engine-defined limits.
 * Different voices have different natural sounding ranges of pitch.
 * Typical male voices are between 80 and 180 Hertz. Female pitches
 * typically vary from 150 to 300 Hertz.
 *
 * See: speech_synthesis_synthesizer_properties_get_pitch()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the synthesizer rejects or
 *		limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_properties_set_pitch(
                           SpeechSynthesisSynthesizerProperties *properties,
                           float hertz)
{
    printf("speech_synthesis_synthesizer_properties_set_pitch called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_set_pitch_range:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 * @hertz: the new pitch range for the current synthesis voice.
 *
 * Set the pitch range for the current synthesis voice. A narrow
 * pitch range provides monotonous output while wide range provide
 * a more lively voice. This setting is a hint to the synthesis engine.
 * Engines may choose to ignore unreasonable requests. Some
 * synthesizers may not support pitch variability. The pitch range is
 * typically between 20% and 80% of the baseline pitch.
 *
 * See: speech_synthesis_synthesizer_properties_get_pitch_range()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the synthesizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_properties_set_pitch_range(
                           SpeechSynthesisSynthesizerProperties *properties,
                           float hertz)
{
    printf("speech_synthesis_synthesizer_properties_set_pitch_range called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_set_speaking_rate:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 * @wpm: the target speaking rate (in words per minute).
 *
 * Set the target speaking rate for the synthesis voice in words per minute.
 *
 * Reasonable speaking rates depend upon the synthesizer and the
 * current voice (some voices sound better at higher or lower speed
 * than others).
 *
 * Speaking rate is also dependent upon the language because of
 * different conventions for what is a "word". A reasonable speaking
 * rate for English is 200 words per minute.
 *
 * See: speech_synthesis_synthesizer_properties_get_speaking_rate()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the synthesizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_properties_set_speaking_rate(
                           SpeechSynthesisSynthesizerProperties *properties,
                           float wpm)
{
    printf("speech_synthesis_synthesizer_properties_set_speaking_rate called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_set_voice:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 * @voice: the synthesizer voice.
 *
 * Set the current synthesizer voice.
 *
 * The list of available voices for a #SpeechSynthesisSynthesizer is
 * returned by the speech_synthesis_synthesizer_mode_desc_get_voices()
 * function of the synthesizer's #SpeechSynthesisSynthesizerModeDesc. 
 * Any one of the voices returned by that function can be passed to 
 * speech_synthesis_synthesizer_properties_set_voice() to set the
 * current speaking voice.
 *
 * Alternatively, the voice parameter may be an application-created
 * partially specified #SpeechVoice object. If there is no matching 
 * voice, the call is ignored.
 *
 * See: speech_synthesis_synthesizer_properties_get_voice()
 * See: #SpeechSynthesisSynthesizerModeDesc
 * See: speech_engine_get_engine_mode_desc()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the synthesizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_properties_set_voice(
                           SpeechSynthesisSynthesizerProperties *properties,
                           SpeechSynthesisVoice *voice)
{
    printf("speech_synthesis_synthesizer_properties_set_voice called.\n");
}


/**
 * speech_synthesis_synthesizer_properties_set_volume:
 * @properties: the #SpeechSynthesisSynthesizerProperties object that this
 *              operation will be applied to.
 * @volume: the volume for the synthesizer's speech output as a value 
 *          between 0.0 and 1.0.
 *
 * Set the volume for the synthesizer's speech output as a value
 * between 0.0 and 1.0. A value of 0.0 indicates silence. A value
 * of 1.0 is maximum volume and is usually the synthesizer default.
 *
 * A synthesizer may change the voice's style with volume. For example,
 * a quiet volume might produce whispered output and loud might
 * produce shouting. Most synthesizer <em>do not</em> make this type
 * of change.
 *
 * See: speech_synthesis_synthesizer_properties_get_volume()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the synthesizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_synthesis_synthesizer_properties_set_volume(
                           SpeechSynthesisSynthesizerProperties *properties,
                           float volume)
{
    printf("speech_synthesis_synthesizer_properties_set_volume called.\n");
}
