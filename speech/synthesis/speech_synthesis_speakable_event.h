
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Event issued during spoken output of text. */

#ifndef __SPEECH_SYNTHESIS_SPEAKABLE_EVENT_H__
#define __SPEECH_SYNTHESIS_SPEAKABLE_EVENT_H__

#include <speech/speech_event.h>
#include <speech/synthesis/speech_synthesis_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SPEAKABLE_EVENT              (speech_synthesis_speakable_event_get_type())
#define SPEECH_SYNTHESIS_SPEAKABLE_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_EVENT, SpeechSynthesisSpeakableEvent))
#define SPEECH_SYNTHESIS_SPEAKABLE_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_EVENT, SpeechSynthesisSpeakableEventClass))
#define SPEECH_IS_SYNTHESIS_SPEAKABLE_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_EVENT))
#define SPEECH_IS_SYNTHESIS_SPEAKABLE_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_EVENT))
#define SPEECH_SYNTHESIS_SPEAKABLE_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_EVENT, SpeechSynthesisSpeakableEventClass))

typedef struct _SpeechSynthesisSpeakableEvent            SpeechSynthesisSpeakableEvent;
typedef struct _SpeechSynthesisSpeakableEventClass       SpeechSynthesisSpeakableEventClass;

struct _SpeechSynthesisSpeakableEvent {
    SpeechEvent event;
};

struct _SpeechSynthesisSpeakableEventClass {
    SpeechEventClass parent_class;
};


/*
 * Issued when an item on the synthesizer's speech output queue
 * reaches the top of the queue. If the #SpeechSynthesisSynthesizer
 * is not paused, the #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE
 * event will be followed immediately by the 
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_STARTED event. If the 
 * #SpeechSynthesisSynthesizer is paused, the
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_STARTED event will be 
 * delayed until the #SpeechSynthesisSynthesizer is resumed.
 *
 * A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED is also issued 
 * when the speech output queue changes (e.g. a new item at the top of 
 * the queue). The #SpeechSynthesisSpeakableEvent is issued prior to the
 * #SpeechSynthesisSynthesizerEvent.
 *
 * See: speech_synthesis_speakable_listener_top_of_queue()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_ENGINE_EVENT_PAUSED
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE = 601;

/*
 * Issued at the start of audio output of an item on the speech
 * output queue. This event immediately follows the
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE unless the 
 * #SpeechSynthesisSynthesizer is paused when the speakable text 
 * is promoted to the top of the output queue.
 *
 * See: speech_synthesis_speakable_listener_speakable_started()
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_STARTED = 602;

/*
 * Issued with the completion of audio output of an object on
 * the speech output queue as the object is removed from the queue.
 *
 * A #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED or 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED event
 * is also issued when the speech output queue changes because the
 * speech output of the item at the top of queue is completed. The
 * #SpeechSynthesisSpeakableEvent is issued prior to the
 * #SpeechSynthesisSynthesizerEvent.
 *
 * See: speech_synthesis_speakable_listener_speakable_ended()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED = 603;

/*
 * Issued when audio output of the item at the top of a synthesizer's
 * speech output queue is paused. The 
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_PAUSED
 * #SpeechSynthesisSpeakableEvent is issued prior to the
 * #SPEECH_ENGINE_EVENT_PAUSED event that is issued to the
 * #SpeechSynthesisSynthesizerListener.
 *
 * See: speech_synthesis_speakable_listener_speakable_paused()
 * See: #SPEECH_ENGINE_EVENT_PAUSED
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_PAUSED = 604;

/*
 * Issued when audio output of the item at the top of a synthesizer's
 * speech output queue is resumed after a previous pause. The
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_RESUMED 
 * #SpeechSynthesisSpeakableEvent is issued prior to the 
 * #SPEECH_ENGINE_EVENT_RESUMED event that is issued to the 
 * #SpeechSynthesisSynthesizerListener.
 *
 * See: speech_synthesis_speakable_listener_speakable_resumed()
 * See: #SPEECH_ENGINE_EVENT_RESUMED
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_RESUMED = 605;

/*
 * Issued when an item on the synthesizer's speech output queue is
 * cancelled and removed from the queue. A speech output queue item
 * may be cancelled at any time following a call to one of the various
 * speak functions.
 * An item can be cancelled even if it is not at the top of the
 * speech output queue (other #SpeechSynthesisSpeakableEvent's are issued
 * only to the top-of-queue item). Once cancelled, the listener for the
 * cancelled object receives no further #SpeechSynthesisSpeakableEvent's.
 *
 * The #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED
 * #SpeechSynthesisSpeakableEvent is issued prior to the 
 * #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED or
 * #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED event that is 
 * issued to the #SpeechSynthesisSynthesizerListener.
 *
 * See: speech_synthesis_speakable_listener_speakable_cancelled()
 * See: speech_synthesis_synthesizer_cancel()
 * See: speech_synthesis_synthesizer_cancel_item()
 * See: speech_synthesis_synthesizer_cancel_all()
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_UPDATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_EVENT_QUEUE_EMPTIED
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED = 606;

/*
 * Issued when a synthesis engine starts the audio output of a word
 * in the speech output queue item. The #text, #word_start and #word_end 
 * parameters defines the segment of the speakable's string which is now 
 * being spoken.
 *
 * See: speech_synthesis_speakable_listener_word_started()
 * See: speech_synthesis_speakable_event_get_text()
 * See: speech_synthesis_speakable_event_get_word_start()
 * See: speech_synthesis_speakable_event_get_word_end()
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED = 607;

/*
 * Issued when audio output reaches a marker contained in the marked
 * up text of a speech output queue item. The event text is the string
 * of the #MARK attribute. The #marker_type indicates whether the mark 
 * is at the opening or close of a marked up element or is an attribute 
 * of an empty element (no close).
 *
 * See: speech_synthesis_speakable_listener_marker_reached()
 * See: speech_synthesis_speakable_event_get_text()
 * See: speech_synthesis_speakable_event_get_marker_type()
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_OPEN
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_CLOSE
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_EMPTY
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED = 608;

/*
 * The type of #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED event 
 * issued at the opening of a marked up container element with a #MARK
 * attribute. An #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_OPEN event 
 * is followed by a #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_CLOSE 
 * event for the closing of the element (unless the 
 * #SpeechSynthesisSpeakable> is cancelled).
 *
 * Example: the event for the #MARK attribute on the opening 
 * #SENT tag will be issued before the start of the word "Testing" in:
 *
 *	&lt;SENT MARK="open"&gt;Testing one, &lt;MARKER MARK="here"/&gt; two, three.&lt;/SENT&gt;
 *
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED
 * See: speech_synthesis_speakable_event_get_marker_type()
 */
static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_OPEN = 620;

/*
 * The type of #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED event 
 * issued at the close of a marked up container element that has a 
 * #MARK attribute on the matching opening tag. The
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_CLOSE event always follows 
 * a matching #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_OPEN event for 
 * the matching openning tag.
 *
 * Example: the event for the closing #SENT tag for the #MARK attribute 
 * at the opening of the #SENT element. The event will be 
 * issued after the word "three" is spoken.
 *
 *	&lt;SENT MARK="open"&gt;Testing one, &lt;MARKER MARK="here"/&gt;
two, three.&lt;/SENT&gt;
 *
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED
 * See: speech_synthesis_speakable_event_get_marker_type()
 */
static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_CLOSE = 621;

/*
 * The type of #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED event 
 * issued when an empty marked up element with a #MARK attribute is
 * reached. (An empty JSML has no closing tag and is indicated by
 * a slash ('/') before the '&gt;' character.)
 *
 * Example: the #MARKER tag below is empty event so an
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_EMPTY type of 
 * #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED event is issued before 
 * the word "two" is spoken in:
 *
 *	&lt;SENT MARK="open"&gt;Testing one, &lt;MARKER MARK="here"/&gt;
two, three.&lt;/SENT&gt;
 *
 * See: #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED
 * See: speech_synthesis_speakable_event_get_marker_type()
 */

static int SPEECH_SYNTHESIS_SPEAKABLE_EVENT_ELEMENT_EMPTY = 622;


GType 
speech_synthesis_speakable_event_get_type(void);

SpeechSynthesisSpeakableEvent * 
speech_synthesis_speakable_event_new(SpeechObject *source,
                                     int id);

SpeechSynthesisSpeakableEvent * 
speech_synthesis_speakable_event_new_with_marker_type(SpeechObject *source,
                                                      int id,
                                                      gchar *text,
                                                      int marker_type);

SpeechSynthesisSpeakableEvent * 
speech_synthesis_speakable_event_new_with_word_end(SpeechObject *source,
                                                   int id,
                                                   gchar *text,
                                                   int word_start,
                                                   int word_end);

int 
speech_synthesis_speakable_event_get_marker_type(
                                        SpeechSynthesisSpeakableEvent *event);

gchar * 
speech_synthesis_speakable_event_get_text(
                                        SpeechSynthesisSpeakableEvent *event);

int 
speech_synthesis_speakable_event_get_word_end(
                                        SpeechSynthesisSpeakableEvent *event);

int 
speech_synthesis_speakable_event_get_word_start(
                                        SpeechSynthesisSpeakableEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SPEAKABLE_EVENT_H__ */
