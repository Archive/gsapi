
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechSynthesisSynthesizerModeDesc extends the #SpeechEngineModeDesc
 * with properties that are specific to speech synthesizers. A
 * #SpeechSynthesisSynthesizerModeDesc inherits engine name, mode name, 
 * locale and running properties from #SpeechEngineModeDesc.
 * #SpeechSynthesisSynthesizerModeDesc adds two properties:
 *
 * <itemizedlist>
 *   <listitem>List of voices provided by the synthesizer.</listitem>
 *   <listitem>Voice to be loaded when the synthesizer is started (not used in
 *	 selection).</listitem>
 * </itemizedlist>
 *
 * Like #SpeechEngineModeDesc, there are two types of
 * #SpeechSynthesisSynthesizerModeDesc: those created by an application which
 * are used in engine selection, and those created by an engine which
 * describe a particular mode of operation of the engine. Descriptor provided
 * engines are obtained through the 
 * speech_central_available_synthesizers() function of the #SpeechCentral 
 * object and must have all their features defined. A descriptor created by 
 * an application may make any or all of the features %NULL which means 
 * "don't care" (%NULL features are ignored in engine selection).
 *
 * Applications can modify application-created descriptors in any way.
 * Applications should never modify a #SpeechSynthesisSynthesizerModeDesc
 * provided by an engine (i.e. returned by the
 * speech_central_available_synthesizers() function).
 *
 * Engine creation is described in the documentation for the #SpeechCentral 
 * object.
 *
 * See: #SpeechCentral
 * See: speech_central_create_synthesizer()
 * See: #SpeechVoice
 */

#include <speech/synthesis/speech_synthesis_synthesizer_mode_desc.h>


GType
speech_synthesis_synthesizer_mode_desc_get_type(void)
{
    printf("speech_synthesis_synthesizer_mode_desc_get_type called.\n");
}


SpeechSynthesisSynthesizerModeDesc *
speech_synthesis_synthesizer_mode_desc_new(void)
{
    printf("speech_synthesis_synthesizer_mode_desc_new called.\n");
}


SpeechSynthesisSynthesizerModeDesc *
speech_synthesis_synthesizer_mode_desc_new_with_locale(gchar *locale)
{
    printf("speech_synthesis_synthesizer_mode_desc_new_with_locale called.\n");
}


/**
 * speech_synthesis_synthesizer_mode_desc_new_with_name_and_state_and_voices:
 * @engine_name: the synthesizer engine name.
 * @mode_name: the synthesizer mode name.
 * @locale: the locale the synthesizer engine is running in..
 * @running: whether the engine is already running.
 * @voices: list of voices provided by the synthesizer.
 *
 * Create a fully-specified descriptor. Any of the features may be %NULL.
 *
 * Returns:
 */

SpeechSynthesisSynthesizerModeDesc *
speech_synthesis_synthesizer_mode_desc_new_with_name_and_state_and_voices(
                                             gchar *engine_name,
                                             gchar *mode_name,
                                             gchar *locale,
                                             SpeechTristate running,
                                             SpeechSynthesisVoice *voices[])
{
    printf("speech_synthesis_synthesizer_mode_desc_new_with_name_and_state_and_voices called.\n");
}


/**
 * speech_synthesis_synthesizer_mode_desc_add_voice:
 * @mode_desc: the #SpeechSynthesisSynthesizerModeDesc object that this
 *             operation will be applied to.
 * @voice: voice to add.
 *
 * Append a voice to the list of voices.
 */

void
speech_synthesis_synthesizer_mode_desc_add_voice(
                                SpeechSynthesisSynthesizerModeDesc *mode_desc,
                                SpeechSynthesisVoice *voice)
{
    printf("speech_synthesis_synthesizer_mode_desc_add_voice called.\n");
}


/**
 * speech_synthesis_synthesizer_mode_desc_get_voices:
 * @mode_desc: the #SpeechSynthesisSynthesizerModeDesc object that this
 *             operation will be applied to.
 *
 * Returns the list of voices available in this synthesizer mode.
 *
 * Returns: the list of voices available.
 */

SpeechSynthesisVoice **
speech_synthesis_synthesizer_mode_desc_get_voices(
                                SpeechSynthesisSynthesizerModeDesc *mode_desc)
{
    printf("speech_synthesis_synthesizer_mode_desc_get_voices called.\n");
}


/**
 * speech_synthesis_synthesizer_mode_desc_set_voices:
 * @mode_desc: the #SpeechSynthesisSynthesizerModeDesc object that this
 *             operation will be applied to.
 * @voices: the list of synthesizer voices.
 *
 * Set the list of synthesizer voices.
 */

void
speech_synthesis_synthesizer_mode_desc_set_voices(
                                SpeechSynthesisSynthesizerModeDesc *mode_desc,
                                SpeechSynthesisVoice *voices[])
{
    printf("speech_synthesis_synthesizer_mode_desc_set_voices called.\n");
}
