
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_SYNTHESIS_SPEAKABLE_H__
#define __SPEECH_SYNTHESIS_SPEAKABLE_H__

#include <speech/synthesis/speech_synthesis_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SPEAKABLE              (speech_synthesis_speakable_get_type())
#define SPEECH_SYNTHESIS_SPEAKABLE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE, SpeechSynthesisSpeakable))
#define SPEECH_SYNTHESIS_SPEAKABLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SPEAKABLE, SpeechSynthesisSpeakableClass))
#define SPEECH_IS_SYNTHESIS_SPEAKABLE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE))
#define SPEECH_IS_SYNTHESIS_SPEAKABLE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SPEAKABLE))
#define SPEECH_SYNTHESIS_SPEAKABLE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE, SpeechSynthesisSpeakableClass))

typedef struct _SpeechSynthesisSpeakable            SpeechSynthesisSpeakable;
typedef struct _SpeechSynthesisSpeakableClass       SpeechSynthesisSpeakableClass;

struct _SpeechSynthesisSpeakable {
    SpeechObject object;
};

struct _SpeechSynthesisSpeakableClass {
    SpeechObjectClass parent_class;
};


GType 
speech_synthesis_speakable_get_type(void);

SpeechSynthesisSpeakable * 
speech_synthesis_speakable_new(void);

gchar * 
speech_synthesis_speakable_get_markup_text(SpeechSynthesisSpeakable *speakable);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SPEAKABLE_H__ */
