
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Represents an object on the speech output queue of a
 * #SpeechSynthesisSynthesizer. The item is described by the source object,
 * the speakable text, a boolean value indicating whether it is a plain
 * text object, and the #SpeechSynthesisSpeakableListener for the object.
 *
 * The source object is the object provided to the various _speak functions
 * (a #SpeechSynthesisSpeakable objects, a #SpeechMarkupURL, or a string).
 * The text is the Java Speech Markup Language string or plain text obtained
 * from the source object. The listener is the 
 * #SpeechSynthesisSpeakableListener object passed to the _speak function,
 * or %NULL.
 *
 * See: #SpeechSynthesisSynthesizer
 * See: speech_synthesis_synthesizer_get_number_of_queue_items()
 * See: speech_synthesis_synthesizer_get_queue_item()
 */

#include <speech/synthesis/speech_synthesis_synthesizer_queue_item.h>


GType
speech_synthesis_synthesizer_queue_item_get_type(void)
{
    printf("speech_synthesis_synthesizer_queue_item_get_type called.\n");
}


/**
 * speech_synthesis_synthesizer_queue_item_new:
 * @source: the source object.
 * @text: the speakable text.
 * @plain_text: an indication of whether the text is plain or contains markup.
 * @listener:
 *
 * Create a #SpeechSynthesisSynthesizerQueueItem with the source object 
 * and speakable text.
 *
 * Returns:
 */

SpeechSynthesisSynthesizerQueueItem *
speech_synthesis_synthesizer_queue_item_new(SpeechObject *source,
                                   gchar *text,
                                   gboolean plain_text,
                                   SpeechSynthesisSpeakableListener *listener)
{
    printf("speech_synthesis_synthesizer_queue_item_new called.\n");
}


/**
 * speech_synthesis_synthesizer_queue_item_get_source:
 * @item: the #SpeechSynthesisSynthesizerQueueItem object that this operation
 *        will be applied to.
 *
 * Return the source object for an item on the speech output queue
 * of a #SpeechSynthesisSynthesizer. The source is one of the three
 * object types passed to the various speak functions of 
 * #SpeechSynthesisSynthesizer:
 * a #SpeechSynthesisSpeakable objects, a #SpeechMarkupURL, or a string.
 *
 * Returns: the source object for an item on the speech output queue.
 */

SpeechObject *
speech_synthesis_synthesizer_queue_item_get_source(
                                  SpeechSynthesisSynthesizerQueueItem *item)
{
    printf("speech_synthesis_synthesizer_queue_item_get_source called.\n");
}


/**
 * speech_synthesis_synthesizer_queue_item_get_speakable_listener:
 * @item: the #SpeechSynthesisSynthesizerQueueItem object that this operation
 *        will be applied to.
 *
 * Return the #SpeechSynthesisSpeakableListener object for this speech
 * output queue item, or %NULL if none was provided to the speak function.
 *
 * Returns: the #SpeechSynthesisSpeakableListener object for this speech
 *	   output queue item.
 */

SpeechSynthesisSpeakableListener *
speech_synthesis_synthesizer_queue_item_get_speakable_listener(
                                  SpeechSynthesisSynthesizerQueueItem *item)
{
    printf("speech_synthesis_synthesizer_queue_item_get_speakable_listener called.\n");
}


/**
 * speech_synthesis_synthesizer_queue_item_get_text:
 * @item: the #SpeechSynthesisSynthesizerQueueItem object that this operation
 *        will be applied to.
 *
 * Return the speakable text for an item on the speech output queue
 * of a #SpeechSynthesisSynthesizer. The text is either a marked up
 * string (JSML or SSML) or a plain text string that was obtained
 * from source object.
 *
 * Returns: the speakable text for an item on the speech output queue.
 */

gchar *
speech_synthesis_synthesizer_queue_item_get_text(
                                  SpeechSynthesisSynthesizerQueueItem *item)
{
    printf("speech_synthesis_synthesizer_queue_item_get_text called.\n");
}


/**
 * speech_synthesis_synthesizer_queue_item_is_plain_text:
 * @item: the #SpeechSynthesisSynthesizerQueueItem object that this operation
 *        will be applied to.
 *
 * Return true if the item contains plain text (not marked up text).
 *
 * Returns: a boolean indication of whether this queue item contains
 *	   plain text.
 */

gboolean
speech_synthesis_synthesizer_queue_item_is_plain_text(
                                  SpeechSynthesisSynthesizerQueueItem *item)
{
    printf("speech_synthesis_synthesizer_queue_item_is_plain_text called.\n");
}
