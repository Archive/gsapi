
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Signals that a Speech API exception has occurred. */

#ifndef __SPEECH_SYNTHESIS_SPEAKABLE_LISTENER_H__
#define __SPEECH_SYNTHESIS_SPEAKABLE_LISTENER_H__

#include <speech/synthesis/speech_synthesis_types.h>
#include <speech/synthesis/speech_synthesis_speakable_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_SYNTHESIS_SPEAKABLE_LISTENER              (speech_synthesis_speakable_listener_get_type())
#define SPEECH_SYNTHESIS_SPEAKABLE_LISTENER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_LISTENER, SpeechSynthesisSpeakableListener))
#define SPEECH_SYNTHESIS_SPEAKABLE_LISTENER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_LISTENER, SpeechSynthesisSpeakableListenerClass))
#define SPEECH_IS_SYNTHESIS_SPEAKABLE_LISTENER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_LISTENER))
#define SPEECH_IS_SYNTHESIS_SPEAKABLE_LISTENER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_LISTENER))
#define SPEECH_SYNTHESIS_SPEAKABLE_LISTENER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_SYNTHESIS_SPEAKABLE_LISTENER, SpeechSynthesisSpeakableListenerClass))

typedef struct _SpeechSynthesisSpeakableListener            SpeechSynthesisSpeakableListener;
typedef struct _SpeechSynthesisSpeakableListenerClass       SpeechSynthesisSpeakableListenerClass;

struct _SpeechSynthesisSpeakableListener {
    SpeechObject object;
};

struct _SpeechSynthesisSpeakableListenerClass {
    SpeechObjectClass parent_class;
};


GType 
speech_synthesis_speakable_listener_get_type(void);

SpeechSynthesisSpeakableListener * 
speech_synthesis_speakable_listener_new(void);

void 
speech_synthesis_speakable_listener_marker_reached(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);

void 
speech_synthesis_speakable_listener_speakable_cancelled(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);

void 
speech_synthesis_speakable_listener_speakable_ended(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);

void 
speech_synthesis_speakable_listener_speakable_paused(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);

void 
speech_synthesis_speakable_listener_speakable_resumed(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);

void 
speech_synthesis_speakable_listener_speakable_started(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);

void 
speech_synthesis_speakable_listener_top_of_queue(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);

void 
speech_synthesis_speakable_listener_word_started(
                               SpeechSynthesisSpeakableListener *listener,
                               SpeechSynthesisSpeakableEvent *e);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SPEAKABLE_LISTENER_H__ */
