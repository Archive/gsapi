
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The listener object for receiving notification of events during spoken
 * output of a #SpeechSynthesisSpeakable. Events are requested by either:
 *
 * <itemizedlist>
 *   <listitem>Providing a #SpeechSynthesisSpeakableListener object when 
 *       calling one of the speak functions of a #SpeechSynthesisSynthesizer.
 *       </listitem>
 *   <listitem>Attaching a #SpeechSynthesisSpeakableListener to a
 *	   #SpeechSynthesisSynthesizer with its 
 *         speech_synthesis_synthesizer_add_speakable_listener()
 *	   function.</listitem>
 * </itemizedlist>
 *
 * The speakable events and the sequencing of events is defined in the
 * documentation for #SpeechSynthesisSpeakableEvent. The source of each
 * #SpeechSynthesisSpeakableEvent is the object from which marked up text was
 * derived: a #SpeechSynthesisSpeakable object, a #SpeechMarkupURL, or
 * a string.
 *
 * See: #SpeechSynthesisSpeakable
 * See: #SpeechSynthesisSpeakableEvent
 * See: #SpeechSynthesisSynthesizer
 * See: speech_synthesis_synthesizer_add_speakable_listener()
 * See: speech_synthesis_synthesizer_speak_markup_text()
 * See: speech_synthesis_synthesizer_speak_text_from_url()
 * See: speech_synthesis_synthesizer_speak_speakable()
 * See: speech_synthesis_synthesizer_speak_plain_text()
 */

#include <speech/synthesis/speech_synthesis_speakable_listener.h>


GType
speech_synthesis_speakable_listener_get_type(void)
{
    printf("speech_synthesis_speakable_listener_get_type called.\n");
}


SpeechSynthesisSpeakableListener *
speech_synthesis_speakable_listener_new(void)
{
    printf("speech_synthesis_speakable_listener_new called.\n");
}


/**
 * speech_synthesis_speakable_listener_marker_reached:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_MARKER_REACHED event has occurred.
 */

void
speech_synthesis_speakable_listener_marker_reached(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_marker_reached called.\n");
}


/**
 * speech_synthesis_speakable_listener_speakable_cancelled:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_CANCELLED event has occurred.
 */

void
speech_synthesis_speakable_listener_speakable_cancelled(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_speakable_cancelled called.\n");
}


/**
 * speech_synthesis_speakable_listener_speakable_ended:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_ENDED event has occurred.
 */

void
speech_synthesis_speakable_listener_speakable_ended(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_speakable_ended called.\n");
}


/**
 * speech_synthesis_speakable_listener_speakable_paused:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_PAUSED event has occurred.
 */

void
speech_synthesis_speakable_listener_speakable_paused(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_speakable_paused called.\n");
}


/**
 * speech_synthesis_speakable_listener_speakable_resumed:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_RESUMED event has occurred.
 */

void
speech_synthesis_speakable_listener_speakable_resumed(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_speakable_resumed called.\n");
}


/**
 * speech_synthesis_speakable_listener_speakable_started:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event.
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_SPEAKABLE_STARTED event has occurred.
 */

void
speech_synthesis_speakable_listener_speakable_started(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_speakable_started called.\n");
}


/**
 * speech_synthesis_speakable_listener_top_of_queue:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event:
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_TOP_OF_QUEUE event has occurred.
 */

void
speech_synthesis_speakable_listener_top_of_queue(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_top_of_queue called.\n");
}


/**
 * speech_synthesis_speakable_listener_word_started:
 * @listener: the #SpeechSynthesisSpeakableListener object that this operation
 *            will be applied to.
 * @e: the event:
 *
 * A #SPEECH_SYNTHESIS_SPEAKABLE_EVENT_WORD_STARTED event has occurred.
 */

void
speech_synthesis_speakable_listener_word_started(
                                     SpeechSynthesisSpeakableListener *listener,
                                     SpeechSynthesisSpeakableEvent *e)
{
    printf("speech_synthesis_speakable_listener_word_started called.\n");
}
