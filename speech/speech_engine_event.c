
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechEngineEvent notifies changes in state of a speech synthesis
 * or recognition engine. #SpeechEngineEvent's are issued to each
 * #SpeechEngineListener attached to an engine. The
 * #SpeechRecognitionRecognizerEvent and #SpeechSynthesisSynthesizerEvent
 * objects both extend #SpeechEngineEvent to provide specific events for
 * recognizers and synthesizers.
 *
 * See: #SpeechEngineListener
 * See: #SpeechRecognitionRecognizerEvent
 * See: #SpeechSynthesisSynthesizerEvent
 */

#include <speech/speech_engine_event.h>


/**
 * speech_engine_event_get_type:
 *
 * Returns: the type ID for #SpeechEngineEvent.
 */

GType
speech_engine_event_get_type(void)
{
    printf("speech_engine_event_get_type called.\n");
}


/**
 * speech_engine_event_new:
 * @source: the object that issued the event.
 * @id: the identifier for the event type.
 * @old_engine_state: engine state prior to this event.
 * @new_engine_state: engine state following this event.
 *
 * Constructs a #SpeechEngineEvent with an event identifier,
 * old engine state and new engine state.
 *
 * Returns: a new #SpeechEngineEvent object.
 *
 * See: speech_engine_get_engine_state()
 */

SpeechEngineEvent *
speech_engine_event_new(SpeechEngine *source,
                        int id,
                        long old_engine_state,
                        long new_engine_state)
{
    printf("speech_engine_event_new called.\n");
}


/**
 * speech_engine_event_get_new_engine_state:
 * @event: the #SpeechEngineEvent that this operation will be applied to.
 *
 * Return the state following this #SpeechEngineEvent.
 * The value matches the speech_engine_get_engine_state() function.
 *
 * Returns: the state following this #SpeechEngineEvent.
 *
 * See: speech_engine_get_engine_state()
 */

long
speech_engine_event_get_new_engine_state(SpeechEngineEvent *event)
{
    printf("speech_engine_event_get_new_engine_state called.\n");
}


/**
 * speech_engine_event_get_old_engine_state:
 * @event: the #SpeechEngineEvent that this operation will be applied to.
 *
 * Return the state prior to this #SpeechEngineEvent.
 * The value matches the speech_engine_get_engine_state() function.
 *
 * Returns: the state prior to this #SpeechEngineEvent.
 *
 * See: speech_engine_get_engine_state()
 */

long
speech_engine_event_get_old_engine_state(SpeechEngineEvent *event)
{
    printf("speech_engine_event_get_old_engine_state called.\n");
}
