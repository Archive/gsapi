
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechEngineProperties object defines the set of run-time
 * properties of a #SpeechEngine. This object is extended for
 * each type of speech engine. #SpeechSynthesisSynthesizerProperties and
 * #SpeechRecognitionRecognizerProperties define the additional run-time
 * properties of synthesizers and recognizers respectively.
 * The #SpeechEngineProperties object for a #SpeechEngine is obtained from 
 * the speech_engine_get_engine_properties() function of the engine, and 
 * should be cast appropriately for the type of engine.
 *
 * Each property of an engine has a set and get function.
 *
 * The run-time properties of an engine affect the behavior of a
 * running engine. Technically, properties affect engines in the
 * #SPEECH_ENGINE_ALLOCATED state. Normally, property changes are made
 * on a #SPEECH_ENGINE_ALLOCATED engine and take effect immediately or
 * soon after the change call.
 *
 * The #SpeechEngineProperties object for an engine is, however,
 * available in all states of a #SpeechEngine. Changes made to the
 * properties of engine in the #SPEECH_ENGINE_DEALLOCATED or the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state take effect when the engine next
 * enters the #SPEECH_ENGINE_ALLOCATED state. A typical scenario for setting
 * the properties of a non-allocated is determining the initial state of the
 * engine. For example, setting the initial voice of a
 * #SpeechSynthesisSynthesizer, or the initial 
 * #SpeechRecognitionSpeakerProfile of a #SpeechRecognitionRecognizer.
 * (Setting these properties prior to allocation is desirable because 
 * allocating the engine and then changing the voice or the speaker can be 
 * computationally expensive).
 *
 * When setting any engine property:
 *
 * <itemizedlist>
 *   <listitem>The engine may choose to ignore a set value either because it 
 *       does not support changes in a property or because it is 
 *       out-of-range.</listitem>
 *   <listitem>The engine will apply the property change as soon as possible,
 *       but the change is not necessarily immediate.  Thus, all set functions 
 *       are asynchronous (call may return before the effect takes place).
 *       </listitem>
 *   <listitem>All properties of an engine are bound properties - In other 
 *       words, a property for which an event is issued when the property 
 *       changes. A #SpeechPropertyChangeListener can be attached to the
 *	 #SpeechEngineProperties object to receive a property change
 *	 event when a change takes effect.</listitem>
 * </itemizedlist>
 *
 * For example, a call to the 
 * speech_synthesis_synthesizer_properties_set_pitch() function of the
 * #SpeechSynthesisSynthesizerProperties object to change pitch from 120Hz
 * to 200Hz might fail because the value is out-of-range. If it does succeed,
 * the pitch change might be deferred until the synthesizer can make the
 * change by waiting to the end of the current word, sentence, paragraph or
 * text object. When the change does take effect, a
 * #SpeechPropertyChangeEvent is issued to all attached listeners
 * with the name of the changed property ("Pitch"), the old value (120) and
 * the new value (200).
 *
 * Set calls take effect in the order in which they are received.
 * If multiple changed are requested are requested for a single property,
 * a separate event is issued for each call, even if the multiple changes
 * take place simultaneously.
 *
 * The properties of an engine are persistent across sessions where possible.
 * It is the engine's responsibility to store and restore the property
 * settings. In multi-user and client-server environments the store/restore
 * policy is at the discretion of the engine.
 *
 * <title>Control Component</title>
 *
 * An engine may provide a control object through the
 * speech_engine_properties_get_control_component() function of its
 * #SpeechEngineProperties object. The control object is a GTK+
 * widget. If provided, that widget can be displayed to a user for
 * customization of the engine. Because the component is widget by the
 * engine, the display may support customization of engine-specific
 * properties that are not accessible through the standard properties
 * objects.
 *
 * <title>Properties</title>
 *
 * The set/get property patterns are followed for engine properties.
 * A property is defined by its name and its property type (for example,
 * "Pitch" is a float).	 The property is accessed through get and set
 * functions.  The signature of the property accessor functions are:
 *
 *	 void set_&lt;property_name&gt;(&lt;property_type&gt; value);
 *	 &lt;property_type&gt; get_&lt;property_name&gt;();
 *
 * For boolean-valued properties, the get_&lt;property_name&gt;()
 * may also be is_&lt;property_name&gt;()
 *
 * For indexed properties (arrays) the signature of the
 * property accessor functions are:
 *
 *	 void set_&lt;property_name&gt;(&lt;property_type&gt[]; value);
 *	 void set_&lt;property_name&gt;(int i, &lt;property_type&gt; value);
 *	 &lt;property_type&gt;[] get_&lt;property_name&gt;();
 *	 &lt;property_type&gt; get_&lt;property_name&gt;(int i);
 *
 * For example speaking rate (for a #SpeechSynthesisSynthesizer) is a 
 * floating value and has the following functions:
 *
 *	 void speech_synthesis_synthesizer_properties_set_speaking_rate(float value);
 *	 float speech_synthesis_synthesizer_properties_get_speaking_rate();
 *
 * See: #SpeechEngine
 * See: speech_engine_get_engine_properties()
 * See: #SpeechRecognitionRecognizerProperties
 * See: #SpeechSynthesisSynthesizerProperties
 * See: #SpeechPropertyChangeListener
 * See: #SpeechPropertyChangeEvent
 */

#include <speech/speech_engine_properties.h>


/**
 * speech_engine_properties_get_type:
 *
 * Returns: the type ID for #SpeechEngineProperties.
 */

GType
speech_engine_properties_get_type(void)
{
    printf("speech_engine_properties_get_type called.\n");
}


SpeechEngineProperties *
speech_engine_properties_new(void)
{
    printf("speech_engine_properties_new called.\n");
}


/**
 * speech_engine_properties_add_property_change_listener:
 * @properties: the #SpeechEngineProperties object that this operation will 
 *              be applied to.
 * @listener: the #SpeechPropertyChangeListener to be added.
 *
 * Add a #SpeechPropertyChangeListener to the listener list.
 * The listener is registered for all properties of the engine.
 *
 * A #SpeechPropertyChangeEvent is fired in response to setting any bound 
 * property.
 */

void
speech_engine_properties_add_property_change_listener(
                                             SpeechEngineProperties *properties,
                                             SpeechEventListener *listener)
{
    printf("speech_engine_properties_add_property_change_listener called.\n");
}


/**
 * speech_engine_properties_get_control_component:
 * @properties: the #SpeechEngineProperties object that this operation will 
 *              be applied to.
 *
 * Obtain the graphics widget that provides the default user interface
 * for setting the properties of this #SpeechEngine.
 * If this #SpeechEngine has no default control panel, the return is %NULL.
 *
 * Returns: a component that provides the default user interface
 * for setting the properties of this #SpeechEngine.
 */

SpeechControlComponent *
speech_engine_properties_get_control_component(
                                           SpeechEngineProperties *properties)
{
    printf("speech_engine_properties_get_control_component called.\n");
}


/**
 * speech_engine_properties_remove_property_change_listener:
 * @properties: the #SpeechEngineProperties object that this operation will 
 *              be applied to.
 * @listener: the #SpeechPropertyChangeListener to be removed.
 *
 * Remove a #SpeechPropertyChangeListener from the listener list.
 */

void
speech_engine_properties_remove_property_change_listener(
                                             SpeechEngineProperties *properties,
                                             SpeechEventListener *listener)
{
    printf("speech_engine_properties_remove_property_change_listener called.\n");
}


/**
 * speech_engine_properties_reset:
 * @properties: the #SpeechEngineProperties object that this operation will 
 *              be applied to.
 *
 * The reset function returns all properties to reasonable defaults
 * for the #SpeechEngine. A property change event is issued for each 
 * engine property that changes as the reset takes effect.
 */

void
speech_engine_properties_reset(SpeechEngineProperties *properties)
{
    printf("speech_engine_properties_reset called.\n");
}
