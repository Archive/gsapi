
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* A PropertyVetoException is thrown when a proposed change to a property 
 * represents an unacceptable value. 
 */

#ifndef __SPEECH_PROPERTY_VETO_EXCEPTION_H__
#define __SPEECH_PROPERTY_VETO_EXCEPTION_H__

#include <speech/speech_types.h>
#include <speech/speech_exception.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_PROPERTY_VETO_EXCEPTION              (speech_property_veto_exception_get_type())
#define SPEECH_PROPERTY_VETO_EXCEPTION(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_PROPERTY_VETO_EXCEPTION, SpeechPropertyVetoException))
#define SPEECH_PROPERTY_VETO_EXCEPTION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_PROPERTY_VETO_EXCEPTION, SpeechPropertyVetoExceptionClass))
#define SPEECH_IS_PROPERTY_VETO_EXCEPTION(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_PROPERTY_VETO_EXCEPTION))
#define SPEECH_IS_PROPERTY_VETO_EXCEPTION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_PROPERTY_VETO_EXCEPTION))
#define SPEECH_PROPERTY_VETO_EXCEPTION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_PROPERTY_VETO_EXCEPTION, SpeechPropertyVetoExceptionClass))

typedef struct _SpeechPropertyVetoException            SpeechPropertyVetoException;
typedef struct _SpeechPropertyVetoExceptionClass       SpeechPropertyVetoExceptionClass;

struct _SpeechPropertyVetoException {
    SpeechException exception;
};

struct _SpeechPropertyVetoExceptionClass {
    SpeechExceptionClass parent_class;
};


GType 
speech_property_veto_exception_get_type(void);

SpeechPropertyVetoException * 
speech_property_veto_exception_new(void);

SpeechPropertyVetoException * 
speech_property_veto_exception_new_with_values(int min_value,
                                               int max_value);

int 
speech_property_veto_exception_get_min_value(
                                      SpeechPropertyVetoException *exception);

int 
speech_property_veto_exception_get_max_value(
                                      SpeechPropertyVetoException *exception);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_PROPERTY_VETO_EXCEPTION_H__ */
