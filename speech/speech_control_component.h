
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Control component object. */

#ifndef __SPEECH_CONTROL_COMPONENT_H__
#define __SPEECH_CONTROL_COMPONENT_H__

#include <gtk/gtkwidget.h>
#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_CONTROL_COMPONENT              (speech_control_component_get_type())
#define SPEECH_CONTROL_COMPONENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_CONTROL_COMPONENT, SpeechControlComponent))
#define SPEECH_CONTROL_COMPONENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_CONTROL_COMPONENT, SpeechControlComponentClass))
#define SPEECH_IS_CONTROL_COMPONENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_CONTROL_COMPONENT))
#define SPEECH_IS_CONTROL_COMPONENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_CONTROL_COMPONENT))
#define SPEECH_CONTROL_COMPONENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_CONTROl_COMPONENT, SpeechControlComponentClass))

typedef struct _SpeechControlComponent            SpeechControlComponent;
typedef struct _SpeechControlComponentClass       SpeechControlComponentClass;

struct _SpeechControlComponent {
    GtkWidget widget;
};

struct _SpeechControlComponentClass {
    GtkWidgetClass parent_class;
};


GType 
speech_control_component_get_type(void);

SpeechControlComponent * 
speech_control_component_get_new(void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_CONTROL_COMPONENT_H__ */
