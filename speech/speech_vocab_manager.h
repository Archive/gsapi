
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* For management of words used by a speech Engine. */

#ifndef __SPEECH_VOCAB_MANAGER_H__
#define __SPEECH_VOCAB_MANAGER_H__

#include <speech/speech_types.h>
#include <speech/speech_word.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_VOCAB_MANAGER              (speech_vocab_manager_get_type())
#define SPEECH_VOCAB_MANAGER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_VOCAB_MANAGER, SpeechVocabManager))
#define SPEECH_VOCAB_MANAGER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_VOCAB_MANAGER, SpeechVocabManagerClass))
#define SPEECH_IS_VOCAB_MANAGER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_VOCAB_MANAGER))
#define SPEECH_IS_VOCAB_MANAGER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_VOCAB_MANAGER))
#define SPEECH_VOCAB_MANAGER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_VOCAB_MANAGER, SpeechVocabManagerClass))

typedef struct _SpeechVocabManager            SpeechVocabManager;
typedef struct _SpeechVocabManagerClass       SpeechVocabManagerClass;

struct _SpeechVocabManager {
    SpeechObject object;
};

struct _SpeechVocabManagerClass {
    SpeechObjectClass parent_class;
};


GType 
speech_vocab_manager_get_type(void);

SpeechVocabManager * 
speech_vocab_manager_new(void);

void 
speech_vocab_manager_add_word(SpeechVocabManager *manager,
                              SpeechWord *word);

void 
speech_vocab_manager_add_words(SpeechVocabManager *manager,
                               SpeechWord *words[]);

SpeechWord ** 
speech_vocab_manager_get_words(SpeechVocabManager *manager,
                               gchar *text);

SpeechWord ** 
speech_vocab_manager_list_problem_words(SpeechVocabManager *manager);

SpeechStatus 
speech_vocab_manager_remove_word(SpeechVocabManager *manager,
                                 SpeechWord *word);

SpeechStatus 
speech_vocab_manager_remove_words(SpeechVocabManager *manager,
                                  SpeechWord *words[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_VOCAB_MANAGER_H__ */
