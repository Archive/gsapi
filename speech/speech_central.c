
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The #SpeechCentral object is the initial access point to all speech
 * input and output capabilities.
 * #SpeechCentral provides the ability to locate, select
 * and create speech recognizers and speech synthesizers
 *
 * <title>Creating a #SpeechRecognitionRecognizer or 
 * #SpeechSynthesisSynthesizer</title>
 *
 * The speech_central_create_recognizer() and 
 * speech_central_create_synthesizer()
 * functions are used to create speech engines. Both functions accept a
 * single parameter that defines the required properties for the engine
 * to be created. The parameter is a #SpeechEngineModeDesc and
 * may be one of the sub-classes: #SpeechRecognitionRecognizerModeDesc
 * or #SpeechSynthesisSynthesizerModeDesc.
 *
 * A mode descriptor defines a set of required properties for an engine.
 * For example, a #SpeechSynthesisSynthesizerModeDesc can describe a
 * #SpeechSynthesisSynthesizer for Swiss German that has a male voice.
 * Similarly, a #SpeechRecognitionRecognizerModeDesc can describe a
 * #SpeechRecognitionRecognizer that supports dictation for Japanese.
 *
 * An application is responsible for determining its own functional
 * requirements for speech input/output and providing an appropriate
 * mode descriptor. There are three cases for mode descriptors:
 *
 * <orderedlist>
 *   <listitem>%NULL.</listitem>
 *   <listitem>Created by the application.</listitem>
 *   <listitem>Obtained from the speech_central_available_recognizers() or
 *	 speech_central_available_synthesizers() functions of
 *       #SpeechCentral.</listitem>
 * </orderedlist>
 *
 * The mode descriptor is passed to the speech_central_create_recognizer() or
 * speech_central_create_synthesizer() functions of #SpeechCentral to
 * create a #SpeechRecognitionRecognizer or #SpeechSynthesisSynthesizer. The
 * created engine matches all the engine properties in the mode
 * descriptor passed to the create function. If no suitable speech engine
 * is available, the create functions return %NULL.
 *
 * The create engine functions operate differently for the three cases.
 * That is, engine selection depends upon the type of the mode descriptor:
 *
 * <orderedlist>
 *   <listitem>%NULL mode descriptor: the #SpeechCentral
 *	 class selects a suitable engine for the default locale.</listitem>
 *   <listitem>Application-created mode descriptor: the #SpeechCentral class
 *	 attempts to locate an engine with all application-specified
 *	 properties.</listitem>
 *   <listitem>Mode descriptor from speech_central_available_recognizers() or
 *       speech_central_available_synthesizers(): descriptors returned by these
 *	 two functions identify a specific engine with a specific operating
 *	 mode. #SpeechCentral creates an instance of that engine.
 *	 (Note: these mode descriptors are distinguished because they
 *	 implement the #SpeechEngineCreate object.)</listitem>
 * </orderedlist>
 *
 *
 * <title>Engine Selection Procedure: Cases 1 &amp; 2</title>
 *
 * For cases 1 and 2 there is a defined procedure for selecting an engine to
 * be created.	(For case 3, the application can apply it's own selection
 * procedure).
 *
 * Locale is treated specially in the selection to ensure that language is
 * always considered when selecting an engine. If a locale is not provided,
 * the default locale is used.
 *
 * The selection procedure is:
 *
 * <orderedlist>
 *   <listitem>If the locale is undefined add the language of the default 
 *       locale to the required properties.</listitem>
 *   <listitem>If a #SpeechRecognitionRecognizer or 
 *       #SpeechSynthesisSynthesizer has been created already and it 
 *       has the required properties, return a reference to it. (The 
 *       last created engine is checked).</listitem>
 *   <listitem>Obtain a list of all recognizer or synthesizer modes that 
 *       match the required properties.</listitem>
 *   <listitem>Amongst the matching engines, give preference to:
 *       <itemizedlist>
 *           <listitem>A running engine (speech_engine_mode_desc_get_running()
 *               is %TRUE),</listitem>
 *           <listitem>An engine that matches the default locale's 
 *               country.</listitem>
 *       </itemizedlist>
 *   </listitem>
 * </orderedlist>
 *
 * When more than one engine is a legal match in the final step, the engines
 * are ordered as returned by the speech_central_available_recognizers() or
 * speech_central_available_synthesizers() function.
 *
 * <title>Security</title>
 *
 * A number of functions throughout the API return #SPEECH_SECURITY_EXCEPTION.
 * Individual implementations of #SpeechRecognitionRecognizer and
 * #SpeechSynthesisSynthesizer may return #SPEECH_SECURITY_EXCEPTION on
 * additional functions as required to protect a client from malicious
 * applications.
 *
 * <title>Engine Registration</title>
 *
 * The #SpeechCentral class locates, selects and creates speech engines 
 * from amongst a list of registered engines. Thus, for an engine to be 
 * used by Desktop applications, the engine must register itself with
 * #SpeechCentral.
 *
 * There are two registration mechanisms:
 *
 * <orderedlist>
 *   <listitem>add a #SpeechEngineCentral object to a speech properties 
 *       file.</listitem>
 *   <listitem>temporarily register an engine by calling the
 *	 speech_central_register_engine_central() function.</listitem>
 * </orderedlist>
 *
 * The speech properties files provide <emphasis>persistent</emphasis> 
 * registration of speech engines. When #SpeechCentral is first called, 
 * it looks for properties in two files:
 *
 *   ~/.gconf/desktop/gnome/speech/%gconf.xml
 *    .../etc/gconf/gconf.xml.defaults/schemas/desktop/gnome/speech/%gconf.xml
 *
 * Engines identified in either properties file are made available through
 * the functions of #SpeechCentral.
 *
 * #SpeechCentral looks for the "SupportedRecognizers" gconf resource,
 * which is a comma separated list of the Recognizers that are supported.
 *
 * When it is first called, the #SpeechCentral object will attempt to
 * create an instance of each #SpeechEngineCentral object.
 *
 * <title>Note</title> to engine providers:
 * #SpeechCentral calls each #SpeechEngineCentral for each call
 * to speech_central_available_recognizers() or 
 * speech_central_available_synthesizers() and sometimes 
 * speech_central_create_recognizer() and speech_central_create_synthesizer().
 * The results are not stored. The speech_engine_central_create_engine_list()
 * function should be reasonably efficient.
 */

#include <speech/speech_central.h>


/**
 * speech_central_get_type:
 *
 * Returns: the type ID for #SpeechCentral.
 */

GType
speech_central_get_type(void)
{
    printf("speech_central_get_type called.\n");
}


SpeechCentral *
speech_central_new(void)
{
    printf("speech_central_new called.\n");
}


/**
 * speech_central_available_recognizers:
 * @central: the #SpeechCentral objection this operation will be applied to.
 * @require: a #SpeechEngineModeDesc or #SpeechRecognitionRecognizerModeDesc
 *           defining the required features of the mode descriptors in the
 *           returned list.
 * @list: list of mode descriptors with the required properties.
 *
 * List #SpeechEngineModeDesc objects for available recognition engine modes 
 * that match the required properties. If the @require parameter is %NULL, 
 * then all known recognizers are listed.
 *
 * Returns a zero-length list if no engines are available or if no
 * engines have the required properties. (The function never returns a 
 * %NULL list).
 *
 * The order of the #SpeechEngineModeDesc objects in the list
 * is partially defined. For each registered engine (technically,
 * each registered #SpeechEngineCentral object) the order of the
 * descriptors is preserved.  Thus, each installed speech engine
 * should order its descriptor objects with the most useful modes
 * first, for example, a mode that is already loaded and running
 * on a desktop.
 *
 * Returns: the operation status:
 * <itemizedlist>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the caller does not have 
 *       permission to use speech recognition.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_central_available_recognizers(SpeechCentral *central,
                                     SpeechEngineModeDesc *require,
                                     SpeechEngineList *list)
{
    printf("speech_central_available_recognizers called.\n");
}


/**
 * speech_central_available_synthesizers:
 * @central: the #SpeechCentral objection this operation will be applied to.
 * @require: a #SpeechEngineModeDesc or #SpeechSynthesisSynthesizerModeDesc
 *           defining the required features of the mode descriptors in the
 *	     returned list.
 * @list: list of mode descriptors with the required properties.
 *
 * List #SpeechEngineModeDesc objects for available synthesis engine modes 
 * that match the required properties. If the @require parameter is %NULL,
 * then all available known synthesizers are listed.
 *
 * Returns an empty list (rather than %NULL) if no engines are
 * available or if no engines have the required properties.
 *
 * The order of the #SpeechEngineModeDesc objects in the list is
 * partially defined. For each speech installation (technically,
 * each registered #SpeechEngineCentral object) the order of the
 * descriptors is preserved. Thus, each installed speech
 * engine should order its descriptor objects with the most
 * useful modes first, for example, a mode that is already
 * loaded and running on a desktop.
 *
 * Returns: the operation status:
 * <itemizedlist>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the caller does not have 
 *       permission to use speech engines.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_central_available_synthesizers(SpeechCentral *central,
                                      SpeechEngineModeDesc *require,
                                      SpeechEngineList *list)
{
    printf("speech_central_available_synthesizers called.\n");
}


/**
 * speech_central_create_recognizer:
 * @central: the #SpeechCentral objection this operation will be applied to.
 * @require: required engine properties or %NULL for default engine selection.
 * @recognizer: a recognizer matching the required properties or %NULL if 
 *              none is available
 *
 * Create a #SpeechRecognitionRecognizer with specified required properties.
 * If there is no #SpeechRecognitionRecognizer with the required properties
 * the function returns %NULL.
 *
 * The required properties defined in the input parameter may be
 * provided as either a #SpeechEngineModeDesc object or a
 * #SpeechRecognitionRecognizerModeDesc object. The input parameter may
 * also be %NULL, in which case an engine is selected that supports the 
 * language of the default locale.
 *
 * A non-null mode descriptor may be either application-created or a mode 
 * descriptor returned by the speech_central_available_recognizers() function.
 *
 * The mechanisms for creating a #SpeechRecognitionRecognizer are described 
 * above in detail.
 *
 * See: speech_central_available_recognizers()
 * See: #SpeechRecognitionRecognizerModeDesc
 *
 * Returns: the operation status:
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the properties of the
 *             #SpeechEngineModeDesc do not refer to a known engine or 
 *             engine mode.</listitem>
 *   <listitem>#SPEECH_ENGINE_EXCEPTION if the engine defined by this
 *             #SpeechRecognizerModeDesc could not be properly created.
 *             </listitem>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the caller does not have
 *             speech_central_create_recognizer() permission.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_central_create_recognizer(SpeechCentral *central,
                                 SpeechEngineModeDesc *require,
                                 SpeechRecognitionRecognizer *recognizer)
{
    printf("speech_central_create_recognizer called.\n");
}


/**
 * speech_central_create_synthesizer:
 * @central: the #SpeechCentral objection this operation will be applied to.
 * @require: required engine properties or %NULL for default engine selection.
 * @synthesizer: a #SpeechSynthesisSynthesizer matching the required properties
 *               or %NULL if none is available.
 *
 * Create a #SpeechSynthesisSynthesizer with specified required properties.
 * If there is no #SpeechSynthesisSynthesizer with the required properties 
 * the function returns %NULL.
 *
 * The required properties defined in the input parameter may be
 * provided as either a #SpeechEngineModeDesc object or a
 * #SpeechSynthesisSynthesizerModeDesc object. The input parameter
 * may also be %NULL, in which case an engine is selected that supports 
 * the language of the default locale.
 *
 * A non-null mode descriptor may be either application-created or a mode 
 * descriptor returned by the speech_central_available_synthesizers() function.
 *
 * The mechanisms for creating a #SpeechSynthesisSynthesizer are described 
 * above in detail.
 *
 * See: speech_central_available_synthesizers()
 * See: SpeechSynthesisSynthesizerModeDesc
 *
 * Returns: the operation status:
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the properties of the
 *             #SpeechEngineModeDesc do not refer to a known engine or 
 *             engine mode.</listitem>
 *   <listitem>#SPEECH_ENGINE_EXCEPTION if the engine defined by this
 *             #SpeechSynthesisSynthesizerModeDesc could not be properly
 *             created.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_central_create_synthesizer(SpeechCentral *central,
                                  SpeechEngineModeDesc *require,
                                  SpeechSynthesisSynthesizer *synthesizer)
{
    printf("speech_central_create_synthesizer called.\n");
}


/**
 * speech_central_register_engine_central:
 * @central: the #SpeechCentral objection this operation will be applied to.
 * @class_name: name of a class that implements the #SpeechEngineCentral 
 *              object and provides access to an engine implementation.
 *
 * Register a speech engine with the #SpeechCentral class for use by the 
 * current application. This call adds the specified class name to the list 
 * of #SpeechEngineCentral objects. The registered engine is not stored 
 * persistently in the properties files. If @class_name is already registered,
 * the call has no effect.
 *
 * The object identified by @class_name must have an empty constructor.
 *
 * Returns: the operation status:
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_EXCEPTION if @class_name is not a
 *		legal object or it does not implement the
 *		#SpeechEngineCentral object</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_central_register_engine_central(SpeechCentral *central,
                                       gchar *class_name)
{
    printf("speech_central_register_engine_central called.\n");
}
