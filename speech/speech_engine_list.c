
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechEngineList is a container for a set of #SpeechEngineModeDesc objects.
 * A #SpeechEngineList is used in the selection of speech engines in
 * conjuction with the functions of the #SpeechCentral class.
 * It provides convenience functions for the purpose of testing and manipulating
 * the #SpeechEngineModeDesc objects it contains.
 *
 * A #SpeechEngineList object is typically obtained through the
 * speech_central_available_synthesizers() or 
 * speech_central_available_recognizers() functions of the #SpeechCentral 
 * object.
 * The speech_engine_list_order_by_match(), speech_engine_list_any_match(),
 * speech_engine_list_require_match() and speech_engine_list_reject_match()
 * functions are used to prune the list to find the best match given 
 * multiple criteria.
 *
 * See: #SpeechEngineModeDesc
 * See: #SpeechCentral
 * See: speech_central_available_recognizers()
 * See: speech_central_available_synthesizers()
 */

#include <speech/speech_engine_list.h>


/**
 * speech_engine_list_get_type:
 *
 * Returns: the type ID for #SpeechEngineList.
 */

GType
speech_engine_list_get_type(void)
{
    printf("speech_engine_list_get_type called.\n");
}


SpeechEngineList *
speech_engine_list_new(void)
{
    printf("speech_engine_list_new called.\n");
}


/**
 * speech_engine_list_get_engine_list:
 *
 * Return an array of #SpeechEngineModeDesc objects in this #SpeechEngineList.
 *
 * Returns: an array of #SpeechEngineModeDesc objects.
 */

SpeechEngineList **
speech_engine_list_get_engine_list(SpeechEngineList *engine_list)
{
    printf("speech_engine_list_get_engine_list called.\n");
}


/**
 * speech_engine_list_any_match:
 * @engine_list: the #SpeechEngineList object that this operation will be 
 *               applied to.
 * @require: the required #SpeechEngineModeDesc object to compare against.
 *
 * Return true if one or more #SpeechEngineModeDesc in the
 * #SpeechEngineList match the required properties. The
 * @require object is tested with the speech_engine_mode_desc_match()
 * function of each #SpeechEngineModeDesc in the list.
 * If any match call returns true then this function returns %TRUE.
 *
 * speech_engine_list_any_match() is often used to test whether pruning a
 * list (with speech_engine_list_require_match() or 
 * speech_engine_list_reject_match()) would leave the list empty.
 *
 * Returns: a boolean indication of whether this #SpeechEngineList
 *	   contains one (or more) #SpeechEngineModeDesc objects
 *	   matching @require.
 *
 * See: speech_engine_mode_desc_match()
 */

gboolean
speech_engine_list_any_match(SpeechEngineList *engine_list,
                             SpeechEngineModeDesc *require)
{
    printf("speech_engine_list_any_match called.\n");
}


/**
 * speech_engine_list_order_by_match:
 * @engine_list: the #SpeechEngineList object that this operation will be 
 *               applied to.
 * @require: the #SpeechEngineModeDesc object containing the required features.
 *
 * Order the list so that elements matching the required features are
 * at the head of the list, and others are at the end. Within
 * categories, the original order of the list is preserved.
 */

void
speech_engine_list_order_by_match(SpeechEngineList *engine_list,
                                  SpeechEngineModeDesc *require)
{
    printf("speech_engine_list_order_by_match called.\n");
}


/**
 * speech_engine_list_reject_match:
 * @engine_list: the #SpeechEngineList object that this operation will be 
 *               applied to.
 * @reject: the #SpeechEngineModeDesc object containing the features to remove.
 *
 * Remove #SpeechEngineModeDesc entries from the list that do match 
 * @reject. The speech_engine_mode_desc_match() function for each
 * #SpeechEngineModeDesc in the list is called: if it returns
 * true it is removed from the list.
 *
 * See: speech_engine_list_require_match()
 */

void
speech_engine_list_reject_match(SpeechEngineList *engine_list,
                                SpeechEngineModeDesc *reject)
{
    printf("speech_engine_list_reject_match called.\n");
}


/**
 * speech_engine_list_require_match:
 * @engine_list: the #SpeechEngineList object that this operation will be 
 *               applied to.
 * @require: the #SpeechEngineModeDesc object containing the required features.
 *
 * Remove #SpeechEngineModeDesc entries from the list that do
 * not match @require. The speech_engine_mode_desc_match() function for
 * each #SpeechEngineModeDesc in the list is called: if it returns false 
 * it is removed from the list.
 *
 * See: speech_engine_list_reject_match()
 * See: speech_engine_mode_desc_match()
 */

void
speech_engine_list_require_match(SpeechEngineList *engine_list,
                                 SpeechEngineModeDesc *require)
{
    printf("speech_engine_list_require_match called.\n");
}
