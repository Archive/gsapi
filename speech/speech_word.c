
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The #SpeechWord object provides a standard representation of
 * speakable words for speech engines. A #SpeechWord object
 * provides the following information:
 *
 * <itemizedlist>
 *   <listitem>"Written form" string: text that can be used to present the
 *	 #SpeechWord visually.</listitem>
 *   <listitem>"Spoken form" text: printable string that indicates how the 
 *       word is spoken.</listitem>
 *   <listitem>Pronunciation: a string of phonetic characters indicating 
 *       how the word is spoken.</listitem>
 *   <listitem>Grammatical categories: flags indicating grammatical 
 *       "part-of-speech" information.</listitem>
 * </itemizedlist>
 *
 * The written form string is required. The other properties are optional.
 * Typically, one or more of the optional properties are specified. The
 * #SpeechWord object allows the specification of multiple pronunciations 
 * and multiple grammatical categories. Each pronunciation must be 
 * appropriate to each category. If not, separate #SpeechWord objects 
 * should be created.
 *
 * All the optional properties of a word are hints to the speech engine.
 * Speech engines will use the information as appropriate for their
 * internal design.
 *
 * See: #SpeechVocabManager
 * See: speech_engine_get_vocab_manager()
 */

#include <speech/speech_word.h>


/**
 * speech_word_get_type:
 *
 * Returns: the type ID for #SpeechWord.
 */

GType
speech_word_get_type(void)
{
    printf("speech_word_get_type called.\n");
}


/**
 * speech_word_new:
 * @written_form: the "written form" of the #SpeechWord
 * @spoken_form: the "spoken form" of the #SpeechWord
 * @pron: the pronunciation of the #SpeechWord
 * @cat: the categories of the #SpeechWord
 *
 * Initialise, setting the pronunciations, spoken form, written form, and 
 * categories.
 *
 * Returns:
 */

SpeechWord *
speech_word_new(gchar *written_form,
                gchar *spoken_form,
                gchar *pron[],
                long cat)
{
    printf("speech_word_new called.\n");
}


/**
 * speech_word_get_categories:
 * @word: the #SpeechWord object that this operation will be applied to.
 *
 * Get the categories of the #SpeechWord.
 * Value may be #SPEECH_WORD_UNKNOWN or an OR'ed set of the categories 
 * defined by this object.
 *
 * Returns: the categories of the #SpeechWord.
 */

long
speech_word_get_categories(SpeechWord *word)
{
    printf("speech_word_get_categories called.\n");
}


/**
 * speech_word_get_pronunciations:
 * @word: the #SpeechWord object that this operation will be applied to.
 *
 * Get the pronunciations of the #SpeechWord.
 * The pronunciation string uses the UTF8 IPA subset.
 * Returns %NULL if no pronunciations are specified.
 *
 * Returns: the pronunciations of the #SpeechWord.
 */

gchar **
speech_word_get_pronunciations(SpeechWord *word)
{
    printf("speech_word_get_pronunciations called.\n");
}


/**
 * speech_word_get_spoken_form:
 * @word: the #SpeechWord object that this operation will be applied to.
 *
 * Get the "spoken form" of the #SpeechWord.
 * Returns %NULL if the spoken form is not defined.
 *
 * Returns: the "spoken form" of the #SpeechWord
 */

gchar *
speech_word_get_spoken_form(SpeechWord *word)
{
    printf("speech_word_get_spoken_form called.\n");
}


/**
 * speech_word_get_written_form:
 * @word: the #SpeechWord object that this operation will be applied to.
 *
 * Get the written form of the #SpeechWord
 *
 * Returns: the written form of the #SpeechWord
 */

gchar *
speech_word_get_written_form(SpeechWord *word)
{
    printf("speech_word_get_written_form called.\n");
}


/**
 * speech_word_set_categories:
 * @word: the #SpeechWord object that this operation will be applied to.
 * @cat: the categories of the #SpeechWord.
 *
 * Set the categories of the #SpeechWord
 * The categories may be #SPEECH_WORD_UNKNOWN or may be an OR'ed
 * set of the defined categories such as #SPEECH_WORD_NOUN,
 * #SPEECH_WORD_VERB, #SPEECH_WORD_PREPOSITION.
 *
 * The category information is a guide to the word's grammatical role.
 * Speech synthesizers can use this information to improve phrasing
 * and accenting.
 */

void
speech_word_set_categories(SpeechWord *word,
                           long cat)
{
    printf("speech_word_set_categories called.\n");
}


/**
 * speech_word_set_pronunciations:
 * @word: the #SpeechWord object that this operation will be applied to.
 * @pron: the pronunciation of the #SpeechWord.
 *
 * Set the pronunciation of the #SpeechWord as an array containing a 
 * phonetic character string for each pronunciation of the word.
 *
 * The pronunciation string uses the IPA subset of UTF8.
 *
 * The string should be null if no pronunciation is available.
 * Speech engines should be expected to handle most words of the
 * language they support.
 *
 * Recognizers can use pronunciation information to improve
 * recognition accuracy. Synthesizers use the information to
 * accurately speak unusual words (e.g., foreign words).
 */

void
speech_word_set_pronunciations(SpeechWord *word,
                               gchar **pron)
{
    printf("speech_word_set_pronunciations called.\n");
}


/**
 * speech_word_set_spoken_form:
 * @word: the #SpeechWord object that this operation will be applied to.
 * @text: the "spoken form" of the #SpeechWord.
 *
 * Set the "spoken form" of the #SpeechWord. May be %NULL.
 *
 * The spoken form of a word is useful for mapping the written form
 * to words that are likely to be handled by a speech recognizer or
 * synthesizer. For example, "JavaSoft" to "java soft",
 * "toString" -> "to string", "IEEE" -> "I triple E".
 */

void
speech_word_set_spoken_form(SpeechWord *word,
                            gchar *text)
{
    printf("speech_word_set_spoken_form called.\n");
}


/**
 * speech_word_set_written_form:
 * @word: the #SpeechWord object that this operation will be applied to.
 * @text: the "written form" of the #SpeechWord
 *
 * Set the "written form" of the #SpeechWord.
 * The written form text should be a string that could be
 * used to present the #SpeechWord visually.
 */

void
speech_word_set_written_form(SpeechWord *word,
                             gchar *text)
{
    printf("speech_word_set_written_form called.\n");
}
