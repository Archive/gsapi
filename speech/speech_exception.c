
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The potential detail message associated with an exception.
 *
 * See: #SpeechRecognitionGrammarException
 * See: #SpeechPropertyVetoException
 */

#include <speech/speech_exception.h>


/**
 * speech_exception_get_type:
 *
 * Returns: the type ID for #SpeechException.
 */

GType
speech_exception_get_type(void)
{
    printf("speech_exception_get_type called.\n");
}


SpeechException *
speech_exception_new(void)
{
    printf("speech_exception_new called.\n");
}


/**
 * speech_exception_new_with_message:
 * @message: the detail message
 *
 * Initialises an exception with the specified detail message.
 * A detail message is a string that describes this particular
 * exception.
 *
 * Returns:
 */

SpeechException *
speech_exception_new_with_message(gchar *message)
{
    printf("speech_exception_new_with_message called.\n");
}


/**
 * speech_exception_get_message:
 * @exception: the #SpeechException object that this operation will be applied
 *             to.
 *
 * Returns the detail message associated with this exception value.
 *
 * Returns: the detail message for this exception value.
 */

gchar *
speech_exception_get_message(SpeechException *exception)
{
    printf("speech_exception_get_message called.\n");
}
