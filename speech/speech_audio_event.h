
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Audio input/output event object for an Engine. */

#ifndef __SPEECH_AUDIO_EVENT_H__
#define __SPEECH_AUDIO_EVENT_H__

#include <speech/speech_types.h>
#include <speech/speech_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_AUDIO_EVENT              (speech_audio_event_get_type())
#define SPEECH_AUDIO_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_AUDIO_EVENT, SpeechAudioEvent))
#define SPEECH_AUDIO_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_AUDIO_EVENT, SpeechAudioEventClass))
#define SPEECH_IS_AUDIO_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_AUDIO_EVENT))
#define SPEECH_IS_AUDIO_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_AUDIO_EVENT))
#define SPEECH_AUDIO_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_AUDIO_EVENT, SpeechAudioEventClass))

typedef struct _SpeechAudioEvent            SpeechAudioEvent;
typedef struct _SpeechAudioEventClass       SpeechAudioEventClass;

struct _SpeechAudioEvent {
    SpeechEvent event;
};

struct _SpeechAudioEventClass {
    SpeechEventClass parent_class;
};


GType 
speech_audio_event_get_type(void);

SpeechAudioEvent * 
speech_audio_event_new(SpeechEngine *source,
                       int id);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_AUDIO_EVENT_H__ */
