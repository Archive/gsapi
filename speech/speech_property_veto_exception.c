
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The minimum and maximum range values associated with a property veto
 * exception.
 */

#include <speech/speech_property_veto_exception.h>


/**
 * speech_property_veto_exception_get_type:
 *
 * Returns: the type ID for #SpeechPropertyVetoException.
 */

GType
speech_property_veto_exception_get_type(void)
{
    printf("speech_property_veto_exception_get_type called.\n");
}


SpeechPropertyVetoException *
speech_property_veto_exception_new(void)
{
    printf("speech_property_veto_exception_new called.\n");
}


/**
 * speech_property_veto_exception_new_with_values:
 * @min_value: the minimum value of the range
 * @max_value: the maximum value of the range
 *
 * Initialises a range exception with the minimum and maximum allowed range 
 * values.
 * A detail message is a string that describes this particular exception.
 *
 * Returns:
 */

SpeechPropertyVetoException *
speech_property_veto_exception_new_with_values(int min_value, int max_value)
{
    printf("speech_property_veto_exception_new_with_values called.\n");
}


/**
 * speech_property_veto_exception_get_min_value:
 * @ex: the #SpeechPropertyVetoException object that this operation will be
 *     applied to.
 *
 * Returns the minimum allowed value for this range.
 *
 * Returns: the minimum allowed value for this range.
 */

int
speech_property_veto_exception_get_min_value(SpeechPropertyVetoException *ex)
{
    printf("speech_property_veto_exception_get_min_value called.\n");
}


/**
 * speech_property_veto_exception_get_max_value:
 * @ex: the #SpeechPropertyVetoException object that this operation will be
 *     applied to.
 *
 * Returns the maximum allowed value for this range.
 *
 * Returns: the maximum allowed value for this range.
 */

int
speech_property_veto_exception_get_max_value(SpeechPropertyVetoException *ex)
{
    printf("speech_property_veto_exception_get_max_value called.\n");
}
