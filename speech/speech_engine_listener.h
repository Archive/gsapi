
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Defines functions to be called when state-change events for a speech 
 * engine occur.
 */

#ifndef __SPEECH_ENGINE_LISTENER_H__
#define __SPEECH_ENGINE_LISTENER_H__

#include <speech/speech_types.h>
#include <speech/speech_event_listener.h>
#include <speech/speech_engine_event.h>
#include <speech/speech_engine_error_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_LISTENER              (speech_engine_listener_get_type())
#define SPEECH_ENGINE_LISTENER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_LISTENER, SpeechEngineListener))
#define SPEECH_ENGINE_LISTENER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_LISTENER, SpeechEngineListenerClass))
#define SPEECH_IS_ENGINE_LISTENER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_LISTENER))
#define SPEECH_IS_ENGINE_LISTENER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_LISTENER))
#define SPEECH_ENGINE_LISTENER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_LISTENER, SpeechEngineListenerClass))

typedef struct _SpeechEngineListenerClass       SpeechEngineListenerClass;

struct _SpeechEngineListener {
    SpeechEventListener listener;
};

struct _SpeechEngineListenerClass {
    SpeechEventListenerClass parent_class;
};


GType 
speech_engine_listener_get_type(void);

SpeechEngineListener * 
speech_engine_listener_new(void);

void 
speech_engine_listener_engine_allocated(SpeechEngineListener *listener,
                                        SpeechEngineEvent *e);

void 
speech_engine_listener_engine_allocating_resources(
                                        SpeechEngineListener *listener,
                                        SpeechEngineEvent *e);

void 
speech_engine_listener_engine_deallocated(SpeechEngineListener *listener,
                                          SpeechEngineEvent *e);

void 
speech_engine_listener_engine_deallocating_resources(
                                        SpeechEngineListener *listener,
                                        SpeechEngineEvent *e);

void 
speech_engine_listener_engine_error(SpeechEngineListener *listener,
                                    SpeechEngineErrorEvent *e);

void 
speech_engine_listener_engine_paused(SpeechEngineListener *listener,
                                     SpeechEngineEvent *e);

void 
speech_engine_listener_engine_resumed(SpeechEngineListener *listener,
                                      SpeechEngineEvent *e);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_LISTENER_H__ */
