
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Describes events associated with audio input/output for an
 * #SpeechEngine. The event source is a #SpeechEngine object.
 *
 * Extended by the #SpeechRecognitionRecognizerAudioEvent class that provides
 * specialized events for a #SpeechRecognitionRecognizer.
 *
 * See: #SpeechEngine
 * See: #SpeechRecognitionRecognizerAudioEvent
 */

#include <speech/speech_audio_event.h>


/**
 * speech_audio_event_get_type:
 *
 * Returns: the type ID for #SpeechAudioEvent.
 */

GType
speech_audio_event_get_type(void)
{
    printf("speech_audio_event_get_type called.\n");
}


/**
 * speech_audio_event_new:
 * @source: the #SpeechEngine that produced the event.
 * @id: type of audio event.
 *
 * Initialises a SpeechAudioEvent with a specified id.
 *
 * Returns:
 */

SpeechAudioEvent *
speech_audio_event_new(SpeechEngine *source, int id)
{
    printf("speech_audio_event_new called.\n");
}
