
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Provides a list of SpeechEngineModeDesc objects that define the available
 * operating modes of a speech engine. 
 */

#ifndef __SPEECH_ENGINE_CENTRAL_H__
#define __SPEECH_ENGINE_CENTRAL_H__

#include <speech/speech_types.h>
#include <speech/speech_engine_list.h>
#include <speech/speech_engine_mode_desc.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_CENTRAL              (speech_engine_central_get_type())
#define SPEECH_ENGINE_CENTRAL(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_CENTRAL, SpeechEngineCentral))
#define SPEECH_ENGINE_CENTRAL_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_CENTRAL, SpeechEngineCentralClass))
#define SPEECH_IS_ENGINE_CENTRAL(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_CENTRAL))
#define SPEECH_IS_ENGINE_CENTRAL_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_CENTRAL))
#define SPEECH_ENGINE_CENTRAL_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_CENTRAL, SpeechEngineCentralClass))

typedef struct _SpeechEngineCentral            SpeechEngineCentral;
typedef struct _SpeechEngineCentralClass       SpeechEngineCentralClass;

struct _SpeechEngineCentral {
    SpeechObject object;
};

struct _SpeechEngineCentralClass {
    SpeechObjectClass parent_class;
};


GType 
speech_engine_central_get_type(void);

SpeechEngineCentral * 
speech_engine_central_new(void);

SpeechStatus 
speech_engine_central_create_engine_list(SpeechEngineCentral *engine_central,
                                         SpeechEngineModeDesc *require,
                                         SpeechEngineList *list);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_CENTRAL_H__ */
