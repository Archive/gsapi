
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Base speech object that all others are derived from. */

#ifndef __SPEECH_OBJECT_H__
#define __SPEECH_OBJECT_H__

#include <stdio.h>
#include <glib-object.h>
#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_OBJECT              (speech_object_get_type())
#define SPEECH_OBJECT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_OBJECT, SpeechObject))
#define SPEECH_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_OBJECT, SpeechObjectClass))
#define SPEECH_IS_OBJECT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_OBJECT))
#define SPEECH_IS_OBJECT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_OBJECT))
#define SPEECH_OBJECT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_OBJECT, SpeechObjectClass))

typedef struct _SpeechObject            SpeechObject;
typedef struct _SpeechObjectClass       SpeechObjectClass;

struct _SpeechObject {
    GObject object;
};

struct _SpeechObjectClass {
    GObjectClass parent_class;
};


GType 
speech_object_get_type(void);

SpeechObject * 
speech_object_new(void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_OBJECT_H__ */
