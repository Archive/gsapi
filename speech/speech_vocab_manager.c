
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Object for management of words used by a #SpeechEngine.
 * The #SpeechVocabManager for a #SpeechEngine is returned by
 * the speech_engine_get_vocab_manager() function of the #SpeechEngine
 * object. Engines are not required support the #SpeechVocabManager
 * - the speech_engine_get_vocab_manager() manager may return %NULL.
 *
 * Words, technically known as tokens, are provided to the vocabulary
 * manager with optional information about their pronunciation,
 * grammatical role and spoken form.
 *
 * The #SpeechVocabManager is typically used to provide a speech
 * engine with information on problematic words - usually words for which
 * the engine is unable to guess a pronunciation. For debugging purposes,
 * a #SpeechEngine may provide a list of words it finds difficult
 * through the speech_vocab_manager_list_problem_words() function.
 *
 * Words in the vocabulary manager can be used as tokens in rule grammars
 * for recognizers.
 *
 * See: speech_engine_get_vocab_manager()
 * See: #SpeechWord
 */

#include <speech/speech_vocab_manager.h>


/**
 * speech_vocab_manager_get_type:
 *
 * Returns: the type ID for #SpeechVocabManager.
 */

GType
speech_vocab_manager_get_type(void)
{
    printf("speech_vocab_manager_get_type called.\n");
}


SpeechVocabManager *
speech_vocab_manager_new(void)
{
    printf("speech_vocab_manager_new called.\n");
}


/**
 * speech_vocab_manager_add_word:
 * @manager: the #SpeechVocabManager object that this operation will be applied
 *           to.
 * @word: the word to add.
 *
 * Add a word to the vocabulary.
 */

void
speech_vocab_manager_add_word(SpeechVocabManager *manager,
                              SpeechWord *word)
{
    printf("speech_vocab_manager_add_word called.\n");
}


/**
 * speech_vocab_manager_add_words:
 * @manager: the #SpeechVocabManager object that this operation will be applied
 *           to.
 * @words: the array of words to add.
 *
 * Add an array of words to the vocabulary.
 */

void
speech_vocab_manager_add_words(SpeechVocabManager *manager,
                               SpeechWord *words[])
{
    printf("speech_vocab_manager_add_words called.\n");
}


/**
 * speech_vocab_manager_get_words:
 * @manager: the #SpeechVocabManager object that this operation will be applied
 *           to.
 * @text: word requested from #SpeechVocabManager
 *
 * Get all words from the vocabulary manager matching @text.
 * Returns %NULL if there are no matches. If @text is %NULL all words are 
 * returned. This function only returns words that have been added by the 
 * speech_vocab_manager_add_word() or speech_vocab_manager_add_words()
 * functions - it does not provide access to the engine's internal word lists.
 *
 * Returns: list of words matching text.
 */

SpeechWord **
speech_vocab_manager_get_words(SpeechVocabManager *manager,
                               gchar *text)
{
    printf("speech_vocab_manager_get_words called.\n");
}


/**
 * speech_vocab_manager_list_problem_words:
 * @manager: the #SpeechVocabManager object that this operation will be applied
 *           to.
 *
 * Returns a list of problematic words encountered during a session
 * of using a speech recognizer or synthesizer.
 * This return information is intended for development use (so the
 * application can be enhanced to provide vocabulary information).
 * An engine may return %NULL.
 *
 * If a pronunciation for a problem word is provided through the
 * speech_vocab_manager_add_word() or speech_vocab_manager_add_words()
 * functions, the engine may remove the word from the problem list.
 *
 * An engine may (optionally) include its best-guess pronunciations
 * for problem words in the return array allowing the developer
 * to fix (rather than create) the pronunciation.
 *
 * Returns: a list of problematic words.
 */

SpeechWord **
speech_vocab_manager_list_problem_words(SpeechVocabManager *manager)
{
    printf("speech_vocab_manager_list_problem_words called.\n");
}


/**
 * speech_vocab_manager_remove_word:
 * @manager: the #SpeechVocabManager object that this operation will be applied
 *           to.
 * @word: the word to remove.
 *
 * Remove a word from the vocabulary.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the #SpeechWord is
 *		not known to the #SpeechVocabManager.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_vocab_manager_remove_word(SpeechVocabManager *manager,
                                 SpeechWord *word)
{
    printf("speech_vocab_manager_remove_word called.\n");
}


/**
 * speech_vocab_manager_remove_words:
 * @manager: the #SpeechVocabManager object that this operation will be applied
 *           to.
 * @words: the array of words to remove.
 *
 * Remove an array of words from the vocabulary.
 * To remove a set of words it is often useful to
 *
 *    speech_vocab_manager_remove_words(
 *                     speech_vocab_manager_get_words(manager, "matching"));
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the #SpeechWord is
 *		not known to the #SpeechVocabManager.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_vocab_manager_remove_words(SpeechVocabManager *manager,
                                  SpeechWord *words[])
{
    printf("speech_vocab_manager_remove_words called.\n");
}
