
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* An Audio Manager is provided by a speech Engine - a Recognizer or 
 * Synthesizer - to allow an application to control audio input/output and
 * to monitor audio-related events.
 */

#ifndef __SPEECH_AUDIO_MANAGER_H__
#define __SPEECH_AUDIO_MANAGER_H__

#include <speech/speech_types.h>
#include <speech/speech_audio_listener.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_AUDIO_MANAGER              (speech_audio_manager_get_type())
#define SPEECH_AUDIO_MANAGER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_AUDIO_MANAGER, SpeechAudioManager))
#define SPEECH_AUDIO_MANAGER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_AUDIO_MANAGER, SpeechAudioManagerClass))
#define SPEECH_IS_AUDIO_MANAGER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_AUDIO_MANAGER))
#define SPEECH_IS_AUDIO_MANAGER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_AUDIO_MANAGER))
#define SPEECH_AUDIO_MANAGER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_AUDIO_MANAGER, SpeechAudioManagerClass))

typedef struct _SpeechAudioManager            SpeechAudioManager;
typedef struct _SpeechAudioManagerClass       SpeechAudioManagerClass;

struct _SpeechAudioManager {
    SpeechObject object;
};

struct _SpeechAudioManagerClass {
    SpeechObjectClass parent_class;
};


GType 
speech_audio_manager_get_type(void);

SpeechAudioManager * 
speech_audio_manager_new(void);

void 
speech_audio_manager_add_audio_listener(SpeechAudioManager *manager,
                                        SpeechAudioListener *listener);

void 
speech_audio_manager_remove_audio_listener(SpeechAudioManager *manager,
                                           SpeechAudioListener *listener);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_EXCEPTION_H__ */
