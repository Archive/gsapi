
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Provides information about a specific operating mode of a speech engine. */


#ifndef __SPEECH_ENGINE_MODE_DESC_H__
#define __SPEECH_ENGINE_MODE_DESC_H__

#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_MODE_DESC              (speech_engine_mode_desc_get_type())
#define SPEECH_ENGINE_MODE_DESC(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_MODE_DESC, SpeechEngineModeDesc))
#define SPEECH_ENGINE_MODE_DESC_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_MODE_DESC, SpeechEngineModeDescClass))
#define SPEECH_IS_ENGINE_MODE_DESC(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_MODE_DESC))
#define SPEECH_IS_ENGINE_MODE_DESC_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_MODE_DESC))
#define SPEECH_ENGINE_MODE_DESC_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_MODE_DESC, SpeechEngineModeDescClass))

typedef struct _SpeechEngineModeDesc            SpeechEngineModeDesc;
typedef struct _SpeechEngineModeDescClass       SpeechEngineModeDescClass;

struct _SpeechEngineModeDesc {
    SpeechObject object;
};

struct _SpeechEngineModeDescClass {
    SpeechObjectClass parent_class;
};


GType 
speech_engine_mode_desc_get_type(void);

SpeechEngineModeDesc * 
speech_engine_mode_desc_new(void);

SpeechEngineModeDesc * 
speech_engine_mode_desc_new_with_locale(gchar *locale);

SpeechEngineModeDesc * 
speech_engine_mode_desc_new_with_name_and_state(gchar *engine_name,
                                                gchar * mode_name,
                                                gchar * locale,
                                                SpeechTristate running);

gboolean 
speech_engine_mode_desc_equals(SpeechEngineModeDesc *mode_desc,
                               SpeechEngineModeDesc *an_object);

gchar * 
speech_engine_mode_desc_get_engine_name(SpeechEngineModeDesc *mode_desc);

gchar * 
speech_engine_mode_desc_get_locale(SpeechEngineModeDesc *mode_desc);

gchar * 
speech_engine_mode_desc_get_mode_name(SpeechEngineModeDesc *mode_desc);

SpeechTristate 
speech_engine_mode_desc_get_running(SpeechEngineModeDesc *mode_desc);

gboolean 
speech_engine_mode_desc_match(SpeechEngineModeDesc *mode_desc,
                              SpeechEngineModeDesc *require);

void 
speech_engine_mode_desc_set_engine_name(SpeechEngineModeDesc *mode_desc,
                                        gchar *engine_name);

void 
speech_engine_mode_desc_set_locale(SpeechEngineModeDesc *mode_desc,
                                   gchar *locale);

void 
speech_engine_mode_desc_set_mode_name(SpeechEngineModeDesc *mode_desc,
                                      gchar *mode_name);

void 
speech_engine_mode_desc_set_running(SpeechEngineModeDesc *mode_desc,
                                    SpeechTristate running);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_MODE_DESC_H__ */
