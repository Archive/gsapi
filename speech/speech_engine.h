
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* The parent object for all speech engines including 
 * #SpeechRecognitionRecognizer and #SpeechSynthesisSynthesizer.
 */

#ifndef __SPEECH_ENGINE_H__
#define __SPEECH_ENGINE_H__

#include <speech/speech_types.h>
#include <speech/speech_audio_manager.h>
#include <speech/speech_engine_listener.h>
#include <speech/speech_engine_properties.h>
#include <speech/speech_engine_mode_desc.h>
#include <speech/speech_vocab_manager.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE              (speech_engine_get_type())
#define SPEECH_ENGINE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE, SpeechEngine))
#define SPEECH_ENGINE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE, SpeechEngineClass))
#define SPEECH_IS_ENGINE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE))
#define SPEECH_IS_ENGINE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE))
#define SPEECH_ENGINE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE, SpeechEngineClass))

typedef struct _SpeechEngineClass       SpeechEngineClass;

struct _SpeechEngine {
    SpeechObject object;
};

struct _SpeechEngineClass {
    SpeechObjectClass parent_class;
};


/*
 * Bit of state that is set when a #SpeechEngine is in the
 * deallocated state. A deallocated engine does not have the
 * resources necessary for it to carry out its basic functions.
 *
 * In the #SPEECH_ENGINE_DEALLOCATED state, many of the functions of
 * a #SpeechEngine return an exception when called. The
 * #SPEECH_ENGINE_DEALLOCATED state has no sub-states.
 *
 * A #SpeechEngine is always created in the #SPEECH_ENGINE_DEALLOCATED state.
 * A #SPEECH_ENGINE_DEALLOCATED can transition to the 
 * #SPEECH_ENGINE_ALLOCATED state via the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state following a call to the speech_engine_allocate() function. 
 * A #SpeechEngine returns to the #SPEECH_ENGINE_DEALLOCATED state via the 
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES state with a call to the 
 * speech_engine_deallocate() function.
 *
 * See: speech_engine_allocate()
 * See: speech_engine_deallocate()
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 */

static long SPEECH_ENGINE_DEALLOCATED = 1L << 0;

/*
 * Bit of state that is set when a #SpeechEngine is being
 * allocated - the transition state between #SPEECH_ENGINE_DEALLOCATED
 * to #SPEECH_ENGINE_ALLOCATED following a call to the
 * speech_engine_allocate() function. The #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state has no sub-states. In the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state, many of the functions of #SpeechEngine,
 * #SpeechRecognitionRecognizer, and #SpeechSynthesisSynthesizer will block
 * until the #SpeechEngine reaches the #SPEECH_ENGINE_ALLOCATED
 * state and the action can be performed.
 *
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 */

static long SPEECH_ENGINE_ALLOCATING_RESOURCES = 1L << 1;

/*
 * Bit of state that is set when a #SpeechEngine is in the allocated state.
 * An engine in the #SPEECH_ENGINE_ALLOCATED state has acquired the
 * resources required for it to carry out its core functions.
 *
 * The #SPEECH_ENGINE_ALLOCATED states has sub-states for
 * #SPEECH_ENGINE_RESUMED and #SPEECH_ENGINE_PAUSED.
 * Both #SpeechSynthesisSynthesizer and #SpeechRecognitionRecognizer define
 * additional sub-states of #SPEECH_ENGINE_ALLOCATED.
 *
 * A #SpeechEngine is always created in the
 * #SPEECH_ENGINE_DEALLOCATED state. It reaches the
 * #SPEECH_ENGINE_ALLOCATED state via the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state with a call to the
 * speech_engine_allocate() function.
 *
 * See: #SpeechSynthesisSynthesizer
 * See: #SpeechRecognitionRecognizer
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 */

static long SPEECH_ENGINE_ALLOCATED = 1L << 2;

/*
 * Bit of state that is set when a #SpeechEngine is being
 * deallocated - the transition state between #SPEECH_ENGINE_ALLOCATED
 * to #SPEECH_ENGINE_DEALLOCATED. The #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * state has no sub-states. In the #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * state, most functions of #SpeechEngine, #SpeechRecognitionRecognizer
 * and #SpeechSynthesisSynthesizer return an exception.
 *
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 */

static long SPEECH_ENGINE_DEALLOCATING_RESOURCES = 1L << 3;

/*
 * Bit of state that is set when a #SpeechEngine is is in the
 * #SPEECH_ENGINE_ALLOCATED state and is #SPEECH_ENGINE_PAUSED. In the
 * #SPEECH_ENGINE_PAUSED state, audio input or output stopped.
 *
 * A #SPEECH_ENGINE_ALLOCATED engine is always in either in the
 * #SPEECH_ENGINE_PAUSED or #SPEECH_ENGINE_RESUMED. The #SPEECH_ENGINE_PAUSED
 * and #SPEECH_ENGINE_RESUMED states are sub-states of the
 * #SPEECH_ENGINE_ALLOCATED state.
 *
 * See: #SPEECH_ENGINE_RESUMED
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 */

static long SPEECH_ENGINE_PAUSED = 1L << 8;

/*
 * Bit of state that is set when a #SpeechEngine is is in the
 * #SPEECH_ENGINE_ALLOCATED state and is #SPEECH_ENGINE_RESUMED. In the
 * #SPEECH_RESUMED state, audio input or output active.
 *
 * An #SPEECH_ENGINE_ALLOCATED engine is always in either in the
 * #SPEECH_ENGINE_PAUSED or #SPEECH_ENGINE_RESUMED. The 
 * #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED states are 
 * sub-states of the #SPEECH_ENGINE_ALLOCATED state.
 *
 * See: #SPEECH_ENGINE_RESUMED
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: speech_engine_get_engine_state()
 * See: speech_engine_wait_engine_state()
 */

static long SPEECH_ENGINE_RESUMED = 1L << 9;


GType 
speech_engine_get_type(void);

SpeechEngine * 
speech_engine_new(void);

void 
speech_engine_add_engine_listener(SpeechEngine *engine,
                                  SpeechEngineListener *listener);

void 
speech_engine_remove_engine_listener(SpeechEngine *engine,
                                     SpeechEngineListener *listener);

SpeechStatus 
speech_engine_allocate(SpeechEngine *engine);

SpeechStatus 
speech_engine_deallocate(SpeechEngine *engine);

SpeechAudioManager * 
speech_engine_get_audio_manager(SpeechEngine *engine);

SpeechStatus 
speech_engine_get_engine_mode_desc(SpeechEngine *engine,
                                   SpeechEngineModeDesc *mode_desc);

SpeechEngineProperties * 
speech_engine_get_engine_properties(SpeechEngine *engine);

long 
speech_engine_get_engine_state(SpeechEngine *engine);

SpeechStatus 
speech_engine_get_vocab_manager(SpeechEngine *engine,
                                SpeechVocabManager *manager);

SpeechStatus 
speech_engine_pause(SpeechEngine *engine);

SpeechStatus 
speech_engine_resume(SpeechEngine *engine);

SpeechStatus 
speech_engine_test_engine_state(SpeechEngine *engine,
                                long state,
                                gboolean *result);

SpeechStatus 
speech_engine_wait_engine_state(SpeechEngine *engine,
                                long state);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_H__ */
