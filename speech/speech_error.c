
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The potential detail message associated with an error.
 *
 * See: #SpeechEngineStateError
 */

#include <speech/speech_error.h>


/**
 * speech_error_get_type:
 *
 * Returns: the type ID for #SpeechError.
 */

GType
speech_error_get_type(void)
{
    printf("speech_error_get_type called.\n");
}


SpeechError *
speech_error_new(void)
{
    printf("speech_error_new called.\n");
}


/**
 * speech_error_new_with_message:
 * @message: the detail message
 *
 * Initialises an error with the specified detail message.
 * A detail message is a string that describes this particular
 * error.
 *
 * Returns:
 */

SpeechError *
speech_error_new_with_message(gchar *message)
{
    printf("speech_error_new_with_message called.\n");
}


/**
 * speech_error_get_message:
 * @error: the #SpeechError object this this operation will be applied to.
 *
 * Returns the detail message associated with this error value.
 *
 * Returns: the detail message for this error value.
 */

gchar *
speech_error_get_message(SpeechError *error)
{
    printf("speech_error_get_message called.\n");
}
