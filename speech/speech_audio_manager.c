
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The #SpeechAudioManager is provided by a #SpeechEngine
 * - a #SpeechRecognitionRecognizer or #SpeechSynthesisSynthesizer - to 
 * allow an application to control audio input/output and to monitor 
 * audio-related events. The #SpeechAudioManager for an engine is obtained by
 * calling its speech_engine_get_audio_manager() function.
 *
 * See: speech_engine_get_audio_manager()
 */

#include <speech/speech_audio_manager.h>


/**
 * speech_audio_manager_get_type:
 *
 * Returns: the type ID for #SpeechAudioManager.
 */

GType
speech_audio_manager_get_type(void)
{
    printf("speech_audio_manager_get_type called.\n");
}


SpeechAudioManager *
speech_audio_manager_new(void)
{
    printf("speech_audio_manager_new called.\n");
}


/**
 * speech_audio_manager_add_audio_listener:
 * @manager: the audio manager this operation will be applied to.
 * @listener: the audio listener to add.
 *
 * Request notifications of audio events to a #SpeechAudioListener.
 * An application can attach multiple audio listeners to a #SpeechAudioManager.
 * If the engine is a #SpeechRecognitionRecognizer, a
 * #SpeechRecognizerAudioListener may be attached since the 
 * #SpeechRecognitionRecognizerAudioListener
 * object extends the #SpeechAudioListener object.
 *
 * See: #SpeechAudioListener
 * See: #SpeechRecognitionRecognizerAudioListener
 */

void
speech_audio_manager_add_audio_listener(SpeechAudioManager *manager,
                                        SpeechAudioListener *listener)
{
    printf("speech_audio_manager_add_audio_listener called.\n");
}


/**
 * speech_audio_manager_remove_audio_listener:
 * @manager: the audio manager this operation will be applied to.
 * @listener: the audio listener to remove.
 *
 * Remove an audio listener from this #SpeechAudioManager.
 */

void
speech_audio_manager_remove_audio_listener(SpeechAudioManager *manager,
                                           SpeechAudioListener *listener)
{
    printf("speech_audio_manager_remove_audio_listener called.\n");
}
