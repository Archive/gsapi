
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RECOGNIZER_H__
#define __SPEECH_RECOGNITION_RECOGNIZER_H__

#include <speech/speech_engine.h>
#include <speech/speech_input_stream.h>
#include <speech/speech_output_stream.h>
#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_dictation_grammar.h>
#include <speech/recognition/speech_recognition_rule_grammar.h>
#include <speech/recognition/speech_recognition_result.h>
#include <speech/recognition/speech_recognition_recognizer_properties.h>
#include <speech/recognition/speech_recognition_speaker_manager.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RECOGNIZER              (speech_recognition_recognizer_get_type())
#define SPEECH_RECOGNITION_RECOGNIZER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER, SpeechRecognitionRecognizer))
#define SPEECH_RECOGNITION_RECOGNIZER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER, SpeechRecognitionRecognizerClass))
#define SPEECH_IS_RECOGNITION_RECOGNIZER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER))
#define SPEECH_RECOGNITION_RECOGNIZER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER, SpeechRecognitionRecognizerClass))

typedef struct _SpeechRecognitionRecognizerClass       SpeechRecognitionRecognizerClass;

struct _SpeechRecognitionRecognizer {
    SpeechEngine engine;
};

struct _SpeechRecognitionRecognizerClass {
    SpeechEngineClass parent_class;
};


/*
 * #SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_JSGF is the Java Speech Grammar 
 * Format grammar type. It is one of the valid values for a #SpeechGrammarType,
 * and can be used in conjunction with the
 * speech_recognition_recognizer_load_grammar_from_stream(),
 * speech_recognition_recognizer_load_grammar_from_url() and
 * speech_recognition_recognizer_load_grammar_from_url_with_imports() 
 * functions., to load a #SpeechRecognitionRuleGrammar for this 
 * #SpeechRecognitionRecognizer.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_SRGS
 * See: speech_recognition_recognizer_load_grammar_from_stream()
 * See: speech_recognition_recognizer_load_grammar_from_url()
 * See: speech_recognition_recognizer_load_grammar_from_url_with_imports()
 * See: #SpeechRecognitionRuleGrammar
 * See: speech_recognition_recognizer_is_grammar_type_supported()
 */

static gchar *SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_JSGF = "JSGF_GRAMMAR";

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_SRGS is the Speech Recognition 
 * Grammar Specification grammar type. It is one of the valid values for
 * a #SpeechGrammarType, and can be used in conjunction with the
 * speech_recognition_recognizer_load_grammar_from_stream(),
 * speech_recognition_recognizer_load_grammar_from_url() and
 * speech_recognition_recognizer_load_grammar_from_url_with_imports()
 * functions to load a #SpeechRecognitionRuleGrammar for this 
 * #SpeechRecognitionRecognizer.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_JSGF
 * See: speech_recognition_recognizer_load_grammar_from_stream()
 * See: speech_recognition_recognizer_load_grammar_from_url()
 * See: speech_recognition_recognizer_load_grammar_from_url_with_imports()
 * See: #SpeechRecognitionRuleGrammar
 * See: speech_recognition_recognizer_is_grammar_type_supported()
 */

static gchar *SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_SRGS = "SRGS_GRAMMAR";


/* ********************* RECOGNIZER STATES *********************** */

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING is the bit of state that is 
 * set when a #SPEECH_ENGINE_ALLOCATED #SpeechRecognitionRecognizer is 
 * listening to incoming audio for speech that may match an active 
 * grammar but has not yet detected speech.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING event is 
 * issued to indicate a transition out of the
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state and into the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event is 
 * issued to indicate a transition out of the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state and into the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event is 
 * issued to indicate a transition into the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state.
 *
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: speech_engine_get_engine_state()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 */

static long SPEECH_RECOGNITION_RECOGNIZER_LISTENING = 1L << 0;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING is the bit of state that is 
 * set when an #SPEECH_ENGINE_ALLOCATED #SpeechRecognitionRecognizer is 
 * producing a #SpeechRecognitionResult for incoming speech that may match 
 * an active grammar.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 * event is issued to indicate a transition out of the
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state and into the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state when recognition of 
 * a #SpeechRecognitionResult is completed.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 * event is issued to indicate a transition into the
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state when the start 
 * of a new result is detected.
 *
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: speech_engine_get_engine_state()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 */

static long SPEECH_RECOGNITION_RECOGNIZER_PROCESSING = 1L << 1;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED is the bit of state that 
 * is set when a #SPEECH_ENGINE_ALLOCATED #SpeechRecognitionRecognizer
 * is temporarily suspended while grammar definition and grammar 
 * enabled settings are updated. The #SpeechRecognitionRecognizer 
 * enters this state whenever recognition of a #SpeechRecognitionResult
 * is finalized, and in response to a call to 
 * speech_recognition_recognizer_suspend()
 *
 * The primary difference between the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED and #SPEECH_ENGINE_EVENT_PAUSED
 * states of a #SpeechRecognitionRecognizer is that audio input is buffered 
 * in the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state. By contrast, the 
 * #SPEECH_ENGINE_EVENT_PAUSED state indicates that audio input to the 
 * #SpeechRecognitionRecognizer is being ignored. In addition, the
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state is a temporary state, 
 * whereas a #SpeechRecognitionRecognizer can stay in the 
 * #SPEECH_ENGINE_EVENT_PAUSED state indefinitely.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event is issued 
 * to indicate a transition out of the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state and into the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state when all changes to 
 * grammars are committed to the recognition process.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED event is issued to indicate 
 * a transition into the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state 
 * from either the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state or 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state.
 *
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: #SPEECH_ENGINE_EVENT_PAUSED
 * See: speech_engine_get_engine_state()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 */

static long SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED = 1L << 2;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON is the bit of state that is 
 * set when an #SPEECH_ENGINE_ALLOCATED #SpeechRecognitionRecognizer has 
 * the speech focus of the underlying speech recognition engine.
 *
 * As recognizer focus is gained and lost, a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED or 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event is issued 
 * to indicate the state change. The 
 * speech_recognition_recognizer_request_focus() and
 * speech_recognition_recognizer_release_focus() functions allow 
 * management of speech focus.
 *
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF
 * See: speech_recognition_recognizer_request_focus()
 * See: speech_recognition_recognizer_release_focus()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST
 * See: speech_engine_get_engine_state()
 */

static long SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON = 1L << 16;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF is the bit of state that is 
 * set when a #SPEECH_ENGINE_ALLOCATED #SpeechRecognitionRecognizer does 
 * not have the speech focus of the underlying speech recognition engine.
 *
 * As recognizer focus is gained and lost, a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED or
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event is issued to 
 * indicate the state change. The 
 * speech_recognition_recognizer_release_focus() and 
 * speech_recognition_recognizer_release_focus() functions allow 
 * management of speech focus.
 *
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON
 * See: speech_recognition_recognizer_request_focus()
 * See: speech_recognition_recognizer_release_focus()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST
 * See: speech_engine_get_engine_state()
 */

static long SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF = 1L << 17;


GType 
speech_recognition_recognizer_get_type(void);

SpeechRecognitionRecognizer * 
speech_recognition_recognizer_new(void);

void 
speech_recognition_recognizer_add_result_listener(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionResultListener *listener);

void 
speech_recognition_recognizer_remove_result_listener(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionResultListener *listener);

SpeechStatus 
speech_recognition_recognizer_commit_changes(
                          SpeechRecognitionRecognizer *recognizer);

SpeechStatus 
speech_recognition_recognizer_delete_rule_grammar(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_force_finalize(
                          SpeechRecognitionRecognizer *recognizer,
                          gboolean flush);

SpeechStatus 
speech_recognition_recognizer_get_dictation_grammar(
                          SpeechRecognitionRecognizer *recognizer,
                          gchar *name,
                          SpeechRecognitionDictationGrammar *grammar);

SpeechRecognitionRecognizerProperties * 
speech_recognition_recognizer_get_recognizer_properties(
                          SpeechRecognitionRecognizer *recognizer);

SpeechStatus 
speech_recognition_recognizer_get_rule_grammar(
                          SpeechRecognitionRecognizer *recognizer,
                          gchar *name,
                          SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_get_speaker_manager(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionSpeakerManager *manager);

SpeechStatus 
speech_recognition_recognizer_list_rule_grammars(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionRuleGrammar *grammars[]);

gboolean 
speech_recognition_recognizer_is_grammar_type_supported(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechGrammarType type);

SpeechStatus 
speech_recognition_recognizer_load_grammar_from_stream(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechGrammarType type,
                          SpeechInputStream *input_stream,
                          SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_load_grammar_from_url(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechGrammarType type,
                          SpeechGrammarURL url_context,
                          gchar *grammar_name,
                          SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_load_grammar_from_url_with_imports(
                                  SpeechRecognitionRecognizer *recognizer,
                                  SpeechGrammarType type,
                                  SpeechGrammarURL url_context,
                                  gchar *grammar_name,
                                  gboolean load_imports,
                                  gboolean reload_grammars,
                                  SpeechRecognitionGrammar *loaded_grammars[],
                                  SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_new_rule_grammar(
                                  SpeechRecognitionRecognizer *recognizer,
                                  gchar *name,
                                  SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_read_vendor_grammar(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechInputStream *input,
                                       SpeechRecognitionGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_write_vendor_grammar(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechOutputStream *output,
                                       SpeechRecognitionGrammar *grammar);

SpeechStatus 
speech_recognition_recognizer_read_vendor_result(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechInputStream *input,
                                       SpeechRecognitionResult *result);

SpeechStatus 
speech_recognition_recognizer_write_vendor_result(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechOutputStream *output,
                                       SpeechRecognitionResult *result);

SpeechStatus 
speech_recognition_recognizer_release_focus(
                                   SpeechRecognitionRecognizer *recognizer);

SpeechStatus 
speech_recognition_recognizer_request_focus(
                                   SpeechRecognitionRecognizer *recognizer);

SpeechStatus 
speech_recognition_recognizer_suspend(
                                   SpeechRecognitionRecognizer *recognizer);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_SYNTHESIS_SYNTHESIZER_H__ */
