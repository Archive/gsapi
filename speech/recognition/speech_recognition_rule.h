
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_H__
#define __SPEECH_RECOGNITION_RULE_H__

#include <speech/recognition/speech_recognition_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE              (speech_recognition_rule_get_type())
#define SPEECH_RECOGNITION_RULE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE, SpeechRecognitionRule))
#define SPEECH_RECOGNITION_RULE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE, SpeechRecognitionRuleClass))
#define SPEECH_IS_RECOGNITION_RULE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE))
#define SPEECH_IS_RECOGNITION_RULE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE))
#define SPEECH_RECOGNITION_RULE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE, SpeechRecognitionRuleClass))

typedef struct _SpeechRecognitionRule            SpeechRecognitionRule;
typedef struct _SpeechRecognitionRuleClass       SpeechRecognitionRuleClass;

struct _SpeechRecognitionRule {
    SpeechObject object;
};

struct _SpeechRecognitionRuleClass {
    SpeechObjectClass parent_class;
};


GType 
speech_recognition_rule_get_type(void);

SpeechRecognitionRule * 
speech_recognition_rule_new(void);

SpeechRecognitionRule * 
speech_recognition_rule_copy(SpeechRecognitionRule *rule);

gchar * 
speech_recognition_rule_to_string(SpeechRecognitionRule *rule);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_H__ */
