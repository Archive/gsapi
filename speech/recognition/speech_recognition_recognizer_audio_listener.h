
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RECOGNIZER_AUDIO_LISTENER_H__
#define __SPEECH_RECOGNITION_RECOGNIZER_AUDIO_LISTENER_H__

#include <speech/speech_audio_listener.h>
#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_recognizer_audio_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_LISTENER              (speech_recognition_recognizer_audio_listener_get_type())
#define SPEECH_RECOGNITION_RECOGNIZER_AUDIO_LISTENER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_LISTENER, SpeechRecognitionRecognizerAudioListener))
#define SPEECH_RECOGNITION_RECOGNIZER_AUDIO_LISTENER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_LISTENER, SpeechRecognitionRecognizerAudioListenerClass))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_AUDIO_LISTENER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_LISTENER))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_AUDIO_LISTENER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_LISTENER))
#define SPEECH_RECOGNITION_RECOGNIZER_AUDIO_LISTENER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_LISTENER, SpeechRecognitionRecognizerAudioListenerClass))

typedef struct _SpeechRecognitionRecognizerAudioListener            SpeechRecognitionRecognizerAudioListener;
typedef struct _SpeechRecognitionRecognizerAudioListenerClass       SpeechRecognitionRecognizerAudioListenerClass;

struct _SpeechRecognitionRecognizerAudioListener {
    SpeechAudioListener listener;
};

struct _SpeechRecognitionRecognizerAudioListenerClass {
    SpeechAudioListenerClass parent_class;
};


GType 
speech_recognition_recognizer_audio_listener_get_type(void);

SpeechRecognitionRecognizerAudioListener * 
speech_recognition_recognizer_audio_listener_new(void);

void 
speech_recognition_recognizer_audio_listener_audio_level(
                             SpeechRecognitionRecognizerAudioListener *listener,
                             SpeechRecognitionRecognizerAudioEvent *e);

void 
speech_recognition_recognizer_audio_listener_speech_started(
                             SpeechRecognitionRecognizerAudioListener *listener,
                             SpeechRecognitionRecognizerAudioEvent *e);

void 
speech_recognition_recognizer_audio_listener_speech_stopped(
                             SpeechRecognitionRecognizerAudioListener *listener,
                             SpeechRecognitionRecognizerAudioEvent *e);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RECOGNIZER_AUDIO_LISTENER_H__ */
