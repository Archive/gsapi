
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Provides control of #SpeechRecognitionSpeakerProfiles for a
 * #SpeechRecognitionRecognizer. The #SpeechRecognitionSpeakerManager for a
 * #SpeechRecognitionRecognizer is obtained through its
 * speech_recognition_recognizer_get_speaker_manager() function. Recognizers
 * that do not maintain speaker profiles - known as speaker-independent 
 * recognizers return %NULL for this function.
 *
 * Each #SpeechRecognitionSpeakerProfile stored with a 
 * #SpeechRecognitionRecognizer stored information about an enrollment of 
 * a user with the recognizer. The user information allows the recognizer 
 * to adapt to the characteristic of the user with the goal of improving 
 * performance and recognition accuracy. For example, the recognizer might 
 * adjust to vocabulary preferences and accent.
 *
 * The role of the #SpeechRecognitionSpeakerManager is provide access to 
 * the known #SpeechRecognitionSpeakerProfiles, to enable storage and 
 * loading of the profiles once a recognizer is running, and to provide 
 * other management functions (storage to file, deletion etc). The 
 * #SpeechRecognitionSpeakerManager has a "current speaker" - the profile 
 * which is currently being used by the recognizer.
 *
 * <title>Storing and Loading</title>
 *
 * Speaker data is typically persistent - a user will want their profile to be
 * available from session to session. An application must explicitly request
 * a recognizer to save a speaker profile. It is good practice to check with a
 * user before storing their speaker profile in case it has become corrupted.
 *
 * The #SpeechRecognitionSpeakerManager object provides a revert function 
 * which requests the engine to restore the last saved profile (possibly the
 * profile loaded at the start of a session).
 *
 * The speaker profile is potentially a large amount of data (often up to
 * several MBytes). So loading and storing profiles, reverting to an old
 * profile and changing speakers may all be slow operations.
 *
 * The #SpeechRecognitionSpeakerManager provides functions to load and store
 * speaker profiles to and from streams (e.g. files, URLs). Speaker
 * profiles contain engine-specific and often proprietary information, so
 * a speaker profile from one recognizer can not usually be loaded into
 * a recognizer from a different company.
 *
 * The #SpeechRecognitionSpeakerManager for a #SpeechRecognitionRecognizer 
 * can be obtained from the #SpeechRecognitionRecognizer in any state of 
 * the recognizer. However, most functions of the 
 * #SpeechRecognitionSpeakerManager operate correctly only when the 
 * #SpeechRecognitionRecognizer is in the #SPEECH_ENGINE_ALLOCATED.
 *
 * The speech_recognition_speaker_manager_get_current_speaker(),
 * speech_recognition_speaker_manager_set_current_speaker() and
 * speech_recognition_speaker_manager_list_known_speakers() functions
 * operate in any #SpeechRecognitionRecognizer state. This allows the 
 * initial speaker profile for a #SpeechRecognitionRecognizer to be 
 * loaded at allocation time.
 *
 * See: #SpeechRecognitionSpeakerProfile
 * See: speech_recognition_recognizer_mode_desc_get_speaker_profiles()
 * See: #SpeechCentral
 */

#include <speech/recognition/speech_recognition_speaker_manager.h>


GType
speech_recognition_speaker_manager_get_type(void)
{
    printf("speech_recognition_speaker_manager_get_type called.\n");
}


SpeechRecognitionSpeakerManager *
speech_recognition_speaker_manager_new(void)
{
    printf("speech_recognition_speaker_manager_new called.\n");
}


/**
 * speech_recognition_speaker_manager_delete_speaker:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 * @speaker: the speaker to delete.
 *
 * Delete a #SpeechRecognitionSpeakerProfile. If the deleted speaker is
 * the current speaker, the current speaker is set to %NULL.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the speaker is not 
 *       known.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_speaker_manager_delete_speaker(
                               SpeechRecognitionSpeakerManager *manager,
                               SpeechRecognitionSpeakerProfile *speaker)
{
    printf("speech_recognition_speaker_manager_delete_speaker called.\n");
}


/**
 * speech_recognition_speaker_manager_get_control_component:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 *
 * Obtain a component that provides the engine's user interface for
 * managing speaker data and training. If this #SpeechRecognitionRecognizer
 * has no default control panel, the return value is %NULL and the
 * application is responsible for providing an appropriate control
 * panel.
 *
 * Note: because the interface is provided by the recognizer, it
 *	 may allow the management of properties that are not otherwise
 *	 accessible through the standard API.
 *
 * Returns: a component that provides the engine's user interface for
 *	   managing speaker data and training.
 */

SpeechControlComponent *
speech_recognition_speaker_manager_get_control_component(
                                  SpeechRecognitionSpeakerManager *manager)
{
    printf("speech_recognition_speaker_manager_get_control_component called.\n");
}


/**
 * speech_recognition_speaker_manager_get_current_speaker:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 *
 * Get the current #SpeechRecognitionSpeakerProfile. Returns %NULL if there 
 * is no current speaker.
 *
 * Returns: the current #SpeechRecognitionSpeakerProfile.
 */

SpeechRecognitionSpeakerProfile *
speech_recognition_speaker_manager_get_current_speaker(
                                  SpeechRecognitionSpeakerManager *manager)
{
    printf("speech_recognition_speaker_manager_get_current_speaker called.\n");
}


/**
 * speech_recognition_speaker_manager_list_known_speakers:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 *
 * List the #SpeechRecognitionSpeakerProfiles known to this
 * #SpeechRecognitionRecognizer. Returns %NULL if there is no known speaker.
 *
 * Returns: a list of the #SpeechRecognitionSpeakerProfiles known to
 *	   this #SpeechRecognitionRecognizer.
 */

SpeechRecognitionSpeakerProfile **
speech_recognition_speaker_manager_list_known_speakers(
                                   SpeechRecognitionSpeakerManager *manager)
{
    printf("speech_recognition_speaker_manager_list_known_speakers called.\n");
}


/**
 * speech_recognition_speaker_manager_new_speaker_profile:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 * @profile: the speaker profile.
 * @result: return a new #SpeechRecognitionSpeakerProfile for this
 *	   #SpeechRecognitionRecognizer.
 *
 * Create a new #SpeechRecognitionSpeakerProfile for this
 * #SpeechRecognitionRecognizer. The #SpeechRecognitionSpeakerProfile object
 * returned by this function is different from the object passed to
 * the function. The input profile contains the new id, name and
 * variant. The returned object is a reference to a recognizer-internal 
 * profile with those settings but with all the additional 
 * recognizer-specific information associated with a profile.
 *
 * This function does not change the current speaker.
 *
 * If the input profile's identifier or user name is not specified (is
 * %NULL), the recognizer should assign a unique temporary identifier.
 * The application should request that the user update the id.
 *
 * If the input profile is null, the recognizer should assign a temporary 
 * id and user name. The application should query the user for details.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the speaker id is already
 *		being used.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_speaker_manager_new_speaker_profile(
                               SpeechRecognitionSpeakerManager *manager,
                               SpeechRecognitionSpeakerProfile *profile,
                               SpeechRecognitionSpeakerProfile *result)
{
    printf("speech_recognition_speaker_manager_new_speaker_profile called.\n");
}


/**
 * speech_recognition_speaker_manager_read_vendor_speaker_profile:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 * @in_stream: the #SpeechInputStream to read the speaker profile off.
 * @profile: return a reference to a #SpeechRecognitionSpeakerProfile.
 *
 * Read a #SpeechRecognitionSpeakerProfile from a stream and return a
 * reference to it. This function loads data that may have been stored
 * previously with the 
 * speech_recognition_speaker_manager_write_vendor_speaker_profile() function.
 *
 * If the speaker profile contained in the input stream already exists,
 * the recognizer should create a modified name. An application should
 * inform the user of the name that is loaded and consider giving them
 * an option to modify it.
 *
 * Since speaker profiles are stored in vendor-specific formats, they
 * can only be loaded into a recognizer that understands that format -
 * typically a recognizer from the same provider.
 *
 * Note: The speaker profile is potentially large (up to several MBytes).
 *
 * See: speech_recognition_speaker_manager_write_vendor_speaker_profile()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_VENDOR_DATA_EXCEPTION if the data format is not known to
 *		the recognizer.</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occured.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_speaker_manager_read_vendor_speaker_profile(
                                    SpeechRecognitionSpeakerManager *manager,
                                    SpeechInputStream *in_stream,
                                    SpeechRecognitionSpeakerProfile *profile)
{
    printf("speech_recognition_speaker_manager_read_vendor_speaker_profile called.\n");
}


/**
 * speech_recognition_speaker_manager_revert_current_speaker:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 *
 * Restore the speaker profile for the current speaker to the last
 * saved version. If the speaker profile has not been saved during
 * the session, the restored version will be the version loaded at
 * the start of the session.
 *
 * Because of the large potential size of the speaker profile, this
 * may be a slow operation.
 *
 * See: speech_recognition_speaker_manager_save_current_speaker_profile()
 * See: speech_recognition_speaker_manager_read_vendor_speaker_profile()
 */

void
speech_recognition_speaker_manager_revert_current_speaker(
                                SpeechRecognitionSpeakerManager *manager)
{
    printf("speech_recognition_speaker_manager_revert_current_speaker called.\n");
}


/**
 * speech_recognition_speaker_manager_save_current_speaker_profile:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 *
 * Save the speaker profile for the current speaker. The speaker
 * profile is stored internally by the recognizer and should be
 * available for future sessions.
 *
 * Because of the large potential size of the speaker profile, this
 * may be a slow operation.
 *
 * See: speech_recognition_speaker_manager_revert_current_speaker()
 * See: speech_recognition_speaker_manager_write_vendor_speaker_profile()
 */

void
speech_recognition_speaker_manager_save_current_speaker_profile(
                                  SpeechRecognitionSpeakerManager *manager)
{
    printf("speech_recognition_speaker_manager_save_current_speaker_profile called.\n");
}


/**
 * speech_recognition_speaker_manager_set_current_speaker:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 *
 * Set the current #SpeechRecognitionSpeakerProfile. The
 * #SpeechRecognitionSpeakerProfile object should be one of the
 * objects returned by the 
 * speech_recognition_speaker_manager_list_known_speakers() function.
 *
 * Because a #SpeechRecognitionSpeakerProfile may store preferred user
 * settings for the #SpeechRecognitionRecognizerProperties, those
 * properties may change as a result of this call.
 *
 * @speaker: the new #SpeechRecognitionSpeakerProfile.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the speaker is not 
 *       known.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_speaker_manager_set_current_speaker(
                                    SpeechRecognitionSpeakerManager *manager,
                                    SpeechRecognitionSpeakerProfile *speaker)
{
    printf("speech_recognition_speaker_manager_set_current_speaker called.\n");
}


/**
 * speech_recognition_speaker_manager_write_vendor_speaker_profile:
 * @manager: the #SpeechRecognitionSpeakerManager object that this operation
 *           will be applied to.
 * @out_stream: the #SpeechOutputStream to write to.
 * @speaker: the speaker profile.
 *
 * Write the speaker profile of the named speaker to a stream. This
 * function allows speaker data to be stored and to be transferred
 * between machines.
 *
 * The speaker profile is stored in a vendor-specific format, so it
 * can only be loaded into a recognizer that understands that format -
 * typically a recognizer from the same provider. Speaker profiles
 * are loaded with the 
 * speech_recognition_speaker_manager_read_vendor_speaker_profile() function.
 *
 * Note: The speaker profile is potentially large (up to several MBytes).
 *
 * See: speech_recognition_speaker_manager_read_vendor_speaker_profile()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occured.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_speaker_manager_write_vendor_speaker_profile(
                                    SpeechRecognitionSpeakerManager *manager,
                                    SpeechOutputStream *out_stream,
                                    SpeechRecognitionSpeakerProfile *speaker)
{
    printf("speech_recognition_speaker_manager_write_vendor_speaker_profile called.\n");
}
