
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_FINAL_DICTATION_RESULT_H__
#define __SPEECH_RECOGNITION_FINAL_DICTATION_RESULT_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_final_result.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_FINAL_DICTATION_RESULT              (speech_recognition_final_dictation_result_get_type())
#define SPEECH_RECOGNITION_FINAL_DICTATION_RESULT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_FINAL_DICTATION_RESULT, SpeechRecognitionFinalDictationResult))
#define SPEECH_RECOGNITION_FINAL_DICTATION_RESULT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_FINAL_DICTATION_RESULT, SpeechRecognitionFinalDictationResultClass))
#define SPEECH_IS_RECOGNITION_FINAL_DICTATION_RESULT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_FINAL_DICTATION_RESULT))
#define SPEECH_IS_RECOGNITION_FINAL_DICTATION_RESULT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_FINAL_DICTATION_RESULT))
#define SPEECH_RECOGNITION_FINAL_DICTATION_RESULT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_FINAL_DICTATION_RESULT, SpeechRecognitionFinalDictationResultClass))


GType 
speech_recognition_final_dictation_result_get_type(void);

SpeechRecognitionFinalDictationResult * 
speech_recognition_final_dictation_result_new(void);

SpeechStatus 
speech_recognition_final_dictation_result_get_alternative_tokens(
                                SpeechRecognitionFinalDictationResult *result,
                                SpeechRecognitionResultToken *from_token,
                                SpeechRecognitionResultToken *to_token,
                                int max,
                                SpeechRecognitionResultToken *results[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_FINAL_DICTATION_RESULT_H__ */
