
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		   See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_H__
#define __SPEECH_RECOGNITION_H__

#include <speech/recognition/speech_recognition_audio_clip.h>
#include <speech/recognition/speech_recognition_dictation_grammar.h>
#include <speech/recognition/speech_recognition_final_dictation_result.h>
#include <speech/recognition/speech_recognition_final_result.h>
#include <speech/recognition/speech_recognition_final_rule_result.h>
#include <speech/recognition/speech_recognition_grammar.h>
#include <speech/recognition/speech_recognition_grammar_event.h>
#include <speech/recognition/speech_recognition_grammar_exception.h>
#include <speech/recognition/speech_recognition_grammar_listener.h>
#include <speech/recognition/speech_recognition_grammar_syntax_detail.h>
#include <speech/recognition/speech_recognition_recognizer.h>
#include <speech/recognition/speech_recognition_recognizer_audio_event.h>
#include <speech/recognition/speech_recognition_recognizer_audio_listener.h>
#include <speech/recognition/speech_recognition_recognizer_event.h>
#include <speech/recognition/speech_recognition_recognizer_listener.h>
#include <speech/recognition/speech_recognition_recognizer_mode_desc.h>
#include <speech/recognition/speech_recognition_recognizer_properties.h>
#include <speech/recognition/speech_recognition_result.h>
#include <speech/recognition/speech_recognition_result_event.h>
#include <speech/recognition/speech_recognition_result_listener.h>
#include <speech/recognition/speech_recognition_result_token.h>
#include <speech/recognition/speech_recognition_rule.h>
#include <speech/recognition/speech_recognition_rule_alternatives.h>
#include <speech/recognition/speech_recognition_rule_count.h>
#include <speech/recognition/speech_recognition_rule_grammar.h>
#include <speech/recognition/speech_recognition_rule_name.h>
#include <speech/recognition/speech_recognition_rule_parse.h>
#include <speech/recognition/speech_recognition_rule_sequence.h>
#include <speech/recognition/speech_recognition_rule_tag.h>
#include <speech/recognition/speech_recognition_rule_token.h>
#include <speech/recognition/speech_recognition_speaker_manager.h>
#include <speech/recognition/speech_recognition_speaker_profile.h>

#endif /* __SPEECH_RECOGNITION_H__ */
