
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_NAME_H__
#define __SPEECH_RECOGNITION_RULE_NAME_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_rule.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE_NAME              (speech_recognition_rule_name_get_type())
#define SPEECH_RECOGNITION_RULE_NAME(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE_NAME, SpeechRecognitionRuleName))
#define SPEECH_RECOGNITION_RULE_NAME_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE_NAME, SpeechRecognitionRuleName))
#define SPEECH_IS_RECOGNITION_RULE_NAME(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE_NAME))
#define SPEECH_IS_RECOGNITION_RULE_NAME_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE_NAME))
#define SPEECH_RECOGNITION_RULE_NAME_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE_NAME, SpeechRecognitionRuleNameClass))

typedef struct _SpeechRecognitionRuleName            SpeechRecognitionRuleName;
typedef struct _SpeechRecognitionRuleNameClass       SpeechRecognitionRuleNameClass;

struct _SpeechRecognitionRuleName {
    SpeechRecognitionRule rule;
};

struct _SpeechRecognitionRuleNameClass {
    SpeechRecognitionRuleClass parent_class;
};


GType 
speech_recognition_rule_name_get_type(void);

SpeechRecognitionRuleName * 
speech_recognition_rule_name_new(void);

SpeechRecognitionRuleName * 
speech_recognition_rule_name_new_with_name(gchar *rule_name);

SpeechRecognitionRuleName * 
speech_recognition_rule_name_new_from_components(gchar *package_name,
                                                 gchar *simple_grammar_name,
                                                 gchar *simple_rule_name);

gchar * 
speech_recognition_rule_name_get_full_grammar_name(
                                  SpeechRecognitionRuleName *rule_name);

gchar * 
speech_recognition_rule_name_get_package_name(
                                  SpeechRecognitionRuleName *rule_name);

gchar * 
speech_recognition_rule_name_get_rule_name(
                                  SpeechRecognitionRuleName *rule_name);

gchar * 
speech_recognition_rule_name_get_simple_grammar_name(
                                  SpeechRecognitionRuleName *rule_name);

gchar * 
speech_recognition_rule_name_get_simple_rule_name(
                                  SpeechRecognitionRuleName *rule_name);

gboolean 
speech_recognition_rule_name_is_legal_rule_name(
                                  SpeechRecognitionRuleName *rule_name);

gboolean 
speech_recognition_rule_name_is_legal_rule_name_from_name(
                                  SpeechRecognitionRuleName *rule_name,
                                  gchar *name);

gboolean 
speech_recognition_rule_name_is_rule_name_part(
                                  SpeechRecognitionRuleName *rule_name,
                                  char c);

void 
speech_recognition_rule_name_set_rule_name(
                                  SpeechRecognitionRuleName *rule_name,
                                  gchar *ruleName);

SpeechStatus 
speech_recognition_rule_name_set_rule_name_with_components(
                                  SpeechRecognitionRuleName *rule_name,
                                  gchar *package_name,
                                  gchar *simple_grammar_name,
                                  gchar *simple_rule_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_NAME_H__ */
