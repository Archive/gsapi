
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_GRAMMAR_H__
#define __SPEECH_RECOGNITION_RULE_GRAMMAR_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_grammar.h>
#include <speech/recognition/speech_recognition_rule_name.h>
#include <speech/recognition/speech_recognition_rule_parse.h>
#include <speech/recognition/speech_recognition_final_rule_result.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE_GRAMMAR              (speech_recognition_rule_grammar_get_type())
#define SPEECH_RECOGNITION_RULE_GRAMMAR(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE_GRAMMAR, SpeechRecognitionRuleGrammar))
#define SPEECH_RECOGNITION_RULE_GRAMMAR_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE_GRAMMAR, SpeechRecognitionRuleGrammarClass))
#define SPEECH_IS_RECOGNITION_RULE_GRAMMAR(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE_GRAMMAR))
#define SPEECH_IS_RECOGNITION_RULE_GRAMMAR_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE_GRAMMAR))
#define SPEECH_RECOGNITION_RULE_GRAMMAR_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE_GRAMMAR, SpeechRecognitionRuleGrammarClass))


GType 
speech_recognition_rule_grammar_get_type(void);

SpeechRecognitionRuleGrammar * 
speech_recognition_rule_grammar_new(void);

void 
speech_recognition_rule_grammar_add_import(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     SpeechRecognitionRuleName *import_name);

SpeechStatus 
speech_recognition_rule_grammar_delete_rule(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name);

SpeechRecognitionRule * 
speech_recognition_rule_grammar_get_rule(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *s);

SpeechRecognitionRule * 
speech_recognition_rule_grammar_get_rule_internal(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name);

SpeechStatus 
speech_recognition_rule_grammar_is_enabled_with_rule_name(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name,
                                     gboolean *enabled);

SpeechStatus 
speech_recognition_rule_grammar_is_rule_public(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name,
                                     gboolean *public);

SpeechRecognitionRuleName ** 
speech_recognition_rule_grammar_list_imports(
                                     SpeechRecognitionRuleGrammar *grammar);

gchar ** 
speech_recognition_rule_grammar_list_rule_names(
                                     SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_rule_grammar_parse(SpeechRecognitionRuleGrammar *grammar,
                                      gchar *text,
		                      gchar *rule_name,
                                      SpeechRecognitionRuleParse *rule_parse);

SpeechStatus 
speech_recognition_rule_grammar_parse_nth_best(
                           SpeechRecognitionRuleGrammar *grammar,
                           SpeechRecognitionFinalRuleResult *final_rule_result,
                           int n_best,
                           gchar *rule_name,
                           SpeechRecognitionRuleParse *rule_parse);

SpeechStatus 
speech_recognition_rule_grammar_parse_token_sequence(
                             SpeechRecognitionRuleGrammar *grammar,
                             gchar *tokens[],
                             gchar *rule_name,
                             SpeechRecognitionRuleParse *rule_parse);

SpeechStatus 
speech_recognition_rule_grammar_remove_import(
                             SpeechRecognitionRuleGrammar *grammar,
                             SpeechRecognitionRuleName *import_name);

SpeechStatus 
speech_recognition_rule_grammar_resolve(SpeechRecognitionRuleGrammar *grammar,
                                        SpeechRecognitionRuleName *rule_name,
                                        SpeechRecognitionRuleName *result);

SpeechStatus 
speech_recognition_rule_grammar_rule_for_grammar(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *grammar_text,
                                     SpeechRecognitionRule *rule);

SpeechStatus 
speech_recognition_rule_grammar_set_enabled_for_rule(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name,
                                     gboolean enabled);

SpeechStatus 
speech_recognition_rule_grammar_set_enabled_for_rules(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_names[],
                                     gboolean enabled);

SpeechStatus 
speech_recognition_rule_grammar_set_rule(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name,
                                     SpeechRecognitionRule rule,
                                     gboolean is_public);

gchar * 
speech_recognition_rule_grammar_to_string(
                                     SpeechRecognitionRuleGrammar *grammar);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_GRAMMAR_H__ */
