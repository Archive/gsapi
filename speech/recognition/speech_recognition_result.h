
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RESULT_H__
#define __SPEECH_RECOGNITION_RESULT_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_result_listener.h>
#include <speech/recognition/speech_recognition_grammar.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RESULT              (speech_recognition_result_get_type())
#define SPEECH_RECOGNITION_RESULT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RESULT, SpeechRecognitionResult))
#define SPEECH_RECOGNITION_RESULT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RESULT, SpeechRecognitionResultClass))
#define SPEECH_IS_RECOGNITION_RESULT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RESULT))
#define SPEECH_IS_RECOGNITION_RESULT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RESULT))
#define SPEECH_RECOGNITION_RESULT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RESULT, SpeechRecognitionResultClass))

#include <speech/recognition/speech_recognition_result_token.h>

/*
 * speech_recognition_result_get_result_state() returns 
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED while a result is still 
 * being recognized. A #SpeechRecognitionResult is in the 
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state when the
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event is issued.
 * Result states are described above in detail.
 *
 * See: speech_recognition_result_get_result_state()
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 */

static int SPEECH_RECOGNITION_RESULT_UNFINALIZED = 300;

/*
 * speech_recognition_result_get_result_state() returns 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED once recognition of the result 
 * is completed and the #SpeechRecognitionResult object has been 
 * finalized by being accepted. When a #SpeechRecognitionResult changes 
 * to the #SPEECH_RECOGNITION_RESULT_ACCEPTED state a
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event is issued.
 * Result states are described above in detail.
 *
 * See: speech_recognition_result_get_result_state()
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED
 */

static int SPEECH_RECOGNITION_RESULT_ACCEPTED = 301;

/*
 * speech_recognition_result_get_result_state() returns 
 * #SPEECH_RECOGNITION_RESULT_REJECTED once recognition of the result 
 * complete and the #SpeechRecognitionResult object has been finalized 
 * by being rejected. When a #SpeechRecognitionResult changes to the 
 * #SPEECH_RECOGNITION_RESULT_REJECTED state a
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event is issued.
 * Result states are described above in detail.
 *
 * See: speech_recognition_result_get_result_state()
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED
 */

static int SPEECH_RECOGNITION_RESULT_REJECTED = 302;


GType 
speech_recognition_result_get_type(void);

SpeechRecognitionResult * 
speech_recognition_result_new(void);

void 
speech_recognition_result_add_result_listener(
                              SpeechRecognitionResult *result,
                              SpeechRecognitionResultListener *listener);

void 
speech_recognition_result_remove_result_listener(
                              SpeechRecognitionResult *result,
                              SpeechRecognitionResultListener *listener);

SpeechStatus 
speech_recognition_result_get_best_token(int tok_num,
                              SpeechRecognitionResult *result,
                              SpeechRecognitionResultToken *token);

SpeechRecognitionResultToken ** 
speech_recognition_result_get_best_tokens(SpeechRecognitionResult *result);

SpeechRecognitionGrammar * 
speech_recognition_result_get_grammar(SpeechRecognitionResult *result);

int 
speech_recognition_result_get_result_state(SpeechRecognitionResult *result);

SpeechRecognitionResultToken ** 
speech_recognition_result_get_unfinalized_tokens(
                              SpeechRecognitionResult *result);

int 
speech_recognition_result_num_tokens(SpeechRecognitionResult *result);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RESULT_H__ */
