
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RESULT_EVENT_H__
#define __SPEECH_RECOGNITION_RESULT_EVENT_H__

#include <speech/speech_event.h>
#include <speech/recognition/speech_recognition_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RESULT_EVENT              (speech_recognition_result_event_get_type())
#define SPEECH_RECOGNITION_RESULT_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RESULT_EVENT, SpeechRecognitionResultEvent))
#define SPEECH_RECOGNITION_RESULT_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RESULT_EVENT, SpeechRecognitionResultEventClass))
#define SPEECH_IS_RECOGNITION_RESULT_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RESULT_EVENT))
#define SPEECH_IS_RECOGNITION_RESULT_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RESULT_EVENT))
#define SPEECH_RECOGNITION_RESULT_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RESULT_EVENT, SpeechRecognitionResultEventClass))

typedef struct _SpeechRecognitionResultEvent            SpeechRecognitionResultEvent;
typedef struct _SpeechRecognitionResultEventClass       SpeechRecognitionResultEventClass;

struct _SpeechRecognitionResultEvent {
    SpeechEvent event;
};

struct _SpeechRecognitionResultEventClass {
    SpeechEventClass parent_class;
};


/*
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED is issued when a new
 * #SpeechRecognitionResult is created. The event is received by each
 * #SpeechRecognitionResultListener attached to the 
 * #SpeechRecognitionRecognizer.
 *
 * When a result is created, it is in the 
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state. When created the result 
 * may have zero or more finalized tokens and zero or more unfinalized 
 * tokens. The presence of finalized and unfinalized tokens is indicated 
 * by the #isTokenFinalized and #isUnfinalizedTokensChanged flags.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event follows the
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING event which 
 * transitions the #SpeechRecognitionRecognizer from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state.
 *
 * See: speech_recognition_result_listener_result_created()
 * See: #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 * See: speech_recognition_result_event_is_token_finalized()
 * See: speech_recognition_result_event_is_unfinalized_tokens_changed()
 */

static int SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED = 801;

/*
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED is issued when one or 
 * more tokens of a #SpeechRecognitionResult are finalized or when the 
 * unfinalized tokens of a result are changed. The #isTokenFinalized
 * and #isUnfinalizedTokensChanged flags are set appropriately.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED event only occurs 
 * when a #SpeechRecognitionResult is in the 
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state.
 *
 * See: speech_recognition_result_event_is_token_finalized()
 * See: speech_recognition_result_event_is_unfinalized_tokens_changed()
 * See: #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 * See: speech_recognition_result_listener_result_updated()
 */

static int SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED = 802;

/*
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED is issued when the
 * #SpeechRecognitionGrammar matched by a #SpeechRecognitionResult is
 * identified and finalized. Before this event the
 * speech_recognition_result_get_grammar() function of a 
 * #SpeechRecognitionResult returns %NULL. Following the event it is 
 * guaranteed to return non-null and the grammar is guaranteed not to change.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event only occurs 
 * for a #SpeechRecognitionResult in the 
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event does not 
 * affect finalized or unfinalized tokens.
 *
 * See: speech_recognition_result_event_is_token_finalized()
 * See: speech_recognition_result_event_is_unfinalized_tokens_changed()
 * See: #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 * See: speech_recognition_result_listener_grammar_finalized()
 */

static int SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED = 803;

/*
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event is issued when a
 * #SpeechRecognitionResult is successfully finalized and indicates a
 * state change from #SPEECH_RECOGNITION_RESULT_UNFINALIZED to 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED.
 *
 * In the finalization transition, zero or more tokens may be
 * finalized and the unfinalized tokens are set to %NULL. The
 * speech_recognition_result_event_is_token_finalized() and
 * speech_recognition_result_event_is_unfinalized_tokens_changed() 
 * flags are set appropriately.
 *
 * Since the #SpeechRecognitionResult is finalized (accepted), the
 * functions of #SpeechRecognitionFinalResult and either
 * #SpeechRecognitionFinalRuleResult or #SpeechRecognitionFinalDictationResult
 * can be used. (Use the speech_recognition_result_get_grammar() function
 * of #SpeechRecognitionResult to determine the type of grammar matched).
 * Applications should use type casting to ensure that only the
 * appropriate objects and functions are used.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event is issued 
 * after the #SpeechRecognitionRecognizer issues a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event to 
 * transition from the #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state to
 * the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state. Any changes made 
 * to grammars or the enabled state of grammars during the processing of the
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event are automatically 
 * committed once the #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event 
 * has been processed by all #SpeechRecognitionResultListener's.
 * Once those changes have been committed, the #SpeechRecognitionRecognizer
 * returns to the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state with a
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event. A call to
 * speech_recognition_recognizer_commit_changes() is not required. (Except,
 * if there is a call to speech_recognition_recognizer_suspend() without a 
 * subsequent call to speech_recognition_recognizer_commit_changes(), the
 * #SpeechRecognitionRecognizer defers the commit until the 
 * speech_recognition_recognizer_commit_changes() call is received).
 *
 * See: speech_recognition_result_listener_result_accepted()
 * See: speech_recognition_result_event_is_token_finalized()
 * See: speech_recognition_result_event_is_unfinalized_tokens_changed()
 * See: speech_recognition_result_get_result_state()
 * See: speech_recognition_result_get_grammar()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 */

static int SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED = 804;

/*
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event is issued when a
 * #SpeechRecognitionResult is unsuccessfully finalized and indicates
 * a change from the #SPEECH_RECOGNITION_RESULT_UNFINALIZED state to the
 * #SPEECH_RECOGNITION_RESULT_REJECTED state.
 *
 * In the state transition, zero or more tokens may be finalized and
 * the unfinalized tokens are set to %NULL. The
 * speech_recognition_result_event_is_token_finalized() and
 * speech_recognition_result_event_is_unfinalized_tokens_changed() flags 
 * are set appropriately. However, because the result is rejected, the
 * tokens are quite likely to be incorrect.
 *
 * Since the #SpeechRecognitionResult is finalized (rejected), the
 * functions of #SpeechRecognitionFinalResult can be used. If the grammar
 * is known (#SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event 
 * was issued and the speech_recognition_result_get_grammar() function 
 * returns non-null) then the #SpeechRecognitionFinalRuleResult or 
 * #SpeechRecognitionFinalDictationResult object can also be used 
 * depending upon whether the matched grammar was a 
 * #SpeechRecognitionRuleGrammar or #SpeechRecognitionDictationGrammar.
 *
 * Other state transition behavior for 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTEDRESULT_REJECTED
 * is the same as for the #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED
 * event.
 *
 * See: speech_recognition_result_event_is_token_finalized()
 * See: speech_recognition_result_event_is_unfinalized_tokens_changed()
 * See: speech_recognition_result_get_result_state()
 * See: speech_recognition_result_get_grammar()
 * See: speech_recognition_result_listener_result_rejected()
 */

static int SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED = 805;

/*
 * #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED event is issued when 
 * the audio information associated with a #SpeechRecognitionFinalResult
 * object is released. The release may have been requested by an application 
 * call to speech_recognition_final_result_release_audio() in the
 * #SpeechRecognitionFinalResult object or may be initiated by the
 * recognizer to reclaim memory. The
 * speech_recognition_final_result_is_audio_available() function returns 
 * %FALSE after this event.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED event is only 
 * issued for results in a finalized state 
 * (speech_recognition_result_get_result_state() returns either
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or
 * SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED).
 *
 * See: speech_recognition_final_result_release_audio()
 * See: speech_recognition_final_result_is_audio_available()
 * See: speech_recognition_result_get_result_state()
 * See: speech_recognition_result_listener_audio_released()
 */

static int SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED = 806;

/*
 * #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event is 
 * issued when the training information for a finaliized result is 
 * released. The release may have been requested by an application 
 * call to the speech_recognition_final_result_release_training_info()
 * function in the #SpeechRecognitionFinalResult object or may be 
 * initiated by the recognizer to reclaim memory. The
 * speech_recognition_final_result_is_training_info_available() function
 * of #SpeechRecognitionFinalResult returns %FALSE after this event.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event is 
 * only issued for results in a finalized state 
 * speech_recognition_result_get_result_state() returns either 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or
 * SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED)
 *
 * See: speech_recognition_final_result_release_training_info()
 * See: speech_recognition_final_result_is_training_info_available()
 * See: speech_recognition_result_get_result_state()
 * See: speech_recognition_result_listener_training_info_released()
 */

static int SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED = 807;


GType 
speech_recognition_result_event_get_type(void);

SpeechRecognitionResultEvent * 
speech_recognition_result_event_new(SpeechRecognitionResult *source,
                                    int id);

SpeechRecognitionResultEvent * 
speech_recognition_result_event_new_with_state(
                                     SpeechRecognitionResult *source,
                                     int id,
                                     gboolean is_token_finalized,
                                     gboolean is_unfinalized_tokens_changed);

gboolean 
speech_recognition_result_event_is_token_finalized(
                                     SpeechRecognitionResultEvent *event);

gboolean 
speech_recognition_result_event_is_unfinalized_tokens_changed(
                                     SpeechRecognitionResultEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RESULT_EVENT_H__ */
