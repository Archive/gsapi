
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_GRAMMAR_EVENT_H__
#define __SPEECH_RECOGNITION_GRAMMAR_EVENT_H__

#include <speech/speech_event.h>
#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_grammar.h>
#include <speech/recognition/speech_recognition_grammar_exception.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_GRAMMAR_EVENT              (speech_recognition_grammar_event_get_type())
#define SPEECH_RECOGNITION_GRAMMAR_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_EVENT, SpeechRecognitionGrammarEvent))
#define SPEECH_RECOGNITION_GRAMMAR_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR_EVENT, SpeechRecognitionGrammarEventClass))
#define SPEECH_IS_RECOGNITION_GRAMMAR_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_EVENT))
#define SPEECH_IS_RECOGNITION_GRAMMAR_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR_EVENT))
#define SPEECH_RECOGNITION_GRAMMAR_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_EVENT, SpeechRecognitionGrammarEventClass))

typedef struct _SpeechRecognitionGrammarEventClass       SpeechRecognitionGrammarEventClass;

struct _SpeechRecognitionGrammarEvent {
    SpeechEvent event;
};

struct _SpeechRecognitionGrammarEventClass {
    SpeechEventClass parent_class;
};


/*
 * A #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event is 
 * issued when a #SpeechRecognitionRecognizer completes committing changes 
 * to a #SpeechRecognitionGrammar. The event is issued immediately following
 * the #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event 
 * that is issued to #SpeechRecognitionRecognizerListener's. That event 
 * indicates that changes have been applied to all grammars of a
 * #SpeechRecognitionRecognizer. The 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event is 
 * specific to each individual grammar.
 *
 * The event is issued when the definition of the grammar is changed,
 * when its #enabled property is changed, or both. The #enabled_changed
 * and #definition_changed flags are set accordingly.
 *
 * A #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event can 
 * be triggered without an explicit call to 
 * speech_recognition_recognizer_commit_changes() - there is usually an 
 * implicit speech_recognition_recognizer_commit_changes() at the completion
 * of result finalization event processing. If any syntactic or logical 
 * errors are detected for a #SpeechRecognitionGrammar during the commit, 
 * the generated #SpeechRecognitionGrammarException is included with this 
 * event. If no problem is found the value is %NULL.
 *
 * See: speech_recognition_grammar_listener_grammar_changes_committed()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * See: speech_recognition_recognizer_commit_changes()
 * See: speech_recognition_grammar_event_get_definition_changed()
 * See: speech_recognition_grammar_event_get_enabled_changed()
 * See: speech_recognition_grammar_event_get_grammar_exception()
 */

static int SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED = 200;

/*
 * A #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED event is issued 
 * when a grammar changes state from deactivated to activated. The
 * speech_recognition_grammar_is_active() function of the 
 * #SpeechRecognitionGrammar will now return %TRUE.
 *
 * Grammar activation changes follow one of two
 * #SpeechRecognitionRecognizerEvents:
 * <orderedlist>
 *   <listitem>a #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED 
 *       event in which a grammar's #enabled flag is set %TRUE or
 *   <listitem>a #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED event. 
 *       The full details of the activation conditions under which a 
 *       #SpeechRecognitionGrammar is activated are described in the 
 *       documentation for the #SpeechRecognitionGrammar object.
 * </orderedlist>
 *
 * See: speech_recognition_grammar_listener_grammar_activated()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED
 */

static int SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED = 201;

/*
 * A #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED event is issued 
 * when a grammar changes state from activated to deactivated. The
 * speech_recognition_grammar_is_active() function of the 
 * #SpeechRecognitionGrammar will now return %FALSE.
 *
 * Grammar deactivation changes follow one of two
 * #SpeechRecognitionRecognizerEvents:
 * <orderedlist>
 *   <listitem>a #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 *       event in which a grammar's @enabled flag is set %FALSE or
 *   <listitem>a #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event. 
 *       The full details of the activation conditions under which a 
 *       #SpeechRecognitionGrammar is deactivated are described in the 
 *       documentation for the #SpeechRecognitionGrammar object.
 * </orderedlist>
 *
 * See: speech_recognition_grammar_listener_grammar_deactivated()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST
 */

static int SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED = 202;


GType 
speech_audio_event_get_type(void);

SpeechRecognitionGrammarEvent * 
speech_recognition_grammar_event_new(SpeechRecognitionGrammar *source,
                                     int id);

SpeechRecognitionGrammarEvent * 
speech_recognition_grammar_event_new_with_state(
                         SpeechRecognitionGrammar *source,
                         int id,
                         gboolean enabled_changed,
                         gboolean definition_changed,
                         SpeechRecognitionGrammarException *grammar_exception);

gboolean 
speech_recognition_grammar_event_get_definition_changed(
                         SpeechRecognitionGrammarEvent *event);

gboolean 
speech_recognition_grammar_event_get_enabled_changed(
                         SpeechRecognitionGrammarEvent *event);

SpeechRecognitionGrammarException * 
speech_recognition_grammar_event_get_grammar_exception(
                         SpeechRecognitionGrammarEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_GRAMMAR_EVENT_H__ */
