
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_COUNT_H__
#define __SPEECH_RECOGNITION_RULE_COUNT_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_rule.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE_COUNT              (speech_recognition_rule_count_get_type())
#define SPEECH_RECOGNITION_RULE_COUNT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE_COUNT, SpeechRecognitionRuleCount))
#define SPEECH_RECOGNITION_RULE_COUNT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE_COUNT, SpeechRecognitionRuleCount))
#define SPEECH_IS_RECOGNITION_RULE_COUNT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE_COUNT))
#define SPEECH_IS_RECOGNITION_RULE_COUNT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE_COUNT))
#define SPEECH_RECOGNITION_RULE_COUNT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE_COUNT, SpeechRecognitionRuleCountClass))

typedef struct _SpeechRecognitionRuleCount            SpeechRecognitionRuleCount;
typedef struct _SpeechRecognitionRuleCountClass       SpeechRecognitionRuleCountClass;

struct _SpeechRecognitionRuleCount {
    SpeechRecognitionRule rule;
};

struct _SpeechRecognitionRuleCountClass {
    SpeechRecognitionRuleClass parent_class;
};


/*
 * #SPEECH_RECOGNITION_RULE_COUNT_OPTIONAL indicates that the 
 * #SpeechRecognitionRule is optional: zero or one occurrences. 
 * An optional #SpeechRecognitionRule is surrounded by "[]" in 
 * Java Speech Grammar Format.
 *
 * See: speech_recognition_rule_count_get_count()
 */

static int SPEECH_RECOGNITION_RULE_COUNT_OPTIONAL = 2;

/*
 * #SPEECH_RECOGNITION_RULE_COUNT_ONCE_OR_MORE indicates that the 
 * #SpeechRecognitionRule may be spoken one or more times. This is 
 * indicated by the "+" operator in Java Speech Grammar Format.
 *
 * See: speech_recognition_rule_count_get_count()
 */

static int SPEECH_RECOGNITION_RULE_COUNT_ONCE_OR_MORE = 3;

/*
 * #SPEECH_RECOGNITION_RULE_COUNT_ZERO_OR_MORE indicates that the 
 * #SpeechRecognitionRule may be spoken zero or more times. This 
 * is indicated by the "*" operator in Java Speech Grammar Format.
 *
 * See: speech_recognition_rule_count_get_count()
 */

static int SPEECH_RECOGNITION_RULE_COUNT_ZERO_OR_MORE = 4;


GType 
speech_recognition_rule_count_get_type(void);

SpeechRecognitionRuleCount * 
speech_recognition_rule_count_new(void);

SpeechRecognitionRuleCount * 
speech_recognition_rule_count_new_with_rule(SpeechRecognitionRule *rule,
                                            int count);

int 
speech_recognition_rule_count_get_count(SpeechRecognitionRuleCount *rule_count);

void 
speech_recognition_rule_count_set_count(SpeechRecognitionRuleCount *rule_count,
                                        int count);

SpeechRecognitionRule * 
speech_recognition_rule_count_get_rule(SpeechRecognitionRuleCount *rule_count);

void 
speech_recognition_rule_count_set_rule(SpeechRecognitionRuleCount *rule_count,
                                       SpeechRecognitionRule *rule);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_COUNT_H__ */
