
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_SPEAKER_PROFILE_H__
#define __SPEECH_RECOGNITION_SPEAKER_PROFILE_H__

#include <speech/recognition/speech_recognition_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_SPEAKER_PROFILE              (speech_recognition_speaker_profile_get_type())
#define SPEECH_RECOGNITION_SPEAKER_PROFILE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_SPEAKER_PROFILE, SpeechRecognitionSpeakerProfile))
#define SPEECH_RECOGNITION_SPEAKER_PROFILE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_SPEAKER_PROFILE, SpeechRecognitionSpeakerProfileClass))
#define SPEECH_IS_RECOGNITION_SPEAKER_PROFILE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_SPEAKER_PROFILE))
#define SPEECH_IS_RECOGNITION_SPEAKER_PROFILE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_SPEAKER_PROFILE))
#define SPEECH_RECOGNITION_SPEAKER_PROFILE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_SPEAKER_PROFILE, SpeechRecognitionSpeakerProfileClass))

typedef struct _SpeechRecognitionSpeakerProfile            SpeechRecognitionSpeakerProfile;
typedef struct _SpeechRecognitionSpeakerProfileClass       SpeechRecognitionSpeakerProfileClass;

struct _SpeechRecognitionSpeakerProfile {
    SpeechObject object;
};

struct _SpeechRecognitionSpeakerProfileClass {
    SpeechObjectClass parent_class;
};


GType 
speech_recognition_speaker_profile_get_type(void);

SpeechRecognitionSpeakerProfile * 
speech_recognition_speaker_profile_new(void);

gboolean 
speech_recognition_speaker_profile_equals(
                               SpeechRecognitionSpeakerProfile *profile,
                               SpeechRecognitionSpeakerProfile *an_object);

gchar * 
speech_recognition_speaker_profile_get_id(
                               SpeechRecognitionSpeakerProfile *profile);

SpeechStatus 
speech_recognition_speaker_profile_set_id(
                               SpeechRecognitionSpeakerProfile *profile,
                               gchar *identifier);

gchar * 
speech_recognition_speaker_profile_get_name(
                               SpeechRecognitionSpeakerProfile *profile);

void 
speech_recognition_speaker_profile_set_name(
                               SpeechRecognitionSpeakerProfile *profile,
                               gchar *name);

gchar * 
speech_recognition_speaker_profile_get_variant(
                               SpeechRecognitionSpeakerProfile *profile);

void 
speech_recognition_speaker_profile_set_variant(
                               SpeechRecognitionSpeakerProfile *profile,
                               gchar *variant);

gboolean 
speech_recognition_speaker_profile_match(
                               SpeechRecognitionSpeakerProfile *profile,
                               SpeechRecognitionSpeakerProfile *require);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_SPEAKER_PROFILE_H__ */
