
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechRecognitionRuleGrammar object describes a #SpeechRecognitionGrammar
 * that defines what users may say by a set rules. The rules may be
 * defined as #SpeechRecognitionRule objects that represent the rule in a
 * data structure or defined in the supported grammar format.
 *
 * All #SpeechRecognitionRuleGrammars are created and managed through the
 * #SpeechRecognitionRecognizer object. A #SpeechRecognitionRuleGrammar may be
 * created with the speech_recognition_recognizer_new_rule_grammar() function.
 * A #SpeechRecognitionRuleGrammar is also created when grammar formatted
 * text is loaded with the #loadMarkup functions either from a 
 * #SpeechInputStream or from a #SpeechGrammarURL.
 *
 * A #SpeechRecognitionRuleGrammar contains the same information as a grammar
 * definition in the supported grammar format. That information includes:
 *
 * <itemizedlist>
 *   <listitem>The name of the grammar (inherited from 
 *       #SpeechRecognitionGrammar object),</listitem>
 *   <listitem>A set of imports: each import references a single public rule
 *	 of another #SpeechRecognitionRuleGrammar or all public rules of
 *	 another #SpeechRecognitionRuleGrammar.</listitem>
 *   <listitem>A set of defined rules: each definition is identified by 
 *       a unique rulename (unique within the #SpeechRecognitionRuleGrammar),
 *       a boolean flag indicating whether the rule is public, and a 
 *       #SpeechRecognitionRule object that provides the logical expansion 
 *       of the rule (how it spoken).</listitem>
 * </itemizedlist>
 *
 * The set of imports and the rule definitions can be changed by applications.
 * For any change to take effect the application must call the
 * speech_recognition_recognizer_commit_changes() function of the 
 * #SpeechRecognitionRecognizer.
 *
 * A #SpeechRecognitionRuleGrammar can be printed in Java Speech Grammar
 * Format using the speech_recognition_rule_grammar_to_string() function.
 * Individual #SpeechRecognitionRule objects can be converted to grammar 
 * format with their speech_recognition_rule_XXXX_to_string() functions.
 *
 * Note that in JSGF a rulename is surrounded by angle brackets (e.g.
 * &lt;rulename&gt;). The angle brackets are ignored in calls to the functions
 * of a #SpeechRecognitionRuleGrammar - they may be included but are stripped
 * automatically and are not included in rulenames returned by
 * #SpeechRecognitionRuleGrammar functions.
 *
 * The rules defined in a #SpeechRecognitionRuleGrammar are either public or
 * private. A public rule may be:
 *
 * <orderedlist>
 *   <listitem>Imported into other #SpeechRecognitionRuleGrammars,</listitem>
 *   <listitem>Enabled for recognition,</listitem>
 *   <listitem>Or both.</listitem>
 * </orderedlist>
 *
 * When a #SpeechRecognitionRuleGrammar is enabled and when the activation
 * conditions are appropriate (as described in the documentation for
 * #SpeechRecognitionGrammar) then the #SpeechRecognitionGrammar is 
 * activated and any of the public rules of the grammar may be spoken. The
 * #SpeechRecognitionRuleGrammar object extends the enable functions of the
 * #SpeechRecognitionGrammar object to allow individual rules to be enabled
 * and disabled independently.
 *
 * A public rule may reference private rules and imported rules. Only the top
 * public rule needs to be enabled for it to be spoken. The referenced rules
 * (local private or imported public rules) do not need to be enabled to be
 * spoken as part of the enabled rule.
 *
 * See: #SpeechRecognitionRecognizer
 * See: #SpeechRecognitionRule
 * See: #SpeechRecognitionGrammar
 * See: Java Speech Grammar Format
 */

#include <speech/recognition/speech_recognition_rule_grammar.h>


GType
speech_recognition_rule_grammar_get_type(void)
{
    printf("speech_recognition_rule_grammar_get_type called.\n");
}


SpeechRecognitionRuleGrammar *
speech_recognition_rule_grammar_new(void)
{
    printf("speech_recognition_rule_grammar_new called.\n");
}


/**
 * speech_recognition_rule_grammar_add_import:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @import_name: the name of the rule to import.
 *
 * Import all rules or a specified rule from another grammar.
 * The #SpeechRecognitionRuleName should be in one these forms:
 *
 *   &lt;package.grammar.ruleName&gt;
 *   &lt;package.grammar.*&gt;
 *   &lt;grammar.ruleName&gt;
 *   &lt;grammar.*&gt;
 *
 * which are equivalent to the following declarations in the Java Speech 
 * Grammar Format.
 *
 *   // import all public rules of a grammar
 *   import &lt;package.grammar.*&gt;
 *   import &lt;grammar.*&gt;
 *
 *   // import a specific public rule name of a grammar
 *   import &lt;package.grammar.ruleName&gt;
 *   import &lt;grammar.ruleName&gt;
 *
 * The forms without a package are only legal when the full grammar
 * name does not include a package name.
 *
 * speech_recognition_rule_grammar_add_import() takes effect when grammar 
 * changes are committed. When changes are committed, all imports must be 
 * resolvable. Specifically, every #SpeechRecognitionRuleGrammar listed in  
 * an import must exist, and every fully qualified rulename listed in an 
 * import must exist. If any omissions are found, the
 * speech_recognition_recognizer_commit_changes() function returns an 
 * exception and the changes do not take effect.
 *
 * It is not an exception to import a rule or set of rules and not
 * reference them.
 *
 * See: speech_recognition_rule_grammar_list_imports()
 * See: speech_recognition_rule_grammar_remove_import()
 * See: speech_recognition_recognizer_commit_changes()
 */

void
speech_recognition_rule_grammar_add_import(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     SpeechRecognitionRuleName *import_name)
{
    printf("speech_recognition_rule_grammar_add_import called.\n");
}


/**
 * speech_recognition_rule_grammar_delete_rule:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: name of the defined rule to be deleted.
 *
 * Delete a rule from the grammar. The deletion only takes effect when
 * grammar changes are next committed.
 *
 * If the rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then the brackets are ignored.
 *
 * See: speech_recognition_recognizer_commit_changes()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if ruleName is unknown. 
 *       </listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_delete_rule(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name)
{
    printf("speech_recognition_rule_grammar_delete_rule called.\n");
}


/**
 * speech_recognition_rule_grammar_get_rule:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: the rule name to be returned.
 *
 * Returns a #SpeechRecognitionRule object for a specified rulename.
 * Returns %NULL if the rule is unknown. The returned object is
 * a copy of the recognizer's internal object so it can be
 * modified in any way without affecting the recognizer. The
 * speech_recognition_rule_grammar_set_rule() function should be 
 * called when a change to the returned rule object needs to be 
 * applied to the recognizer.
 *
 * The speech_recognition_rule_to_string() function can be used to 
 * convert the return object to a printable string in Java Speech 
 * Grammar Format.
 *
 * If there is a rule structure currently pending for a
 * speech_recognition_recognizer_commit_changes() that structure is 
 * returned. Otherwise, the current structure being used by the 
 * recognizer on incoming speech is returned.
 *
 * If the rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then the brackets are ignored.
 *
 * If fast, read-only access to the rule object is required (e.g. in
 * parsing), then the application may use 
 * speech_recognition_rule_grammar_get_rule_internal()
 *
 * See: speech_recognition_rule_grammar_set_rule()
 * See: speech_recognition_rule_grammar_get_rule_internal()
 * See: speech_recognition_recognizer_commit_changes()
 *
 * Returns: the definition of @rule_name or %NULL.
 */

SpeechRecognitionRule *
speech_recognition_rule_grammar_get_rule(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name)
{
    printf("speech_recognition_rule_grammar_get_rule called.\n");
}


/**
 * speech_recognition_rule_grammar_get_rule_internal:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: the rule name to be returned.
 *
 * Returns a reference to a recognizer's internal rule object
 * identified by a rule name. The application should never modify
 * the returned object. This function is intented for use by parsers
 * and other software that needs to quickly analyse a recognizer's
 * grammars without modifying them (without the overhead of making
 * copies, as required by speech_recognition_rule_grammar_get_rule().
 * If the returned object is ever modified in any way, 
 * speech_recognition_rule_grammar_get_rule() and
 * speech_recognition_rule_grammar_set_rule() should be used.
 *
 * Returns %NULL if the rule is unknown.
 *
 * If there is a #SpeechRecognitionRule structure currently pending for
 * a speech_recognition_recognizer_commit_changes() that structure is 
 * returned. Otherwise, the current structure being used by the recognizer
 * on incoming speech is returned.
 *
 * If the rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then the brackets are ignored.
 *
 * See: speech_recognition_rule_grammar_set_rule()
 * See: speech_recognition_rule_grammar_get_rule()
 * See: speech_recognition_recognizer_commit_changes()
 */

SpeechRecognitionRule *
speech_recognition_rule_grammar_get_rule_internal(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name)
{
    printf("speech_recognition_rule_grammar_get_rule_internal called.\n");
}


/**
 * speech_recognition_rule_grammar_is_enabled_with_rule_name:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: the name of the rule being tested.
 * @enabled: return %TRUE if @rule_name is enabled, otherwise %FALSE.
 *
 * Test whether recognition of a specified rule of this
 * #SpeechRecognitionRuleGrammar is enabled. If @rule_name is %NULL,
 * the function is equivalent to speech_recognition_grammar_is_enabled().
 *
 * If the rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then the brackets are ignored.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if @rule_name is unknown 
 *       or if it is a non-public rule.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_is_enabled_with_rule_name(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name,
                                     gboolean *enabled)
{
    printf("speech_recognition_rule_grammar_is_enabled_with_rule_name called.\n");
}


/**
 * speech_recognition_rule_grammar_is_rule_public:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: the name of the rule being tested.
 * @public: return %TRUE if @rule_name is public.
 *
 * Test whether a rule is public. Public rules may be enabled to be
 * activated for recognition and/or may be imported into other
 * #SpeechRecognitionRuleGrammars.
 *
 * If the rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then the brackets are ignored.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if @rule_name is 
 *       unknown.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_is_rule_public(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name,
                                     gboolean *public)
{
    printf("speech_recognition_rule_grammar_is_rule_public called.\n");
}


/**
 * speech_recognition_rule_grammar_list_imports:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 *
 * Return a list of the current imports.
 * Returns zero length array if no #SpeechRecognitionRuleGrammars are
 * imported.
 *
 * See: speech_recognition_rule_grammar_add_import()
 * See: speech_recognition_rule_grammar_remove_import()
 *
 * Returns: a list of the current imports
 */

SpeechRecognitionRuleName **
speech_recognition_rule_grammar_list_imports(
                                     SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_rule_grammar_list_imports called.\n");
}


/**
 * speech_recognition_rule_grammar_list_rule_names:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 *
 * List the names of all rules defined in this
 * #SpeechRecognitionRuleGrammar. If any rules are pending deletion
 * they are not listed (between a call to 
 * speech_recognition_rule_grammar_delete_rule() and a 
 * speech_recognition_recognizer_commit_changes() taking effect).
 *
 * The returned names do not include the &lt;&gt; symbols.
 *
 * Returns: the names of all rules defined in this
 *	   #SpeechRecognitionRuleGrammar.
 */

gchar **
speech_recognition_rule_grammar_list_rule_names(
                                     SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_rule_grammar_list_rule_names called.\n");
}


/**
 * speech_recognition_rule_grammar_parse:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @text: the text string to parse.
 * @rule_name: the rule to parse against, or %NULL.
 * @rule_parse: returna a #RuleParse object.
 *
 * Parse a string against a #SpeechRecognitionRuleGrammar. Parsing is
 * the process of matching the text against the rules that are
 * defined in the grammar. The text is tokenized as a Java Speech
 * Grammar Format string (white-spacing and quotes are significant).
 *
 * The string is parsed against the rule identified by @rule_name, 
 * which may identify any defined rule of this rule grammar (public 
 * or private, enabled or disabled). If the @rule_name is %NULL, the
 * string is parsed against all enabled rules of the grammar.
 *
 * The function returns a #SpeechRecognitionRuleParse object. The
 * documentation #SpeechRecognitionRuleParse describes how the parse
 * structure and grammar structure correspond.
 *
 * If parse fails, then the return value is %NULL. To succeed, the
 * parse must match the complete input string.
 *
 * For some grammars and strings, multiple parses are legal. The parse
 * function returns only one. It is not defined which of the legal
 * parses should be returned. Development tools will help to analyse
 * grammars for such ambiguities. Also grammar libraries can be used
 * to parse results to check for these ambiguities.
 *
 * See: speech_recognition_rule_grammar_parse_token_sequence()
 * See: speech_recognition_rule_grammar_parse_nth_best()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if an error is found 
 *       in the definition of the #SpeechRecognitionRuleGrammar.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_parse(SpeechRecognitionRuleGrammar *grammar,
                                      gchar *text,
	                              gchar *rule_name,
                                      SpeechRecognitionRuleParse *rule_parse)
{
    printf("speech_recognition_rule_grammar_parse called.\n");
}


/**
 * speech_recognition_rule_grammar_parse_nth_best:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @final_rule_result: the #SpeechRecognitionFinalRuleResult to parse.
 * @n_best: the nth best value.
 * @rule_name: the rule to parse against, or %NULL.
 * @rule_parse: return a #SpeechRecognition RuleParse object.
 *
 * Parse the nth best result of a #SpeechRecognitionFinalRuleResult
 * against a #SpeechRecognitionRuleGrammar. In other respects this
 * parse function is equivalent to the speech_recognition_rule_grammar_parse()
 * function described above.
 *
 * A rejected result (#SPEECH_RECOGNITION_RESULT_REJECTED state) is not
 * guaranteed to parse. Also, if the #SpeechRecognitionRuleGrammar
 * has been modified since the result was issued, parsing is
 * not guaranteed to succeed.
 *
 * See: speech_recognition_rule_grammar_parse()
 * See: speech_recognition_rule_grammar_parse_token_sequence()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if an error is found 
 *       in the definition of the #SpeechRecognitionRuleGrammar.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_parse_nth_best(
                           SpeechRecognitionRuleGrammar *grammar,
                           SpeechRecognitionFinalRuleResult *final_rule_result,
                           int n_best,
                           gchar *rule_name,
                           SpeechRecognitionRuleParse *rule_parse)
{
    printf("speech_recognition_rule_grammar_parse_nth_best called.\n");
}


/**
 * speech_recognition_rule_grammar_parse_token_sequence:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @tokens: the sequence of tokens to parse.
 * @rule_name: the rule to parse against, or %NULL.
 * @rule_parse: return a #SpeechRecognitionRuleParse object.
 *
 * Parse a sequence of tokens against a #SpeechRecognitionRuleGrammar.
 * In all other respects this parse function is equivalent to the
 * speech_recognition_rule_grammar_parse() function except that the 
 * string is pre-tokenized.
 *
 * See: speech_recognition_rule_grammar_parse()
 * See: speech_recognition_rule_grammar_parse_nth_best()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if an error is found 
 *       in the definition of the #SpeechRecognitionRuleGrammar.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_parse_token_sequence(
                             SpeechRecognitionRuleGrammar *grammar,
                             gchar *tokens[],
                             gchar *rule_name,
                             SpeechRecognitionRuleParse *rule_parse)
{
    printf("speech_recognition_rule_grammar_parse_token_sequence called.\n");
}


/**
 * speech_recognition_rule_grammar_remove_import:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @import_name: the name of the import to remove.
 *
 * Remove an import. The name follows the format of
 * speech_recognition_rule_grammar_add_import().
 *
 * The change in imports only takes effect when grammar changes are committed.
 *
 * See: speech_recognition_rule_grammar_add_import()
 * See: speech_recognition_recognizer_commit_changes()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if @import_name is not
 *		currently imported.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_remove_import(
                             SpeechRecognitionRuleGrammar *grammar,
                             SpeechRecognitionRuleName *import_name)
{
    printf("speech_recognition_rule_grammar_remove_import called.\n");
}


/**
 * speech_recognition_rule_grammar_resolve:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: reference to rulename to be resolved.
 * @result: return the fully-qualified reference to a rulename.
 *
 * Resolve a rulename reference within a #SpeechRecognitionRuleGrammar to a
 * fully-qualified rulename. The input rulename may be a simple rulename, 
 * qualified or fully-qualified. If the rulename cannot be resolved, the 
 * function returns %NULL.
 *
 * If the rulename being resolved is a local reference, the return
 * value is a fully-qualified rulename with its grammar part being
 * the name of this #SpeechRecognitionRuleGrammar.
 *
 * Example: in a grammar that imports the rule &lt;number&gt; from
 * the grammar "com.sun.examples", the following would return the
 * fully-qualified rulename com.sun.examples.number.
 *
 *   speech_recognition_rule_grammar_resolve(grammar, 
 *        speech_recognition_rule_name_new_with_name("number"));
 *
 * If the input rulename is a fully-qualified rulename, then the
 * function checks whether that rulename exists (and could therefore
 * be successfully referenced in this #SpeechRecognitionRuleGrammar).
 * If the rulename exists, then the return value is the same as
 * the input value, otherwise the function returns %NULL.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if an error is found 
 *       in the definition of the #SpeechRecognitionRuleGrammar or if
 *	 @rule_name is an ambiguous reference.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_resolve(
                             SpeechRecognitionRuleGrammar *grammar,
                             SpeechRecognitionRuleName *rule_name,
                             SpeechRecognitionRuleName *result)
{
    printf("speech_recognition_rule_grammar_resolve called.\n");
}


/**
 * speech_recognition_rule_grammar_rule_for_grammar:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @grammar_text: string in partial grammar format.
 * @rule: the returned #SpeechRecognitionRule object.
 *
 * Convert a string in partial grammar format to a #SpeechRecognitionRule
 * object. The string can be any legal expansion from the supported
 * grammar language.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_GRAMMAR_EXCEPTION if the grammar formatted text 
 *       contains any errors.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_rule_for_grammar(
                             SpeechRecognitionRuleGrammar *grammar,
                             gchar *grammar_text,
                             SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_grammar_rule_for_grammar called.\n");
}


/**
 * speech_recognition_rule_grammar_set_enabled_for_rule:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: name of the rule to be enabled or disabled.
 * @enabled: %TRUE to enable @rule_name, %FALSE to disable.
 *
 * Set the enabled property for a specified public rule. This function
 * behaves the same as the speech_recognition_grammar_set_enabled() 
 * function except that it affects only a single public rule. The 
 * enabled state of other rules is unchanged.
 *
 * Individual rules of a #SpeechRecognitionRuleGrammar may be individually
 * enabled and disabled. Once any rule is enabled, the
 * #SpeechRecognitionRuleGrammar is considered to be enabled. If all
 * rules are disabled, then the #SpeechRecognitionRuleGrammar is
 * considered disabled.
 *
 * A change in the enabled property of a #SpeechRecognitionRule or a
 * #SpeechRecognitionRuleGrammar only takes effect when
 * grammar changes are next committed.
 *
 * If the rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then the brackets are ignored.
 *
 * See: speech_recognition_grammar_set_enabled()
 * See: speech_recognition_rule_grammar_set_enabled_for_rules()
 * See: speech_recognition_recognizer_commit_changes()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if @rule_name is
 *       unknown or if it is a non-public rule.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_set_enabled_for_rule(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_name,
                                     gboolean enabled)
{
    printf("speech_recognition_rule_grammar_set_enabled_for_rule called.\n");
}


/**
 * speech_recognition_rule_grammar_set_enabled_for_rules:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_names: the set of rulenames to be enabled or disabled.
 * @enabled: %TRUE to enabled rulenames, %FALSE to disable.
 *
 * Set the enabled property for a set of public rules of a
 * #SpeechRecognitionRuleGrammar. This function behaves the same as the
 * speech_recognition_grammar_set_enabled() function except that it 
 * only affects a defined single public rule. This call does not affect the
 * enabled state of other public rules of the #SpeechRecognitionRuleGrammar.
 *
 * If any one or more rules are enabled, the #SpeechRecognitionRuleGrammar
 * is considered to be enabled. If all rules are disabled, then
 * the #SpeechRecognitionRuleGrammar is considered disabled.
 *
 * A change in the enabled property of #SpeechRecognitionRules or a
 * #SpeechRecognitionRuleGrammar only takes effect when
 * grammar changes are next committed.
 *
 * If any rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then the brackets are ignored.
 *
 * See: speech_recognition_recognizer_commit_changes()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if one or more @rule_names
 *       is unknown or if any is a non-public rule.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_set_enabled_for_rules(
                                     SpeechRecognitionRuleGrammar *grammar,
                                     gchar *rule_names[],
                                     gboolean enabled)
{
    printf("speech_recognition_rule_grammar_set_enabled_for_rules called.\n");
}


/**
 * speech_recognition_rule_grammar_set_rule:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 * @rule_name: unique name of rule being defined (unique for this
 *		#SpeechRecognitionRuleGrammar).
 * @rule: logical expansion for the rulename.
 * @is_public: %TRUE if this rule can be imported into other
 *		#SpeechRecognitionRuleGrammars or enabled.
 *
 * Set a rule in the grammar either by creating a new rule or
 * updating an existing rule. The rule being set is identified by its
 * @rule_name and defined by the #SpeechRecognitionRule object and its
 * @is_public flag. The speech_recognition_rule_grammar_set_rule() function
 * is equivalent to a rule definition in the Java supported markup language.
 *
 * The change in the #SpeechRecognitionRuleGrammar takes effect when
 * grammar changes are committed.
 *
 * The rule object represents the expansion of a grammar formatted
 * definition (the right hand side). It may be a
 *
 * #SpeechRecognitionRuleToken,
 * #SpeechRecognitionRuleName,
 * #SpeechRecognitionRuleAlternatives,
 * #SpeechRecognitionRuleSequence,
 * #SpeechRecognitionRuleCount or
 * #SpeechRecognitionRuleTag.
 *
 * Each of these 6 object types is an extension of the
 * #SpeechRecognitionRule object. (The rule object cannot be an
 * instance of #SpeechRecognitionRuleParse which is also an
 * extension of #SpeechRecognitionRule.)
 *
 * A rule is most easily created from Java Speech Grammar Format text
 * using the speech_recognition_rule_grammar_rule_for_grammar() function.
 * e.g.
 *
 *    speech_recognition_rule_grammar_set_rule(grammar, rule_name,
 *	speech_recognition_rule_grammar_rule_for_grammar(grammar,
 *                                          "open the &lt;object&gt;"), true);
 *
 * The @is_public flag defines whether this rule may be enabled and 
 * active for recognition and/or imported into other rule grammars.
 * It is equivalent to the public declaration in the supported grammar 
 * format language.
 *
 * If the #SpeechRecognitionRule object contains a fully-qualified
 * reference to a rule of another #SpeechRecognitionRuleGrammar, an
 * import is automatically generated for that rule if it is not
 * already imported. A subsequent call to
 * speech_recognition_rule_grammar_list_imports() will return that 
 * import statement.
 *
 * If the rule name contains both start/end angle brackets
 * (e.g. "&lt;ruleName&gt;"), then they are automatically stripped.
 *
 * The #SpeechRecognitionRule object passed to the
 * #SpeechRecognitionRuleGrammar is copied before being applied to
 * recognizer. Thus, an application can modify and re-use a @rule 
 * object without unexpected side-effects.
 * Also, a different object will be returned by the
 * speech_recognition_rule_grammar_get_rule() and
 * speech_recognition_rule_grammar_get_rule_internal() functions
 * (although it will contain the same information).
 *
 * See: speech_recognition_rule_grammar_rule_for_grammar()
 * See: speech_recognition_rule_grammar_get_rule()
 * See: speech_recognition_rule_grammar_get_rule_internal()
 * See: speech_recognition_recognizer_commit_changes()
 * See: #SpeechRecognitionRuleAlternatives
 * See: #SpeechRecognitionRuleCount
 * See: #SpeechRecognitionRuleName
 * See: #SpeechRecognitionRuleSequence
 * See: #SpeechRecognitionRuleTag
 * See: #SpeechRecognitionRuleToken
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_NULL_POINTER_EXCEPTION if @rule_name or
 *		rule are %NULL.</listitem>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if rule is not a legal
 *		instance of #SpeechRecognitionRule.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_grammar_set_rule(SpeechRecognitionRuleGrammar *grammar,
                                         gchar *rule_name,
                                         SpeechRecognitionRule rule,
                                         gboolean is_public)
{
    printf("speech_recognition_rule_grammar_set_rule called.\n");
}


/**
 * speech_recognition_rule_grammar_to_string:
 * @grammar: the #SpeechRecognitionRuleGrammar object that this operation
 *           will be applied to.
 *
 * Return a string containing a specification for this
 * #SpeechRecognitionRuleGrammar in Java Speech Grammar Format. The
 * string includes the grammar name declaration, import statements
 * and all the rule definitions. When sending grammar formatted
 * text to a stream (e.g. a file) the application should prepend
 * the header line with the appropriate character encoding
 * information:
 *
 * Returns: a string containing a specification for this
 *	   #SpeechRecognitionRuleGrammar in the supported grammar format.
 */

gchar *
speech_recognition_rule_grammar_to_string(
                                     SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_rule_grammar_to_string called.\n");
}
