
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechRecognitionRuleToken represents speakable text in a
 * #SpeechRecognitionRuleGrammar. It is the primitive type of a
 * #SpeechRecognitionRule (eventually any rule must break down into a
 * sequence of #SpeechRecognitionRuleTokens that may be spoken).
 * It is also the primitive type of a #SpeechRecognitionRuleParse.
 *
 * See: #SpeechRecognitionRule
 * See: #SpeechRecognitionRuleAlternatives
 * See: #SpeechRecognitionRuleCount
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionRuleName
 * See: #SpeechRecognitionRuleParse
 * See: #SpeechRecognitionRuleSequence
 * See: #SpeechRecognitionRuleTag
 */

#include <speech/recognition/speech_recognition_rule_token.h>


GType
speech_recognition_rule_token_get_type(void)
{
    printf("speech_recognition_rule_token_get_type called.\n");
}


SpeechRecognitionRuleToken *
speech_recognition_rule_token_new(void)
{
    printf("speech_recognition_rule_token_new called.\n");
}


/**
 * speech_recognition_rule_token_new_with_string:
 * @text: the speakable text string.
 *
 * Initialize a #SpeechRecognitionRuleToken with the speakable string.
 * The string should not include the surrounding quotes or escapes of 
 * grammar tokens.
 *
 * Returns: a new #SpeechRecognitionRuleToken object.
 */

SpeechRecognitionRuleToken *
speech_recognition_rule_token_new_with_string(gchar *text)
{
    printf("speech_recognition_rule_token_new_with_string called.\n");
}


/**
 * speech_recognition_rule_token_get_text:
 * @rule_token: the #SpeechRecognitionRuleToken object that this operation
 *              will be applied to.
 *
 * Get the text of the token. The returned string is not in grammar
 * format (backslash and quote characters are not escaped and
 * surrounding quote characters are not included). Use
 * speech_recognition_rule_token_to_string() to obtain a grammar-compliant 
 * string.
 *
 * See: speech_recognition_rule_token_to_string()
 */

gchar *
speech_recognition_rule_token_get_text(SpeechRecognitionRuleToken *rule_token)
{
    printf("speech_recognition_rule_token_get_text called.\n");
}


/**
 * speech_recognition_rule_token_set_text:
 * @rule_token: the #SpeechRecognitionRuleToken object that this operation
 *              will be applied to.
 * @text: the speakable text string.
 *
 * Set the text.
 */

void
speech_recognition_rule_token_set_text(SpeechRecognitionRuleToken *rule_token,
                                       gchar *text)
{
    printf("speech_recognition_rule_token_set_text called.\n");
}


gchar *
speech_recognition_rule_token_to_string(SpeechRecognitionRuleToken *rule_token)
{
    printf("speech_recognition_rule_token_to_string called.\n");
}
