
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_GRAMMAR_EXCEPTION_H__
#define __SPEECH_RECOGNITION_GRAMMAR_EXCEPTION_H__

#include <speech/speech_exception.h>
#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_grammar_syntax_detail.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_GRAMMAR_EXCEPTION              (speech_recognition_grammar_exception_get_type())
#define SPEECH_RECOGNITION_GRAMMAR_EXCEPTION(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_EXCEPTION, SpeechRecognitionGrammarException))
#define SPEECH_RECOGNITION_GRAMMAR_EXCEPTION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR_EXCEPTION, SpeechRecognitionGrammarExceptionClass))
#define SPEECH_IS_RECOGNITION_GRAMMAR_EXCEPTION(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_EXCEPTION))
#define SPEECH_IS_RECOGNITION_GRAMMAR_EXCEPTION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR_EXCEPTION))
#define SPEECH_RECOGNITION_GRAMMAR_EXCEPTION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_EXCEPTION, SpeechRecognitionGrammarExceptionClass))

typedef struct _SpeechRecognitionGrammarException            SpeechRecognitionGrammarException;
typedef struct _SpeechRecognitionGrammarExceptionClass       SpeechRecognitionGrammarExceptionClass;

struct _SpeechRecognitionGrammarException {
    SpeechException exception;
};

struct _SpeechRecognitionGrammarExceptionClass {
    SpeechExceptionClass parent_class;
};


GType 
speech_recognition_grammar_exception_get_type(void);

SpeechRecognitionGrammarException * 
speech_recognition_grammar_exception_new(void);

SpeechRecognitionGrammarException * 
speech_recognition_grammar_exception_new_with_message(gchar *message);

SpeechRecognitionGrammarException * 
speech_recognition_grammar_exception_new_with_details(gchar *message,
                     SpeechRecognitionGrammarSyntaxDetail *details[]);

void 
speech_recognition_grammar_exception_add_detail(
                     SpeechRecognitionGrammarException *exception,
                     SpeechRecognitionGrammarSyntaxDetail *detail);

SpeechRecognitionGrammarSyntaxDetail ** 
speech_recognition_grammar_exception_get_details(
                     SpeechRecognitionGrammarException *exception);

void 
speech_recognition_grammar_exception_set_details(
                     SpeechRecognitionGrammarException *exception,
                     SpeechRecognitionGrammarSyntaxDetail *details[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_GRAMMAR_EXCEPTION_H__ */
