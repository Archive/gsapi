
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionRuleName is a reference to a named rule. A
 * #SpeechRecognitionRuleName is equivalent to the various forms of
 * rulename syntax in the supported grammar format.
 *
 * A <emphasis>fully-qualified</emphasis> rulename consists of a 
 * <emphasis>full grammar name</emphasis> plus the simple rulename. A full 
 * grammar name consists of a package name and a simple grammar name.
 *
 * Note that the three legal forms of a rulename allowed by the Java Speech
 * Grammar Format (JSGF) and in the #SpeechRecognitionRuleName object are:
 *
 * <itemizedlist>
 *   <listitem>Simple rulename:
 *	 &lt;simpleRulename&gt;
 *
 *	 e.g. &lt;digits&gt;, &lt;date&gt;</listitem>
 *   <listitem>Qualified rulename:
 *	 &lt;simpleGrammarName.simpleRulename&gt;
 *
 *	 e.g. &lt;numbers.digits&gt;, &lt;places.cities&gt;</listitem>
 *   <listitem>Fully-qualified rulename:
 *	 &lt;packageName.simpleGrammarName.simpleRulename&gt;
 *
 *	 e.g.
 *	 &lt;com.sun.numbers.digits&gt;
 *	 &lt;com.acme.places.zipCodes&gt;</listitem>
 * </itemizedlist>
 *
 * The full grammar name is the following portion of the fully-qualified
 * rulename: packageName.simpleGrammarName.
 *
 * For example,
 *	&lt;com.sun.numbers&gt;
 *	&lt;com.acme.places&gt;.
 *
 * There are two special rules defined in JSGF,
 * &lt;NULL&gt; and &lt;VOID&gt;.
 * Both have static instances in this class for convenience. These rulenames
 * can be referenced in any grammar without an import statement.
 *
 * There is a special case of using a RuleName to declare and manage imports
 * of a #SpeechRecognitionRuleGrammar. This form is used with the
 * speech_recognition_rule_grammar_add_import() and 
 * speech_recognition_rule_grammar_remove_import() functions of a
 * #SpeechRecognitionRuleGrammar. It requires a full grammar name plus the 
 * string "*" for the simple rulename.
 *
 * For example:
 *
 *	&lt;com.acme.places.*&gt;
 *
 * The angle brackets placed around rulenames are syntactic constructs in JSGF
 * but are not a part of the rulename. For clarity of code, the angle brackets
 * may be included in calls to this class but they are automatically stripped.
 *
 * The following referencing and name resolution conditions of JSGF apply.
 *
 * <itemizedlist>
 *   <listitem>Any rule in a local grammar may be referenced with a simple 
 *       rulename, qualified rulename or fully-qualified rulename.</listitem>
 *   <listitem>A public rule of another grammar may be referenced by its simple
 *	 rulename or qualified rulename if the rule is imported and the
 *	 name is not ambiguous with another imported rule.</listitem>
 *   <listitem>A public rule of another grammar may be referenced by its
 *	 fully-qualified rulename with or without a corresponding import
 *	 statement.</listitem>
 * </itemizedlist>
 *
 * See: #SpeechRecognitionRule
 * See: speech_recognition_rule_grammar_add_import()
 */

#include <speech/recognition/speech_recognition_rule_name.h>


GType
speech_recognition_rule_name_get_type(void)
{
    printf("speech_recognition_rule_name_get_type called.\n");
}


SpeechRecognitionRuleName *
speech_recognition_rule_name_new(void)
{
    printf("speech_recognition_rule_name_new called.\n");
}


/**
 * speech_recognition_rule_name_new_with_name:
 * @rule_name: the rule name.
 *
 * Initialize a #SpeechRecognitionRuleName from a string. Leading and
 * trailing angle brackets are stripped if found. The rulename may
 * be a simple rulename, qualified rulename or full-qualified rulename.
 *
 * Returns: a new #SpeechRecognitionRuleName object.
 */

SpeechRecognitionRuleName *
speech_recognition_rule_name_new_with_name(gchar *rule_name)
{
    printf("speech_recognition_rule_name_new_with_name called.\n");
}


/**
 * speech_recognition_rule_name_new_from_components:
 * @package_name: the package name of a fully-qualified rulename or %NULL.
 * @simple_grammar_name: the grammar name of a fully-qualified or qualified 
 *                       rule or %NULL.
 * @simple_rule_name: the simple rulename.
 *
 * Initialize a #SpeechRecognitionRuleName from its package-name,
 * grammar-name and simple-name components. Leading and trailing
 * angle brackets are stripped from @simple_rule_name if found.
 * The package name may be %NULL. The grammar name may be %NULL only
 * if packageName is %NULL.
 *
 * Returns: a new #SpeechRecognitionRuleName object or %NULL if
 *          %NULL @simple_grammar_name with non-null @package_name.
 */

SpeechRecognitionRuleName *
speech_recognition_rule_name_new_from_components(gchar *package_name,
                                                 gchar *simple_grammar_name,
                                                 gchar *simple_rule_name)
{
    printf("speech_recognition_rule_name_new_from_components called.\n");
}


/**
 * speech_recognition_rule_name_get_full_grammar_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 *
 * Get the full grammar name. If the #package_name is %NULL, the return 
 * value is the simple grammar name. May return %NULL.
 *
 * Returns: the full grammar name.
 */

gchar *
speech_recognition_rule_name_get_full_grammar_name(
                                        SpeechRecognitionRuleName *rule_name)
{
    printf("speech_recognition_rule_name_get_full_grammar_name called.\n");
}


/**
 * speech_recognition_rule_name_get_package_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 *
 * Get the rule's package name.
 *
 * Returns: the rule's package name.
 */

gchar *
speech_recognition_rule_name_get_package_name(
                                        SpeechRecognitionRuleName *rule_name)
{
    printf("speech_recognition_rule_name_get_package_name called.\n");
}


/**
 * speech_recognition_rule_name_get_rule_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 *
 * Get the rulename including the package and grammar name if they are
 * non-null. The return value may be a fully-qualified rulename,
 * qualified rulename, or simple rulename.
 *
 * Returns: the rulename including the package and grammar name if
 *	   they are non-null.
 */

gchar *
speech_recognition_rule_name_get_rule_name(
                                        SpeechRecognitionRuleName *rule_name)
{
    printf("speech_recognition_rule_name_get_rule_name called.\n");
}


/**
 * speech_recognition_rule_name_get_simple_grammar_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 *
 * Get the simple grammar name. May be %NULL.
 *
 * Returns: the simple grammar name.
 */

gchar *
speech_recognition_rule_name_get_simple_grammar_name(
                                        SpeechRecognitionRuleName *rule_name)
{
    printf("speech_recognition_rule_name_get_simple_grammar_name called.\n");
}


/**
 * speech_recognition_rule_name_get_simple_rule_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 *
 * Get the simple rulename.
 *
 * Returns: the simple rulename.
 */

gchar *
speech_recognition_rule_name_get_simple_rule_name(
                                        SpeechRecognitionRuleName *rule_name)
{
    printf("speech_recognition_rule_name_get_simple_rule_name called.\n");
}


/**
 * speech_recognition_rule_name_is_legal_rule_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 *
 * Tests whether this #SpeechRecognitionRuleName is a legal grammar
 * format rulename. The 
 * speech_recognition_rule_name_is_legal_rule_name_from_name() function 
 * defines legal rulename forms.
 *
 * See: speech_recognition_rule_name_is_legal_rule_name_from_name()
 * See: speech_recognition_rule_name_is_rule_name_part()
 *
 * Returns: a boolean indication of whether this #SpeechRecognitionRuleName
 *	   is a legal grammar format rulename.
 */

gboolean
speech_recognition_rule_name_is_legal_rule_name(
                                        SpeechRecognitionRuleName *rule_name)
{
    printf("speech_recognition_rule_name_is_legal_rule_name called.\n");
}


/**
 * speech_recognition_rule_name_is_legal_rule_name_from_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 * @name: string to test.
 *
 * Tests whether a string is a legal grammar rulename format. The
 * legal patterns for rulenames are defined above. The function does 
 * not test whether the rulename exists or is resolvable (see the 
 * speech_recognition_rule_grammar_resolve() function of
 * #SpeechRecognitionRuleGrammar).
 *
 * An import string (e.g. "com.acme.*") is considered legal even
 * though the "*" character is not a legal rulename character. If
 * present, starting and ending angle brackets are ignored.
 *
 * Returns: an indication of whether @name is a legal grammar rulename format.
 *
 * See: speech_recognition_rule_name_is_legal_rule_name()
 * See: speech_recognition_rule_name_is_rule_name_part()
 */

gboolean
speech_recognition_rule_name_is_legal_rule_name_from_name(
                                        SpeechRecognitionRuleName *rule_name,
                                        gchar *name)
{
    printf("speech_recognition_rule_name_is_legal_rule_name_from_name called.\n");
}


/**
 * speech_recognition_rule_name_is_rule_name_part:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 * @c character: to test.
 *
 * Tests whether a character is a legal part of a Java Speech Grammar
 * Format rulename.
 *
 * Returns: an indication of whether @c is a legal part of a Java
 *	   Speech Grammar Format rulename.
 *
 * See: speech_recognition_rule_name_is_legal_rule_name()
 * See: speech_recognition_rule_name_is_legal_rule_name_from_name()
 */

gboolean
speech_recognition_rule_name_is_rule_name_part(
                                  SpeechRecognitionRuleName *rule_name,
                                  char c)
{
    printf("speech_recognition_rule_name_is_rule_name_part called.\n");
}


/**
 * speech_recognition_rule_name_set_rule_name:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 * @rule_name: the rule name.
 *
 * Set the rulename. The rulename may be a simple, qualified or
 * fully-qualified rulename. Leading and trailing angle brackets
 * are stripped if found.
 */

void
speech_recognition_rule_name_set_rule_name(
                                  SpeechRecognitionRuleName *rule_name,
                                  gchar *ruleName)
{
    printf("speech_recognition_rule_name_set_rule_name called.\n");
}


/**
 * speech_recognition_rule_name_set_rule_name_with_components:
 * @rule_name: the #SpeechRecognitionRuleName object that this operation will
 *             be applied to.
 * @package_name: the package name of a fully-qualified rulename or %NULL.
 * @simple_grammar_name: the grammar name of a fully-qualified or
 *                          qualified rule or %NULL.
 * @simple_rule_name: the simple rulename.
 *
 * Set the rule's name with package name, simple grammar name and
 * simple rulename components. Leading and trailing angle brackets
 * are stripped from @simple_rule_name. The package name may be %NULL.
 * The simple grammar name may be %NULL only if @package_name is %NULL.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if %NULL
 *       @simple_grammar_name with non-null @package_name</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_name_set_rule_name_with_components(
                                  SpeechRecognitionRuleName *rule_name,
                                  gchar *package_name,
                                  gchar *simple_grammar_name,
                                  gchar *simple_rule_name)
{
    printf("speech_recognition_rule_name_set_rule_name_with_components called.\n");
}
