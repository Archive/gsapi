
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The potential detail message associated with a grammar exception.
 *
 * See: #SpeechRecognitionGrammarException
 */

#include <speech/recognition/speech_recognition_grammar_exception.h>


GType
speech_recognition_grammar_exception_get_type(void)
{
    printf("speech_recognition_grammar_exception_get_type called.\n");
}


SpeechRecognitionGrammarException *
speech_recognition_grammar_exception_new(void)
{
    printf("speech_recognition_grammar_exception_new called.\n");
}


SpeechRecognitionGrammarException *
speech_recognition_grammar_exception_new_with_message(gchar *message)
{
    printf("speech_recognition_grammar_exception_new_with_message called.\n");
}


/**
 * speech_recognition_grammar_exception_new_with_message:
 * @message: a printable detail message.
 * @details: details of each error encountered or null.
 *
 * Initializes a #SpeechRecognitionGrammarException with the specified
 * detail message and an optional programmatic description of each
 * error.
 *
 * Returns:
 */

SpeechRecognitionGrammarException *
speech_recognition_grammar_exception_new_with_details(gchar *message,
                         SpeechRecognitionGrammarSyntaxDetail *details[])
{
    printf("speech_recognition_grammar_exception_new_with_details called.\n");
}


/**
 * speech_recognition_grammar_exception_add_detail:
 * @exception: the #SpeechRecognitionGrammarException object that this
 *             operation will be applied to.
 * @detail: a syntax error description.
 *
 * Add a syntax error description (appended to the existing array of details).
 */

void
speech_recognition_grammar_exception_add_detail(
                     SpeechRecognitionGrammarException *exception,
                     SpeechRecognitionGrammarSyntaxDetail *detail)
{
    printf("speech_recognition_grammar_exception_add_detail called.\n");
}


/**
 * speech_recognition_grammar_exception_get_details:
 * @exception: the #SpeechRecognitionGrammarException object that this
 *             operation will be applied to.
 *
 * Returns: the list of grammar syntax problem descriptions.
 */

SpeechRecognitionGrammarSyntaxDetail **
speech_recognition_grammar_exception_get_details(
                                SpeechRecognitionGrammarException *exception)
{
    printf("speech_recognition_grammar_exception_get_details called.\n");
}


/**
 * speech_recognition_grammar_exception_set_details:
 * @exception: the #SpeechRecognitionGrammarException object that this
 *             operation will be applied to.
 * @details: a list of grammar syntax problem descriptions.
 *
 * Set the grammar syntax problem descriptions.
 */

void
speech_recognition_grammar_exception_set_details(
                     SpeechRecognitionGrammarException *exception,
                     SpeechRecognitionGrammarSyntaxDetail *details[])
{
    printf("speech_recognition_grammar_exception_set_details called.\n");
}
