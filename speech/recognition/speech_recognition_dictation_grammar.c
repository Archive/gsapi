
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Provides access to the dictation capabilities of a 
 * #SpeechRecognitionRecognizer. If a recognizer supports dictation, it 
 * provides a #SpeechRecognitionDictationGrammar which is obtained through the
 * speech_recognition_recognizer_get_dictation_grammar() function of the
 * #SpeechRecognitionRecognizer.
 *
 * A #SpeechRecognitionDictationGrammar is named with the same convention as a
 * #SpeechRecognitionRuleGrammar and will typically reflect the language and
 * domain it supports. The grammar name is obtained through the
 * speech_recognition_grammar_get_name() function. For example, the general 
 * dictation for US English from Acme speech company might be called:
 *
 *	 com.acme.dictation.english.us.general
 *
 * A #SpeechRecognitionDictationGrammar is characterized by:
 *
 * <itemizedlist>
 *   <listitem>Typically large vocabulary.</listitem>
 *   <listitem>Grammar is built-into the recognizer,</listitem>
 *   <listitem>General purpose or domain-specific (e.g. legal, radiology, 
 *       medical),</listitem>
 *   <listitem>May support continuous or discrete speech,</listitem>
 * </itemizedlist>
 *
 * A dictation grammar is built into a recognizer (if supported). Moreover,
 * a recognizer provides a single dictation grammar. Applications cannot
 * create or directly modify the grammar beyond the relatively simple functions
 * provided by this object. By comparison, an application can change any
 * part of a #SpeechRecognitionRuleGrammar. (Some vendors provide tools for
 * constructing dictation grammars, but these tools operate separate from
 * the Speech API.)
 *
 * A recognizer that supports dictation:
 *
 * <itemizedlist>
 *   <listitem>Returns true for the
 *	 speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported() 
 *       function. This value can be used to select a dictation recognizer.
 *       </listitem>
 *   <listitem>Typically resource intensive (CPU, disk, memory),</listitem>
 *   <listitem>Often requires training by user (supports the
 *	 #SpeechRecognitionSpeakerManager object).</listitem>
 * </itemizedlist>
 *
 * <title>DictationGrammar Extends Grammar</title>
 *
 * The #SpeechRecognitionDictationGrammar object extends the
 * #SpeechRecognitionGrammar object. Thus, a #SpeechRecognitionDictationGrammar
 * provides all the #SpeechRecognitionGrammar functionality:
 *
 * <itemizedlist>
 *   <listitem>The dictation grammar name is returned by the
 *	 speech_recognition_grammar_get_name() function.</listitem>
 *   <listitem>The dictation grammar is enabled and disabled for recognition 
 *       by the speech_recognition_grammar_set_enabled() function inherited 
 *       from #SpeechRecognitionGrammar.</listitem>
 *   <listitem>The activation mode is set through the 
 *       speech_recognition_grammar_set_activation_mode() function of 
 *       #SpeechRecognitionGrammar. Note: a #SpeechRecognitionDictationGrammar
 *	 should never have #SPEECH_RECOGNITION_GRAMMAR_GLOBAL activation 
 *       mode.</listitem>
 *   <listitem>The current activation state of the #SpeechRecognitionGrammar
 *       is tested by the speech_recognition_grammar_is_active() function 
 *       and the #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED and
 *       SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED events.
 *       </listitem>
 *   <listitem>Grammar listeners are attached and removed by the
 *	 speech_recognition_grammar_add_grammar_listener() and 
 *	 speech_recognition_grammar_remove_grammar_listener() functions
 *       </listitem>
 * </itemizedlist>
 *
 *
 * <title>Context</title>
 *
 * Dictation recognizers can use the current textual context to improve
 * recognition accuracy. Applications should use either
 * speech_recognition_dictation_grammar_set_context() or
 * speech_recognition_dictation_grammar_set_context_from_array() to inform 
 * the recognizer each time the context changes. For example, when editing 
 * this sentence with the cursor after the word "sentence", the preceding 
 * context is "When editing this sentence" and the following context is 
 * "with the cursor after...". Any time the text context changes, the 
 * application should inform the recognizer.
 *
 * Applications should provide a minimum of 3 or 4 words of context (when it's
 * available). Different recognizers process context differently. Some
 * recognizers can take advantage of several paragraphs of context others
 * look only at a few words.
 *
 * speech_recognition_dictation_grammar_set_context() takes one string each 
 * for preceding and following context (as in the example above).
 * speech_recognition_dictation_grammar_set_context_from_array() takes an 
 * array each of strings for preceding and following context. Arrays should 
 * be provided if the surrounding context * is made of tokens from previous 
 * recognition results. When possible, providing tokens is the preferred 
 * function because recognizers are able to use token information more 
 * reliably and more efficiently.
 *
 * <title>Word Lists</title>
 *
 * Words can be added to and removed from the 
 * #SpeechRecognitionDictationGrammar using the 
 * speech_recognition_dictation_grammar_add_word() and 
 * speech_recognition_dictation_grammar_remove_word() functions. Lists
 * of the added and removed words are available through the
 * speech_recognition_dictation_grammar_list_added_words() and 
 * speech_recognition_dictation_grammar_list_removed_words() functions.
 * In a speaker-adaptive system (#SpeechRecognitionSpeakerManager supported)
 * word data is stored as part of the speaker's data.
 *
 * Adding a word allows a recognizer to learn new words. Removing a word is
 * useful when a recognizer consistently misrecognizes similar sounding
 * words.  For example, if each time a user says "grammar", the recognizer
 * hears "grandma", the user can remove grandma (assuming they don't want
 * to use the word "grandma").
 *
 * See: #SpeechRecognitionSpeakerManager
 * See: #SpeechRecognitionRecognizer
 * See: #SpeechRecognitionGrammar
 */

#include <speech/recognition/speech_recognition_dictation_grammar.h>


GType
speech_recognition_dictation_grammar_get_type(void)
{
    printf("speech_recognition_dictation_grammar_get_type called.\n");
}


SpeechRecognitionDictationGrammar *
speech_recognition_dictation_grammar_new(void)
{
    printf("speech_recognition_dictation_grammar_new called.\n");
}


/**
 * speech_recognition_dictation_grammar_add_word:
 * @grammar: the #SpeechRecognitionDictationGrammar object that this operation
 *           will be applied to.
 * @word: the word to be added.
 *
 * Add a word to the #SpeechRecognitionDictationGrammar. The
 * speech_recognition_dictation_grammar_add_word() function can undo the 
 * effects of an speech_recognition_dictation_grammar_remove_word() call.
 *
 * A change in a #SpeechRecognitionDictationGrammar is applied to
 * the recognition process only after changes have been committed.
 *
 * See: speech_recognition_recognizer_commit_changes()
 */

void
speech_recognition_dictation_grammar_add_word(
                                   SpeechRecognitionDictationGrammar * grammar,
                                   gchar *word)
{
    printf("speech_recognition_dictation_grammar_add_word called.\n");
}


/**
 * speech_recognition_dictation_grammar_remove_word:
 * @grammar: the #SpeechRecognitionDictationGrammar object that this operation
 *           will be applied to.
 * @word: the word to be removed.
 *
 * Remove a word from the #SpeechRecognitionDictationGrammar. The
 * speech_recognition_dictation_grammar_remove_word() function can undo the 
 * effects of a speech_recognition_dictation_grammar_add_word() call.
 *
 * A change in a #SpeechRecognitionDictationGrammar is applied to
 * the recognition process only after changes have been committed.
 *
 * See: speech_recognition_recognizer_commit_changes()
 */

void
speech_recognition_dictation_grammar_remove_word(
                                  SpeechRecognitionDictationGrammar * grammar,
                                  gchar *word)
{
    printf("speech_recognition_dictation_grammar_remove_word called.\n");
}


/**
 * speech_recognition_dictation_grammar_list_added_words:
 * @grammar: the #SpeechRecognitionDictationGrammar object that this operation
 *           will be applied to.
 *
 * List the words that have been added to the
 * #SpeechRecognitionDictationGrammar.
 *
 * Returns: the list of words that have been added to the
 *          #SpeechRecognitionDictationGrammar.
 */

gchar **
speech_recognition_dictation_grammar_list_added_words(
                                  SpeechRecognitionDictationGrammar * grammar)
{
    printf("speech_recognition_dictation_grammar_list_added_words called.\n");
}


/**
 * speech_recognition_dictation_grammar_list_removed_words:
 * @grammar: the #SpeechRecognitionDictationGrammar object that this operation
 *           will be applied to.
 *
 * List the words that have been removed from the
 * #SpeechRecognitionDictationGrammar.
 *
 * Returns: the list of words that have been removed from the
 *          #SpeechRecognitionDictationGrammar.
 */

gchar **
speech_recognition_dictation_grammar_list_removed_words(
                                 SpeechRecognitionDictationGrammar * grammar)
{
    printf("speech_recognition_dictation_grammar_list_removed_words called.\n");
}


/**
 * speech_recognition_dictation_grammar_set_context:
 * @grammar: the #SpeechRecognitionDictationGrammar object that this operation
 *           will be applied to.
 * @preceding: the preceding context.
 * @following: the following context.
 *
 * Provide the recognition engine with the current textual context.
 * Dictation recognizers use the context information to improve
 * recognition accuracy. (Context is discussed above.)
 *
 * When dictating a sequence of words, the recognizer updates its
 * context. The app does not need to inform the recognizer when
 * results arrive. Instead it should call 
 * speech_recognition_dictation_grammar_set_context()
 * for events such as cursor movement, cut, paste etc.
 *
 * The preceding or following context may be null if there is no
 * preceding or following context. This is appropriate for a new
 * document or in situations where context is not clear.
 *
 * The alternative 
 * speech_recognition_dictation_grammar_set_context_from_array() that 
 * accepts arrays of tokens for context, should be used when the current 
 * context includes tokens from previous results.
 *
 * See: speech_recognition_dictation_grammar_set_context_from_array()
 */

void
speech_recognition_dictation_grammar_set_context(
                                  SpeechRecognitionDictationGrammar * grammar,
                                  gchar *preceding,
                                  gchar *following)
{
    printf("speech_recognition_dictation_grammar_set_context called.\n");
}


/**
 * speech_recognition_dictation_grammar_set_context_from_array:
 * @grammar: the #SpeechRecognitionDictationGrammar object that this operation
 *           will be applied to.
 * @preceding: the preceding context.
 * @following: the following context.
 *
 * Provide the recognition engine with the current textual context
 * with arrays of the previous and following tokens.  Dictation
 * recognizers use the context information to improve recognition
 * accuracy.  (Context is discussed above.)
 *
 * When dictating a sequence of words, the recognizer updates its
 * context.  The app does not need to inform the recognizer when
 * results arrive.  Instead it should call 
 * speech_recognition_dictation_grammar_set_context() for events such 
 * as cursor movement, cut, paste etc.
 *
 * The preceding or following context may be null
 * if there is no preceding or following context.  This is appropriate
 * for a new document or in situations where context is not clear.
 *
 * See: speech_recognition_dictation_grammar_set_context()
 */

void
speech_recognition_dictation_grammar_set_context_from_array(
                                   SpeechRecognitionDictationGrammar * grammar,
                                   gchar *preceding[],
                                   gchar *following[])
{
    printf("speech_recognition_dictation_grammar_set_context_from_array called.\n");
}
