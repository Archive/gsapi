
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RECOGNIZER_EVENT_H__
#define __SPEECH_RECOGNITION_RECOGNIZER_EVENT_H__

#include <speech/speech_engine_event.h>
#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_grammar_exception.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RECOGNIZER_EVENT              (speech_recognition_recognizer_event_get_type())
#define SPEECH_RECOGNITION_RECOGNIZER_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_EVENT, SpeechRecognitionRecognizerEvent))
#define SPEECH_RECOGNITION_RECOGNIZER_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_EVENT, SpeechRecognitionRecognizerEventClass))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_EVENT))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_EVENT))
#define SPEECH_RECOGNITION_RECOGNIZER_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_EVENT, SpeechRecognitionRecognizerEventClass))

typedef struct _SpeechRecognitionRecognizerEvent            SpeechRecognitionRecognizerEvent;
typedef struct _SpeechRecognitionRecognizerEventClass       SpeechRecognitionRecognizerEventClass;

struct _SpeechRecognitionRecognizerEvent {
    SpeechEngineEvent event;
};

struct _SpeechRecognitionRecognizerEventClass {
    SpeechEngineEventClass parent_class;
};


/*
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING event is 
 * issued when a #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state to indicate that it is
 * actively processing a recognition #SpeechRecognitionResult. The
 * transition is triggered when the recognizer detects speech in
 * the incoming audio stream that may match and active grammar. The
 * transition occurs immediately before the 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED is issued to 
 * #SpeechRecognitionResultListener's.
 *
 * See: speech_recognition_recognizer_listener_recognizer_processing()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_LISTENING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 * See: speech_engine_get_engine_state()
 * See: speech_event_get_id()
 */

static int SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING = 1200;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event is 
 * issued when a #SpeechRecognitionRecognizer changes from either the
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state or the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state.
 *
 * A transition from the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state 
 * to the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state is triggered by 
 * a call to either the speech_recognition_recognizer_suspend() function 
 * or the speech_recognition_recognizer_commit_changes() function.
 *
 * A transition from the #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state 
 * to the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state is triggered by 
 * the finalization of the result currently being recognized. In this 
 * instance, the #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 * event is followed immediately by either the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RESULT_ACCEPTED or
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RESULT_REJECTED event that 
 * finalizes the result.
 *
 * See: speech_recognition_recognizer_listener_recognizer_suspended()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_LISTENING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: speech_recognition_recognizer_suspend()
 * See: speech_recognition_recognizer_commit_changes()
 * See: speech_engine_get_engine_state()
 * See: speech_event_get_id()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RESULT_ACCEPTED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RESULT_REJECTED
 */

static int SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED = 1202;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event is issued 
 * when a #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state. This state transition
 * takes place when changes to the definition and enabled state of all
 * a recognizer's grammars have been applied. The new grammar
 * definitions are used as incoming speech is recognized in the
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING and 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING states of the 
 * #SpeechRecognitionRecognizer.
 *
 * Immediately following the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event,
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED events 
 * are issued to the #SpeechRecognitionGrammarListener's of each 
 * changed #SpeechRecognitionGrammar.
 *
 * If any errors are detected in any grammar's definition during the
 * commit, a #SpeechRecognitionGrammarException is provided with this event.
 * The #SpeechRecognitionGrammarException is also included with the
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event to
 * #SpeechRecognitionGrammar with the error. The
 * #SpeechRecognitionGrammarException has the same function as the
 * #SpeechRecognitionGrammarException returned on the
 * speech_recognition_recognizer_commit_changes() function.
 *
 * The causes and timing of the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event
 * are described with the state transition documentation for a
 * #SpeechRecognitionRecognizer with the committing changes documentation 
 * for a #SpeechRecognitionGrammar.
 *
 * See: speech_recognition_recognizer_listener_changes_committed()
 * See: speech_recognition_recognizer_commit_changes()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_LISTENING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * See: speech_engine_get_engine_state()
 * See: speech_event_get_id()
 */

static int SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED = 1203;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED event is issued when a
 * #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON state. This event typically
 * occurs as a result of a call to the 
 * speech_recognition_recognizer_request_focus() function of the
 * #SpeechRecognitionRecognizer.
 *
 * The event indicates that the #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON 
 * bit of the engine state is set.
 *
 * Since recognizer focus is a key factor in the activation policy
 * for grammars, a #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED 
 * event is followed by a #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED
 * event to the #SpeechRecognitionGrammarListener's of each 
 * #SpeechRecognitionGrammar that is activated. Activation conditions and 
 * the role of recognizer focus are detailed in the documentation for the
 * #SpeechRecognitionGrammar object.
 *
 * See: speech_recognition_recognizer_listener_focus_gained()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST
 * See: #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON
 * See: speech_recognition_recognizer_request_focus()
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED
 * See: #SpeechRecognitionGrammar
 */

static int SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED = 1204;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event is issued when a
 * #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF state. This event may occur
 * as a result of a call to the 
 * speech_recognition_recognizer_release_focus() function 
 * of the #SpeechRecognitionRecognizer or because another application
 * has requested recognizer focus.
 *
 * The event indicates that the #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF
 * bit of the engine state is set.
 *
 * Since recognizer focus is a key factor in the activation policy
 * for grammars, a #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event 
 * is followed by a #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED
 * event to the #SpeechRecognitionGrammarListener's of each 
 * #SpeechRecognitionGrammar that loses activatation. Activation 
 * conditions and the role of recognizer focus are detailed in the 
 * documentation for the #SpeechRecognitionGrammar object.
 *
 * See: speech_recognition_recognizer_listener_focus_lost()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF
 * See: speech_recognition_recognizer_release_focus()
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED
 * See: #SpeechRecognitionGrammar
 */

static int SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST = 1205;


GType 
speech_recognition_recognizer_event_get_type(void);

SpeechRecognitionRecognizerEvent * 
speech_recognition_recognizer_event_new(SpeechEngine *source,
                        int id,
                        long old_engine_state,
                        long new_engine_state,
                        SpeechRecognitionGrammarException *grammar_exception);

SpeechRecognitionGrammarException * 
speech_recognition_recognizer_event_get_grammar_exception(
                        SpeechRecognitionRecognizerEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RECOGNIZER_EVENT_H__ */
