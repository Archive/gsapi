
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_AUDIO_CLIP_H__
#define __SPEECH_RECOGNITION_AUDIO_CLIP_H__

#include <speech/recognition/speech_recognition_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_AUDIO_CLIP              (speech_recognition_audio_clip_get_type())
#define SPEECH_RECOGNITION_AUDIO_CLIP(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_AUDIO_CLIP, SpeechRecognitionAudioClip))
#define SPEECH_RECOGNITION_AUDIO_CLIP_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_AUDIO_CLIP, SpeechRecognitionAudioClipClass))
#define SPEECH_IS_RECOGNITION_AUDIO_CLIP(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_AUDIO_CLIP))
#define SPEECH_IS_RECOGNITION_AUDIO_CLIP_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_AUDIO_CLIP))
#define SPEECH_RECOGNITION_AUDIO_CLIP_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_AUDIO_CLIP, SpeechRecognitionAudioClipClass))

typedef struct _SpeechRecognitionAudioClip            SpeechRecognitionAudioClip;
typedef struct _SpeechRecognitionAudioClipClass       SpeechRecognitionAudioClipClass;

struct _SpeechRecognitionAudioClip {
    SpeechObject object;
};

struct _SpeechRecognitionAudioClipClass {
    SpeechObjectClass parent_class;
};


GType 
speech_recognition_audio_clip_get_type(void);

SpeechRecognitionAudioClip * 
speech_recognition_audio_clip_new(void);

void 
speech_recognition_audio_clip_loop(SpeechRecognitionAudioClip *clip);

void 
speech_recognition_audio_clip_play(SpeechRecognitionAudioClip *clip);

void 
speech_recognition_audio_clip_stop(SpeechRecognitionAudioClip *clip);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_AUDIO_CLIP_H__ */
