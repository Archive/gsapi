
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_FINAL_RULE_RESULT_H__
#define __SPEECH_RECOGNITION_FINAL_RULE_RESULT_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_final_result.h>
#include <speech/recognition/speech_recognition_rule_grammar.h>
#include <speech/recognition/speech_recognition_result_token.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_FINAL_RULE_RESULT              (speech_recognition_final_rule_result_get_type())
#define SPEECH_RECOGNITION_FINAL_RULE_RESULT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_FINAL_RULE_RESULT, SpeechRecognitionFinalRuleResult))
#define SPEECH_RECOGNITION_FINAL_RULE_RESULT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_FINAL_RULE_RESULT, SpeechRecognitionFinalRuleResultClass))
#define SPEECH_IS_RECOGNITION_FINAL_RULE_RESULT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_FINAL_RULE_RESULT))
#define SPEECH_IS_RECOGNITION_FINAL_RULE_RESULT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_FINAL_RULE_RESULT))
#define SPEECH_RECOGNITION_FINAL_RULE_RESULT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_FINAL_RULE_RESULT, SpeechRecognitionFinalRuleResultClass))


GType 
speech_recognition_final_rule_result_get_type(void);

SpeechRecognitionFinalRuleResult * 
speech_recognition_final_rule_result_new(void);

SpeechStatus 
speech_recognition_final_rule_result_get_alternative_tokens(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int n_best,
                                     SpeechRecognitionResultToken *tokens[]);

SpeechStatus 
speech_recognition_final_rule_result_get_number_guesses(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int *n);

SpeechStatus 
speech_recognition_final_rule_result_get_rule_grammar(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int n_best,
                                     SpeechRecognitionRuleGrammar *grammar);

SpeechStatus 
speech_recognition_final_rule_result_get_rule_name(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int n_best,
                                     gchar *name);

SpeechStatus 
speech_recognition_final_rule_result_get_tags(
                                     SpeechRecognitionFinalRuleResult *result,
                                     gchar *tags[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_FINAL_RULE_RESULT_H__ */
