
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_PARSE_H__
#define __SPEECH_RECOGNITION_RULE_PARSE_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_rule.h>
#include <speech/recognition/speech_recognition_rule_name.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE_PARSE              (speech_recognition_rule_parse_get_type())
#define SPEECH_RECOGNITION_RULE_PARSE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE_PARSE, SpeechRecognitionRuleParse))
#define SPEECH_RECOGNITION_RULE_PARSE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE_PARSE, SpeechRecognitionRuleParse))
#define SPEECH_IS_RECOGNITION_RULE_PARSE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE_PARSE))
#define SPEECH_IS_RECOGNITION_RULE_PARSE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE_PARSE))
#define SPEECH_RECOGNITION_RULE_PARSE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE_PARSE, SpeechRecognitionRuleParseClass))

typedef struct _SpeechRecognitionRuleParse            SpeechRecognitionRuleParse;
typedef struct _SpeechRecognitionRuleParseClass       SpeechRecognitionRuleParseClass;

struct _SpeechRecognitionRuleParse {
    SpeechRecognitionRule rule;
};

struct _SpeechRecognitionRuleParseClass {
    SpeechRecognitionRuleClass parent_class;
};


GType 
speech_recognition_rule_parse_get_type(void);

SpeechRecognitionRuleParse * 
speech_recognition_rule_parse_new(void);

SpeechRecognitionRuleParse * 
speech_recognition_rule_parse_new_with_named_rule(
                                    SpeechRecognitionRuleName *rule_name,
                                    SpeechRecognitionRule *rule);

SpeechRecognitionRuleName * 
speech_recognition_rule_parse_get_rule_name(
                                    SpeechRecognitionRuleParse *rule_parse);

void 
speech_recognition_rule_parse_set_rule_name(
                                    SpeechRecognitionRuleParse *rule_parse,
                                    SpeechRecognitionRuleName *rule_name);

SpeechRecognitionRule * 
speech_recognition_rule_parse_get_rule(SpeechRecognitionRuleParse *rule_parse);

void 
speech_recognition_rule_parse_set_rule(SpeechRecognitionRuleParse *rule_parse,
                                       SpeechRecognitionRule *rule);

gchar ** 
speech_recognition_rule_parse_get_tags(SpeechRecognitionRuleParse *rule_parse);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_PARSE_H__ */
