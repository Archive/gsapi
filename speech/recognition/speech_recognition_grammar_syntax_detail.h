
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_GRAMMAR_SYNTAX_DETAIL_H__
#define __SPEECH_RECOGNITION_GRAMMAR_SYNTAX_DETAIL_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_rule_name.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_GRAMMAR_SYNTAX_DETAIL              (speech_recognition_grammar_syntax_detail_get_type())
#define SPEECH_RECOGNITION_GRAMMAR_SYNTAX_DETAIL(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_SYNTAX_DETAIL, SpeechRecognitionGrammarSyntaxDetail))
#define SPEECH_RECOGNITION_GRAMMAR_SYNTAX_DETAIL_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR_SYNTAX_DETAIL, SpeechRecognitionGrammarSyntaxDetailClass))
#define SPEECH_IS_RECOGNITION_GRAMMAR_SYNTAX_DETAIL(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_SYNTAX_DETAIL))
#define SPEECH_IS_RECOGNITION_GRAMMAR_SYNTAX_DETAIL_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR_SYNTAX_DETAIL))
#define SPEECH_RECOGNITION_GRAMMAR_SYNTAX_DETAIL_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR_SYNTAX_DETAIL, SpeechRecognitionGrammarSyntaxDetailClass))

typedef struct _SpeechRecognitionGrammarSyntaxDetail            SpeechRecognitionGrammarSyntaxDetail;
typedef struct _SpeechRecognitionGrammarSyntaxDetailClass       SpeechRecognitionGrammarSyntaxDetailClass;

struct _SpeechRecognitionGrammarSyntaxDetail {
    SpeechObject object;
};

struct _SpeechRecognitionGrammarSyntaxDetailClass {
    SpeechObjectClass parent_class;
};


GType 
speech_recognition_grammar_syntax_detail_get_type(void);

SpeechRecognitionGrammarSyntaxDetail * 
speech_recognition_grammar_syntax_detail_new(void);

SpeechRecognitionGrammarSyntaxDetail * 
speech_recognition_grammar_syntax_detail_new_with_problem_details(
                                        gchar *grammar_name,
                                        SpeechGrammarURL grammar_location,
                                        gchar *rule_name,
                                        SpeechRecognitionRuleName *import_name,
                                        int line_number,
                                        int char_number,
                                        gchar *message);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_GRAMMAR_SYNTAX_DETAIL_H__ */
