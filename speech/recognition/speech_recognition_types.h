
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_TYPES_H__
#define __SPEECH_RECOGNITION_TYPES_H__

#include <speech/speech_types.h>
#include <speech/speech_object.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef gchar * SpeechGrammarURL;
typedef gchar * SpeechGrammarType;


/* Forward declarations of commonly used types. */

typedef struct _SpeechRecognitionGrammarEvent    SpeechRecognitionGrammarEvent;
typedef struct _SpeechRecognitionGrammarListener SpeechRecognitionGrammarListener;
typedef struct _SpeechRecognitionRecognizer      SpeechRecognitionRecognizer;
typedef struct _SpeechRecognitionResultToken     SpeechRecognitionResultToken;


struct _SpeechRecognitionGrammar {
    SpeechObject object;
};

struct _SpeechRecognitionGrammarClass {
    SpeechObjectClass parent_class;
};

typedef struct _SpeechRecognitionGrammar      SpeechRecognitionGrammar;
typedef struct _SpeechRecognitionGrammarClass SpeechRecognitionGrammarClass;


struct _SpeechRecognitionDictationGrammar {
    SpeechRecognitionGrammar grammar;
};

struct _SpeechRecognitionDictationGrammarClass {
    SpeechRecognitionGrammarClass parent_class;
};

typedef struct _SpeechRecognitionDictationGrammar      SpeechRecognitionDictationGrammar;
typedef struct _SpeechRecognitionDictationGrammarClass SpeechRecognitionDictationGrammarClass;


struct _SpeechRecognitionRuleGrammar {
    SpeechRecognitionGrammar grammar;
};

struct _SpeechRecognitionRuleGrammarClass {
    SpeechRecognitionGrammarClass parent_class;
};

typedef struct _SpeechRecognitionRuleGrammar      SpeechRecognitionRuleGrammar;
typedef struct _SpeechRecognitionRuleGrammarClass SpeechRecognitionRuleGrammarClass;


struct _SpeechRecognitionResult {
    SpeechObject object;
};

struct _SpeechRecognitionResultClass {
    SpeechObjectClass parent_class;
};

typedef struct _SpeechRecognitionResult      SpeechRecognitionResult;
typedef struct _SpeechRecognitionResultClass SpeechRecognitionResultClass;


struct _SpeechRecognitionFinalResult {
    SpeechRecognitionResult result;
};

struct _SpeechRecognitionFinalResultClass {
    SpeechRecognitionResultClass parent_class;
};

typedef struct _SpeechRecognitionFinalResult      SpeechRecognitionFinalResult;
typedef struct _SpeechRecognitionFinalResultClass SpeechRecognitionFinalResultClass;


struct _SpeechRecognitionFinalRuleResult {
    SpeechRecognitionFinalResult result;
};

struct _SpeechRecognitionFinalRuleResultClass {
    SpeechRecognitionFinalResultClass parent_class;
};

typedef struct _SpeechRecognitionFinalRuleResult      SpeechRecognitionFinalRuleResult;
typedef struct _SpeechRecognitionFinalRuleResultClass SpeechRecognitionFinalRuleResultClass;


struct _SpeechRecognitionFinalDictationResult {
    SpeechRecognitionFinalResult result;
};

struct _SpeechRecognitionFinalDictationResultClass {
    SpeechRecognitionFinalResultClass parent_class;
};

typedef struct _SpeechRecognitionFinalDictationResult      SpeechRecognitionFinalDictationResult;
typedef struct _SpeechRecognitionFinalDictationResultClass SpeechRecognitionFinalDictationResultClass;


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_TYPES_H__ */
