
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Event issued to indicate detection of speech in the incoming audio stream
 * or to periodically indicate the audio input level.
 * #SpeechRecognitionRecognizerAudioEvents are issued to each
 * #SpeechRecognitionRecognizerAudioListener attached to the
 * #SpeechAudioManager for a #SpeechRecognitionRecognizer.
 *
 * #SpeechRecognitionRecognizerAudioListener events are timed with the input
 * audio. In most architectures this indicates that the events occur in
 * near to real time.
 *
 * <title>#SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED and 
 *    #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED</title>
 *
 * The #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED and 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED events
 * are used to indicate possible detection of speech in the incoming audio.
 * Applications may use this event to display visual feedback to a user
 * indicating that the recognizer is listening - for example, by maintaining
 * a icon indicating microphone status.
 *
 * It is sometimes difficult to quickly distinguish between speech and other
 * noises (e.g. coughs, microphone bumps), so the 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED
 * event is not always followed by a #SpeechRecognitionResult.
 *
 * If a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED is issued for the 
 * detected speech, it will usually occur soon after the 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED event but
 * may be delayed for a number of reasons.
 *
 * <itemizedlist>
 *   <listitem>The recognizer may be slower than real time and lag audio 
 *       input.</listitem>
 *
 *   <listitem>The recognizer may defer issuing a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED until it is 
 *       confident that it has detected speech that matches one of the 
 *       active grammars - in some cases the 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED may be issued 
 *       at the end of the spoken sentence.</listitem>
 *
 *   <listitem>The recognizer may be delayed because it is in the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state causing it to 
 *       buffer audio and "catch up" once it returns to the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state.</listitem>
 *
 *   <listitem>Many other reasons.</listitem>
 * </itemizedlist>
 *
 * Some recognizers will allow a user to speak more than one commands without
 * a break. In these cases, a single 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED event may
 * be followed by more than one 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event and result
 * finalization before the 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED event is issued.
 *
 * In longer speech (e.g. dictating a paragraph), short pauses in the user's
 * speech may lead to a 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED event followed 
 * by a #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED event as 
 * the user resumes speaking. These events do not always indicate that the 
 * current result will be finalized.
 *
 * In short, applications should treat the 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED and
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED events as 
 * operating entirely independently from the result system of a 
 * #SpeechRecognitionRecognizer.
 *
 * <title>Audio Level Events</title>
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL event indicates 
 * a change in the volume of the audio input to a recognizer. The level is 
 * defined on a scale from 0 to 1. 0.0 represents silence. 0.25 represents 
 * quiet input. 0.75 represents loud input. 1.0 indicates the maximum level.
 *
 * Audio level events are suitable for display of a visual "VU meter" (like
 * the bar on stereo systems which goes up and down with the volume).
 * Different colors are often used to indicate the different levels: for
 * example, red for loud, green for normal, and blue for background.
 *
 * Maintaining audio quality is important for reliable recognition. A common
 * problem is a user speaking too loudly or too quietly. The color on a VU
 * meter is one way to provide feedback to the user. Note that a quiet level
 * (below 0.25) does not necessarily indicate that the user is speaking too
 * quietly. The input is also quiet when the user is not speaking.
 *
 * See: #SpeechRecognitionRecognizer
 * See: #SpeechAudioManager
 * See: #SpeechRecognitionRecognizerAudioListener
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 */

#include <speech/recognition/speech_recognition_recognizer_audio_event.h>


GType
speech_recognition_recognizer_audio_event_get_type(void)
{
    printf("speech_recognition_recognizer_audio_event_get_type called.\n");
}


/**
 * speech_recognition_recognizer_audio_event_new:
 * @source: the #SpeechRecognitionRecognizer that issued the event.
 * @id: the identifier for the event type.
 *
 * Initializes an #SpeechRecognitionRecognizerAudioEvent with a specified
 * event identifier. The #audio_level is set to 0.0.
 *
 * Returns:
 */

SpeechRecognitionRecognizerAudioEvent *
speech_recognition_recognizer_audio_event_new(
                                    SpeechRecognitionRecognizer *source,
                                    int id)
{
    printf("speech_recognition_recognizer_audio_event_new called.\n");
}


/**
 * speech_recognition_recognizer_audio_event_new_with_level:
 * @source: the #SpeechRecognitionRecognizer that issued the event.
 * @id: the identifier for the event type.
 * @audio_level: the audio level for this event.
 *
 * Initializes an #SpeechRecognitionRecognizerAudioEvent with a specified
 * event identifier and audio level. The @audio_level should be 0.0 for 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED and
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED events.
 *
 * Returns:
 */

SpeechRecognitionRecognizerAudioEvent *
speech_recognition_recognizer_audio_event_new_with_level(
                                    SpeechRecognitionRecognizer *source,
                                    int id,
                                    float audio_level)
{
    printf("speech_recognition_recognizer_audio_event_new_with_level called.\n");
}


/**
 * speech_recognition_recognizer_audio_event_get_audio_level:
 * @event: the #SpeechRecognitionRecognizerAudioEvent object that this
 *         operation will be applied to.
 *
 * Get the audio input level in the range 0 to 1. A value below 0.25
 * indicates quiet input with 0.0 being silence. A value above 0.75
 * indicates loud input with 1.0 indicating the maximum level.
 *
 * The level is provided only for the 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL event type. 
 * The level should be ignored for 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED and 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED events.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL
 *
 * Returns: the audio input level in the range 0 to 1.
 */

float
speech_recognition_recognizer_audio_event_get_audio_level(
                              SpeechRecognitionRecognizerAudioEvent *event)
{
    printf("speech_recognition_recognizer_audio_event_get_audio_level called.\n");
}
