
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_DICTATION_GRAMMAR_H__
#define __SPEECH_RECOGNITION_DICTATION_GRAMMAR_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_grammar.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_DICTATION_GRAMMAR              (speech_recognition_dictation_grammar_get_type())
#define SPEECH_RECOGNITION_DICTATION_GRAMMAR(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_DICTATION_GRAMMAR, SpeechRecognitionDictationGrammar))
#define SPEECH_RECOGNITION_DICTATION_GRAMMAR_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_DICTATION_GRAMMAR, SpeechRecognitionDictationGrammarClass))
#define SPEECH_IS_RECOGNITION_DICTATION_GRAMMAR(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_DICTATION_GRAMMAR))
#define SPEECH_IS_RECOGNITION_DICTATION_GRAMMAR_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_DICTATION_GRAMMAR))
#define SPEECH_RECOGNITION_DICTATION_GRAMMAR_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_DICTATION_GRAMMAR, SpeechRecognitionDictationGrammarClass))


GType 
speech_recognition_dictation_grammar_get_type(void);

SpeechRecognitionDictationGrammar * 
speech_recognition_dictation_grammar_new(void);

void 
speech_recognition_dictation_grammar_add_word(
                              SpeechRecognitionDictationGrammar *grammar,
                              gchar *word);

void 
speech_recognition_dictation_grammar_remove_word(
                              SpeechRecognitionDictationGrammar *grammar,
                              gchar *word);

gchar ** 
speech_recognition_dictation_grammar_list_added_words(
                              SpeechRecognitionDictationGrammar *grammar);

gchar ** 
speech_recognition_dictation_grammar_list_removed_words(
                              SpeechRecognitionDictationGrammar *grammar);

void 
speech_recognition_dictation_grammar_set_context(
                              SpeechRecognitionDictationGrammar *grammar,
                              gchar *preceding,
                              gchar *following);

void 
speech_recognition_dictation_grammar_set_context_from_array(
                              SpeechRecognitionDictationGrammar *grammar,
                              gchar *preceding[],
                              gchar *following[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_DICTATION_GRAMMAR_H__ */
