
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Parent object supported by all recognition grammars including
 * #SpeechRecognitionDictationGrammar and #SpeechRecognitionRuleGrammar.
 * A #SpeechRecognitionGrammar defines a set of tokens (words) that may 
 * be spoken and the patterns in which those tokens may be spoken. 
 * Different grammar types (dictation vs. rule) define the words and the
 * patterns in different ways.
 *
 * The core functionality provided through the #SpeechRecognitionGrammar
 * object includes:
 *
 * <itemizedlist>
 *   <listitem>Naming: each grammar of a #SpeechRecognitionRecognizer has 
 *       a unique name.</listitem>
 *   <listitem>Enabling: grammars may be enabled or disabled for activation of
 *	 recognition.</listitem>
 *   <listitem>Activation: the activation mode can be set and the activation
 *       state tested.</listitem>
 *   <listitem>Adding and removing #SpeechRecognitionGrammarListeners for 
 *       grammar-related events.</listitem>
 *   <listitem>Adding and removing #SpeechRecognitionResultListeners to 
 *       receive result events for results matching the 
 *       #SpeechRecognitionGrammar.</listitem>
 *   <listitem>Access the #SpeechRecognitionRecognizer that owns the 
 *       grammar</listitem>
 * </itemizedlist>
 *
 * Each #SpeechRecognitionGrammar is associated with a specific 
 * #SpeechRecognitionRecognizer. All grammars are located, created and 
 * managed through the #SpeechRecognitionRecognizer object. The basic
 * steps in using any #SpeechRecognitionGrammar are:
 *
 * <orderedlist>
 *   <listitem> Create a new #SpeechRecognitionGrammar or get a reference 
 *       to an existing grammar through the #SpeechRecognitionRecognizer
 *       object.</listitem>
 *   <listitem> Attach a #SpeechRecognitionResultListener to get results.
 *       </listitem>
 *   <listitem> As necessary, setup or modify the grammar for the 
 *       application's context.</listitem>
 *   <listitem> Enabled and disable recognition of the 
 *       #SpeechRecognitionGrammar as required by the 
 *       application's context.</listitem>
 *   <listitem> Commit grammar changes and enabled state into the recognition
 *	 process.</listitem>
 *   <listitem> Repeat the update, enable and commit steps as required.
 *       </listitem>
 *   <listitem> For application-created grammars, delete the 
 *       #SpeechRecognitionGrammar when it is no longer needed.</listitem>
 * </orderedlist>
 *
 * <title>Grammar Types</title>
 *
 * There are two types of #SpeechRecognitionGrammar:
 * #SpeechRecognitionDictationGrammar and #SpeechRecognitionRuleGrammar.
 * Both are defined by objects that extend the #SpeechRecognitionGrammar
 * object. All recognizers support #SpeechRecognitionRuleGrammars.
 * A recognizer may optionally support a #SpeechRecognitionDictationGrammar.
 * The #SpeechRecognitionRuleGrammar and #SpeechRecognitionDictationGrammar
 * objects define specialized mechanisms for controlling and modifying
 * those types of grammar.
 *
 * <title>Grammar Activation</title>
 *
 * When a #SpeechRecognitionGrammar is active, the #SpeechRecognitionRecognizer
 * listens to incoming audio for speech that matches that
 * #SpeechRecognitionGrammar. To be activated, an application must first set
 * the #enabled property to true (enable activation) and set the
 * #activationMode property to indicate the activation conditions.
 *
 * There are three activation modes: 
 * #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS,
 * #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL and 
 * #SPEECH_RECOGNITION_GRAMMAR_GLOBAL. For each mode a certain set of 
 * activation conditions must be met for the grammar to be activated for 
 * recognition. The activation conditions are determined by
 * #SpeechRecognitionRecognizer focus and possibly by the activation of other
 * grammars. Recognizer focus is managed with the
 * speech_recognition_recognizer_request_focus() and
 * speech_recognition_recognizer_release_focus() functions of a
 * #SpeechRecognitionRecognizer.
 *
 * The modes are listed here by priority.  Always use the lowest priority
 * mode possible.
 *
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_GLOBAL activation mode: if 
 *       #enabled, the #SpeechRecognitionGrammar is always active 
 *       irrespective of whether the #SpeechRecognitionRecognizer of 
 *       this application has focus.</listitem>
 *
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL activation mode: if
 *	 #enabled, the #SpeechRecognitionGrammar is always active when the 
 *       #SpeechRecognitionRecognizer of this application has focus. 
 *       Furthermore, enabling a modal grammar deactivates any grammars in 
 *       the same #SpeechRecognitionRecognizer with the
 *	 #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS activation mode.
 *       </listitem>
 *
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS activation mode 
 *       (default): if #enabled, the #SpeechRecognitionGrammar is active 
 *       when the #SpeechRecognitionRecognizer of this application has focus.
 *       The exception is that if any other grammar of this application is 
 *       enabled with #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL activation
 *       mode, then this grammar is not activated.</listitem>
 * </itemizedlist>
 *
 * The current activation state of a grammar can be tested with the
 * speech_recognition_grammar_is_active() function. Whenever a grammar's 
 * activation changes either a 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED or 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED
 * event is issued to each attached #SpeechRecognitionGrammarListener.
 *
 * An application may have zero, one or many grammar enabled at any time. As
 * the conventions below indicate, well-behaved applications always minimize
 * the number of active grammars.
 *
 * The activation and deactivation of grammars is independent of
 * #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED states of the
 * #SpeechRecognitionRecognizer. However, when a #SpeechRecognitionRecognizer
 * is #SPEECH_ENGINE_PAUSED, audio input to the #SpeechRecognitionRecognizer is
 * turned off, so speech can't be detected. Note that just after pausing
 * a recognizer there may be some remaining audio in the input buffer
 * that could contain recognizable speech and thus an active grammar may
 * continue to receive result events.
 *
 * Well-behaved applications adhere to the following conventions to minimize
 * impact on other applications and other components of the same application:
 *
 * <itemizedlist>
 *   <listitem>Never apply the SPEECH_RECOGNITION_GRAMMAR_GLOBAL activation 
 *       mode to a #SpeechRecognitionDictationGrammar.</listitem>
 *   <listitem>Always use the default activation mode 
 *       #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS unless there is a 
 *       good reason to use another mode.</listitem>
 *   <listitem>Only use the #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL
 *       when certain that deactivating 
 *       #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS grammars will not
 *	 adversely affect the user interface.</listitem>
 *   <listitem>Minimize the complexity and the number of 
 *       #SpeechRecognitionRuleGrammars with 
 *       #SPEECH_RECOGNITION_GRAMMAR_GLOBAL activation mode.</listitem>
 *   <listitem>Only enable a grammar when it is appropriate for a user to say
 *	 something matching that grammar. Otherwise disable the grammar to
 *	 improve recognition response time and recognition accuracy for other
 *	 grammars.</listitem>
 *   <listitem>Only request focus when confident that the user's speech focus
 *	 (attention) is directed to grammars of your application. Release
 *	 focus when it is not required.</listitem>
 * </itemizedlist>
 *
 * The general principal underlying these conventions is that increasing the
 * number of active grammars and/or increasing the complexity of those
 * grammars can lead to slower response time, greater CPU load and reduced
 * recognition accuracy (more mistakes).
 *
 * <title>Committing Changes</title>
 *
 * Grammars can be dynamically changed and enabled and disabled. Changes
 * may be necessary as the application changes context, as new information
 * becomes available and so on. As with graphical interface programming most
 * grammar updates occur during processing of events. Very often grammars
 * are updated in response to speech input from a user (a
 * #SpeechRecognitionResultEvent). Other asynchronous events (e.g., Gtk+ events)
 * are another common trigger of grammar changes. Changing grammars during
 * normal operation of a recognizer is usually necessary to ensure natural
 * and usable speech-enabled applications.
 *
 * Different grammar types allow different types of run-time change.
 * #SpeechRecognitionRuleGrammars can be created and deleted during normal
 * recognizer operation. Also, any aspect of a created
 * #SpeechRecognitionRuleGrammar can be redefined: rules can be modified,
 * deleted or added, imports can be changed an so on.  Certain properties
 * of a #SpeechRecognitionDictationGrammar can be changed: the context can be
 * updated and the vocabulary modified.
 *
 * For any grammar changes to take effect they must be committed. Committing
 * changes builds the grammars into a format that can be used by the internal
 * processes of the recognizer and applies those changes to the processing of
 * incoming audio.
 *
 * There are two ways in which a commit takes place:
 *
 * <itemizedlist>
 *   <listitem>An explicit call by an application to the
 *	 speech_recognition_recognizer_commit_changes() function
 *	 of the #SpeechRecognitionRecognizer. The documentation for the
 *	 function describes when and how changes are committed when called.
 *	 (The speech_recognition_recognizer_commit_changes() function is 
 *       typically used in conjunction with the 
 *       speech_recognition_recognizer_suspend() function of
 *	 #SpeechRecognitionRecognizer.)</listitem>
 *
 *   <listitem>Changes to all grammars are implicitly committed at the 
 *       completion of processing of a result finalization event (either a
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event).
 *	 This simplifies the common scenario in which grammars are changed
 *	 during result finalization process because the user's input has
 *	 changed the application's state. (This implicit commit is deferred
 *	 following a call to speech_recognition_recognizer_suspend() and 
 *       until a call to speech_recognition_recognizer_commit_changes()).
 *       </listitem>
 * </itemizedlist>
 *
 * The #SpeechRecognitionRecognizer issues a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event to
 * signal that changes have been committed. This event signals a 
 * transition from the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * state to the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state. 
 * Once in the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state the 
 * #SpeechRecognitionRecognizer resumes the processing of incoming 
 * audio with the newly committed grammars.
 *
 * Also each changed #SpeechRecognitionGrammar receives a 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_GRAMMAR_CHANGES_COMMITTED through
 * attached #SpeechRecognitionGrammarListeners whenever changes to it are 
 * committed.
 *
 * The commit changes mechanism has two important properties:
 *
 * <itemizedlist>
 *   <listitem>Updates to grammar definations and the enabled property take 
 *       effect atomically (all changes take effect at once). There are no
 *	 intermediate states in which some, but not all, changes have been
 *	 applied.</listitem>
 *
 *   <listitem>It is a function of #SpeechRecognitionRecognizer so all changes 
 *       to all #SpeechRecognitionGrammars are committed at once. Again, 
 *       there are no intermediate states in which some, but not all, 
 *       changes have been applied.</listitem>
 * </itemizedlist>
 *
 * <title>Grammar and Result Listeners</title>
 *
 * An application can attach one or more #SpeechRecognitionGrammarListeners to
 * any #SpeechRecognitionGrammar. The listener is notified of status changes
 * for the #SpeechRecognitionGrammar: when changes are committed and when
 * activation changes.
 *
 * An application can attach one or more #SpeechRecognitionResultListener 
 * objects to each #SpeechRecognitionGrammar. The listener is notified of 
 * all events for all results that match this grammar. The listeners receive
 * #SpeechRecognitionResultEvents starting with the 
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event (not the 
 * preceding #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED or
 * SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events).
 *
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionDictationGrammar
 * See: #SpeechRecognitionGrammarListener
 * See: #SpeechRecognitionResultListener
 * See: #SpeechRecognitionResult
 * See: #SpeechRecognitionRecognizer
 * See: speech_recognition_recognizer_commit_changes()
 */

#include <speech/recognition/speech_recognition_grammar.h>


GType
speech_recognition_grammar_get_type(void)
{
    printf("speech_recognition_grammar_get_type called.\n");
}


SpeechRecognitionGrammar *
speech_recognition_grammar_new(void)
{
    printf("speech_recognition_grammar_new called.\n");
}


/**
 * speech_recognition_grammar_add_grammar_listener:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 * @listener: grammar listener to add.
 *
 * Request notifications of events of related to this
 * #SpeechRecognitionGrammar. An application can attach multiple
 * listeners to a #SpeechRecognitionGrammar.
 *
 * See: speech_recognition_grammar_remove_grammar_listener()
 */

void
speech_recognition_grammar_add_grammar_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionGrammarListener *listener)
{
    printf("speech_recognition_grammar_add_grammar_listener called.\n");
}


/**
 * speech_recognition_grammar_remove_grammar_listener:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 * @listener: grammar listener to remove.
 *
 * Remove a listener from this #SpeechRecognitionGrammar.
 *
 * See: speech_recognition_grammar_add_grammar_listener()
 */

void
speech_recognition_grammar_remove_grammar_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionGrammarListener *listener)
{
    printf("speech_recognition_grammar_remove_grammar_listener called.\n");
}


/**
 * speech_recognition_grammar_add_result_listener:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 * @listener: result listener to add.
 *
 * Request notifications of events from any #SpeechRecognitionResult
 * that matches this #SpeechRecognitionGrammar. An application can
 * attach multiple ResultListeners to a #SpeechRecognitionGrammar.
 * A listener is removed with the 
 * speech_recognition_result_remove_result_listener() function.
 *
 * A #SpeechRecognitionResultListener attached to a #SpeechRecognitionGrammar
 * receives result events starting from the
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event - the event 
 * which indicates that the matched grammar is known. A 
 * #SpeechRecognitionResultListener attached to a #SpeechRecognitionGrammar 
 * will never receive a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED 
 * event and does not receive any 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events that occurred 
 * before the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event.
 * A #SpeechRecognitionResultListener attached to a #SpeechRecognitionGrammar
 * is guaranteed to receive a result finalization event - 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED - some time after the
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event.
 *
 * #SpeechRecognitionResultListener objects can also be attached to a
 * #SpeechRecognitionRecognizer and to any #SpeechRecognitionResult. A
 * listener attached to the #SpeechRecognitionRecognizer receives
 * all events for all results produced by that #SpeechRecognitionRecognizer.
 * A listener attached to a #SpeechRecognitionResult receives all events
 * for that result from the time at which the listener is attached.
 *
 * See: speech_recognition_recognizer_remove_result_listener()
 * See: speech_recognition_recognizer_add_result_listener()
 * See: speech_recognition_result_add_result_listener()
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED
 */

void
speech_recognition_grammar_add_result_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionResultListener *listener)
{
    printf("speech_recognition_grammar_add_result_listener called.\n");
}


/**
 * speech_recognition_grammar_remove_result_listener:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 * @listener: the result listener to remove.
 *
 * Remove a #SpeechRecognitionResultListener from this 
 * #SpeechRecognitionGrammar.
 *
 * See: speech_recognition_grammar_add_result_listener()
 * See: speech_recognition_recognizer_remove_result_listener()
 * See: speech_recognition_grammar_remove_result_listener()
 */

void
speech_recognition_grammar_remove_result_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionResultListener *listener)
{
    printf("speech_recognition_grammar_remove_result_listener called.\n");
}


/**
 * speech_recognition_grammar_get_activation_mode:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 *
 * Return the current activation mode for the #SpeechRecognitionGrammar.
 * The default value for a grammar is 
 * #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS.
 *
 * Returns: the current activation mode for the #SpeechRecognitionGrammar.
 *
 * See: speech_recognition_grammar_set_activation_mode()
 * See: speech_recognition_grammar_set_enabled()
 * See: speech_recognition_grammar_is_active()
 */

int
speech_recognition_grammar_get_activation_mode(
                                          SpeechRecognitionGrammar *grammar)
{
    printf("speech_recognition_grammar_get_activation_mode called.\n");
}


/**
 * speech_recognition_grammar_get_name:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 *
 * Get the name of a grammar. A grammar's name must be unique for
 * a recognizer. Grammar names use a similar naming convention to
 * Java classes. The naming convention are defined in the Java
 * Speech Grammar Format Specification.
 *
 * Grammar names are used with a #SpeechRecognitionRuleGrammar for
 * resolving imports and references between grammars. The name of
 * a #SpeechRecognitionRuleGrammar is set when the grammar is created
 * (either by loading a grammar formatted document or creating a
 * new #SpeechRecognitionRuleGrammar).
 *
 * The name of a #SpeechRecognitionDictationGrammar should reflect the
 * language domain it supports. For example:
 * com.acme.dictation.us.general for general US Dictation from
 * Acme corporation. Since a #SpeechRecognitionDictationGrammar is
 * built into a #SpeechRecognitionRecognizer, its name is determined
 * by the #SpeechRecognitionRecognizer.
 *
 * Returns: the name of the grammar.
 *
 * See: #SpeechRecognitionDictationGrammar
 * See: #SpeechRecognitionRuleGrammar
 */

gchar *
speech_recognition_grammar_get_name(SpeechRecognitionGrammar *grammar)
{
    printf("speech_recognition_grammar_get_name called.\n");
}


/**
 * speech_recognition_grammar_get_recognizer:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 *
 * Returns: a reference to the #SpeechRecognitionRecognizer that owns
 *	   this grammar.
 */

SpeechRecognitionRecognizer *
speech_recognition_grammar_get_recognizer(SpeechRecognitionGrammar *grammar)
{
    printf("speech_recognition_grammar_get_recognizer called.\n");
}


/**
 * speech_recognition_grammar_is_active:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 *
 * Test whether a #SpeechRecognitionGrammar is currently active for
 * recognition. When a grammar is active, the recognizer is
 * matching incoming audio against the grammar (and other active
 * grammars) to detect speech that matches the grammar.
 *
 * A #SpeechRecognitionGrammar is activated for recognition if the
 * #enabled property is set to true and the activation conditions are met.
 * Activation is not directly controlled by applications and so
 * can only be tested (there is no ...set_active function).
 *
 * Rules of a #SpeechRecognitionRuleGrammar can be individuallly enabled
 * and disabled. However all rules share the same #ActivationMode and the 
 * same activation state. Thus, when a #SpeechRecognitionRuleGrammar is 
 * active, all the enabled rules of the grammar are active for recognition.
 * Changes in the activation state are indicated by 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED
 * and #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED events issued 
 * to the #SpeechRecognitionGrammarListener. A change in activation state can
 * follow these #SpeechRecognitionRecognizerEvents:
 *
 * <itemizedlist>
 *   <listitem>A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED 
 *       event that applies a change in the #enabled state or
 *	 #ActivationMode of this or another #SpeechRecognitionGrammar.
 *       </listitem>
 *   <listitem>A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED or 
 *       SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event.</listitem>
 * </itemizedlist>
 *
 * Returns: a boolean indication of whether this grammar is active.
 *
 * See: speech_recognition_grammar_set_enabled()
 * See: speech_recognition_grammar_set_activation_mode()
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST
 */

gboolean
speech_recognition_grammar_is_active(SpeechRecognitionGrammar *grammar)
{
    printf("speech_recognition_grammar_is_active called.\n");
}


/**
 * speech_recognition_grammar_is_enabled:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 *
 * Return the #enabled property of the #SpeechRecognitionGrammar. More 
 * specialized behaviour is specified by the #SpeechRecognitionRuleGrammar 
 * object.
 *
 * Returns: the #enabled property of the #SpeechRecognitionGrammar.
 *
 * See: speech_recognition_grammar_set_enabled()
 * See: speech_recognition_grammar_is_enabled()
 * See: speech_recognition_rule_grammar_is_enabled_with_rule_name()
 */

gboolean
speech_recognition_grammar_is_enabled(SpeechRecognitionGrammar *grammar)
{
    printf("speech_recognition_grammar_is_enabled called.\n");
}


/**
 * speech_recognition_grammar_set_activation_mode:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 * @mode: the grammar activation mode.
 *
 * Set the activation mode of a #SpeechRecognitionGrammar as
 * #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS, 
 * #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL, or
 * SPEECH_RECOGNITION_GRAMMAR_GLOBAL. The role of the activation mode in the
 * activation conditions for a #SpeechRecognitionGrammar are described above.
 * The default activation mode - #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS
 * - should be used unless there is a user interface design reason to use 
 * another mode.
 * 
 * The individual rules of a #SpeechRecognitionRuleGrammar can be
 * separately enabled and disabled. However, all rules share the
 * same #ActivationMode since the mode is a property of the complete 
 * #SpeechRecognitionGrammar. A consequence is that all enabled rules 
 * of a #SpeechRecognitionRuleGrammar are activated and deactivated together.
 *
 * A change in activation mode only takes effect once changes are committed.
 * For some recognizers changing the activation mode is computationally 
 * expensive.
 *
 * The activation mode of a grammar can be tested by the
 * speech_recognition_grammar_get_activation_mode() function.
 *
 * [Note: future releases may modify the set of activation modes.]
 *
 * See: speech_recognition_grammar_get_activation_mode()
 * See: #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS
 * See: #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL
 * See: #SPEECH_RECOGNITION_GRAMMAR_GLOBAL
 * See: speech_recognition_grammar_set_enabled()
 * See: speech_recognition_grammar_is_active()
 * See: speech_recognition_recognizer_commit_changes()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if an attempt is made to set
 *		#SPEECH_RECOGNITION_GRAMMAR_GLOBAL mode for a
 *		#SpeechRecognitionDictationGrammar</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_grammar_set_activation_mode(
                                     SpeechRecognitionGrammar *grammar,
                                     int mode)
{
    printf("speech_recognition_grammar_set_activation_mode called.\n");
}


/**
 * speech_recognition_grammar_set_enabled:
 * @grammar: the #SpeechRecognitionGrammar object that this operation will be
 *           applied to.
 * @enabled: the state for the enabled property of a
 *		  #SpeechRecognitionGrammar.
 *
 * Set the enabled property of a #SpeechRecognitionGrammar. A change in
 * the enabled property takes effect only after grammar changes are 
 * committed.  Once a grammar is enabled and when the activation conditions 
 * are met, it is activated for recognition. When a grammar is activated, the
 * #SpeechRecognitionRecognizer listens to incoming audio for speech that
 * matches the grammar and produces a #SpeechRecognitionResult when matching 
 * speech is detected.
 *
 * The enabled property of a grammar is tested with the
 * speech_recognition_grammar_is_enabled() function. The activation state 
 * of a grammar is tested with the speech_recognition_grammar_is_active()
 * function.
 *
 * The #SpeechRecognitionRuleGrammar object extends the enabling
 * property to allow individual rules to be enabled and disabled.
 *
 * See: speech_recognition_recognizer_commit_changes()
 * See: speech_recognition_grammar_set_enabled()
 * See: speech_recognition_rule_grammar_set_enabled_for_rule()
 * See: speech_recognition_rule_grammar_set_enabled_for_rules()
 * See: speech_recognition_rule_grammar_is_enabled_with_rule_name()
 */

void
speech_recognition_grammar_set_enabled(SpeechRecognitionGrammar *grammar,
                                       gboolean enabled)
{
    printf("speech_recognition_grammar_set_enabled called.\n");
}
