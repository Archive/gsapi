
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Represents the output of a parse of a #SpeechRecognitionResult or a string
 * against a #SpeechRecognitionRuleGrammar. The #SpeechRecognitionRuleParse 
 * object indicates how the result or text matches to the rules of the
 * #SpeechRecognitionRuleGrammar and the rules imported by that grammar.
 *
 * The #SpeechRecognitionRuleParse structure returned by parsing closely 
 * matches the structure of the grammar it is parsed against: if the grammar
 * contains #SpeechRecognitionRuleTag, #SpeechRecognitionRuleSequence,
 * #SpeechRecognitionRuleToken objects and so on, the returned
 * #SpeechRecognitionRuleParse will contain paired objects.
 *
 * The #SpeechRecognitionRuleParse object itself represents the match of text
 * to a named rule or to any rule referenced within a rule. The rulename
 * field of the #SpeechRecognitionRuleParse is the fully-qualified name of the
 * rule being matched. The #SpeechRecognitionRule field of the
 * #SpeechRecognitionRuleParse represents the parse structure (how that 
 * rulename is matched).
 *
 * The expansion (or logical structure) of a #SpeechRecognitionRuleParse 
 * matches the structure of the definition of the rule being parsed. The 
 * following indicates the mapping of an entity in the rule being parsed 
 * to the paired object in the #SpeechRecognitionRuleParse.
 *
 * <itemizedlist>
 *   <listitem>#SpeechRecognitionRuleAlternatives: maps to a
 *	 #SpeechRecognitionRuleAlternatives object containing a single
 *	 #SpeechRecognitionRule object for the one entity in the set of
 *	 alternatives that is matched.</listitem>
 *   <listitem>#SpeechRecognitionRuleCount: maps to a 
 *       #SpeechRecognitionRuleSequence containing a #SpeechRecognitionRule 
 *       for each match of the rule contained by #SpeechRecognitionRuleCount.
 *       The sequence may contain zero, one or multiple rule matches (for 
 *       optional, zero-or-more or one-or-more operators).</listitem>
 *   <listitem>#SpeechRecognitionRuleName: maps to a 
 *       #SpeechRecognitionRuleParse indicating how the referenced rule was 
 *       matched. The rulename field of the #SpeechRecognitionRuleParse is 
 *       the matched #SpeechRecognitionRuleName. The exception for NULL is 
 *       described below.</listitem>
 *   <listitem>#SpeechRecognitionRuleSequence: maps to a 
 *       #SpeechRecognitionRuleSequence with a matching rule for each rule 
 *       in the original sequence.</listitem>
 *   <listitem>#SpeechRecognitionRuleTag: maps to a matching 
 *       #SpeechRecognitionRuleTag (same tag) with a match of the contained 
 *       rule.</listitem>
 *   <listitem>#SpeechRecognitionRuleToken: maps to an identical 
 *       #SpeechRecognitionRuleToken object.</listitem>
 * </itemizedlist>
 *
 * [Note: the #SpeechRecognitionRuleParse object is never used in defining a
 * #SpeechRecognitionRuleGrammar so it doesn't need to be matched.]
 *
 *
 * If a #SpeechRecognitionRuleName object in a grammar is
 * &lt;%NULL&gt;, then the #SpeechRecognitionRuleParse contains
 * the &lt;NULL&gt; object too.
 *
 * <title>Example</title>
 *
 * Consider a simple grammar:
 *
 *   public &lt;command&gt; = &lt;action&gt; &lt;object&gt; [&lt;polite&gt;]
 *   &lt;action&gt; = open {OP} | close {CL} | move {MV};
 *   &lt;object&gt; = [&lt;this_that_etc&gt;] (window | door);
 *   &lt;this_that_etc&gt; = a | the | this | that | the current;
 *   &lt;polite&gt; = please | kindly;
 *
 * We will analyze the parse of "close that door please" against
 * &lt;command&gt; rule which is returned by the 
 * speech_recognition_rule_grammar_parse() function of the 
 * #SpeechRecognitionRuleGrammar against the &lt;command&gt; rule:
 *
 *   result = speech_recognition_rule_grammar_parse(rule_grammar,
 *                                         "close that door please", 
 *                                         "command",
 *                                         rule_parse);
 *
 * The call returns a #SpeechRecognitionRuleParse that is the match of
 * "close that door please" against &lt;command&gt;.
 *
 * Because &lt;command&gt; is defined as a sequence of 3 entities
 * (action, object and optional polite), the #SpeechRecognitionRuleParse
 * will contain a #SpeechRecognitionRuleSequence with length 3.
 *
 * The first two entities in &lt;command&gt; are #SpeechRecognitionRuleNames,
 * so the first two entities in the parse's #SpeechRecognitionRuleSequence 
 * will be #SpeechRecognitionRuleParse objects with rulenames of "action" 
 * and "object".
 *
 * The third entity in &lt;command&gt; is an optional 
 * #SpeechRecognitionRuleName (a #SpeechRecognitionRuleCount containing 
 * a #SpeechRecognitionRuleName), so the third entity in the sequence is 
 * a #SpeechRecognitionRuleSequence containing a single 
 * #SpeechRecognitionRuleParse indicating how the &lt;polite&gt; rule is 
 * matched. (Recall that a #SpeechRecognitionRuleCount object maps to a 
 * #SpeechRecognitionRuleSequence).
 *
 * The #SpeechRecognitionRuleParse for &lt;polite&gt; will contain a
 * #SpeechRecognitionRuleAlternatives object with the single entry which is a
 * #SpeechRecognitionRuleToken set to "please". Skipping the rest of the
 * structure, the entire #SpeechRecognitionRuleParse object has the following
 * structure.
 *
 * RuleParse(<em>&lt;command&gt;</em> =	    // Match &lt;command&gt;
 *   RuleSequence(                          //	by a sequence of 3 entities
 *	RuleParse(<em>&lt;action&gt;</em> =  // First match &lt;action&gt;
 *	 RuleAlternatives(		    // One of a set of alternatives
 *	   RuleTag(                         // matching the tagged
 *		RuleToken("<b>close</b>"), "CL")))	   //	token "close"
 *	RuleParse(<em>&lt;object&gt;</em> =	   // Now match &lt;object&gt;
 *	 RuleSequence(                      //	 by a sequence of 2 entities
 *	   RuleSequence(		    // RuleCount becomes RuleSequence
 *		RuleParse(<em>&lt;this_that_etc&gt;</em> =	   // Match &lt;this_that_etc&gt;
 *		RuleAlternatives(	    // One of a set of alternatives
 *		 RuleToken("<b>that</b>"))))	   //	is the token "that"
 *	   RuleAlternatives(			   // Match "window | door"
 *		RuleToken("<b>door</b>"))))	   //	as the token "door"
 *	RuleSequence(                        // RuleCount becomes RuleSequence
 *	 RuleParse(<em>&lt;polite&gt;</em> =	   // Now match &lt;polite&gt;
 *	   RuleAlternatives(			   //	by 1 of 2 alternatives
 *		RuleToken("<b>please</b>"))))	   // The token "please"
 *   )
 * )
 *
 * (Parse structures are hard to read and understand but can be easily
 * processed by recursive function calls.)
 *
 * See: #SpeechRecognitionRule
 * See: #SpeechRecognitionRuleAlternatives
 * See: #SpeechRecognitionRuleCount
 * See: #SpeechRecognitionRuleGrammar
 * See: speech_recognition_rule_grammar_parse()
 * See: #SpeechRecognitionRuleName
 * See: #SpeechRecognitionRuleSequence
 * See: #SpeechRecognitionRuleTag
 * See: #SpeechRecognitionRuleToken
 */

#include <speech/recognition/speech_recognition_rule_parse.h>


GType
speech_recognition_rule_parse_get_type(void)
{
    printf("speech_recognition_rule_parse_get_type called.\n");
}


SpeechRecognitionRuleParse *
speech_recognition_rule_parse_new(void)
{
    printf("speech_recognition_rule_parse_new called.\n");
}


/**
 * speech_recognition_rule_parse_new_with_named_rule:
 * @rule_name: the name of the rule.
 * @rule: the rule.
 *
 * Initialize a #SpeechRecognitionRuleParse object for a named rule
 * and a #SpeechRecognitionRule object that represents the parse
 * structure. The structure of the rule object is described
 * above. The rulename should be a fully-qualified name.
 *
 * Returns:
 */

SpeechRecognitionRuleParse *
speech_recognition_rule_parse_new_with_named_rule(
                                    SpeechRecognitionRuleName *rule_name,
                                    SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_parse_new_with_named_rule called.\n");
}


/**
 * speech_recognition_rule_parse_get_rule_name:
 * @rule_parse: the #SpeechRecognitionRuleParse object that this operation
 *              will be applied to.
 *
 * Returns: the matched #SpeechRecognitionRuleName. Should be a
 * fully-qualified rulename.
 */

SpeechRecognitionRuleName *
speech_recognition_rule_parse_get_rule_name(
                                      SpeechRecognitionRuleParse *rule_parse)
{
    printf("speech_recognition_rule_parse_get_rule_name called.\n");
}


/**
 * speech_recognition_rule_parse_set_rule_name:
 * @rule_parse: the #SpeechRecognitionRuleParse object that this operation
 *              will be applied to.
 * @rule_name: the name of the rule.
 *
 * Set the matched #SpeechRecognitionRuleName. Should be a
 * fully-qualified rulename.
 */

void
speech_recognition_rule_parse_set_rule_name(
                                    SpeechRecognitionRuleParse *rule_parse,
                                    SpeechRecognitionRuleName *rule_name)
{
    printf("speech_recognition_rule_parse_set_rule_name called.\n");
}


/**
 * speech_recognition_rule_parse_get_rule:
 * @rule_parse: the #SpeechRecognitionRuleParse object that this operation
 *              will be applied to.
 *
 * Returns: the #SpeechRecognitionRule matched by the #SpeechRecognitionRuleName.
 */

SpeechRecognitionRule *
speech_recognition_rule_parse_get_rule(SpeechRecognitionRuleParse *rule_parse)
{
    printf("speech_recognition_rule_parse_get_rule called.\n");
}


/**
 * speech_recognition_rule_parse_set_rule:
 * @rule_parse: the #SpeechRecognitionRuleParse object that this operation
 *              will be applied to.
 * @rule: the rule.
 *
 * Set the #SpeechRecognitionRule object matched to the 
 * #SpeechRecognitionRuleName.
 */

void
speech_recognition_rule_parse_set_rule(SpeechRecognitionRuleParse *rule_parse,
                                       SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_parse_set_rule called.\n");
}


/**
 * speech_recognition_rule_parse_get_tags:
 * @rule_parse: the #SpeechRecognitionRuleParse object that this operation
 *              will be applied to.
 *
 * List the tags matched in this parse structure. Tags are listed in
 * the order of tokens (from start to end) and from the lowest to
 * highest attachment. (See the
 * speech_recognition_final_rule_result_get_tags() function for an example.)
 *
 * Returns: a list of the tags matched in this parse structure.
 *
 * See: speech_recognition_final_rule_result_get_tags()
 */

gchar **
speech_recognition_rule_parse_get_tags(SpeechRecognitionRuleParse *rule_parse)
{
    printf("speech_recognition_rule_parse_get_tags called.\n");
}
