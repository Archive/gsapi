
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The functions of a #SpeechRecognitionResultListener receive notifications 
 * of events related to a #SpeechRecognitionResult object. A 
 * #SpeechRecognitionResultListener may be attached to any of three entities:
 *
 * <itemizedlist>
 *   <listitem>#SpeechRecognitionRecognizer: a #SpeechRecognitionResultListener
 *       attached to a #SpeechRecognitionRecognizer receives notification of 
 *       all #SpeechRecognitionResultEvents for all results produced by that 
 *       recognizer.</listitem>
 *   <listitem>#SpeechRecognitionGrammar: a #SpeechRecognitionResultListener 
 *       attached to a #SpeechRecognitionGrammar receives all events for all 
 *       results that have been finalized for that grammar. Specifically, it 
 *       receives the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 *       event and following events. (Note: it never receives a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event because
 *	 that event always precedes the 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event).</listitem>
 *   <listitem>#SpeechRecognitionResult: a #SpeechRecognitionResultListener 
 *       attached to a #SpeechRecognitionResult receives all events for that 
 *       result starting from the time at which the listener is attached. 
 *       (Note: it never receives a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event because a 
 *       listener can only be attached to a #SpeechRecognitionResult once a
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event has already 
 *       been issued.)</listitem>
 * </itemizedlist>
 *
 * The events are issued to the listeners in the order of most specific to
 * least specific: #SpeechRecognitionResultListeners attached to the
 * #SpeechRecognitionResult are notified first, then listeners attached to the
 * matched #SpeechRecognitionGrammar, and finally to listeners attached to the
 * #SpeechRecognitionRecognizer.
 *
 * A single #SpeechRecognitionResultListener may be attached to multiple 
 * objects (#SpeechRecognitionRecognizer, #SpeechRecognitionGrammar or 
 * #SpeechRecognitionResult), and multiple #SpeechRecognitionResultListeners 
 * may be attached to a single object.
 *
 * The source for all #SpeechRecognitionResultEvents issued to a
 * #SpeechRecognitionResultListener is the #SpeechRecognitionResult that 
 * generated the event. The full state system for results and the associated 
 * events are described in the documentation for #SpeechRecognitionResultEvent.
 *
 * See: #SpeechRecognitionResultEvent
 * See: speech_recognition_result_add_result_listener()
 * See: speech_recognition_grammar_add_result_listener()
 * See: speech_recognition_recognizer_add_result_listener()
 */

#include <speech/recognition/speech_recognition_result_listener.h>


GType
speech_recognition_result_listener_get_type(void)
{
    printf("speech_recognition_result_listener_get_type called.\n");
}


SpeechRecognitionResultListener *
speech_recognition_result_listener_new(void)
{
    printf("speech_recognition_result_listener_new called.\n");
}


/**
 * speech_recognition_result_listener_audio_released:
 * @listener: the #SpeechRecognitionResultListener object that this operation
 *            will be applied to.
 * @e: the #SpeechRecognitionResult event.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED event has occured. 
 * This event is only issued to finalized results. See the documentation 
 * of the speech_recognition_final_result_is_audio_available() function 
 * in the #SpeechRecognitionFinalResult object for details.
 *
 * The event is issued to each #SpeechRecognitionResultListener attached
 * to the #SpeechRecognitionRecognizer and to the #SpeechRecognitionResult.
 * If a #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event was issued,
 * then the matched #SpeechRecognitionGrammar is known, and the event is also
 * issued to each #SpeechRecognitionResultListener attached to that
 * #SpeechRecognitionGrammar.
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED
 * See: speech_recognition_final_result_is_audio_available()
 */

void
speech_recognition_result_listener_audio_released(
                                     SpeechRecognitionResultListener *listener,
                                     SpeechRecognitionResultEvent *e)
{
    printf("speech_recognition_result_listener_audio_released called.\n");
}


/**
 * speech_recognition_result_listener_grammar_finalized:
 * @listener: the #SpeechRecognitionResultListener object that this operation
 *            will be applied to.
 * @e: the #SpeechRecognitionResult event.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event has occured 
 * because the #SpeechRecognitionRecognizer has determined which 
 * #SpeechRecognitionGrammar is matched by the incoming speech.
 *
 * The event is issued to each #SpeechRecognitionResultListener attached
 * to the #SpeechRecognitionRecognizer, #SpeechRecognitionResult, and matched
 * #SpeechRecognitionGrammar.
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 */

void
speech_recognition_result_listener_grammar_finalized(
                                     SpeechRecognitionResultListener *listener,
                                     SpeechRecognitionResultEvent *e)
{
    printf("speech_recognition_result_listener_grammar_finalized called.\n");
}


/**
 * speech_recognition_result_listener_result_accepted:
 * @listener: the #SpeechRecognitionResultListener object that this operation
 *            will be applied to.
 * @e: the #SpeechRecognitionResult event.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event has occured 
 * indicating that a #SpeechRecognitionResult has transitioned from the
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state to the 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED state.
 *
 * Since the #SpeechRecognitionResult source for this event is finalized,
 * the #SpeechRecognitionResult object can be safely cast to the
 * #SpeechRecognitionFinalResult object.
 *
 * Because the result is accepted, the matched #SpeechRecognitionGrammar
 * for the result is guaranteed to be non-null. If the matched
 * #SpeechRecognitionGrammar is a #SpeechRecognitionRuleGrammar, then the
 * result object can be safely cast to #SpeechRecognitionFinalRuleResult
 * (functions of #SpeechRecognitionFinalDictationResult return an exception).
 * If the matched #SpeechRecognitionGrammar is a
 * #SpeechRecognitionDictationGrammar, then the result object can be
 * safely cast to #SpeechRecognitionFinalDictationResult (functions of
 * #SpeechRecognitionFinalRuleResult return an exception).
 *
 * The event is issued to each #SpeechRecognitionResultListener attached
 * to the #SpeechRecognitionRecognizer, #SpeechRecognitionResult, and matched
 * #SpeechRecognitionGrammar.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event is issued 
 * following the #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 * #SpeechRecognitionRecognizerEvent and while the 
 * #SpeechRecognitionRecognizer is in the #SPEECH_RECOGNITION_RESULT_SUSPENDED
 * state. Once the #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RESULT_ACCEPTED
 * event has been processed by all listeners, the #SpeechRecognitionRecognizer
 * automatically commits all changes to grammars and returns to the 
 * SPEECH_RECOGNITION_RESULT_LISTENING state. The only exception is when a 
 * call has been made to speech_recognition_recognizer_suspend()
 * without a following call to speech_recognition_recognizer_commit_changes().
 * In this case the #SpeechRecognitionRecognizer remains 
 * #SPEECH_RECOGNITION_RESULT_SUSPENDED until 
 * speech_recognition_recognizer_commit_changes() is called.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RESULT_ACCEPTED
 * See: #SpeechRecognitionFinalResult
 * See: #SpeechRecognitionFinalRuleResult
 * See: #SpeechRecognitionFinalDictationResult
 * See: #SpeechRecognitionRecognizerEvent
 * See: speech_recognition_recognizer_commit_changes()
 */

void
speech_recognition_result_listener_result_accepted(
                                     SpeechRecognitionResultListener *listener,
                                     SpeechRecognitionResultEvent *e)
{
    printf("speech_recognition_result_listener_result_accepted called.\n");
}


/**
 * speech_recognition_result_listener_result_created:
 * @listener: the #SpeechRecognitionResultListener object that this operation
 *            will be applied to.
 * @e: the #SpeechRecognitionResult event.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event is issued when a
 * #SpeechRecognitionRecognizer detects incoming speech that may match
 * an active grammar of an application.
 *
 * The event is issued to each #SpeechRecognitionResultListener attached
 * to the #SpeechRecognitionRecognizer. (#SpeechRecognitionResultListeners
 * attached to a #SpeechRecognitionGrammar or to a #SpeechRecognitionResult
 * never receive a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event.)
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED follows the
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING event that 
 * is issued #SpeechRecognitionRecognizerListeners to indicate that the
 * #SpeechRecognitionRecognizer has changed from the
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state.
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 */

void
speech_recognition_result_listener_result_created(
                                     SpeechRecognitionResultListener *listener,
                                     SpeechRecognitionResultEvent *e)
{
    printf("speech_recognition_result_listener_result_created called.\n");
}


/**
 * speech_recognition_result_listener_result_rejected:
 * @listener: the #SpeechRecognitionResultListener object that this operation
 *            will be applied to.
 * @e: the #SpeechRecognitionResult event.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event has occured 
 * indicating that a #SpeechRecognitionResult has transitioned from the
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state to the 
 * #SPEECH_RECOGNITION_RESULT_REJECTED state.
 *
 * The casting behavior of a rejected result is the same as for a
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event. The exception 
 * is that if the grammar is not known (no 
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event),
 * then the result cannot be cast to either
 * #SpeechRecognitionFinalRuleResult or #SpeechRecognitionFinalDictationResult.
 *
 * The state behavior and grammar committing actions are the same as
 * for the SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event.
 *
 * The event is issued to each #SpeechRecognitionResultListener attached
 * to the #SpeechRecognitionRecognizer and to the #SpeechRecognitionResult.
 * If a SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event was issued,
 * then the matched #SpeechRecognitionGrammar is known, and the event is also
 * issued to each #SpeechRecognitionResultListener attached to that
 * #SpeechRecognitionGrammar.
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED
 * See: #SpeechRecognitionFinalResult
 * See: #SpeechRecognitionFinalRuleResult
 * See: #SpeechRecognitionFinalDictationResult
 */

void
speech_recognition_result_listener_result_rejected(
                                     SpeechRecognitionResultListener *listener,
                                     SpeechRecognitionResultEvent *e)
{
    printf("speech_recognition_result_listener_result_rejected called.\n");
}


/**
 * speech_recognition_result_listener_result_updated:
 * @listener: the #SpeechRecognitionResultListener object that this operation
 *            will be applied to.
 * @e: the #SpeechRecognitionResult event.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED event has occured 
 * because a token has been finalized and/or the unfinalized text of a 
 * result has changed.
 *
 * The event is issued to each #SpeechRecognitionResultListener attached
 * to the #SpeechRecognitionRecognizer, to each 
 * #SpeechRecognitionResultListener attached to the #SpeechRecognitionResult,
 * and if the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event has 
 * already been released to each #SpeechRecognitionResultListener attached 
 * to the matched #SpeechRecognitionGrammar.
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED
 */

void
speech_recognition_result_listener_result_updated(
                                     SpeechRecognitionResultListener *listener,
                                     SpeechRecognitionResultEvent *e)
{
    printf("speech_recognition_result_listener_result_updated called.\n");
}


/**
 * speech_recognition_result_listener_training_info_released:
 * @listener: the #SpeechRecognitionResultListener object that this operation
 *            will be applied to.
 * @e: the #SpeechRecognitionResult event.
 *
 * A #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event has 
 * occured. This event is only issued to finalized results. See the 
 * documentation of the 
 * speech_recognition_recognizer_properties_is_training_provided() function
 * of the #SpeechRecognitionFinalResult object for details.
 *
 * The event is issued to each #SpeechRecognitionResultListener attached
 * to the #SpeechRecognitionRecognizer and to the #SpeechRecognitionResult.
 * If a #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event was issued,
 * then the matched #SpeechRecognitionGrammar is known, and the event is also
 * issued to each #SpeechRecognitionResultListener attached to that
 * #SpeechRecognitionGrammar.
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED
 * See: speech_recognition_recognizer_properties_is_training_provided()
 */

void
speech_recognition_result_listener_training_info_released(
                                     SpeechRecognitionResultListener *listener,
                                     SpeechRecognitionResultEvent *e)
{
    printf("speech_recognition_result_listener_training_info_released called.\n");
}
