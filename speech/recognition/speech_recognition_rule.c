
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionRule object is the basic component of a
 * #SpeechRecognitionRuleGrammar and represents anything that may appear on
 * the right-hand side of a rule definition in Java Speech Grammar Format.
 * Technically a #SpeechRecognitionRule represents a grammat formatted 
 * "expansion".
 *
 * #SpeechRecognitionRule is an abstract class that is sub-classed by:
 *
 * <itemizedlist>
 *   <listitem>#SpeechRecognitionRuleAlternatives: set of alternatives
 *       #SpeechRecognitionRule objects</listitem>
 *   <listitem>#SpeechRecognitionRuleCount: contains a 
 *       #SpeechRecognitionRule that may occur optionally, zero or more 
 *       times, or one or more times.</listitem>
 *   <listitem>#SpeechRecognitionRuleName: reference to a 
 *       #SpeechRecognitionRule</listitem>
 *   <listitem>#SpeechRecognitionRuleSequence: set of rules that occur 
 *       in sequence</listitem>
 *   <listitem>#SpeechRecognitionRuleTag: contains a #SpeechRecognitionRule 
 *       tagged by a string</listitem>
 *   <listitem>#SpeechRecognitionRuleToken: reference to a token that may 
 *       be spoken.</listitem>
 * </itemizedlist>
 *
 * Another sub-class of #SpeechRecognitionRule is #SpeechRecognitionRuleParse
 * which is returned by the parse function of #SpeechRecognitionRuleGrammar 
 * to represent the structure of parsed text.
 *
 * Any #SpeechRecognitionRule object can be converted to a partial Java Speech
 * Grammar Format String using its speech_recognition_rule_to_string() 
 * function.
 *
 * See: #SpeechRecognitionRuleAlternatives
 * See: #SpeechRecognitionRuleCount
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionRuleName
 * See: #SpeechRecognitionRuleParse
 * See: #SpeechRecognitionRuleSequence
 * See: #SpeechRecognitionRuleTag
 * See: #SpeechRecognitionRuleToken
 */

#include <speech/recognition/speech_recognition_rule.h>


GType
speech_recognition_rule_get_type(void)
{
    printf("speech_recognition_rule_get_type called.\n");
}


SpeechRecognitionRule *
speech_recognition_rule_new(void)
{
    printf("speech_recognition_rule_new called.\n");
}


/**
 * speech_recognition_rule_copy:
 * @rule: the #SpeechRecognitionRule object that this operation will be
 *        applied to.
 *
 * Return a deep copy of a #SpeechRecognitionRule. A deep copy implies
 * that for a rule that contains other rules (i.e.
 * #SpeechRecognitionRuleAlternatives, #SpeechRecognitionRuleCount,
 * #SpeechRecognitionRuleParse, #SpeechRecognitionRuleSequence,
 * #SpeechRecognitionRuleTag) the sub-rules are also copied.
 *
 * Returns: deep copy of a #SpeechRecognitionRule.
 */

SpeechRecognitionRule *
speech_recognition_rule_copy(SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_copy called.\n");
}


/**
 * speech_recognition_rule_to_string:
 * @rule: the #SpeechRecognitionRule object that this operation will be
 *        applied to.
 *
 * Return a string representing the #SpeechRecognitionRule in partial
 * Java Speech Grammar Format. The string represents a portion of
 * Java Speech Grammar Format that could appear on the right hand
 * side of a rule definition.
 *
 * Returns: a printable Java Speech Grammar Format string.
 */

gchar *
speech_recognition_rule_to_string(SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_to_string called.\n");
}
