
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionRecognizer provides access to speech recognition
 * capabilities. The #SpeechRecognitionRecognizer object extends the
 * #SpeechEngine object and so inherits the basic engine capabilities 
 * and provides additional specialized capabilities.
 *
 * The primary capabilities provided by a recognizer are grammar management
 * and result handling. An application is responsible for providing a
 * recognizer with grammars.
 * A #SpeechRecognitionGrammar defines a set of words (technically known 
 * as tokens) and defines patterns in which the tokens may be spoken. When 
 * a grammar is active the recognizer listens for speech in the incoming 
 * audio which matches the grammar. When speech is detected the recognizer 
 * produces a #SpeechRecognitionResult. The result object is passed to the 
 * application and contains information on which words were heard.
 *
 * The types of grammars, mechanisms for creating, modifying and managing
 * grammars, types of results, result handling and other recognizer functions
 * are described in more detail below.
 *
 * <title>Creating a Recognizer</title>
 *
 * A #SpeechRecognitionRecognizer is created by a call to the
 * speech_central_create_recognizer() function of the #SpeechCentral object.
 * Detailed descriptions of the procedures for locating, selecting, creating
 * and initializing a Recognizer are provided in the documentation for
 * #SpeechCentral.
 *
 * <title>Inherited and Extended Engine Capabilities</title>
 *
 * The #SpeechRecognitionRecognizer object and the other objects of the 
 * #SpeechRecognition package extend and modify the basic engine 
 * capabilities in the following ways.
 *
 * <itemizedlist>
 *   <listitem>- Inherits engine location by
 *       speech_central_available_recognizers() function and
 *       #SpeechEngineModeDesc.</listitem>
 *
 *   <listitem>- Extends #SpeechEngineModeDesc as
 *       #SpeechRecognitionRecognizerModeDesc.</listitem>
 *
 *   <listitem>- Inherits speech_engine_allocate() and
 *       speech_engine_deallocate() functions from #SpeechEngine.</listitem>
 *
 *   <listitem>- Inherits speech_engine_pause() and
 *       speech_engine_resume() functions from #SpeechEngine.</listitem>
 *
 *   <listitem>- Inherits the speech_engine_get_engine_state(),
 *       speech_engine_wait_engine_state() and 
 *       speech_engine_test_engine_state() functions from the #SpeechEngine
 *       object for handling engine state.</listitem>
 *
 *   <listitem>- Inherits the
 *	   #SPEECH_ENGINE_DEALLOCATED, #SPEECH_ENGINE_ALLOCATED,
 *	   #SPEECH_ENGINE_ALLOCATING_RESOURCES and
 *	   #SPEECH_ENGINE_DEALLOCATING_RESOURCES states from the
 *	   #SpeechEngine object.</listitem>
 *
 *   <listitem>- Inherits the
 *	   #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED sub-states of the
 *	   #SPEECH_ENGINE_ALLOCATED state.</listitem>
 *
 *   <listitem>- Adds
 *	   #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING,
 *	   #SPEECH_RECOGNITION_RECOGNIZER_LISTENING and
 *	   #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 *	   sub-states of the #SPEECH_ENGINE_ALLOCATED state.</listitem>
 *
 *   <listitem>- Adds
 *	   #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON and
 *	   #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF sub-states of the
 *	   #SPEECH_ENGINE_ALLOCATED state.</listitem>
 *
 *   <listitem>- Adds
 *	   speech_recognition_recognizer_request_focus() and
 *	   speech_recognition_recognizer_release_focus()
 *	   functions for managing an application's speech focus.</listitem>
 *
 *   <listitem>- Adds special suspend and commit mechanisms for updating
 *	   definitions and enabling of grammars:
 *	   speech_recognition_recognizer_suspend(),
 *	   speech_recognition_recognizer_commit_changes().</listitem>
 *
 *   <listitem>- Inherits audio management:
 *	   see speech_engine_get_audio_manager() and
 *	   #SpeechAudioManager.</listitem>
 *
 *   <listitem>- Inherits the
 *	   speech_engine_get_engine_properties() function and adds the
 *	   speech_recognition_recognizer_get_recognizer_properties()
 *	   so that a cast is not required.</listitem>
 *
 *   <listitem>- Extends audio event mechanism:
 *	   see #SpeechRecognitionRecognizerAudioListener and
 *	   #SpeechRecognitionRecognizerAudioEvent.</listitem>
 *
 *   <listitem>- Inherits vocabulary management:
 *	   see speech_engine_get_vocab_manager() and
 *	   #SpeechVocabManager.</listitem>
 *
 *   <listitem>- Inherits
 *	  speech_engine_add_engine_listener() and
 *	  speech_engine_remove_engine_listener() functions from the
 *	  #SpeechEngine object.</listitem>
 *
 *   <listitem>- Extends #SpeechEngineListener object as
 *	   #SpeechRecognitionRecognizerListener.</listitem>
 *
 *   <listitem>- Adds grammar management of #SpeechRecognitionGrammar,
 *	   #SpeechRecognitionRuleGrammar,
 *	   #SpeechRecognitionDictationGrammar,
 *	   #SpeechRecognitionGrammarEventODE and
 *	   #SpeechRecognitionGrammarListener.</listitem>
 *
 *   <listitem>- Adds recognition result mechanisms through
 *	   #SpeechRecognitionResult,
 *	   #SpeechRecognitionFinalResult,
 *	   #SpeechRecognitionFinalRuleResult,
 *	   #SpeechRecognitionFinalDictationResult,
 *	   #SpeechRecognitionResultListener and
 *	   #SpeechRecognitionResultEvent.</listitem>
 * </itemizedlist>
 *
 * <title>Grammars</title>
 *
 * The creation, modification, enabling, activation and other management of
 * grammars is a core function provided by any recognizer. A recognizer must
 * be provided with one or more grammars that indicate what words and word
 * sequences it should listen for. The basic process for dealing with a
 * grammar are:
 *
 * <orderedlist>
 *   <listitem>Create a new #SpeechRecognitionGrammar or obtain a reference 
 *       to an existing grammar.</listitem>
 *   <listitem>Attach a #SpeechRecognitionResultListener to either the
 *	 #SpeechRecognitionRecognizer or #SpeechRecognitionGrammar to get
 *	 #SpeechRecognitionResult events.</listitem>
 *   <listitem>As necessary, setup or modify the grammar according to the
 *	 application context.</listitem>
 *   <listitem>Enable and disable the #SpeechRecognitionGrammar for 
 *       recognition as required.</listitem>
 *   <listitem>Commit changes to grammar definition and enabled status.
 *       </listitem>
 *   <listitem>Repeat steps 1 through 5 as required.</listitem>
 *   <listitem>Delete application-created grammars when they are no longer 
 *       needed.</listitem>
 * </orderedlist>
 *
 * Each grammar must be identified by a unique grammar name which is defined
 * when the #SpeechRecognitionGrammar is created. The following functions deal 
 * with grammar names:
 *
 * <itemizedlist>
 *   <listitem>speech_recognition_grammar_get_name() returns the name of a 
 *       #SpeechRecognitionGrammar object.</listitem>
 *   <listitem>speech_recognition_recognizer_get_rule_grammar()
 *	 returns a reference to a #SpeechRecognitionGrammar given the 
 *       grammar's names.</listitem>
 * </itemizedlist>
 *
 * A #SpeechRecognitionRecognizer provides several functions for the creation 
 * and loading of #SpeechRecognitionRuleGrammars:
 *
 * <itemizedlist>
 *   <listitem>speech_recognition_recognizer_new_rule_grammar():
 *	 create a #SpeechRecognitionRuleGrammar from scratch.</listitem>
 *   <listitem>speech_recognition_recognizer_load_grammar_from_stream()
 *	 creates a #SpeechRecognitionRuleGrammar(s) from the Grammar Format
 *	 text obtained from a #SpeechInputStream and
 *       speech_recognition_recognizer_load_grammar_from_url() does the same
 *       but from a #SpeechGrammarURL. The advanced
 *	 speech_recognition_recognizer_load_grammar_from_url_with_imports()
 *	 function provides additional load controls.</listitem>
 *   <listitem>speech_recognition_recognizer_read_vendor_grammar():
 *	 read a grammar stored in a vendor-specific (non-portable) 
 *       format.</listitem>
 * </itemizedlist>
 *
 * Other important rule grammar functions are:
 *
 * <itemizedlist>
 *   <listitem>speech_recognition_recognizer_delete_rule_grammar()
 *	 deletes a loaded grammar.</listitem>
 *   <listitem>speech_recognition_recognizer_list_rule_grammars)
 *	 returns an array of references to all grammars loaded into a
 *	 #SpeechRecognitionRecognizer.</listitem>
 *   <listitem>speech_recognition_rule_grammar_to_string() produces a 
 *       string representing a #SpeechRecognitionRuleGrammar in Java 
 *       Speech Grammar Format.</listitem>
 *   <listitem>speech_recognition_recognizer_write_vendor_grammar():
 *	 write a grammar in a vendor-specific (non-portable) format.
 *       </listitem>
 * </itemizedlist>
 *
 * In addition to #SpeechRecognitionRuleGrammars that an application creates,
 * some recognizers have "built-in" grammars. A built-in grammar is
 * automatically loaded into the recognizer when the recognizer is created
 * and are accessible through the 
 * speech_recognition_recognizer_list_rule_grammars() function.
 * Different recognizers will have different built-in grammars so
 * applications should not rely upon built-in grammars if they need to be
 * portable.
 *
 * A #SpeechRecognitionDictationGrammar is the most important kind of built-in
 * grammar. A dictation grammar supports relatively free-form input of text.
 * If a recognizer supports dictation, then the
 * speech_recognition_recognizer_get_dictation_grammar() function returns 
 * non-null and the 
 * speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported() 
 * function of the #SpeechRecognitionRecognizerModeDesc returns %TRUE.
 *
 * <title>Grammar Scope</title>
 *
 * Each #SpeechRecognitionRecognizer object has a separate name-space.
 * Applications should be aware that changes to a #SpeechRecognitionGrammar
 * object affect any part of the application that uses that
 * #SpeechRecognitionGrammar. However, if two separate applications create a
 * #SpeechRecognitionRecognizer and use a #SpeechRecognitionGrammar with the
 * same name, they are effectively independent - changes by on app do not
 * affect the operation of the other app.
 *
 * <title>Efficiency</title>
 *
 * The processing of grammars (particularly large grammars) can be
 * computationally expensive. Loading a new grammar can take seconds or
 * sometimes minutes. Updates to a #SpeechRecognitionGrammar may also be slow.
 * Therefore, applications should take precautions to build grammars in
 * advance and to modify them only when necessary. Furthermore, an
 * application should minimize the number of grammars active at any point
 * in time.
 *
 * <title>Recognizer States</title>
 *
 * A #SpeechRecognitionRecognizer inherits the ##SPEECH_ENGINE_PAUSED and
 * ##SPEECH_ENGINE_RESUMED sub-states of the ##SPEECH_ENGINE_ALLOCATED
 * state from the #SpeechEngine object. The #SpeechRecognitionRecognizer 
 * object adds five more independent sub-states systems to the 
 * #SPEECH_ENGINE_ALLOCATED state:
 *
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RECOGNIZER_LISTENING, 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING,
 *	 #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON,
 *       #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF</listitem>
 * </itemizedlist>
 *
 * <title>Recognizer States: Focus</title>
 *
 * A #SpeechRecognitionRecognizer adds the two state sub-state-system for
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON and 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF to indicate whether
 * this instance of the #SpeechRecognitionRecognizer currently has the speech
 * focus. Focus is important in a multi-application environment
 * because more than one application can connect to an underlying speech
 * recognition engine, but the user gives speech focus to only one application
 * at a time. Since it is normal for an application to use only one
 * #SpeechRecognitionRecognizer, #SpeechRecognitionRecognizer focus and 
 * application focus normally mean the same thing. (Multi-recognizer apps 
 * cannot make this assumption.)
 *
 * Focus is not usually relevant in telephony applications in which
 * there is a single input audio stream to a single application.
 *
 * The focus status is a key factor in determining the activation of grammars
 * and therefore in determining when results will and will not be generated.
 * The activation conditions for grammars and the role of focus are described
 * in the documentation for the #SpeechRecognitionGrammar object.
 *
 * When recognizer focus is received a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED event is issued to 
 * #SpeechRecognitionRecognizerListeners. When recognizer focus is released 
 * or otherwise lost, a #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST 
 * event is issued to #SpeechRecognitionRecognizerListeners.
 *
 * Applications can request speech focus for the #SpeechRecognitionRecognizer
 * by calling the speech_recognition_recognizer_request_focus() mechanism. 
 * This asynchronous function may return before focus in received. To determine
 * when focus is on, check for 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED events or test the 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON bit of engine state.
 *
 * Applications can release speech focus from the #SpeechRecognitionRecognizer
 * by calling the speech_recognition_recognizer_release_focus() mechanism. 
 * This asynchronous function may return before focus in lost. To determine 
 * whether focus is off, check for 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST events or test the 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF bit of engine state.
 *
 * In desktop environments, it is normal (but not a requirement) for speech
 * focus to follow window focus. Therefore, it is common for the
 * speech_recognition_recognizer_request_focus() function to be called on 
 * graphical events such as "FocusEvent" and "WindowEvent".
 *
 * A well-behaved application only requests focus when it knows that it has
 * the speech focus of the user (the user is talking to it and not to other
 * applications). A well-behaved application will also release focus as soon
 * as it finishes with it.
 *
 * <title>Recognizer States: Result Recognition</title>
 *
 * A #SpeechRecognitionRecognizer adds the three state sub-state-system for
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING, 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING and 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED to indicate the status of the 
 * recognition of incoming speech. These three states are loosely coupled 
 * with the #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED states but 
 * only to the extent that turning on and off audio input will affect the 
 * recognition process. A #SPEECH_ENGINE_ALLOCATED recognizer is always in 
 * one of these three states:
 *
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RECOGNIZER_LISTENING state: the 
 *       #SpeechRecognitionRecognizer is listening to incoming audio for 
 *       speech that may match an active grammar but has not detected 
 *       speech yet.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state: the 
 *       #SpeechRecognitionRecognizer is processing incoming speech that 
 *       may match an active grammar to produce a result.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state: the 
 *       #SpeechRecognitionRecognizer is temporarily suspended while 
 *       grammars are updated. While suspended, audio input is buffered 
 *       for processing once the recognizer returns to the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING and 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING states.</listitem>
 * </itemizedlist>
 *
 * <title>Recognizer States: Result Recognition: Typical Event Cycle</title>
 *
 * The typical state cycle is as follows.
 *
 * <orderedlist>
 *   <listitem>A #SpeechRecognitionRecognizer starts in the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state with a certain set 
 *       of grammars enabled. When incoming audio is detected that may 
 *       match an active grammar, the #SpeechRecognitionRecognizer 
 *       transitions to the #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 *	 state with a 
 *       #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 *	 #SpeechRecognitionRecognizerEvent. The #SpeechRecognitionRecognizer 
 *       then creates a new #SpeechRecognitionResult and issues a
 *	 #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RESULT_CREATED event to 
 *       provide it to the application.</listitem>
 *
 *   <listitem>The #SpeechRecognitionRecognizer remains in the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state until it completes 
 *       recognition of the result.</listitem>
 *
 *   <listitem>The recognizer indicates completion of recognition by issuing a
 *	 #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 *       #SpeechRecognitionRecognizerEvent to transition from the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state to the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state. Once in that 
 *       state, it issues a result finalization event to 
 *       #SpeechRecognitionResultListeners
 *	 (#SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or 
 *        #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event).</listitem>
 *
 *   <listitem>The #SpeechRecognitionRecognizer remains in the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state until processing 
 *       of the result finalization event is completed. Applications will 
 *       usually make any necessary grammar changes while the recognizer is 
 *       #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED. In this state the
 *	 #SpeechRecognitionRecognizer buffers incoming audio. This buffering
 *	 allows a user to continue speaking without speech data being lost.
 *	 Once the #SpeechRecognitionRecognizer returns to the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state the buffered audio 
 *       is processed to give the user the perception of real-time 
 *       processing.</listitem>
 *
 *   <listitem>The #SpeechRecognitionRecognizer commits all grammar changes,
 *	 issues a #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 *       event to #SpeechRecognitionRecognizerListeners to return to the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state. It also issues
 *	 #SPEECH_RECOGNITION_RECOGNIZER_EVENT_GRAMMAR_CHANGES_COMMITTED
 *       events to #SpeechRecognitionGrammarListeners of changed grammars.
 *       The commit applies all grammar changes made at any point up to 
 *       the end of result finalization, typically including changes made 
 *       in the result finalization events.</listitem>
 *
 *   <listitem>The #SpeechRecognitionRecognizer is back in the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state listening for 
 *       speech that matches the new grammars.</listitem>
 * </orderedlist>
 *
 * In this state cycle, the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING and
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED events are 
 * triggered by user actions: starting and stopping speaking. The third 
 * state transition -- #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * -- is triggered programmatically some time after the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event.
 *
 * <title>Recognizer States: Result Recognition: Non-Speech Events</title>
 *
 * For applications that deal only with spoken input the state cycle above
 * handles most normal speech interactions. For applications that handle
 * other asynchronous input, additional state transitions are possible.
 * Other types of asynchronous input include graphical user interface events
 * (e.g., graphical events), timer events, multi-threading events, socket 
 * events and much more.
 *
 * When a non-speech event occurs which changes the application state or
 * applicatin data it is often necessary to update the recognizer's grammars.
 * Furthermore, it is typically necessary to do this as if the change occurred
 * in real time - at exactly the point in time at which the event occurred.
 *
 * The speech_recognition_recognizer_suspend() and 
 * speech_recognition_recognizer_commit_changes() functions of a
 * #SpeechRecognitionRecognizer are used to handle non-speech asynchronous 
 * events.  The typical cycle for updating grammars is as follows:
 *
 * <orderedlist>
 *   <listitem>Assume that the #SpeechRecognitionRecognizer is in the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state (the user is not 
 *       currently speaking). As soon as the event is received, the 
 *       application calls speech_recognition_recognizer_suspend() to 
 *       indicate that it is about to change grammars.</listitem>
 *
 *   <listitem>The recognizer issues a 
 *       #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event 
 *       and transitions from the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING
 *       state to the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state.
 *       </listitem>
 *
 *   <listitem>The application makes all necessary changes to the 
 *       grammars.</listitem>
 *
 *   <listitem>Once all changes are completed the application calls the
 *	 speech_recognition_recognizer_commit_changes() function. The 
 *       recognizer applies the new grammars, issues a 
 *       #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event to
 *	 transition from the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state
 *       back to the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING, and issues
 *	 #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 *       events to all changed grammars.</listitem>
 *
 *   <listitem>The #SpeechRecognitionRecognizer resumes recognition of the 
 *       buffered audio and then live audio with the new grammars.</listitem>
 * </orderedlist>
 *
 * Because audio is buffered from the time of the asynchronous event to the
 * time at which the #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * occurs, the audio is processed as if the new grammars were applied exactly
 * at the time of the asynchronous event. The user has the perception of 
 * real-time processing.
 *
 * Although audio is buffered in the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * state, applications should make changes and 
 * speech_recognition_recognizer_commit_changes() as quickly as possible. 
 * This minimizes the possibility of a buffer overrun. It also reduces 
 * delays in recognizing speech and responding to the user.
 *
 * Note: an application is not technically required to call
 * speech_recognition_recognizer_suspend() prior to calling 
 * speech_recognition_recognizer_commit_changes(). If the 
 * speech_recognition_recognizer_suspend() call is omitted the 
 * #SpeechRecognitionRecognizer behaves as if 
 * speech_recognition_recognizer_suspend() had been called 
 * immediately prior to calling 
 * speech_recognition_recognizer_commit_changes(). An application that 
 * does not call speech_recognition_recognizer_suspend() risks a commit 
 * occurring unexpectedly and leaving grammars in an inconsistent state.
 *
 * <title>Recognizer States: Result Recognition: Mixing Speech and Non-Speech Events</title>
 *
 * There is no guarantee that a speech and non-speech events will not be mixed.
 * If a speech event occurs in the absence of non-speech events, the
 * normal event cycle takes place. If a non-speech event occurs in the 
 * absence of any speech events, the non-speech event cycle takes place.
 *
 * We need to consider two cases in which speech and non-speech events
 * interact:
 *
 * <orderedlist>
 *   <listitem>when a non-speech event occurs during the processing of a 
 *       speech event, and</listitem>
 *   <listitem>when a speech event occurs during the processing of a 
 *       non-speech event.</listitem>
 * </orderedlist>
 *
 * <orderedlist>
 *   <listitem>Non-speech event occurs during processing of a speech event:
 *
 *	 Technically, this is the case in which a non-speech event is
 *	 issued while the #SpeechRecognitionRecognizer is in either the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state or the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state. In both cases 
 *       the event processing for the non-speech is no different than 
 *       normal. The non-speech event handler calls
 *	 speech_recognition_recognizer_suspend() to indicate it is about 
 *       to change grammars, makes the grammar changes, and then calls 
 *       speech_recognition_recognizer_commit_changes() to apply the changes.
 *
 *	 The effect is that the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event that
 *	 would normally occur in the normal event cycle may be delayed until
 *	 the speech_recognition_recognizer_commit_changes() function is 
 *       explicitly called and that the commit applies changes made in 
 *       response to both the speech and non-speech events. If the 
 *       speech_recognition_recognizer_commit_changes() call for the 
 *       non-speech event is made before the end of the result finalization
 *       event, there is no delay of the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event.
 *       </listitem>
 *
 *   <listitem>Speech event occurs during processing of a non-speech event:
 *
 *	 This case is simpler. If the user starts speaking while a non-speech
 *	 event is being processed, then the #SpeechRecognitionRecognizer is in
 *	 the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state, that speech is 
 *       buffered, and the speech event is actually delayed until the 
 *       #SpeechRecognitionRecognizer returns to the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state. Once the
 *	 #SpeechRecognitionRecognizer returns to the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state, the incoming 
 *       speech is processed with the normal event cycle.</listitem>
 * </orderedlist>
 *
 * See: #SpeechRecognitionRecognizerEvent
 * See: #SpeechEngineListener
 * See: #SpeechRecognitionRecognizerListener
 * See: #SpeechRecognitionGrammar
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionDictationGrammar
 * See: #SpeechRecognitionResult
 * See: #SpeechRecognitionFinalResult
 */

#include <speech/recognition/speech_recognition_recognizer.h>


GType
speech_recognition_recognizer_get_type(void)
{
    printf("speech_recognition_recognizer_get_type called.\n");
}


SpeechRecognitionRecognizer *
speech_recognition_recognizer_new(void)
{
    printf("speech_recognition_recognizer_new called.\n");
}


/**
 * speech_recognition_recognizer_add_result_listener:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @listener: the listener to add.
 *
 * Request notifications of all events for all #SpeechRecognitionResult
 * produced by this #SpeechRecognitionRecognizer. An application can
 * attach multiple #SpeechRecognitionResultListeners to a
 * #SpeechRecognitionRecognizer. A listener is removed with the
 * speech_recognition_result_remove_result_listener() function.
 *
 * #SpeechRecognitionResultListeners attached to a 
 * #SpeechRecognitionRecognizer are the only 
 * #SpeechRecognitionResultListeners to receive the
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event and all 
 * subsequent events.
 *
 * #SpeechRecognitionResultListener objects can also be attached to any
 * #SpeechRecognitionGrammar or to any #SpeechRecognitionResult. A listener
 * attached to the #SpeechRecognitionGrammar receives all events that
 * match that #SpeechRecognitionGrammar following a
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event. A listener 
 * attached to a #SpeechRecognitionResult receives all events for that 
 * result from the time at which the listener is attached.
 *
 * A #SpeechRecognitionResultListener can be attached or removed in any
 * #SpeechEngine state.
 *
 * See: speech_recognition_result_remove_result_listener()
 * See: speech_recognition_grammar_add_result_listener()
 * See: speech_recognition_result_add_result_listener()
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 */

void
speech_recognition_recognizer_add_result_listener(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionResultListener *listener)
{
    printf("speech_recognition_recognizer_add_result_listener called.\n");
}


/**
 * speech_recognition_recognizer_remove_result_listener:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @listener: the listener to remove.
 *
 * A #SpeechRecognitionResultListener can be attached or removed in any
 * #SpeechEngine state.
 *
 * See: speech_recognition_recognizer_add_result_listener()
 * See: speech_recognition_result_remove_result_listener()
 * See: speech_recognition_grammar_remove_result_listener()
 */

void
speech_recognition_recognizer_remove_result_listener(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionResultListener *listener)
{
    printf("speech_recognition_recognizer_remove_result_listener called.\n");
}


/**
 * speech_recognition_recognizer_commit_changes:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 *
 * Commit changes to all loaded grammars and all changes of grammar
 * enabling since the last speech_recognition_recognizer_commit_changes().
 * Because all changes are applied atomically (all at once) the application
 * does not need to be concerned with intermediate states as it changes
 * grammar definitions and enabling.
 *
 * The speech_recognition_recognizer_commit_changes() call first checks that 
 * all the loaded grammars are legal. If there are any problems with the
 * current definition of any #SpeechRecognitionRuleGrammar an exception
 * is returned. Problems might include undefined rule name, illegal
 * recursion and so on (see the Java Speech Grammar Format Specification
 * and the #SpeechRecognitionGrammarSyntaxDetail object documentation for 
 * details).
 *
 * The speech_recognition_recognizer_commit_changes() call is asynchronous
 * (the changes have not necessarily been committed when the call returns).
 * When the changes have been committed, a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event is issued 
 * to all #SpeechRecognitionRecognizerListeners and to the 
 * #SpeechRecognitionGrammarListeners of all changed #SpeechRecognitionGrammars.
 *
 * Immediately following the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event, a
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 * #SpeechRecognitionGrammarEvent is issued to the 
 * #SpeechRecognitionGrammarListeners of all changed grammars.
 *
 * The roll of speech_recognition_recognizer_commit_changes() in applying 
 * grammar changes is described in the documentation for the
 * #SpeechRecognitionGrammar object. The effect of the 
 * speech_recognition_recognizer_commit_changes() function upon
 * #SpeechRecognitionRecognizer states is described above.
 * The use of speech_recognition_recognizer_suspend() with
 * speech_recognition_recognizer_commit_changes() and their use for 
 * processing asynchronous non-speech events are also described above.
 *
 * It is not an error to speech_recognition_recognizer_commit_changes() 
 * when no grammars have been changed. However, the 
 * #SpeechRecognitionRecognizer performs state transitions in the same 
 * way as when grammars are changed.
 *
 * The speech_recognition_recognizer_commit_changes() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES 
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is thrown if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * state.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_LISTENING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: speech_recognition_recognizer_suspend()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if the loaded grammars 
 *       contain any logical errors.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *	 #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_commit_changes(
                                  SpeechRecognitionRecognizer *recognizer)
{
    printf("speech_recognition_recognizer_commit_changes called.\n");
}


/**
 * speech_recognition_recognizer_delete_rule_grammar:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @grammar: the rule grammar to delete.
 *
 * Delete a #SpeechRecognitionRuleGrammar from the 
 * #SpeechRecognitionRecognizer. The grammar deletion only takes effect 
 * when all grammar changes are committed.
 *
 * Recognizers may chose to ignore the deletion of built-in grammars.
 *
 * The speech_recognition_recognizer_delete_rule_grammar() function 
 * operates as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * See: speech_recognition_recognizer_commit_changes()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_ILLEGAL_ARGUMENT_EXCEPTION if the 
 *       #SpeechRecognitionGrammar is not known to the 
 *       #SpeechRecognitionRecognizer.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_delete_rule_grammar(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_recognizer_delete_rule_grammar called.\n");
}


/**
 * speech_recognition_recognizer_force_finalize:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @flush: whether the recognizer's internally buffered audio
 *		should be processed before forcing the finalize.
 *
 * If the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state (producing a 
 * #SpeechRecognitionResult), force the #SpeechRecognitionRecognizer 
 * to immediately complete processing of that result by finalizing it. 
 * It is acceptable behavior for a #SpeechRecognitionRecognizer to 
 * automatically reject the current result.
 *
 * The @flush flag indicates whether the recognizer's internally buffered 
 * audio should be processed before forcing the finalize. Applications 
 * needing immediate cessation of recognition should request a flush. 
 * If the force finalize is a response to a user event (e.g. keyboard or  
 * mouse press) then the buffer is typically not flushed because incoming  
 * speech from a user could be lost.
 *
 * The state behavior of the #SpeechRecognitionRecognizer is the same as
 * if the #SpeechRecognitionResult had been finalized because of
 * end-of-utterance. The #SpeechRecognitionRecognizer will transition
 * to the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state.
 *
 * The speech_recognition_recognizer_force_finalize() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_force_finalize(
                                    SpeechRecognitionRecognizer *recognizer,
                                    gboolean flush)
{
    printf("speech_recognition_recognizer_force_finalize called.\n");
}


/**
 * speech_recognition_recognizer_get_dictation_grammar:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @name: provided for future extension of the API to allow more than 
 *        one #SpeechRecognitionDictationGrammar to be defined.
 * @grammar: return the #SpeechRecognitionDictationGrammar for a
 *	   #SpeechRecognitionRecognizer.
 *
 * Return the #SpeechRecognitionDictationGrammar for a
 * #SpeechRecognitionRecognizer. Typically, the @name parameter is %NULL
 * to get access to the default #SpeechRecognitionDictationGrammar for 
 * the #SpeechRecognitionRecognizer.
 *
 * If the #SpeechRecognitionRecognizer does not support dictation, or if
 * it does have a #SpeechRecognitionDictationGrammar with the specified
 * name, then the function returns %NULL.
 *
 * An application can determine whether the #SpeechRecognitionRecognizer
 * supports dictation by calling the
 * speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported() 
 * function of the #SpeechRecognitionRecognizerModeDesc.
 *
 * Note: the @name parameter is provided for future extension of the
 *	 API to allow more than one #SpeechRecognitionDictationGrammar
 *	 to be defined.
 *
 * The speech_recognition_recognizer_get_dictation_grammar() function 
 * operates as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in 
 * the #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * state.
 *
 * See: speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_get_dictation_grammar(
                                 SpeechRecognitionRecognizer *recognizer,
                                 gchar *name,
                                 SpeechRecognitionDictationGrammar *grammar)
{
    printf("speech_recognition_recognizer_get_dictation_grammar called.\n");
}


/**
 * speech_recognition_recognizer_get_recognizer_properties:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 *
 * Return the #SpeechRecognitionRecognizerProperties object. The function
 * returns exactly the same object as the
 * speech_engine_get_engine_properties() function in the #SpeechEngine
 * object. However, with the 
 * speech_recognition_recognizer_get_recognizer_properties() function, an
 * application does not need to cast the return value.
 *
 * The #SpeechRecognitionRecognizerProperties are available in any state
 * of a #SpeechEngine. However, changes only take effect once an engine 
 * reaches the #SPEECH_ENGINE_ALLOCATED state.
 *
 * Returns: the #SpeechRecognitionRecognizerProperties object for this engine.
 *
 * See: speech_engine_get_engine_properties()
 */

SpeechRecognitionRecognizerProperties *
speech_recognition_recognizer_get_recognizer_properties(
                                    SpeechRecognitionRecognizer *recognizer)
{
    printf("speech_recognition_recognizer_get_recognizer_properties called.\n");
}


/**
 * speech_recognition_recognizer_get_rule_grammar:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @name: the name of the grammar to be returned.
 * @grammar: return a #SpeechRecognitionRuleGrammar reference or %NULL.
 *
 * Get the #SpeechRecognitionRuleGrammar with the specified name. Returns
 * %NULL if the grammar is not known to the Recognizer.
 *
 * The speech_recognition_recognizer_get_rule_grammar() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is thrown if the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus 
speech_recognition_recognizer_get_rule_grammar(
                                 SpeechRecognitionRecognizer *recognizer,
                                 gchar *name,
                                 SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_recognizer_get_rule_grammar called.\n");
}


/**
 * speech_recognition_recognizer_get_speaker_manager:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @manager: return the #SpeechRecognitionSpeakerManager for this
 *	   #SpeechRecognitionRecognizer.
 *
 * Return an object which provides management of the speakers of a
 * #SpeechRecognitionRecognizer. Returns %NULL if the
 * #SpeechRecognitionRecognizer does not store speaker data - that is,
 * if it is a speaker-independent recognizer in which all speakers
 * are handled the same.
 *
 * speech_recognition_recognizer_get_speaker_manager() returns successfully 
 * in any state of a #SpeechRecognitionRecognizer. The
 * #SpeechRecognitionSpeakerManager functions that list speakers and
 * set the current speaker operate in any #SpeechRecognitionRecognizer
 * state but only take effect in the #SPEECH_ENGINE_ALLOCATED state.
 * This allows an application can set the initial speaker prior
 * to allocating the engine. Other functions of the
 * #SpeechRecognitionSpeakerManager only operate in the
 * #SPEECH_ENGINE_ALLOCATED state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the application does not have
 *		#accessSpeakerProfiles permission.</listitem>
 * </itemizedlist>
 */

SpeechStatus 
speech_recognition_recognizer_get_speaker_manager(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionSpeakerManager *manager)
{
    printf("speech_recognition_recognizer_get_speaker_manager called.\n");
}


/**
 * speech_recognition_recognizer_list_rule_grammars:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @grammars: return the #SpeechRecognitionRuleGrammars known to the
 *	   #SpeechRecognitionRecognizer.
 *
 * List the #SpeechRecognitionRuleGrammars known to the
 * #SpeechRecognitionRecognizer. Returns %NULL if there are no grammars.
 *
 * The speech_recognition_recognizer_list_rule_grammars() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus 
speech_recognition_recognizer_list_rule_grammars(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechRecognitionRuleGrammar *grammars[])
{
    printf("speech_recognition_recognizer_list_rule_grammars called.\n");
}


/**
 * speech_recognition_recognizer_is_grammar_type_supported:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @type: the type of the grammar being checked.
 *
 * Return an indication of whether this #SpeechRecognitionRecognizer
 * supports this grammar type.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_JSGF
 * See: #SPEECH_RECOGNITION_RECOGNIZER_GRAMMAR_SRGS
 *
 * Returns: an indication of whether this #SpeechRecognitionRecognizer
 * supports this grammar type.
 */

gboolean
speech_recognition_recognizer_is_grammar_type_supported(
                                SpeechRecognitionRecognizer *recognizer,
                                SpeechGrammarType type)
{
    printf("speech_recognition_recognizer_is_grammar_type_supported called.\n");
}

/**
 * speech_recognition_recognizer_load_grammar_from_stream:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @type: the type of the grammar being loaded.
 * @input_stream: the #SpeechInputStream from which the grammar text is loaded.
 * @grammar: return a #SpeechRecognitionRuleGrammar from grammar formatted text
 *	   provided by the #SpeechInputStream.
 *
 * Create a #SpeechRecognitionRuleGrammar for the grammar formatted text
 * provided by the #SpeechInputStream. If the grammar contained
 * in the @input_stream already exists, it is over-written. The new grammar 
 * is used for recognition only after changes are committed.
 *
 * The caller is responsible for determining the character encoding
 * of the formatted document. The character encoding information
 * contained in the grammar format header is ignored.
 *
 * The speech_recognition_recognizer_load_grammar_from_stream() function 
 * operate as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * state.
 *
 * See: speech_recognition_recognizer_load_grammar_from_url()
 * See: speech_recognition_recognizer_load_grammar_from_url_with_imports()
 *
 * Returns: a #SpeechRecognitionRuleGrammar from grammar formatted text
 *	   provided by the #SpeechInputStream.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if the formatted text 
 *       contains any errors.</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *              #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_load_grammar_from_stream(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechGrammarType type,
                          SpeechInputStream *input_stream,
                          SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_recognizer_load_grammar_from_stream called.\n");
}


/**
 * speech_recognition_recognizer_load_grammar_from_url:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @type: the type of the grammar being loaded.
 * @url_context: the URL context from which grammar locations are derived or 
 *               %NULL to load exclusively from system resources.
 * @grammar_name: the name of the grammar to be loaded.
 * @grammar: return a reference to @grammar_name
 *
 * Load a #SpeechRecognitionRuleGrammar and its imported grammars from
 * grammar formatted text from URLs or from system resources. The
 * loaded grammars are used for recognition only after changes are committed.
 *
 * The function returns a reference to the named #SpeechRecognitionRuleGrammar.
 * The function never returns %NULL since an exception is returned if
 * @grammar_name cannot be loaded successfully.
 *
 * The function attempts to load all imports of @grammar_name, all imports 
 * of the imported grammars, and so on. This recursive load stops when 
 * grammars are reached that have already been loaded or when no more 
 * imports are found. The intent is to ensure that every grammar needed to 
 * use the named grammar is loaded.
 *
 * For example, if we load grammar X, which imports grammar Y, which
 * imports grammars A and B, then all four grammars are loaded. If any
 * of the grammars are already loaded, then it and its imports are not
 * reloaded.
 *
 * Note that JSGF allows fully-qualified rulename references without a
 * corresponding import statement. This function also attempts to load
 * all grammars referenced by a fully-qualified rulename not already
 * referenced and loaded by a corresponding import statement.
 *
 * The advanced
 * speech_recognition_recognizer_load_grammar_from_url_with_imports()
 * function provides more control of the loading process. This function
 * is equivalent to:
 *
 *   speech_recognition_recognizer_load_grammar_from_url_with_imports(recognizer, type, url, name, true, false, null, &amp;grammar);
 *
 * (load imports, don't reload existing grammars, don't provide a list
 *  of loaded grammars.)
 *
 * <title>Locating Grammars</title>
 *
 * The @url_context parameter is used as the first parameter of the 
 * #SpeechGrammarURL constructor.
 *
 * The @grammar_name is converted to a grammar filename. The conversion 
 * changes each each period character ('.') to a file separator ('/'). 
 * The ".gram" suffix is appended to identify the grammar file. The 
 * @grammar_name "com.sun.numbers" becomes the filename "com/sun/numbers.gram".
 * This filename is used as the spec parameter in the URL constructor.
 *
 * If the context is %NULL, the grammars are searched for only as a
 * system resource.
 *
 * <title>Reading Grammars</title>
 *
 * For each grammar it loads, the recognizer determines the file's
 * character encoding by reading the JSGF header.
 *
 * An exception is thrown if:
 *
 * <orderedlist>
 *   <listitem>any grammar format syntax problems are found,</listitem>
 *   <listitem>if a grammar found in a URL does not match the expected
 *	 name, or</listitem>
 *   <listitem>if a grammar cannot be located as either a URL or system
 *	 resource.</listitem>
 * </orderedlist>
 *
 * If an exception is returned part way through loading a set of grammars
 * the list of loaded grammars not explicitly available. The recognizer
 * does not attempt to remove any partially loaded grammars.
 *
 * The loadGrammar functions operate as defined when the
 * #SpeechRecognitionRecognizer is in the #SPEECH_ENGINE_ALLOCATED state.
 * The call blocks if the #SpeechRecognitionRecognizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and completes when the
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. An error is
 * returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * state.
 *
 * See: speech_recognition_recognizer_load_grammar_from_stream()
 * See: speech_recognition_recognizer_load_grammar_from_url_with_imports()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if any loaded grammar 
 *       formatted text contains an error.</listitem>
 *   <listitem>#SPEECH_MALFORMED_URL_EXCEPTION if problem encountered 
 *       creating a URL.</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_load_grammar_from_url(
                          SpeechRecognitionRecognizer *recognizer,
                          SpeechGrammarType type,
                          SpeechGrammarURL url_context,
                          gchar *grammar_name,
                          SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_recognizer_load_grammar_from_url called.\n");
}


/**
 * speech_recognition_recognizer_load_grammar_from_url_with_imports:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @type: the type of the grammar being loaded.
 * @url_context: the URL context from which grammar locations are derived or 
 *               %NULL to load exclusively from system resources.
 * @grammar_name: the name of the grammar to be loaded.
 * @load_imports: if %TRUE, grammars imported by @grammar_name are loaded 
 *                plus their imports.
 * @reload_grammars: if %TRUE reload all grammars and imports, if %FALSE do 
 *                   not load grammars already loaded into the 
 *                   #SpeechRecognitionRecognizer.
 * @loaded_grammars: if non-null a reference to each loaded
 *		     #SpeechRecognitionRuleGrammar is appended as it is loaded.
 * @grammar: return a reference to @grammar_name
 *
 * Load a #SpeechRecognitionRuleGrammar in grammar formatted text from a URL
 * or from system resources and optionally load its imports. This function 
 * provide an additional control over whether grammars are reloaded even if 
 * they have already been loaded, and allows caller to receive a list of all 
 * grammars loaded by the #SpeechRecognitionRecognizer. The loaded grammars 
 * are used for recognition only after changes are committed.
 *
 * The three additional parameters of this function provide the
 * following extensions over the
 * speech_recognition_recognizer_load_grammar_from_url() function.
 *
 * @load_imports: if %FALSE, the function only loads the 
 * #SpeechRecognitionRuleGrammar specified by the @grammar_name 
 * parameter. If %TRUE, the function behaves like the
 * speech_recognition_recognizer_load_grammar_from_url() function 
 * and recursively loads imported grammars.
 *
 * @reload_grammars: if %TRUE, the function always loads a 
 * #SpeechRecognitionRuleGrammar, even if it is already loaded into 
 * the #SpeechRecognitionRecognizer. The previous version of the 
 * grammar is overwritten. If %FALSE, the function behaves like the
 * speech_recognition_recognizer_load_grammar_from_url() function
 * and does not load grammars that are already loaded, or their imports.
 *
 * @loaded_grammars: if non-null, then as the
 * #SpeechRecognitionRecognizer loads any grammar it appends a
 * reference to that #SpeechRecognitionRuleGrammar to the vector.
 *
 * See: speech_recognition_recognizer_load_grammar_from_stream()
 * See: speech_recognition_recognizer_load_grammar_from_url()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_GRAMMAR_EXCEPTION if any loaded grammar
 *	 formatted text contains an error.</listitem>
 *   <listitem>#SPEECH_MALFORMED_URL_EXCEPTION if problem encountered 
 *       creating a URL.</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_load_grammar_from_url_with_imports(
                                  SpeechRecognitionRecognizer *recognizer,
                                  SpeechGrammarType type,
                                  SpeechGrammarURL url_context,
                                  gchar *grammar_name,
                                  gboolean load_imports,
                                  gboolean reload_grammars,
                                  SpeechRecognitionGrammar *loaded_grammars[],
                                  SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_recognizer_load_grammar_from_url_with_imports called.\n");
}


/**
 * speech_recognition_recognizer_new_rule_grammar:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @name: name of the grammar to be created.
 * @grammar: return a new #SpeechRecognitionRuleGrammar for this recognizer 
 *           with a specified grammar name.
 *
 * Create a new #SpeechRecognitionRuleGrammar for this recognizer with a
 * specified grammar name. The new grammar is used for recognition
 * only after changes are committed.
 *
 * The speech_recognition_recognizer_new_rule_grammar() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if grammar with name 
 *       already exists.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_new_rule_grammar(
                                  SpeechRecognitionRecognizer *recognizer,
                                  gchar *name,
                                  SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_recognizer_new_rule_grammar called.\n");
}


/**
 * speech_recognition_recognizer_read_vendor_grammar:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @input: #SpeechInputStream from which grammar is loaded
 * @grammar: return a reference to the loaded grammar.
 *
 * Create a new grammar by reading in a grammar stored in a
 * vendor-specific format. The data could have been stored using the
 * speech_recognition_recognizer_write_vendor_grammar() function. The 
 * documentation for the speech_recognition_recognizer_write_vendor_grammar()
 * function describes the use of vendor grammar formats.
 *
 * If a grammar of the same name already exists, it is over-written.
 *
 * The speech_recognition_recognizer_read_vendor_grammar() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_VENDOR_DATA_EXCEPTION if the input data format is not
 *		known to the #SpeechRecognitionRecognizer.</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_read_vendor_grammar(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechInputStream *input,
                                       SpeechRecognitionGrammar *grammar)
{
    printf("speech_recognition_recognizer_read_vendor_grammar called.\n");
}


/**
 * speech_recognition_recognizer_write_vendor_grammar:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @output: the #SpeechOutputStream where the grammar is written.
 * @grammar: the #SpeechRecognitionGrammar to be written.
 *
 * Store a grammar in a vendor-specific format. The data can be
 * reloaded at a future time by the 
 * speech_recognition_recognizer_read_vendor_grammar() function.
 *
 * The output format will be specific to the type of recognizer that
 * writes it. For example, data written by an Acme Command and Control
 * Recognizer will be readable only by another Acme Command and Control
 * Recognizer or by other recognizers that understand it's format.
 * When portability is required, use the Java Speech Grammar Format.
 *
 * Why is a Vendor grammar format useful? The recognizer can store
 * information that makes reloading of the grammar faster than when
 * JSGF is used. The recognizer may also store additional information
 * (e.g. pronunciation, statistical and acoustic data) that improve
 * recognition using this grammar.
 *
 * The speech_recognition_recognizer_write_vendor_grammar() function 
 * operates as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>

SpeechStatus
speech_recognition_recognizer_write_vendor_grammar(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechOutputStream *output,
                                       SpeechRecognitionGrammar *grammar)
{
    printf("speech_recognition_recognizer_write_vendor_grammar called.\n");
}


/**
 * speech_recognition_recognizer_read_vendor_result:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @input: the #SpeechInputStream from which #SpeechRecognitionResult is loaded.
 * @result: return a reference to the loaded result.
 *
 * Read a #SpeechRecognitionResult object from a stream in a
 * vendor-specific format. The return value will include the
 * best-guess tokens, and may include N-best results, audio data,
 * timing data, training information and other data the is
 * optionally provided with a finalized Result.
 *
 * The call returns %NULL upon failure. Because result objects are
 * stored in a vendor-specific format they cannot normally be loaded
 * by incompatible recognizers.
 *
 * The speech_recognition_recognizer_read_vendor_result() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_VENDOR_DATA_EXCEPTION if the input data format is not
 *		known to the #SpeechRecognitionRecognizer.</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_read_vendor_result(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechInputStream *input,
                                       SpeechRecognitionResult *result)
{
    printf("speech_recognition_recognizer_read_vendor_result called.\n");
}


/**
 * speech_recognition_recognizer_write_vendor_result:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 * @output: the #SpeechOutputStream where the result is written.
 * @result: the #SpeechRecognitionResult to be written.
 *
 * Store a finalized #SpeechRecognitionResult object in a vendor-specific
 * format so that it can be re-loaded in a future session. All the
 * current information associated with the result is stored. If the
 * application will not need audio data or training information in
 * a future session, they should release that information (through
 * the #SpeechRecognitionFinalResult object) before writing the result
 * to reduce storage requirements.
 *
 * The speech_recognition_recognizer_write_vendor_result() function operates 
 * as defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in 
 * the #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES
 * state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if the 
 *       #SpeechRecognitionResult is not in a finalized state.</listitem>
 *   <listitem>#SPEECH_IO_EXCEPTION if an I/O error occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *	 #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_write_vendor_result(
                                       SpeechRecognitionRecognizer *recognizer,
                                       SpeechOutputStream *output,
                                       SpeechRecognitionResult *result)
{
    printf("speech_recognition_recognizer_write_vendor_result called.\n");
}


/**
 * speech_recognition_recognizer_release_focus:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 *
 * Release the speech focus from this #SpeechRecognitionRecognizer. A
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event is issued to
 * #SpeechRecognitionRecognizerListeners once the focus is released and
 * the #SpeechRecognitionRecognizer state changes from
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF to 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON.
 *
 * Since one one application may have recognition focus at any time,
 * applications should release focus whenever it is not required.
 * Speech focus and other focus issues are discussed above in more detail.
 *
 * It is not an error for an application to release focus for a
 * #SpeechRecognitionRecognizer that does not have speech focus.
 *
 * Focus is implicitly released when a #SpeechRecognitionRecognizer is
 * deallocated.
 *
 * The speech_recognition_recognizer_release_focus() function operates as 
 * defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is returned if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * See: speech_recognition_recognizer_request_focus()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST
 * See: speech_engine_get_engine_state()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *	 #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_release_focus(
                                     SpeechRecognitionRecognizer *recognizer)
{
    printf("speech_recognition_recognizer_release_focus called.\n");
}


/**
 * speech_recognition_recognizer_request_focus:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 *
 * Request the speech focus for this #SpeechRecognitionRecognizer from
 * the underlying speech recognition engine. When the focus is
 * received, a #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED event is 
 * issued to #SpeechRecognitionRecognizerListeners and the 
 * #SpeechRecognitionRecognizer changes state from 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON to 
 * #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF.
 *
 * Since one one application may have recognition focus at any time,
 * applications should only request focus when confident that the
 * user is speaking to that application.
 * Speech focus and other focus issues are discussed above in more detail.
 *
 * It is not an error for an application to request focus for a
 * #SpeechRecognitionRecognizer that already has speech focus.
 *
 * The speech_recognition_recognizer_request_focus() function operates as 
 * defined when the #SpeechRecognitionRecognizer is in the 
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks if the 
 * #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_ALLOCATING_RESOURCES
 * state and completes when the engine reaches the #SPEECH_ENGINE_ALLOCATED
 * state. An error is thrown if the #SpeechRecognitionRecognizer is in the
 * #SPEECH_ENGINE_DEALLOCATED or #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * See: speech_recognition_recognizer_release_focus()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED
 * See: speech_engine_get_engine_state()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_request_focus(
                                    SpeechRecognitionRecognizer *recognizer)
{
    printf("speech_recognition_recognizer_request_focus called.\n");
}


/**
 * speech_recognition_recognizer_suspend:
 * @recognizer: the #SpeechRecognitionRecognizer object that this operation
 *              will be applied to.
 *
 * Temporarily suspend recognition while the application updates
 * grammars prior to a speech_recognition_recognizer_commit_changes() call.
 * The speech_recognition_recognizer_suspend() call places the 
 * #SpeechRecognitionRecognizer in the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state. While in that state the
 * incoming audio is buffered. The buffered audio is processed
 * after the recognizer has committed grammar changes and returned
 * to the #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state.
 *
 * The primary difference between the 
 * speech_recognition_recognizer_suspend() and speech_engine_pause() 
 * functions is that audio is buffered while a #SpeechRecognitionRecognizer
 * is suspended whereas incoming audio is ignored while in the 
 * #SPEECH_ENGINE_PAUSED state. Also, the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state should only be visited 
 * temporarily, whereas a #SpeechRecognitionRecognizer can be 
 * #SPEECH_ENGINE_PAUSED indefinately.
 *
 * The speech_recognition_recognizer_suspend() function is asynchronous.
 * When the call returns, the recognizer is not necessarily suspended. The
 * speech_engine_get_engine_state() function, or the
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED events can be 
 * used to determine when the recognizer is actually suspended.
 *
 * The use of speech_recognition_recognizer_suspend() with 
 * speech_recognition_recognizer_commit_changes() for handling atomic 
 * grammar changes and for handling asynchronous events are described above.
 *
 * Calls to speech_recognition_recognizer_suspend() and 
 * speech_recognition_recognizer_commit_changes() do not nest. A single 
 * call to speech_recognition_recognizer_commit_changes() can release
 * the #SpeechRecognitionRecognizer after multiple calls to
 * speech_recognition_recognizer_suspend().
 *
 * The speech_recognition_recognizer_suspend() function operates as defined 
 * when the #SpeechRecognitionRecognizer is in the #SPEECH_ENGINE_ALLOCATED
 * state.  The call blocks if the #SpeechRecognitionRecognizer in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state and completes when the engine 
 * reaches the #SPEECH_ENGINE_ALLOCATED state. An error is returned if the 
 * #SpeechRecognitionRecognizer is in the #SPEECH_ENGINE_DEALLOCATED or 
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * See: speech_recognition_recognizer_commit_changes()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for a 
 *       #SpeechRecognitionRecognizer in the #SPEECH_ENGINE_DEALLOCATED or
 *       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_suspend(SpeechRecognitionRecognizer *recognizer)
{
    printf("speech_recognition_recognizer_suspend called.\n");
}
