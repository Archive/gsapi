
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#include <speech/recognition/speech_recognition_rule_sequence.h>


GType
speech_recognition_rule_sequence_get_type(void)
{
    printf("speech_recognition_rule_sequence_get_type called.\n");
}


SpeechRecognitionRuleSequence *
speech_recognition_rule_sequence_new(void)
{
    printf("speech_recognition_rule_sequence_new called.\n");
}


/**
 * speech_recognition_rule_sequence_new_with_subrule:
 * @rule: the rule.
 *
 * Initialize a #SpeechRecognitionRuleSequence object containing a
 * single #SpeechRecognitionRule.
 *
 * Returns:
 */

SpeechRecognitionRuleSequence *
speech_recognition_rule_sequence_new_with_subrule(SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_sequence_new_with_subrule called.\n");
}


/**
 * speech_recognition_rule_sequence_new_with_tokens:
 * @tokens: a sequence of strings that are converted to
 * 		 #SpeechRecognitionRuleTokens.
 *
 * Initializer for #SpeechRecognitionRuleSequence that is a sequence
 * of strings that are converted to #SpeechRecognitionRuleTokens.
 *
 * A string containing multiple words (e.g. "san francisco") is
 * treated as a single token. If appropriate, an application should
 * parse such strings to produce separate tokens.
 *
 * The token list may be zero-length or null. This will produce a
 * zero-length sequence which is equivalent to &lt;NULL&gt;.
 *
 * Returns:
 *
 * See: #SpeechRecognitionRuleToken
 */

SpeechRecognitionRuleSequence *
speech_recognition_rule_sequence_new_with_tokens(gchar *tokens[])
{
    printf("speech_recognition_rule_sequence_new_with_tokens called.\n");
}


/**
 * speech_recognition_rule_sequence_new_with_subrules:
 * @rules: an array of sub-rules.
 *
 * Initializes a #SpeechRecognitionRuleSequence object with an array
 * of sub-rules.
 *
 * Returns:
 */

SpeechRecognitionRuleSequence *
speech_recognition_rule_sequence_new_with_subrules(
                                          SpeechRecognitionRule *rules[])
{
    printf("speech_recognition_rule_sequence_new_with_subrules called.\n");
}


/**
 * speech_recognition_rule_sequence_append:
 * @rule_sequence: the #SpeechRecognitionRuleSequence object that this
 *                 operation will be applied to.
 * @rule: the rule.
 *
 * Append a single rule to the end of the sequence.
 */

void
speech_recognition_rule_sequence_append(
                                 SpeechRecognitionRuleSequence *rule_sequence,
                                 SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_sequence_append called.\n");
}


/**
 * speech_recognition_rule_sequence_get_rules:
 * @rule_sequence: the #SpeechRecognitionRuleSequence object that this
 *                 operation will be applied to.
 *
 * Return the array of rules in the sequence.
 *
 * Returns: the array of rules in the sequence.
 */

SpeechRecognitionRule **
speech_recognition_rule_sequence_get_rules(
                                  SpeechRecognitionRuleSequence *rule_sequence)
{
    printf("speech_recognition_rule_sequence_get_rules called.\n");
}


/**
 * speech_recognition_rule_sequence_set_rules:
 * @rule_sequence: the #SpeechRecognitionRuleSequence object that this
 *                 operation will be applied to.
 * @rules: an array of rules.
 *
 * Set the array of rules in the sequence. The array may be
 * zero-length or null. This will produce a zero-length sequence
 * which is equivalent to &lt;NULL&gt;.
 */

void
speech_recognition_rule_sequence_set_rules(
                                  SpeechRecognitionRuleSequence *rule_sequence,
                                  SpeechRecognitionRule *rules[])
{
    printf("speech_recognition_rule_sequence_set_rules called.\n");
}
