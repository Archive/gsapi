
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Description of a problem found in a grammar usually bundled with a
 * #SpeechRecognitionGrammarException. The grammar may have been created
 * programmatically or by loading a Java Speech Grammar Format document.
 * Multiple #SpeechRecognitionGrammarSyntaxDetail objects may be encapsulated
 * by a single #SpeechRecognitionGrammarException.
 *
 * Depending on the type of error and the context in which the error is
 * identified, some or all of the following information may be provided:
 *
 * <itemizedlist>
 *   <listitem>Grammar name,</listitem>
 *   <listitem>Grammar location (URL or system resource),</listitem>
 *   <listitem>Rule name in which error is found,</listitem>
 *   <listitem>Import name,</listitem>
 *   <listitem>Line number of error in grammar formatted text,</listitem>
 *   <listitem>Character number (within line) in grammar formatted text,
 *       </listitem>
 *   <listitem>A printable description string.</listitem>
 * </itemizedlist>
 *
 * The following problems may be encountered when loading marked up text
 * from a #SpeechGrammarURL or #SpeechInputStream, or through
 * the speech_recognition_rule_grammar_rule_for_grammar() function of the
 * #SpeechRecognitionRuleGrammar object.
 *
 * <itemizedlist>
 *   <listitem>Missing or illegal grammar name declaration, or grammar 
 *       name doesn't match URL or file name.</listitem>
 *   <listitem>missing URL or URL does not contain grammar formatted file.
 *       </listitem>
 *   <listitem>Illegal import declaration.</listitem>
 *   <listitem>Illegal rule name or token.</listitem>
 *   <listitem>Missing semi-colon.</listitem>
 *   <listitem>Redefinition of a rule.</listitem>
 *   <listitem>Definition of a reserved rule name 
 *       (&lt;NULL&gt;, &lt;VOID&gt;).</listitem>
 *   <listitem>Unclosed quotes, tags, comment, or rule name.</listitem>
 *   <listitem>Unclosed grouping "()" or "[]".</listitem>
 *   <listitem>Empty rule definition or empty alternative.</listitem>
 *   <listitem>Missing or illegal weight on alternatives.</listitem>
 *   <listitem>Illegal attachment of unary operators (count and tag).
 *       </listitem>
 *   <listitem>Some other error.</listitem>
 * </itemizedlist>
 *
 * When the speech_recognition_recognizer_commit_changes() function of a 
 * #SpeechRecognitionRecognizer is called, it performs addition checks to 
 * ensure all loaded grammars are legal. The following problems may be 
 * encountered:
 *
 * <itemizedlist>
 *   <listitem>Unable to resolve import because grammar or rule is not 
 *       defined.</listitem>
 *   <listitem>Reference to an undefined rule name.</listitem>
 *   <listitem>Illegal recursion: a rule refers to itself illegally.
 *	 Only right recursion is allowed (defined by the supported grammar
 *	 format).</listitem>
 *   <listitem>Ambiguous reference to imported rule.</listitem>
 * </itemizedlist>
 *
 * See: #SpeechRecognitionGrammarException
 * See: speech_recognition_recognizer_load_grammar_from_stream()
 * See: speech_recognition_recognizer_load_grammar_from_url()
 * See: speech_recognition_rule_grammar_rule_for_grammar()
 * See: speech_recognition_recognizer_commit_changes()
 */

#include <speech/recognition/speech_recognition_grammar_syntax_detail.h>


GType
speech_recognition_grammar_syntax_detail_get_type(void)
{
    printf("speech_recognition_grammar_syntax_detail_get_type called.\n");
}


SpeechRecognitionGrammarSyntaxDetail *
speech_recognition_grammar_syntax_detail_new(void)
{
    printf("speech_recognition_grammar_syntax_detail_new called.\n");
}


/**
 * speech_recognition_grammar_syntax_detail_new_with_problem_details:
 * @grammar_name: name of grammar in which problem is encountered.
 * @grammar_location: URL location of grammar in which problem is encountered.
 * @rule_name: name of rule within grammar in which problem is encountered.
 * @import_name: name in grammar import declaration in which problem is 
 *               encountered.
 * @line_number: line number in grammar formatted file for problem.
 * @char_number: character number in line in grammar formatted file for 
 *               problem.
 * @message: printable string describing problem.
 *
 * Complete initializer describing a syntax problem.
 *
 * Returns:
 */

SpeechRecognitionGrammarSyntaxDetail *
speech_recognition_grammar_syntax_detail_new_with_problem_details(
                                        gchar *grammar_name,
                                        SpeechGrammarURL grammar_location,
                                        gchar *rule_name,
                                        SpeechRecognitionRuleName *import_name,
                                        int line_number,
                                        int char_number,
                                        gchar *message)
{
    printf("speech_recognition_grammar_syntax_detail_new_with_problem_details called.\n");
}
