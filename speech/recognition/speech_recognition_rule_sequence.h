
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_SEQUENCE_H__
#define __SPEECH_RECOGNITION_RULE_SEQUENCE_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_rule.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE_SEQUENCE              (speech_recognition_rule_sequence_get_type())
#define SPEECH_RECOGNITION_RULE_SEQUENCE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE_SEQUENCE, SpeechRecognitionRuleSequence))
#define SPEECH_RECOGNITION_RULE_SEQUENCE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE_SEQUENCE, SpeechRecognitionRuleSequence))
#define SPEECH_IS_RECOGNITION_RULE_SEQUENCE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE_SEQUENCE))
#define SPEECH_IS_RECOGNITION_RULE_SEQUENCE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE_SEQUENCE))
#define SPEECH_RECOGNITION_RULE_SEQUENCE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE_SEQUENCE, SpeechRecognitionRuleSequenceClass))

typedef struct _SpeechRecognitionRuleSequence            SpeechRecognitionRuleSequence;
typedef struct _SpeechRecognitionRuleSequenceClass       SpeechRecognitionRuleSequenceClass;

struct _SpeechRecognitionRuleSequence {
    SpeechRecognitionRule rule;
};

struct _SpeechRecognitionRuleSequenceClass {
    SpeechRecognitionRuleClass parent_class;
};


GType 
speech_recognition_rule_sequence_get_type(void);

SpeechRecognitionRuleSequence * 
speech_recognition_rule_sequence_new(void);

SpeechRecognitionRuleSequence * 
speech_recognition_rule_sequence_new_with_subrule(
                                          SpeechRecognitionRule *rule);

SpeechRecognitionRuleSequence * 
speech_recognition_rule_sequence_new_with_tokens(gchar *tokens[]);

SpeechRecognitionRuleSequence * 
speech_recognition_rule_sequence_new_with_subrules(
                                          SpeechRecognitionRule *rules[]);

void 
speech_recognition_rule_sequence_append(
                                  SpeechRecognitionRuleSequence *rule_sequence,
                                  SpeechRecognitionRule *rule);

SpeechRecognitionRule ** 
speech_recognition_rule_sequence_get_rules(
                                  SpeechRecognitionRuleSequence *rule_sequence);

void 
speech_recognition_rule_sequence_set_rules(
                                  SpeechRecognitionRuleSequence *rule_sequence,
                                  SpeechRecognitionRule *rules[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_SEQUENCE_H__ */
