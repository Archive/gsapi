
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_TAG_H__
#define __SPEECH_RECOGNITION_RULE_TAG_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_rule.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE_TAG              (speech_recognition_rule_tag_get_type())
#define SPEECH_RECOGNITION_RULE_TAG(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE_TAG, SpeechRecognitionRuleTag))
#define SPEECH_RECOGNITION_RULE_TAG_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE_TAG, SpeechRecognitionRuleTag))
#define SPEECH_IS_RECOGNITION_RULE_TAG(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE_TAG))
#define SPEECH_IS_RECOGNITION_RULE_TAG_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE_TAG))
#define SPEECH_RECOGNITION_RULE_TAG_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE_TAG, SpeechRecognitionRuleTagClass))

typedef struct _SpeechRecognitionRuleTag            SpeechRecognitionRuleTag;
typedef struct _SpeechRecognitionRuleTagClass       SpeechRecognitionRuleTagClass;

struct _SpeechRecognitionRuleTag {
    SpeechRecognitionRule rule;
};

struct _SpeechRecognitionRuleTagClass {
    SpeechRecognitionRuleClass parent_class;
};


GType 
speech_recognition_rule_tag_get_type(void);

SpeechRecognitionRuleTag * 
speech_recognition_rule_tag_new(void);

SpeechRecognitionRuleTag * 
speech_recognition_rule_tag_new_with_rule_and_tag(SpeechRecognitionRule *rule,
                                                  gchar *tag);

SpeechRecognitionRule * 
speech_recognition_rule_tag_get_rule(SpeechRecognitionRuleTag *rule_tag);

void 
speech_recognition_rule_tag_set_rule(SpeechRecognitionRuleTag *rule_tag,
                                     SpeechRecognitionRule *rule);

gchar * 
speech_recognition_rule_tag_get_tag(SpeechRecognitionRuleTag *rule_tag);

void 
speech_recognition_rule_tag_set_tag(SpeechRecognitionRuleTag *rule_tag,
                                    gchar *tag);

gchar *
speech_recognition_rule_tag_to_string(SpeechRecognitionRuleTag *rule_tag);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_TAG_H__ */
