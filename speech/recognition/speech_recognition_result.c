
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionResult is issued by a #SpeechRecognitionRecognizer 
 * as it recognizes an incoming utterance that matches an active
 * #SpeechRecognitionGrammar. The #SpeechRecognitionResult object 
 * provides the application with access to the following information 
 * about a recognized utterance:
 *
 * <orderedlist>
 *   <listitem>A sequence of finalized tokens (words) that have been 
 *       recognized,</listitem>
 *   <listitem>A sequence of unfinalized tokens,</listitem>
 *   <listitem>Reference to the grammar matched by the result,</listitem>
 *   <listitem>The result state: #SPEECH_RECOGNITION_RESULT_UNFINALIZED,
 *       #SPEECH_RECOGNITION_RESULT_ACCEPTED or 
 *       #SPEECH_RECOGNITION_RESULT_REJECTED.</listitem>
 * </orderedlist>
 *
 * <title>Multiple Result Interfaces</title>
 *
 * <emphasis>
 * Every #SpeechRecognitionResult object provided by a 
 * #SpeechRecognitionRecognizer implements both the 
 * #SpeechRecognitionFinalRuleResult and  
 * #SpeechRecognitionFinalDictationResult objects. Thus, by extension every
 * result also implements the #SpeechRecognitionFinalResult and
 * #SpeechRecognitionResult objects.
 * </emphasis>
 *
 * These multiple objects are designed to explicitly indicate:
 * (a) what information is available at what times in the result life-cycle and
 * (b) what information is available for different types of results.
 * Appropriate casting of results allows compile-time checking of
 * result-handling code and fewer bugs.
 *
 * The #SpeechRecognitionFinalResult extends the #SpeechRecognitionResult 
 * object. It provides access to the additional information about a result 
 * that is available once it has been finalized (once it is in either of the
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED or #SPEECH_RECOGNITION_RESULT_REJECTED
 * states). Calling any function of the #SpeechRecognitionFinalResult object
 * for a result in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED state causes 
 * a #SPEECH_RECOGNITION_RESULT_STATE_ERROR to be returned.
 *
 * The #SpeechRecognitionFinalRuleResult extends the 
 * #SpeechRecognitionFinalResult object. It provides access to the 
 * additional information about a finalized result that matches a 
 * #SpeechRecognitionRuleGrammar. Calling any function of the 
 * #SpeechRecognitionFinalRuleResult object for a non-finalized
 * result or a result that matches a #SpeechRecognitionDictationGrammar 
 * causes a #SPEECH_RECOGNITION_RESULT_STATE_ERROR to be returned.
 *
 * The #SpeechRecognitionFinalDictationResult also extends the
 * #SpeechRecognitionFinalResult object. It provides access to the 
 * additional information about a finalized result that matches a
 * #SpeechRecognitionDictationGrammar. Calling any function of the
 * #SpeechRecognitionFinalDictationResult object for a non-finalized result
 * or a result that matches a #SpeechRecognitionRuleGrammar causes a
 * #SPEECH_RECOGNITION_RESULT_STATE_ERROR to be returned.
 *
 * Note: every result implements both the #SpeechRecognitionFinalRuleResult
 * and #SpeechRecognitionFinalDictationResult objects even though the result
 * will match either a #SpeechRecognitionRuleGrammar or 
 * #SpeechRecognitionDictationGrammar, but never both. The reason for this 
 * is that when the result is created (#SPEECH_RECOGNITION_RESULT_CREATED
 * event), the grammar is not always known.
 *
 * <title>Result States</title>
 *
 * The separate objects determine what information is available for a
 * result in the different stages of its life-cycle. The state of a
 * #SpeechRecognitionResult is determined by calling the
 * speech_recognition_result_get_result_state() function. The three possible
 * states are
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED, 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED and 
 * #SPEECH_RECOGNITION_RESULT_REJECTED.
 *
 * A new result starts in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED state.
 * When the result is finalized is moves to either the 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED or
 * #SPEECH_RECOGNITION_RESULT_REJECTED state. An accepted or rejected 
 * result is termed a finalized result. All values and information 
 * regarding a finalized result are fixed (excepting that audio and 
 * training information may be released).
 *
 * Following are descriptions of a result object in each of the three states
 * including information on which objects can be used in each state.
 *
 *
 * <title>speech_recognition_result_get_result_state() == #SPEECH_RECOGNITION_RESULT_UNFINALIZED</title>
 *
 * <itemizedlist>
 *   <listitem>Recognition of the result is in progress.</listitem>
 *   <listitem>A new result is created with a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event
 *	 that is issued to each #SpeechRecognitionResultListener attached to a
 *	 #SpeechRecognitionRecognizer. The new result is created in in the
 *	 #SPEECH_RECOGNITION_RESULT_UNFINALIZED state.</listitem>
 *   <listitem>A result remains in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 *       state until it is finalized by either a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event.</listitem>
 *   <listitem>Applications should only call the functions of the
 *	 #SpeechRecognitionResult object. A 
 *       #SPEECH_RECOGNITION_RESULT_STATE_ERROR is
 *	 issued on calls to the functions of #SpeechRecognitionFinalResult,
 *	 #SpeechRecognitionFinalRuleResult and 
 *       #SpeechRecognitionFinalDictationResult objects.</listitem>
 *   <listitem>Events 1: zero or more 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events may be
 *       issued as:
 *	 (a) tokens are finalized, or
 *	 (b) as the unfinalized tokens changes.</listitem>
 *   <listitem>Events 2: one 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event must be 
 *       issued in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED state before 
 *       result finalization by an 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event. (Not 
 *       required if a result is rejected).</listitem>
 *   <listitem>Events 3: the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 *       event is optional if the result is finalized by a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event.
 *	 (It is not always possible for a recognizer to identify a
 *	 best-match grammar for a rejected result).</listitem>
 *   <listitem>Prior to the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 *       event, the speech_recognition_result_get_grammar() returns %NULL.
 *	 Following the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 *       event the speech_recognition_result_get_grammar() function returns 
 *       a non-null reference to the active #SpeechRecognitionGrammar that 
 *       is matched by this result.</listitem>
 *   <listitem>speech_recognition_result_num_tokens() returns the number of 
 *       finalized tokens. While in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 *       this number may increase as 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events are 
 *       issued.</listitem>
 *   <listitem>The best guess for each finalized token is available through
 *	 speech_recognition_result_get_best_token(). The best guesses are 
 *       guaranteed not to change through the remaining life of the 
 *       result.</listitem>
 *   <listitem>speech_recognition_result_get_unfinalized_tokens() may return 
 *       zero or more tokens and these may change at any time when a
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED event is 
 *       issued.</listitem>
 * </itemizedlist>
 *
 *
 * <title>speech_recognition_result_get_result_state() == #SPEECH_RECOGNITION_RESULT_ACCEPTED</title>
 *
 * <itemizedlist>
 *   <listitem>Recognition of the #SpeechRecognitionResult is complete and the
 *	 recognizer is confident it has the correct result (not a
 *	 rejected result). Non-rejection is not a guarantee of a correct
 *	 result - only sufficient confidence that the guess is correct.
 *       </listitem>
 *   <listitem>Events 1: a result transitions from the 
 *       #SPEECH_RECOGNITION_RESULT_UNFINALIZED state to the 
 *       #SPEECH_RECOGNITION_RESULT_ACCEPTED state when an
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event is 
 *       issued.</listitem>
 *   <listitem>- Events 2: #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED and
 *	  #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED events may 
 *        occur optionally (once) in the #SPEECH_RECOGNITION_RESULT_ACCEPTED
 *        state.</listitem>
 *   <listitem>speech_recognition_result_num_tokens() will return 1 or 
 *       greater (there must be at least one finalized token) and the number 
 *       of finalized tokens will not change.
 *	 [Note: A rejected result may have zero finalized tokens.]</listitem>
 *   <listitem>The best guess for each finalized token is available through the
 *	 speech_recognition_result_get_best_token() function. The best 
 *       guesses will not change through the remaining life of the result.
 *       </listitem>
 *   <listitem>speech_recognition_result_get_unfinalized_tokens() function
 *       returns %NULL.</listitem>
 *   <listitem>The speech_recognition_result_get_grammar() function returns
 *       the grammar matched by this result. It may be either a 
 *       #SpeechRecognitionRuleGrammar or #SpeechRecognitionDictationGrammar.
 *       </listitem>
 *   <listitem>For either a #SpeechRecognitionRuleGrammar or 
 *       #SpeechRecognitionDictationGrammar the functions of 
 *       #SpeechRecognitionFinalResult may be used to access audio data and 
 *       to perform correction/training.</listitem>
 *   <listitem>If the result matches a #SpeechRecognitionRuleGrammar, the 
 *       functions of #SpeechRecognitionFinalRuleResult may be used to get 
 *       alternative guesses for the complete utterance and to get tags and 
 *       other information associated with the #SpeechRecognitionRuleGrammar.
 *       (Calls to any functions of the #SpeechRecognitionFinalDictationResult
 *       object cause a #SPEECH_RECOGNITION_RESULT_STATE_ERROR).</listitem>
 *   <listitem>If the result matches a #SpeechRecognitionDictationGrammar, 
 *       the functions of #SpeechRecognitionFinalDictationResult may be used 
 *       to get alternative guesses for tokens and token sequences. (Calls 
 *       to any functions of the #SpeechRecognitionFinalRuleResult object 
 *       cause a #SPEECH_RECOGNITION_RESULT_STATE_ERROR.)</listitem>
 * </itemizedlist>
 *
 * <title>speech_recognition_result_get_result_state() == #SPEECH_RECOGNITION_RESULT_REJECTED</title>
 *
 * <itemizedlist>
 *   <listitem>Recognition of the #SpeechRecognitionResult is complete but the
 *	 recognizer believes it does not have the correct result.
 *	 Programmatically, an accepted and rejected result are very
 *	 similar but the contents of a rejected result must be treated
 *	 differently - they are likely to be wrong.</listitem>
 *   <listitem>Events 1: a result transitions from the 
 *       #SPEECH_RECOGNITION_RESULT_UNFINALIZED state to the 
 *       #SPEECH_RECOGNITION_RESULT_REJECTED state when an
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event is 
 *       issued.</listitem>
 *  <listitem>Events 2: (same as for the 
 *      #SPEECH_RECOGNITION_RESULT_ACCEPTED state)
 *	#SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED and 
 *      #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED
 *	events may occur optionally (once) in the 
 *      #SPEECH_RECOGNITION_RESULT_REJECTED state.</listitem>
 *   <listitem>speech_recognition_result_num_tokens() will return 0 or 
 *       greater. The number of tokens will not change for the remaining 
 *       life of the result.
 *	 [Note: an accepted result always has at least one finalized 
 *       token.]</listitem>
 *   <listitem>As with an accepted result, the best guess for each finalized 
 *       token is available through the 
 *       speech_recognition_result_get_best_token() function and the
 *	 tokens are guaranteed not to change through the remaining life of
 *	 the result. Because the result has been rejected the guesses are
 *	 not likely to be correct.</listitem>
 *   <listitem>speech_recognition_result_get_unfinalized_tokens() function
 *       returns %NULL.</listitem>
 *   <listitem>If the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 *       was issued during recognition of the result, the 
 *       speech_recognition_result_get_grammar() function returns the
 *	 grammar matched by this result otherwise it returns %NULL. It may
 *	 be either a #SpeechRecognitionRuleGrammar or 
 *       #SpeechRecognitionDictationGrammar. For rejected results, there 
 *       is a greater chance that this grammar is wrong.</listitem>
 *   <listitem>The #SpeechRecognitionFinalResult object behaves the same 
 *       as for a result in the #SPEECH_RECOGNITION_RESULT_ACCEPTED state 
 *       expect that the information is less likely to be reliable.</listitem>
 *   <listitem>If the grammar is known, the #SpeechRecognitionFinalRuleResult
 *       and #SpeechRecognitionFinalDictationResult objects behave the same 
 *       as for a result in the #SPEECH_RECOGNITION_RESULT_ACCEPTED state 
 *       expect that the information is less likely to be reliable. If the 
 *       grammar is unknown, then a #SPEECH_RECOGNITION_RESULT_STATE_ERROR
 *       is thrown on calls to the functions of both 
 *       #SpeechRecognitionFinalRuleResult and
 *	 #SpeechRecognitionFinalDictationResult.</listitem>
 * </itemizedlist>
 *
 *
 * <title>Result State and Recognizer States</title>
 *
 * The state system of a #SpeechRecognitionRecognizer is linked to the 
 * state of recognition of the current result. The 
 * #SpeechRecognitionRecognizer object documents the normal event cycle
 * for a #SpeechRecognitionRecognizer and for #SpeechRecognitionResults. The
 * following is an overview of the ways in which the two state systems
 * are linked:
 *
 * <itemizedlist>
 *   <listitem>The #SPEECH_ENGINE_ALLOCATED state of a 
 *       #SpeechRecognitionRecognizer has three sub-states. In the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state, the recognizer 
 *       is listening to background audio and there is no result being 
 *       produced. In the #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state,
 *       the recognizer is temporarily buffering audio input while its 
 *       grammars are updated. In the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state, the recognizer 
 *       has detected incoming audio that may match an active grammar and 
 *       is producing a #SpeechRecognitionResult.</listitem>
 *   <listitem>The #SpeechRecognitionRecognizer moves from the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_LISTENING
 *	 state to the #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state with a
 *	 #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING event 
 *       immediately prior to issuing a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event.</listitem>
 *   <listitem>The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED and 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED events are 
 *       produced while the #SpeechRecognitionRecognizer is in the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state.</listitem>
 *   <listitem>The #SpeechRecognitionRecognizer finalizes a 
 *       #SpeechRecognitionResult with 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event
 *	 immediately after it transitions from the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state to the 
 *       #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state with a
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_RECOGNIZER_SUSPENDED event.
 *       </listitem>
 *   <listitem>Unless there is a pending 
 *       speech_recognition_recognizer_suspend(), the
 *	 #SpeechRecognitionRecognizer commits grammar changes with a
 *	 #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event 
 *       as soon as the result finalization event is processed.</listitem>
 *   <listitem>The #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED
 *       and #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED events can 
 *       occur in any state of a #SPEECH_ENGINE_ALLOCATED 
 *       #SpeechRecognitionRecognizer.</listitem>
 * </itemizedlist>
 *
 * <title>Accept or Reject?</title>
 *
 * Rejection of a result indicates that the recognizer is not confident
 * that it has accurately recognized what a user says. Rejection can be
 * controlled through the #SpeechRecognitionRecognizerProperties object with
 * the speech_recognition_recognizer_properties_set_confidence_level()
 * function. Increasing the confidence level requires the recognizer to 
 * have greater confident to accept a result, so more results are likely 
 * to be rejected.
 *
 * <EM>Important</EM>: the acceptance of a result (an
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED event rather than a
 * #SPEECH_RECOGNITION_RESULT_REJECTED event) does not mean the result 
 * is correct. Instead acceptance implies that the recognizer has a 
 * sufficient level of confidence that the result is correct.
 *
 * It is difficult for recognizers to reliably determine when they make
 * mistakes. Applications need to determine the cost of incorrect recognition
 * of any particular results and take appropriate actions. For example,
 * confirm with a user that they said "delete all files" before deleting
 * anything.
 *
 * <title>Result Events</title>
 *
 * Events are issued when a new result is created and when there is any change
 * in the state or information content of a result. The following describes
 * the event sequence for an accepted result. It provides the same information
 * as above for result states, but focusses on legal event sequences.
 *
 * Before a new result is created for incoming speech, a recognizer usually
 * issues a #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED
 * event to the speech_recognition_recognizer_audio_listener_speech_started()
 * function of #SpeechRecognitionRecognizerAudioListener.
 *
 * A newly created #SpeechRecognitionResult is provided to the application by
 * calling the speech_recognition_result_listener_result_created() function
 * of each #SpeechRecognitionResultListener attached to the 
 * #SpeechRecognitionRecognizer with a 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event. The new result may 
 * or may not have any finalized tokens or unfinalized tokens.
 *
 * At any time following the #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 * event, an application may attach a #SpeechRecognitionResultListener to an 
 * individual result. That listener will receive all subsequent events 
 * associated with that #SpeechRecognitionResult.
 *
 * A new #SpeechRecognitionResult is created in the 
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state. In this state, zero or 
 * more #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events may be
 * issued to each #SpeechRecognitionResultListener attached to the
 * #SpeechRecognitionRecognizer and to each #SpeechRecognitionResultListener 
 * attached to that #SpeechRecognitionResult. The 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED indicates that one or 
 * more tokens have been finalized, or that the unfinalized tokens have 
 * changed, or both.
 *
 * When the recognizer determines which grammar is the best match for
 * incoming speech, it issues a 
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event.
 * This event is issued to each #SpeechRecognitionResultListener attached to
 * the #SpeechRecognitionRecognizer and to each 
 * #SpeechRecognitionResultListener attached to that #SpeechRecognitionResult.
 *
 * The #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event is also 
 * issued to each #SpeechRecognitionResultListener attached to the matched 
 * #SpeechRecognitionGrammar. This is the first #SpeechRecognitionResultEvent 
 * received by #SpeechRecognitionResultListeners attached to the 
 * #SpeechRecognitionGrammar. All subsequent result events are issued to 
 * all #SpeechRecognitionResultListeners attached to the matched 
 * #SpeechRecognitionGrammar (as well as to #SpeechRecognitionResultListeners 
 * attached to the #SpeechRecognitionResult and to the 
 * #SpeechRecognitionRecognizer).
 *
 * Zero or more #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events may 
 * be issued after the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED 
 * event but before the result is finalized.
 *
 * Once the recognizer completes recognition of the #SpeechRecognitionResult 
 * that it chooses to accept, it finalizes the result with an
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event that is issued to  
 * the #SpeechRecognitionResultListeners attached to the 
 * #SpeechRecognitionRecognizer, the matched #SpeechRecognitionGrammar, and 
 * the #SpeechRecognitionResult. This event may also indicate finalization 
 * of zero or more tokens, and/or the reseting of the unfinalized tokens 
 * to null. The result finalization event occurs immediately after the 
 * #SpeechRecognitionRecognizer makes a transition from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state with a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event.
 *
 * A finalized result (accepted or rejected state) may issue a
 * #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED or 
 * #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event. These 
 * events may be issued in response to relevant release functions of 
 * #SpeechRecognitionFinalResult and #SpeechRecognitionFinalDictationResult
 * or may be issued when the recognizer independently determines to release
 * audio or training information.
 *
 * When a result is rejected some of the events described above may be skipped.
 * A result may be rejected with the 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event at any time after 
 * a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event instead of an
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event. A result may be 
 * rejected with or without any unfinalized or finalized tokens being 
 * created (no #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events), 
 * and with or without a #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 * event.
 *
 * <title>When does a #SpeechRecognitionResult start and end</title>
 *
 * A new result object is created when a recognizer has detected possible
 * incoming speech which may match an active grammar.
 *
 * To accept the result (i.e. to issue a 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event), the
 * best-guess tokens of the result must match the token patterns defined by
 * the matched grammar. For a  #SpeechRecognitionRuleGrammar this implies 
 * that a call to the speech_recognition_rule_grammar_parse() function of 
 * the matched #SpeechRecognitionRuleGrammar must return successfully.
 * (Note: the parse is not guaranteed if the grammar has been changed.)
 *
 * Because there are no programmatically defined constraints upon word 
 * patterns for a #SpeechRecognitionDictationGrammar, a single result may 
 * represent a single word, a short phrase or sentence, or possibly many 
 * pages of text.
 *
 * The set of conditions that may cause a result matching a 
 * #SpeechRecognitionDictationGrammar to be finalized includes:
 *
 * <itemizedlist>
 *   <listitem>The user pauses for a period of time (a timeout).</listitem>
 *   <listitem>A call to the
 *	 speech_recognition_recognizer_force_finalize() function of the 
 *       recognizer.</listitem>
 *   <listitem>User has spoken text matching an active 
 *       #SpeechRecognitionRuleGrammar (the dictation result is finalized 
 *       and a new #SpeechRecognitionResult is issued for the 
 *       #SpeechRecognitionRuleGrammar).</listitem>
 *   <listitem>The engine is paused.</listitem>
 * </itemizedlist>
 *
 * The following conditions apply to all finalized results:
 *
 * <itemizedlist>
 *   <listitem>N-best alternative token guesses available through the
 *	 #SpeechRecognitionFinalRuleResult and 
 *       #SpeechRecognitionFinalDictationResult objects cannot cross 
 *       result boundaries.</listitem>
 *   <listitem>Correction/training is only possible within a single 
 *       result object.</listitem>
 * </itemizedlist>
 *
 * See: #SpeechRecognitionFinalResult
 * See: #SpeechRecognitionFinalRuleResult
 * See: #SpeechRecognitionFinalDictationResult
 * See: #SpeechRecognitionResultEvent
 * See: #SpeechRecognitionResultListener
 * See: #SpeechRecognitionResultAdapter
 * See: #SpeechRecognitionGrammar
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionDictationGrammar
 * See: speech_recognition_recognizer_force_finalize()
 * See: #SpeechRecognitionRecognizerEvent
 * See: speech_recognition_recognizer_properties_set_confidence_level()
 */

#include <speech/recognition/speech_recognition_result.h>


GType
speech_recognition_result_get_type(void)
{
    printf("speech_recognition_result_get_type called.\n");
}


SpeechRecognitionResult *
speech_recognition_result_new(void)
{
    printf("speech_recognition_result_new called.\n");
}


/**
 * speech_recognition_result_add_result_listener:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 * @listener: the result listener to add.
 *
 * Request notifications of events of related to this
 * #SpeechRecognitionResult. An application can attach multiple
 * listeners to a #SpeechRecognitionResult. A listener can be
 * removed with the speech_recognition_result_remove_result_listener() 
 * function.
 *
 * #SpeechRecognitionResultListener objects can also be attached to a
 * #SpeechRecognitionRecognizer and to any #SpeechRecognitionGrammar. A
 * listener attached to the #SpeechRecognitionRecognizer receives
 * all events for all results produced by that #SpeechRecognitionRecognizer.
 * A listener attached to a #SpeechRecognitionGrammar receives all
 * events for all results that have been finalized for that
 * #SpeechRecognitionGrammar (all events starting with and including
 * the #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event).
 *
 * A #SpeechRecognitionResultListener attached to a #SpeechRecognitionResult
 * only receives events following the point in time at which the
 * listener is attached. Because the listener can only be attached
 * during or after the #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED, it 
 * will not receive the #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED 
 * event. Only #SpeechRecognitionResultListeners attached to the
 * #SpeechRecognitionRecognizer receive 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED events.
 *
 * See: speech_recognition_result_remove_result_listener()
 * See: speech_recognition_recognizer_add_result_listener()
 * See: speech_recognition_grammar_add_result_listener()
 */

void
speech_recognition_result_add_result_listener(
                              SpeechRecognitionResult *result,
                              SpeechRecognitionResultListener *listener)
{
    printf("speech_recognition_result_add_result_listener called.\n");
}


/**
 * speech_recognition_result_remove_result_listener:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 * @listener: the result listener to remove.
 *
 * Remove a listener from this #SpeechRecognitionResult.
 *
 * See: speech_recognition_result_add_result_listener()
 * See: speech_recognition_recognizer_add_result_listener()
 * See: speech_recognition_grammar_add_result_listener()
 */

void
speech_recognition_result_remove_result_listener(
                              SpeechRecognitionResult *result,
                              SpeechRecognitionResultListener *listener)
{
    printf("speech_recognition_result_remove_result_listener called.\n");
}


/**
 * speech_recognition_result_get_best_token:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 * @tok_num: the @tok_num'th token.
 * @token: return the best guess for the @tok_num'th token.
 *
 * Returns the best guess for the @tok_num'th token.
 * @tokNum must be in the range 0 to numTokens()-1.
 *
 * If the result has zero tokens (possible in both the
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED and 
 * #SPEECH_RECOGNITION_RESULT_REJECTED states) an exception is returned.
 *
 * If the result is in the #SPEECH_RECOGNITION_RESULT_REJECTED state, 
 * then the returned tokens are likely to be incorrect. In the
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED state (not rejected) the 
 * recognizer is confident that the tokens are correct but applications 
 * should consider the possibility that the tokens are incorrect.
 *
 * The #SpeechRecognitionFinalRuleResult and
 * #SpeechRecognitionFinalDictationResult objects provide
 * speech_recognition_final_dictation_result_get_alternative_tokens() and
 * speech_recognition_final_rule_result_get_alternative_tokens() functions
 * that return alternative token guesses for finalized results.
 *
 * See: speech_recognition_result_get_unfinalized_tokens()
 * See: speech_recognition_result_get_best_tokens()
 * See: speech_recognition_final_dictation_result_get_alternative_tokens()
 * See: speech_recognition_final_rule_result_get_alternative_tokens()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if @tok_num is out of 
 *       range.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_result_get_best_token(int tok_num,
                                         SpeechRecognitionResult *result,
                                         SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_get_best_token called.\n");
}


/**
 * speech_recognition_result_get_best_tokens:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 *
 * Returns all the best guess tokens for this result.
 * If the result has zero tokens, the return value is %NULL.
 *
 * Returns: all the best guess tokens for this result.
 */

SpeechRecognitionResultToken **
speech_recognition_result_get_best_tokens(SpeechRecognitionResult *result)
{
    printf("speech_recognition_result_get_best_tokens called.\n");
}


/**
 * speech_recognition_result_get_grammar:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 *
 * Return the #SpeechRecognitionGrammar matched by the best-guess
 * finalized tokens of this result or %NULL if the grammar is
 * not known. The return value is null before a
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event and 
 * non-null afterwards.
 *
 * The grammar is guaranteed to be non-null for an accepted result.
 * The grammar may be null or non-null for a rejected result,
 * depending upon whether a 
 * #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event was issued 
 * prior to finalization.
 *
 * For a finalized result, an application should determine the type
 * of matched grammar with an "instanceof" type test. For a
 * result that matches a #SpeechRecognitionRuleGrammar, the functions of
 * #SpeechRecognitionFinalRuleResult can be used (the functions of
 * #SpeechRecognitionFinalDictationResult return an error). For a result
 * that matches a #SpeechRecognitionDictationGrammar, the functions of
 * #SpeechRecognitionFinalDictationResult can be used (the functions of
 * #SpeechRecognitionFinalRuleResult return an error). The functions of
 * #SpeechRecognitionFinalResult can be used for a result matching either
 * kind of grammar.
 *
 * Returns: the #SpeechRecognitionGrammar matched by the best-guess
 *	   finalized tokens of this result or %NULL if the grammar
 *	   is not known.
 *
 * See: speech_recognition_result_get_result_state()
 */

SpeechRecognitionGrammar *
speech_recognition_result_get_grammar(SpeechRecognitionResult *result)
{
    printf("speech_recognition_result_get_grammar called.\n");
}


/**
 * speech_recognition_result_get_result_state:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 *
 * Returns the current state of the Result object:
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED, 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED or
 * #SPEECH_RECOGNITION_RESULT_REJECTED. The details of a 
 * #SpeechRecognitionResult in each state are described above.
 *
 * Returns: the current state of the Result object.
 *
 * See: #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 * See: #SPEECH_RECOGNITION_RESULT_ACCEPTED
 * See: #SPEECH_RECOGNITION_RESULT_REJECTED
 */

int
speech_recognition_result_get_result_state(SpeechRecognitionResult *result)
{
    printf("speech_recognition_result_get_result_state called.\n");
}


/**
 * speech_recognition_result_get_unfinalized_tokens:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 *
 * In the #SPEECH_RECOGNITION_RESULT_UNFINALIZED, return the current guess
 * of the tokens following the finalized tokens. Unfinalized tokens
 * provide an indication of what a recognizer is considering as
 * possible recognition tokens for speech following the finalized
 * tokens.
 *
 * Unfinalized tokens can provide users with feedback on the
 * recognition process. The array may be any length (zero or more
 * tokens), the length may change at any time, and successive calls to
 * speech_recognition_result_get_unfinalized_tokens() may return different 
 * tokens or even different numbers of tokens. When the unfinalized tokens
 * are changed, a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED event is 
 * issued to the #SpeechRecognitionResultListener. The 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED and 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED events finalize a 
 * result and always guarantee that the return value is %NULL. A new result
 * created with a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event may 
 * have a %NULL or non-null value.
 *
 * The returned array is %NULL if there are currently no unfinalized
 * tokens, if the recognizer does not support unfinalized tokens, or
 * after a #SpeechRecognitionResult is finalized (in the
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED or 
 * #SPEECH_RECOGNITION_RESULT_REJECTED state).
 *
 * Returns: the current guess of the tokens following the finalized
 *	   tokens.
 *
 * See: speech_recognition_result_event_is_unfinalized_tokens_changed()
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED
 */

SpeechRecognitionResultToken **
speech_recognition_result_get_unfinalized_tokens(
                                              SpeechRecognitionResult *result)
{
    printf("speech_recognition_result_get_unfinalized_tokens called.\n");
}


/**
 * speech_recognition_result_num_tokens:
 * @result: the #SpeechRecognitionResult object that this operation will be
 *          applied to.
 *
 * Returns the number of finalized tokens in a #SpeechRecognitionResult.
 * Tokens are numbered from 0 to numTokens()-1 and are obtained
 * through the speech_recognition_result_get_best_token() and
 * speech_recognition_result_get_best_tokens() functions of this 
 * (#SpeechRecognitionResult) object and the
 * speech_recognition_final_dictation_result_get_alternative_tokens() and
 * speech_recognition_final_rule_result_get_alternative_tokens() functions
 * of the #SpeechRecognitionFinalRuleResult and 
 * #SpeechRecognitionFinalDictationResult objects for a finalized result.
 *
 * Starting from the #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event 
 * and while the result remains in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 * state, the number of finalized tokens may be zero or greater and can 
 * increase as tokens are finalized. When one or more tokens are finalized in
 * the #SPEECH_RECOGNITION_RESULT_UNFINALIZED state, a 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED event is issued with 
 * the #tokenFinalized flag set %TRUE.
 * The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED and 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED events which finalize 
 * a result can also indicate that one or more tokens have been finalized.
 *
 * In the #SPEECH_RECOGNITION_RESULT_ACCEPTED and 
 * #SPEECH_RECOGNITION_RESULT_REJECTED states,
 * speech_recognition_result_num_tokens() indicates the total number of 
 * tokens that were finalized. The number of finalized tokens never 
 * changes in these states. A #SPEECH_RECOGNITION_RESULT_ACCEPTED result 
 * must have one or more finalized token. A 
 * #SPEECH_RECOGNITION_RESULT_REJECTED result may have zero or more tokens.
 *
 * Returns: the number of finalized tokens in a #SpeechRecognitionResult.
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED
 * See: speech_recognition_result_get_best_token()
 * See: speech_recognition_result_get_best_tokens()
 * See: speech_recognition_final_dictation_result_get_alternative_tokens()
 * See: speech_recognition_final_rule_result_get_alternative_tokens()
 */

int
speech_recognition_result_num_tokens(SpeechRecognitionResult *result)
{
    printf("speech_recognition_result_num_tokens called.\n");
}
