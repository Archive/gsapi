
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RULE_ALTERNATIVES_H__
#define __SPEECH_RECOGNITION_RULE_ALTERNATIVES_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_rule.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RULE_ALTERNATIVES              (speech_recognition_rule_alternatives_get_type())
#define SPEECH_RECOGNITION_RULE_ALTERNATIVES(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RULE_ALTERNATIVES, SpeechRecognitionRuleAlternatives))
#define SPEECH_RECOGNITION_RULE_ALTERNATIVES_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RULE_ALTERNATIVES, SpeechRecognitionRuleAlternativesClass))
#define SPEECH_IS_RECOGNITION_RULE_ALTERNATIVES(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RULE_ALTERNATIVES))
#define SPEECH_IS_RECOGNITION_RULE_ALTERNATIVES_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RULE_ALTERNATIVES))
#define SPEECH_RECOGNITION_RULE_ALTERNATIVES_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RULE_ALTERNATIVES, SpeechRecognitionRuleAlternativesClass))

typedef struct _SpeechRecognitionRuleAlternatives            SpeechRecognitionRuleAlternatives;
typedef struct _SpeechRecognitionRuleAlternativesClass       SpeechRecognitionRuleAlternativesClass;

struct _SpeechRecognitionRuleAlternatives {
    SpeechRecognitionRule rule;
};

struct _SpeechRecognitionRuleAlternativesClass {
    SpeechRecognitionRuleClass parent_class;
};


GType 
speech_recognition_rule_alternatives_get_type(void);

SpeechRecognitionRuleAlternatives * 
speech_recognition_rule_alternatives_new(void);

SpeechRecognitionRuleAlternatives * 
speech_recognition_rule_alternatives_new_with_subrule(
                                              SpeechRecognitionRule *rule);

SpeechRecognitionRuleAlternatives * 
speech_recognition_rule_alternatives_new_with_tokens(gchar *tokens[]);

SpeechRecognitionRuleAlternatives * 
speech_recognition_rule_alternatives_new_with_subrules(
                                              SpeechRecognitionRule *rules[]);

SpeechRecognitionRuleAlternatives * 
speech_recognition_rule_alternatives_new_with_subrules_and_weights(
                                              SpeechRecognitionRule *rules[],
                                              float weights[]);

void 
speech_recognition_rule_alternatives_append(
                           SpeechRecognitionRuleAlternatives *rule_alternatives,
                           SpeechRecognitionRule rule);

SpeechRecognitionRule ** 
speech_recognition_rule_alternatives_get_rules(
                         SpeechRecognitionRuleAlternatives *rule_alternatives);

float ** 
speech_recognition_rule_alternatives_get_weights(
                         SpeechRecognitionRuleAlternatives *rule_alternatives);

void 
speech_recognition_rule_alternatives_set_rules(
                           SpeechRecognitionRuleAlternatives *rule_alternatives,
                           SpeechRecognitionRule *rules[]);

SpeechStatus 
speech_recognition_rule_alternatives_set_weights(
                           SpeechRecognitionRuleAlternatives *rule_alternatives,
                           float weights[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RULE_ALTERNATIVES_H__ */
