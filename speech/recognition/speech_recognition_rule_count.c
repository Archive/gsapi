
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Attaches a count to a contained #SpeechRecognitionRule object to indicate
 * the number of times it may occur. The contained rule may occur optionally
 * (zero or one times), one or more times, or zero or more times. The three
 * count are equivalent to the "[]", "+" and "*" operators of the Java
 * Speech Grammar Format.
 *
 * Any #SpeechRecognitionRule not contained by a #SpeechRecognitionRuleCount 
 * object occurs once only.
 *
 * See: #SpeechRecognitionRule
 * See: #SpeechRecognitionRuleAlternatives
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionRuleName
 * See: #SpeechRecognitionRuleParse
 * See: #SpeechRecognitionRuleSequence
 * See: #SpeechRecognitionRuleTag
 * See: #SpeechRecognitionRuleToken
 */

#include <speech/recognition/speech_recognition_rule_count.h>


GType
speech_recognition_rule_count_get_type(void)
{
    printf("speech_recognition_rule_count_get_type called.\n");
}


SpeechRecognitionRuleCount *
speech_recognition_rule_count_new(void)
{
    printf("speech_recognition_rule_count_new called.\n");
}


/**
 * speech_recognition_rule_count_new_with_rule:
 * @rule: the rule.
 * @count: the count.
 *
 * #SpeechRecognitionRuleCount initializer with contained rule and count.
 *
 * Returns:
 */

SpeechRecognitionRuleCount *
speech_recognition_rule_count_new_with_rule(SpeechRecognitionRule *rule,
                                            int count)
{
    printf("speech_recognition_rule_count_new_with_rule called.\n");
}


/**
 * speech_recognition_rule_count_get_count:
 * @rule_count: the #SpeechRecognitionRuleCount object that this operation
 *              will be applied to.
 *
 * Returns: the count: 
 * #SPEECH_RECOGNITION_RULE_COUNT_OPTIONAL,
 * #SPEECH_RECOGNITION_RULE_COUNT_ZERO_OR_MORE,
 * #SPEECH_RECOGNITION_RULE_COUNT_ONCE_OR_MORE.
 *
 * See: #SPEECH_RECOGNITION_RULE_COUNT_OPTIONAL
 * See: #SPEECH_RECOGNITION_RULE_COUNT_ZERO_OR_MORE
 * See: #SPEECH_RECOGNITION_RULE_COUNT_ONCE_OR_MORE
 */

int
speech_recognition_rule_count_get_count(SpeechRecognitionRuleCount *rule_count)
{
    printf("speech_recognition_rule_count_get_count called.\n");
}


/**
 * speech_recognition_rule_count_set_count:
 * @rule_count: the #SpeechRecognitionRuleCount object that this operation
 *              will be applied to.
 * @count: the count.
 *
 * Set the count. If count is not one of the defined values
 * (#SPEECH_RECOGNITION_RULE_COUNT_OPTIONAL, 
 *  #SPEECH_RECOGNITION_RULE_COUNT_ZERO_OR_MORE,
 *  #SPEECH_RECOGNITION_RULE_COUNT_ONCE_OR_MORE) the call is ignored.
 *
 * See: #SPEECH_RECOGNITION_RULE_COUNT_OPTIONAL
 * See: #SPEECH_RECOGNITION_RULE_COUNT_ZERO_OR_MORE
 * See: #SPEECH_RECOGNITION_RULE_COUNT_ONCE_OR_MORE
 */

void
speech_recognition_rule_count_set_count(SpeechRecognitionRuleCount *rule_count,
                                        int count)
{
    printf("speech_recognition_rule_count_set_count called.\n");
}


/**
 * speech_recognition_rule_count_get_rule:
 * @rule_count: the #SpeechRecognitionRuleCount object that this operation
 *              will be applied to.
 *
 * Returns: the contained #SpeechRecognitionRule object.
 */

SpeechRecognitionRule *
speech_recognition_rule_count_get_rule(SpeechRecognitionRuleCount *rule_count)
{
    printf("speech_recognition_rule_count_get_rule called.\n");
}


/**
 * speech_recognition_rule_count_set_rule:
 * @rule_count: the #SpeechRecognitionRuleCount object that this operation
 *              will be applied to.
 * @rule: the rule.
 *
 * Set the contained #SpeechRecognitionRule object.
 */

void
speech_recognition_rule_count_set_rule(SpeechRecognitionRuleCount *rule_count,
                                       SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_count_set_rule called.\n");
}
