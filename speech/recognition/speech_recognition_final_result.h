
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_FINAL_RESULT_H__
#define __SPEECH_RECOGNITION_FINAL_RESULT_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_result.h>
#include <speech/recognition/speech_recognition_result_token.h>
#include <speech/recognition/speech_recognition_audio_clip.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_FINAL_RESULT              (speech_recognition_final_result_get_type())
#define SPEECH_RECOGNITION_FINAL_RESULT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_FINAL_RESULT, SpeechRecognitionFinalResult))
#define SPEECH_RECOGNITION_FINAL_RESULT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_FINAL_RESULT, SpeechRecognitionFinalResultClass))
#define SPEECH_IS_RECOGNITION_FINAL_RESULT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_FINAL_RESULT))
#define SPEECH_IS_RECOGNITION_FINAL_RESULT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_FINAL_RESULT))
#define SPEECH_RECOGNITION_FINAL_RESULT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_FINAL_RESULT, SpeechRecognitionFinalResultClass))


/* ******************** TRAINING ***************************** */

/*
 * The #SPEECH_RECOGNITION_FINAL_RESULT_MISRECOGNITION flag is used in 
 * a call to speech_recognition_final_result_token_correction() to 
 * indicate that the change is a correction of an error made by the 
 * recognizer.
 *
 * See: speech_recognition_final_result_token_correction()
 */

static int SPEECH_RECOGNITION_FINAL_RESULT_MISRECOGNITION = 400;

/*
 * The #SPEECH_RECOGNITION_FINAL_RESULT_USER_CHANGE flag is used in 
 * a call to speech_recognition_final_result_token_correction() to 
 * indicate that the user has modified the text that was returned by the 
 * recognizer to something different from what they actually said.
 *
 * See: speech_recognition_final_result_token_correction()
 */

static int SPEECH_RECOGNITION_FINAL_RESULT_USER_CHANGE = 401;

/*
 * The #SPEECH_RECOGNITION_FINAL_RESULT_DONT_KNOW flag is used in a 
 * call to speech_recognition_final_result_token_correction() to indicate 
 * that the application does not know whether a change to a result is 
 * because of #SPEECH_RECOGNITION_FINAL_RESULT_MISRECOGNITION or 
 * #SPEECH_RECOGNITION_FINAL_RESULT_USER_CHANGE.
 *
 * See: speech_recognition_final_result_token_correction()
 */

static int SPEECH_RECOGNITION_FINAL_RESULT_DONT_KNOW = 402;


GType 
speech_recognition_final_result_get_type(void);

SpeechRecognitionFinalResult * 
speech_recognition_final_result_new(void);

SpeechStatus 
speech_recognition_final_result_get_audio(SpeechRecognitionFinalResult *result,
                                          SpeechRecognitionAudioClip *audio);

SpeechStatus 
speech_recognition_final_result_get_audio_for_token(
                                    SpeechRecognitionFinalResult *result,
                                    SpeechRecognitionResultToken *from_token,
                                    SpeechRecognitionResultToken *to_token,
                                    SpeechRecognitionAudioClip *audio);

SpeechStatus 
speech_recognition_final_result_is_audio_available(
                                    SpeechRecognitionFinalResult *result,
                                    gboolean *available);

SpeechStatus 
speech_recognition_final_result_release_audio(
                                    SpeechRecognitionFinalResult *result);

SpeechStatus 
speech_recognition_final_result_is_training_info_available(
                                    SpeechRecognitionFinalResult *result,
                                    gboolean *available);

SpeechStatus 
speech_recognition_final_result_release_training_info(
                                    SpeechRecognitionFinalResult *result);

SpeechStatus 
speech_recognition_final_result_token_correction(
                                    SpeechRecognitionFinalResult *result,
                                    gchar *correct_tokens[],
                                    SpeechRecognitionResultToken *from_token,
                                    SpeechRecognitionResultToken *to_token,
                                    int correction_type);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_FINAL_RESULT_H__ */
