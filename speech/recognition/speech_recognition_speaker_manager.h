
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_SPEAKER_MANAGER_H__
#define __SPEECH_RECOGNITION_SPEAKER_MANAGER_H__

#include <speech/speech_input_stream.h>
#include <speech/speech_output_stream.h>
#include <speech/speech_control_component.h>
#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_speaker_profile.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_SPEAKER_MANAGER              (speech_recognition_speaker_manager_get_type())
#define SPEECH_RECOGNITION_SPEAKER_MANAGER(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_SPEAKER_MANAGER, SpeechRecognitionSpeakerManager))
#define SPEECH_RECOGNITION_SPEAKER_MANAGER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_SPEAKER_MANAGER, SpeechRecognitionSpeakerManagerClass))
#define SPEECH_IS_RECOGNITION_SPEAKER_MANAGER(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_SPEAKER_MANAGER))
#define SPEECH_IS_RECOGNITION_SPEAKER_MANAGER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_SPEAKER_MANAGER))
#define SPEECH_RECOGNITION_SPEAKER_MANAGER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_SPEAKER_MANAGER, SpeechRecognitionSpeakerManagerClass))

typedef struct _SpeechRecognitionSpeakerManager            SpeechRecognitionSpeakerManager;
typedef struct _SpeechRecognitionSpeakerManagerClass       SpeechRecognitionSpeakerManagerClass;

struct _SpeechRecognitionSpeakerManager {
    SpeechObject object;
};

struct _SpeechRecognitionSpeakerManagerClass {
    SpeechObjectClass parent_class;
};


GType 
speech_recognition_speaker_manager_get_type(void);

SpeechRecognitionSpeakerManager * 
speech_recognition_speaker_manager_new(void);

SpeechStatus 
speech_recognition_speaker_manager_delete_speaker(
                               SpeechRecognitionSpeakerManager *manager,
                               SpeechRecognitionSpeakerProfile *speaker);

SpeechControlComponent * 
speech_recognition_speaker_manager_get_control_component(
                               SpeechRecognitionSpeakerManager *manager);

SpeechRecognitionSpeakerProfile * 
speech_recognition_speaker_manager_get_current_speaker(
                               SpeechRecognitionSpeakerManager *manager);

SpeechRecognitionSpeakerProfile ** 
speech_recognition_speaker_manager_list_known_speakers(
                               SpeechRecognitionSpeakerManager *manager);

SpeechStatus 
speech_recognition_speaker_manager_new_speaker_profile(
                               SpeechRecognitionSpeakerManager *manager,
                               SpeechRecognitionSpeakerProfile *profile,
                               SpeechRecognitionSpeakerProfile *result);

SpeechStatus 
speech_recognition_speaker_manager_read_vendor_speaker_profile(
                                    SpeechRecognitionSpeakerManager *manager,
                                    SpeechInputStream *in_stream,
                                    SpeechRecognitionSpeakerProfile *profile);

void 
speech_recognition_speaker_manager_revert_current_speaker(
                                    SpeechRecognitionSpeakerManager *manager);

void 
speech_recognition_speaker_manager_save_current_speaker_profile(
                                    SpeechRecognitionSpeakerManager *manager);

SpeechStatus 
speech_recognition_speaker_manager_set_current_speaker(
                                    SpeechRecognitionSpeakerManager *manager,
                                    SpeechRecognitionSpeakerProfile *speaker);

SpeechStatus 
speech_recognition_speaker_manager_write_vendor_speaker_profile(
                                    SpeechRecognitionSpeakerManager *manager,
                                    SpeechOutputStream *out_stream,
                                    SpeechRecognitionSpeakerProfile *speaker);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_SPEAKER_MANAGER_H__ */
