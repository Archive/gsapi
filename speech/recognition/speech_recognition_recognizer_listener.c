
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Defines an extension to the #SpeechEngineListener object for
 * specific events associated with a #SpeechRecognitionRecognizer.
 * A #SpeechRecognitionRecognizerListener object is attached to and 
 * removed from a #SpeechRecognitionRecognizer by calls to the 
 * speech_engine_add_engine_listener() and
 * and speech_engine_remove_engine_listener() functions (which 
 * #SpeechRecognitionRecognizer inherits from the #SpeechEngine object).
 *
 * See: #SpeechRecognitionRecognizerEvent
 * See: #SpeechRecognitionRecognizer
 * See: #SpeechEngine
 * See: speech_engine_add_engine_listener()
 * See: speech_engine_remove_engine_listener()
 */

#include <speech/recognition/speech_recognition_recognizer_listener.h>


GType
speech_recognition_recognizer_listener_get_type(void)
{
    printf("speech_recognition_recognizer_listener_get_type called.\n");
}


SpeechRecognitionRecognizerListener *
speech_recognition_recognizer_listener_new(void)
{
    printf("speech_recognition_recognizer_listener_new called.\n");
}


/**
 * speech_recognition_recognizer_listener_changes_committed:
 * @listener: the #SpeechRecognitionRecognizerListener object that this
 *            operation will be applied to.
 * @e the: recognizer event.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event has been 
 * issued as a #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state and resumed recognition.
 *
 * The #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event 
 * is issued to the #SpeechRecognitionGrammarListeners of all changed grammars
 * immediately following the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 */

void
speech_recognition_recognizer_listener_changes_committed(
                                SpeechRecognitionRecognizerListener *listener,
                                SpeechRecognitionRecognizerEvent *e)
{
    printf("speech_recognition_recognizer_listener_changes_committed called.\n");
}


/**
 * speech_recognition_recognizer_listener_focus_gained:
 * @listener: the #SpeechRecognitionRecognizerListener object that this
 *            operation will be applied to.
 * @e the: recognizer event.
 *
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED event has been issued 
 * as a #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_OFF state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_ON state. A
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED event typically 
 * follows a call to 
 * speech_recognition_recognizer_request_focus() on a 
 * #SpeechRecognitionRecognizer.
 *
 * The #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED event is issued 
 * to the #SpeechRecognitionGrammarListeners of all activated grammars
 * immediately following this #SpeechRecognitionRecognizerEvent.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_GAINED
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED
 * See: speech_recognition_recognizer_request_focus()
 */

void
speech_recognition_recognizer_listener_focus_gained(
                               SpeechRecognitionRecognizerListener *listener,
                               SpeechRecognitionRecognizerEvent *e)
{
    printf("speech_recognition_recognizer_listener_focus_gained called.\n");
}


/**
 * speech_recognition_recognizer_listener_focus_lost:
 * @listener: the #SpeechRecognitionRecognizerListener object that this
 *            operation will be applied to.
 * @e the: recognizer event.
 *
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event has been issued as a
 * #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_ON state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_OFF state. 
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST event may follow a 
 * call to speech_recognition_recognizer_release_focus() on a
 * #SpeechRecognitionRecognizer or follow a request for focus by another
 * application.
 *
 * The #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED event is 
 * issued to the #SpeechRecognitionGrammarListeners of all deactivated 
 * grammars immediately following this #SpeechRecognitionRecognizerEvent.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_FOCUS_LOST
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED
 * See: speech_recognition_recognizer_release_focus()
 */

void
speech_recognition_recognizer_listener_focus_lost(
                                SpeechRecognitionRecognizerListener *listener,
                                SpeechRecognitionRecognizerEvent *e)
{
    printf("speech_recognition_recognizer_listener_focus_lost called.\n");
}


/**
 * speech_recognition_recognizer_listener_recognizer_processing:
 * @listener: the #SpeechRecognitionRecognizerListener object that this
 *            operation will be applied to.
 * @e the: recognizer event.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING event has 
 * been issued as a #SpeechRecognitionRecognizer changes from the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state to the 
 * SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_PROCESSING
 */

void
speech_recognition_recognizer_listener_recognizer_processing(
                             SpeechRecognitionRecognizerListener *listener,
                             SpeechRecognitionRecognizerEvent *e)
{
    printf("speech_recognition_recognizer_listener_recognizer_processing called.\n");
}


/**
 * speech_recognition_recognizer_listener_recognizer_suspended:
 * @listener: the #SpeechRecognitionRecognizerListener object that this
 *            operation will be applied to.
 * @e the: recognizer event.
 *
 * A #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event has 
 * been issued as a #SpeechRecognitionRecognizer changes from either the
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state or 
 * the #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state to the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED state.
 *
 * A #SpeechRecognitionResult finalization event (either a
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event) is issued 
 * immediately following the 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED event.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_RECOGNIZER_SUSPENDED
 */

void
speech_recognition_recognizer_listener_recognizer_suspended(
                              SpeechRecognitionRecognizerListener *listener,
                              SpeechRecognitionRecognizerEvent *e)
{
    printf("speech_recognition_recognizer_listener_recognizer_suspended called.\n");
}
