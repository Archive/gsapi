
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The #SpeechRecognitionAudioClip object is a simple abstraction for playing
 * a sound clip. Multiple #SpeechRecognitionAudioClip items can be playing at
 * the same time, and the resulting sound is mixed together to produce a
 * composite.
 */

#include <speech/recognition/speech_recognition_audio_clip.h>


GType
speech_recognition_audio_clip_get_type(void)
{
    printf("speech_recognition_audio_clip_get_type called.\n");
}


SpeechRecognitionAudioClip *
speech_recognition_audio_clip_new(void)
{
    printf("speech_recognition_audio_clip_new called.\n");
}


/**
 * speech_recognition_audio_clip_loop:
 * @clip: the #SpeechRecognitionAudioClip object that this operation will be
 *        applied to.
 *
 * Starts playing this audio clip in a loop.
 */

void
speech_recognition_audio_clip_loop(SpeechRecognitionAudioClip *clip)
{
    printf("speech_recognition_audio_clip_loop called.\n");
}


/**
 * speech_recognition_audio_clip_play:
 * @clip: the #SpeechRecognitionAudioClip object that this operation will be
 *        applied to.
 *
 * Starts playing this audio clip. Each time this function is called,
 * the clip is restarted from the beginning.
 */

void
speech_recognition_audio_clip_play(SpeechRecognitionAudioClip *clip)
{
    printf("speech_recognition_audio_clip_play called.\n");
}


/**
 * speech_recognition_audio_clip_stop:
 * @clip: the #SpeechRecognitionAudioClip object that this operation will be
 *        applied to.
 *
 * Stops playing this audio clip.
 */

void
speech_recognition_audio_clip_stop(SpeechRecognitionAudioClip *clip)
{
    printf("speech_recognition_audio_clip_stop called.\n");
}
