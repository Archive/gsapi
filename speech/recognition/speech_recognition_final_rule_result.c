
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Provides information on a finalized result for an utterance that matches
 * a #SpeechRecognitionRuleGrammar. A finalized result is one that is in either
 * the #SPEECH_RECOGNITION_RESULT_ACCEPTED or 
 * #SPEECH_RECOGNITION_RESULT_REJECTED state (tested by the
 * speech_recognition_result_get_result_state() function of the 
 * #SpeechRecognitionResult object).
 *
 * This object provides the following information for a finalized result
 * for a #SpeechRecognitionRuleGrammar (in addition to the information 
 * provided through the #SpeechRecognitionResult and 
 * #SpeechRecognitionFinalResult object):
 *
 * <itemizedlist>
 *   <listitem>N-best alternatives for the entire result. For each 
 *       alternative guess the object provides the token sequence 
 *       and the names of the grammar and rule matched by the tokens.
 *       </listitem>
 *   <listitem>Tags for the best-guess result.</listitem>
 * </itemizedlist>
 *
 * Every #SpeechRecognitionResult object provided by a 
 * #SpeechRecognitionRecognizer implements the 
 * #SpeechRecognitionFinalRuleResult and the
 * #SpeechRecognitionFinalDictationResult object (and by inheritence the
 * #SpeechRecognitionFinalResult and #SpeechRecognitionResult objects).
 * However, the functions of #SpeechRecognitionFinalRuleResult should only 
 * be called if the speech_recognition_result_get_grammar() function 
 * returns a #SpeechRecognitionRuleGrammar and once the 
 * #SpeechRecognitionResult has been finalized with either a 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event. Inappropriate 
 * calls will cause a SPEECH_RECOGNITION_RESULT_STATE_ERROR.
 *
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionResult
 * See: #SpeechRecognitionFinalResult
 * See: #SpeechRecognitionFinalDictationResult
 * See: speech_recognition_result_get_result_state()
 * See: speech_recognition_result_get_grammar()
 */

#include <speech/recognition/speech_recognition_final_rule_result.h>


GType
speech_recognition_final_rule_result_get_type(void)
{
    printf("speech_recognition_final_rule_result_get_type called.\n");
}


SpeechRecognitionFinalRuleResult *
speech_recognition_final_rule_result_new(void)
{
    printf("speech_recognition_final_rule_result_new called.\n");
}


/**
 * speech_recognition_final_rule_result_get_alternative_tokens:
 * @result: the #SpeechRecognitionFinalRuleResult object that this operation
 *          will be applied to.
 * @n_best: the nBest value.
 * tokens: return the N-best token sequence for this result.
 *
 * Get the N-best token sequence for this result. The @n_best value 
 * should be in the range of 0 to (getNumberGuesses()-1) inclusive. 
 * For out-of-range values, the function returns %NULL.
 *
 * If @n_best==0, the function returns the best-guess sequence of
 * tokens - identical to the token sequence returned by the
 * speech_recognition_result_get_best_tokens() function of the 
 * #SpeechRecognitionResult object for the same object.
 *
 * If @n_best==1 (or 2, 3...) the function returns the 1st (2nd, 3rd...)
 * alternative to the best guess.
 *
 * The number of tokens for the best guess and the alternatives do
 * not need to be the same.
 *
 * The speech_recognition_recognizer_get_rule_grammar() and 
 * speech_recognition_final_rule_result_get_rule_name()
 * functions indicate which #SpeechRecognitionRuleGrammar and
 * #ruleName are matched by each alternative result sequence.
 *
 * If the #SpeechRecognitionResult is in the 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED state (not rejected), then 
 * the best guess and all the alternatives are accepted. Moreover, 
 * each alternative set of tokens must be a legal match of the 
 * #SpeechRecognitionRuleGrammar and #SpeechRecognitionRuleName 
 * returned by the speech_recognition_final_rule_result_get_rule_grammar()
 * and speech_recognition_final_rule_result_get_rule_name() functions.
 *
 * If the #SpeechRecognitionResult is in the 
 * #SPEECH_RECOGNITION_RESULT_REJECTED state (not accepted), the recognizer 
 * is not confident that the best guess or any of the alternatives are what 
 * the user said. Rejected guesses do not need to match the corresponding
 * #SpeechRecognitionRuleGrammar and rule name.
 *
 * See: speech_recognition_final_rule_result_get_rule_grammar()
 * See: speech_recognition_final_rule_result_get_rule_name()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before a 
 *       result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_rule_result_get_alternative_tokens(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int n_best,
                                     SpeechRecognitionResultToken *tokens[])
{
    printf("speech_recognition_final_rule_result_get_alternative_tokens called.\n");
}


/**
 * speech_recognition_final_rule_result_get_number_guesses:
 * @result: the #SpeechRecognitionFinalRuleResult object that this operation
 *          will be applied to.
 * @n: return the number of guesses for this result.
 *
 * Return the number of guesses for this result.  The guesses are
 * numbered from 0 up. The 0th guess is the best guess for the result
 * and provides the same tokens as the 
 * speech_recognition_result_get_best_tokens() function of the 
 * #SpeechRecognitionResult object.
 *
 * If only the best guess is available (no alternatives) the return
 * value is 1. If the result is was rejected 
 * (#SPEECH_RECOGNITION_RESULT_REJECTED state), the return value may be 
 * 0 if no tokens are available. If a best guess plus alternatives are 
 * available, the return value is greater than 1.
 *
 * The integer parameter to the 
 * speech_recognition_final_rule_result_get_alternative_tokens(),
 * speech_recognition_final_rule_result_get_rule_grammar() and
 * speech_recognition_final_rule_result_get_rule_name() functions
 * are indexed from 0 to 
 * (speech_recognition_final_rule_result_get_number_guesses()-1).
 *
 * See: speech_recognition_result_get_best_tokens()
 * See: #SPEECH_RECOGNITION_RESULT_REJECTED
 * See: speech_recognition_final_rule_result_get_alternative_tokens()
 * See: speech_recognition_final_rule_result_get_rule_grammar()
 * See: speech_recognition_final_rule_result_get_rule_name()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before a 
 *       result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_rule_result_get_number_guesses(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int *n)
{
    printf("speech_recognition_final_rule_result_get_number_guesses called.\n");
}


/**
 * speech_recognition_final_rule_result_get_rule_grammar:
 * @result: the #SpeechRecognitionFinalRuleResult object that this operation
 *          will be applied to.
 * @n_best: the nBest value.
 * @grammar: return the #SpeechRecognitionRuleGrammar matched by the nth guess.
 *
 * Return the #SpeechRecognitionRuleGrammar matched by the nth guess.
 * Return %NULL if @n_best is out-of-range.
 *
 * speech_recognition_final_rule_result_get_rule_name() returns the rule 
 * matched in the #SpeechRecognitionRuleGrammar. See the documentation for
 * speech_recognition_final_rule_result_get_alternative_tokens() for a 
 * description of how tokens, grammars and rules correspond.
 *
 * An application can use the parse function of the matched grammar
 * to analyse a result.
 *
 * See: speech_recognition_rule_grammar_parse()
 * See: speech_recognition_final_rule_result_get_number_guesses()
 * See: speech_recognition_final_rule_result_get_alternative_tokens()
 * See: speech_recognition_final_rule_result_get_rule_name()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before 
 *       a result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_rule_result_get_rule_grammar(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int n_best,
                                     SpeechRecognitionRuleGrammar *grammar)
{
    printf("speech_recognition_final_rule_result_get_rule_grammar called.\n");
}


/**
 * speech_recognition_final_rule_result_get_rule_name:
 * @result: the #SpeechRecognitionFinalRuleResult object that this operation
 *          will be applied to.
 * @n_best: the nBest value.
 * @name: return the #SpeechRecognitionRuleName matched by the nth guess.
 *
 * Return the #SpeechRecognitionRuleName matched by the nth guess.
 * Return %NULL if @n_best is out-of-range. Typically used in combination 
 * with speech_recognition_final_rule_result_get_alternative_tokens() and
 * speech_recognition_final_rule_result_get_rule_grammar() which return 
 * the corresponding tokens and grammar.
 *
 * See: speech_recognition_rule_grammar_parse()
 * See: speech_recognition_final_rule_result_get_alternative_tokens()
 * See: speech_recognition_final_rule_result_get_rule_grammar()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before a 
 *       result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_rule_result_get_rule_name(
                                     SpeechRecognitionFinalRuleResult *result,
                                     int n_best,
                                     gchar *name)
{
    printf("speech_recognition_final_rule_result_get_rule_name called.\n");
}


/**
 * speech_recognition_final_rule_result_get_tags:
 * @result: the #SpeechRecognitionFinalRuleResult object that this operation
 *          will be applied to.
 * @tags: return the list of tags matched by the best-guess token sequence.
 *
 * Return the list of tags matched by the best-guess token sequence.
 * The tags in the array are ordered strictly in the left to right
 * order in which they would appear in the supported grammat format.
 *
 * For example, if the following simple Java Speech Grammar Format
 * (JSGF) rule is active:
 *
 *   public &lt;cmd&gt; = (open {ACT_OPEN} |
 *                   close {ACT_CLOSE}) [(it{WHAT} now) {NOW}];
 *
 * and the user says "close it now", then 
 * speech_recognition_final_rule_result_get_tags() returns an array
 * containing {"ACT_CLOSE", "WHAT", "NOW"}. Note how both the {WHAT}
 * and {NOW} tags are attached starting from the word "it" but that
 * {TAG} appears first in the array. In effect, when tags start at the
 * same token, they are listed "bottom-up".
 *
 * speech_recognition_final_rule_result_get_tags() does not indicate which 
 * tokens are matched by which tags. To obtain this information use the
 * speech_recognition_rule_grammar_parse() function of the 
 * #SpeechRecognitionRuleGrammar.
 * Also, speech_recognition_final_rule_result_get_tags() only provides 
 * tags for the best guess. To get tags for the alternative guesses using 
 * parsing through the #SpeechRecognitionRuleGrammar.
 *
 * The string array returned by the 
 * speech_recognition_final_rule_result_get_tags() function of the 
 * #SpeechRecognitionRuleParse object returned by parsing the
 * best-guess token sequence will be the same as returned by this function.
 *
 * See: speech_recognition_rule_grammar_parse()
 * See: speech_recognition_rule_parse_get_tags()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before 
 *       a result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_rule_result_get_tags(
                                     SpeechRecognitionFinalRuleResult *result,
                                     gchar *tags[])
{
    printf("speech_recognition_final_rule_result_get_tags called.\n");
}
