
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RESULT_TOKEN_H__
#define __SPEECH_RECOGNITION_RESULT_TOKEN_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_result.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RESULT_TOKEN              (speech_recognition_result_token_get_type())
#define SPEECH_RECOGNITION_RESULT_TOKEN(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RESULT_TOKEN, SpeechRecognitionResultToken))
#define SPEECH_RECOGNITION_RESULT_TOKEN_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RESULT_TOKEN, SpeechRecognitionResultTokenClass))
#define SPEECH_IS_RECOGNITION_RESULT_TOKEN(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RESULT_TOKEN))
#define SPEECH_IS_RECOGNITION_RESULT_TOKEN_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RESULT_TOKEN))
#define SPEECH_RECOGNITION_RESULT_TOKEN_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RESULT_TOKEN, SpeechRecognitionResultTokenClass))

typedef struct _SpeechRecognitionResultTokenClass       SpeechRecognitionResultTokenClass;

struct _SpeechRecognitionResultToken {
    SpeechObject object;
};

struct _SpeechRecognitionResultTokenClass {
    SpeechObjectClass parent_class;
};


/* ********************* SPECIAL TOKENS **************************** */

/*
 * Special token representing the written form of the "New Paragraph"
 * directive. Equal to "\u005Cu2029". The spoken form of a
 * "New Paragraph" directive may vary between recognizers.
 */

static gchar *SPEECH_RECOGNITION_RESULT_TOKEN_NEW_PARAGRAPH = "?";

/*
 * Special token representing the written form of the "New Line"
 * directive. Equal to "\\n". The spoken form of a "New Line"
 * directive may vary between recognizers.
 */

static gchar *SPEECH_RECOGNITION_RESULT_TOKEN_NEW_LINE = "\\n";

/* ********************* DICTATION HINTS **************************** */

/*
 * A #SpacingHint returned when a token should be presented separately 
 * from surrounding tokens (preceding and following spaces). Returned 
 * when #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS,
 * #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_FOLLOWING and 
 * ##SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP are all false. 
 * (See the description above.)
 *
 * #SPEECH_RECOGNITION_RESULT_TOKEN_SEPARATE is the default spacing hint value.
 *
 * Example:
 *
 *	if (speech_recognition_result_token_get_spacing_hint(resultToken) ==
                                                        ResultToken.SEPARATE)
 *	 ...;
 *
 * See: speech_recognition_result_token_get_spacing_hint()
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_SEPARATE = 0;

/*
 * A #SpacingHint flag set true when a token should be attached to the 
 * preceding token. (See the description above.)
 *
 * Example:
 *
 *   if (speech_recognition_result_token_get_spacing_hint(resultToken) & 
 *         SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS != 0)
 *	 ...;
 *
 * See: speech_recognition_result_token_get_spacing_hint()
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS = 1;

/*
 * A #SpacingHint flag set true when a token should be attached to the 
 * following token. (See the description above).
 *
 * Example:
 *
 *    if (speech_recognition_result_token_get_spacing_hint(resultToken) & 
 *          SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_FOLLOWING) != 0)
 *	 ...;
 *
 * See: speech_recognition_result_token_get_spacing_hint()
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_FOLLOWING = 2;

/*
 * A #SpacingHint flag set true when a token should be attached to 
 * preceding and/or following tokens which also have the 
 * #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP flag set true. 
 * (See the description above).
 *
 * Example:
 *
 *   if ((speech_recognition_result_token_get_spacing_hint(thisToken) & 
 *         SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP) != 0)
 *	 && (speech_recognition_result_token_get_spacing_hint(prevToken) & 
 *         SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP) != 0))
 *	 ...;
 *
 * See: speech_recognition_result_token_get_spacing_hint()
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP = 4;


/*
 * A #CapitalizationHint indicating that the following word should be 
 * presented without changes in capitalization. This is the default value.
 * (See the description above).
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_CAP_AS_IS = 10;

/*
 * A #CapitalizationHint indicating that the following word should be 
 * presented with the first character in uppercase. 
 * (See the description above).
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_CAP_FIRST = 11;

/*
 * A #CapitalizationHint indicating that the following word should be 
 * presented in uppercase.
 * (See the description above).
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_UPPERCASE = 12;

/*
 * A #CapitalizationHint indicating that the following word should be 
 * presented in lowercase.
 * (See the description above).
 */

static int SPEECH_RECOGNITION_RESULT_TOKEN_LOWERCASE = 13;


GType 
speech_recognition_result_token_get_type(void);

SpeechRecognitionResultToken * 
speech_recognition_result_token_new(void);

int 
speech_recognition_result_token_get_capitalization_hint(
                                       SpeechRecognitionResultToken *token);

SpeechRecognitionResult * 
speech_recognition_result_token_get_result(
                                       SpeechRecognitionResultToken *token);

int 
speech_recognition_result_token_get_spacing_hint(
                                       SpeechRecognitionResultToken *token);

gchar * 
speech_recognition_result_token_get_spoken_text(
                                       SpeechRecognitionResultToken *token);

long 
speech_recognition_result_token_get_start_time(
                                       SpeechRecognitionResultToken *token);

long 
speech_recognition_result_token_get_end_time(
                                       SpeechRecognitionResultToken *token);

gchar * 
speech_recognition_result_token_get_written_text(
                                       SpeechRecognitionResultToken *token);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RESULT_TOKEN_H__ */
