
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RECOGNIZER_MODE_DESC_H__
#define __SPEECH_RECOGNITION_RECOGNIZER_MODE_DESC_H__

#include <speech/speech_engine_mode_desc.h>
#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_speaker_profile.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RECOGNIZER_MODE_DESC              (speech_recognition_recognizer_mode_desc_get_type())
#define SPEECH_RECOGNITION_RECOGNIZER_MODE_DESC(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_MODE_DESC, SpeechRecognitionRecognizerModeDesc))
#define SPEECH_RECOGNITION_RECOGNIZER_MODE_DESC_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_MODE_DESC, SpeechRecognitionRecognizerModeDescClass))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_MODE_DESC(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_MODE_DESC))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_MODE_DESC_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_MODE_DESC))
#define SPEECH_RECOGNITION_RECOGNIZER_MODE_DESC_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_MODE_DESC, SpeechRecognitionRecognizerModeDescClass))

typedef struct _SpeechRecognitionRecognizerModeDesc            SpeechRecognitionRecognizerModeDesc;
typedef struct _SpeechRecognitionRecognizerModeDescClass       SpeechRecognitionRecognizerModeDescClass;

struct _SpeechRecognitionRecognizerModeDesc {
    SpeechEngineModeDesc mode_desc;
};

struct _SpeechRecognitionRecognizerModeDescClass {
    SpeechEngineModeDescClass parent_class;
};


GType 
speech_recognition_recognizer_mode_desc_get_type(void);

SpeechRecognitionRecognizerModeDesc * 
speech_recognition_recognizer_mode_desc_new(void);

SpeechRecognitionRecognizerModeDesc * 
speech_recognition_recognizer_mode_desc_new_with_locale(gchar *locale,
                                 SpeechTristate dictation_grammar_supported);

SpeechRecognitionRecognizerModeDesc * 
speech_recognition_recognizer_mode_desc_new_with_state(
                                 gchar *engine_name,
                                 gchar *mode_name,
                                 gchar *locale,
                                 SpeechTristate running,
                                 SpeechTristate dictation_grammar_supported,
                                 SpeechRecognitionSpeakerProfile *profiles[]);

void 
speech_recognition_recognizer_mode_desc_add_speaker_profile(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechRecognitionSpeakerProfile *profile);

SpeechStatus 
speech_recognition_recognizer_mode_desc_get_speaker_profiles(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechRecognitionSpeakerProfile *profiles[]);

SpeechRecognitionSpeakerProfile ** 
speech_recognition_recognizer_mode_desc_get_speaker_profiles_impl(
                             SpeechRecognitionRecognizerModeDesc *mode_desc);

SpeechTristate 
speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported(
                             SpeechRecognitionRecognizerModeDesc *mode_desc);

void 
speech_recognition_recognizer_mode_desc_set_dictation_grammar_supported(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechTristate dictation_grammar_supported);

void 
speech_recognition_recognizer_mode_desc_set_speaker_profiles(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechRecognitionSpeakerProfile *speakers[]);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RECOGNIZER_MODE_DESC_H__ */
