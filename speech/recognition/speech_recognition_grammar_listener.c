
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionGrammarListener receives notifications of status change
 * events for a #SpeechRecognitionGrammar.
 *
 * A #SpeechRecognitionGrammarListener is attached to and removed from a
 * #SpeechRecognitionGrammar with its 
 * speech_recognition_grammar_add_grammar_listener() and
 * speech_recognition_grammar_remove_grammar_listener() functions.
 * Multiple grammars can share a #SpeechRecognitionGrammarListener object 
 * and one grammar can have multiple #SpeechRecognitionGrammarListener 
 * objects attached.
 *
 * See: #SpeechRecognitionGrammar
 * See: #SpeechRecognitionDictationGrammar
 * See: #SpeechRecognitionRuleGrammar
 * See: speech_recognition_grammar_add_grammar_listener()
 * See: speech_recognition_grammar_remove_grammar_listener()
 */

#include <speech/recognition/speech_recognition_grammar_listener.h>


GType
speech_recognition_grammar_listener_get_type(void)
{
    printf("speech_recognition_grammar_listener_get_type called.\n");
}


SpeechRecognitionGrammarListener *
speech_recognition_grammar_listener_new(void)
{
    printf("speech_recognition_grammar_listener_new called.\n");
}


/**
 * speech_recognition_grammar_listener_grammar_activated:
 * @listener: the #SpeechRecognitionGrammarListener object that this operation
 *            will be applied to.
 * @e: the grammar event.
 *
 * A #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED event is issued 
 * when a grammar changes from deactivated to activated.
 *
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_ACTIVATED
 * See: #SpeechRecognitionGrammar
 */

void
speech_recognition_grammar_listener_grammar_activated(
                               SpeechRecognitionGrammarListener *listener,
                               SpeechRecognitionGrammarEvent *e)
{
    printf("speech_recognition_grammar_listener_grammar_activated called.\n");
}


/**
 * speech_recognition_grammar_listener_grammar_changes_committed:
 * @listener: the #SpeechRecognitionGrammarListener object that this operation
 *            will be applied to.
 * @e: the grammar event.
 *
 * A #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 * event is issued when a #SpeechRecognitionRecognizer has committed changes
 * to a #SpeechRecognitionGrammar.
 *
 * The #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 * immediately follows the 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_CHANGES_COMMITTED event issued to
 * #SpeechRecognitionRecognizerListeners after changes to all grammars
 * have been committed. The circumstances in which changes are committed
 * are described in the documentation for #SpeechRecognitionGrammar.
 *
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 * See: #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED
 * See: #SpeechRecognitionGrammar
 */

void
speech_recognition_grammar_listener_grammar_changes_committed(
                               SpeechRecognitionGrammarListener *listener,
                               SpeechRecognitionGrammarEvent *e)
{
    printf("speech_recognition_grammar_listener_grammar_changes_committed called.\n");
}


/**
 * speech_recognition_grammar_listener_grammar_deactivated:
 * @listener: the #SpeechRecognitionGrammarListener object that this operation
 *            will be applied to.
 * @e: the grammar event.
 *
 * A #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED event is issued 
 * when a grammar changes from activated to deactivated.
 *
 * See: #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_DEACTIVATED
 * See: #SpeechRecognitionGrammar
 */

void
speech_recognition_grammar_listener_grammar_deactivated(
                               SpeechRecognitionGrammarListener *listener,
                               SpeechRecognitionGrammarEvent *e)
{
    printf("speech_recognition_grammar_listener_grammar_deactivated called.\n");
}
