
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Event issued by #SpeechRecognitionRecognizer through
 * #SpeechRecognitionRecognizerListener.
 * Inherits the following event types from #SpeechEngineEvent:
 *  #SPEECH_ENGINE_ALLOCATED,
 *  #SPEECH_ENGINE_DEALLOCATED,
 *  #SPEECH_ENGINE_ALLOCATING_RESOURCES,
 *  #SPEECH_ENGINE_DEALLOCATING_RESOURCES,
 *  #SPEECH_ENGINE_PAUSED,
 *  #SPEECH_ENGINE_RESUMED.
 *
 * The source object for any #SpeechRecognitionRecognizerEvent is the
 * #SpeechRecognitionRecognizer.
 *
 * See: #SpeechEngineEvent
 * See: #SpeechRecognitionRecognizer
 * See: #SpeechRecognitionRecognizerListener
 */

#include <speech/recognition/speech_recognition_recognizer_event.h>


GType
speech_recognition_recognizer_event_get_type(void)
{
    printf("speech_recognition_recognizer_event_get_type called.\n");
}


/**
 * speech_recognition_recognizer_event_new:
 * @source: the recognizer that issued the event.
 * @id: the identifier for the event type.
 * @old_engine_state: engine state prior to this event.
 * @new_engine_state: engine state following this event.
 * @grammar_exception: non-null if an error is detected during
 *                     #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED.
 *
 * Constructs a #SpeechRecognitionRecognizerEvent with a specified event
 * source, event identifier, old and new states, and optionally a
 * #SpeechRecognitionGrammarException for a
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event.
 *
 * Returns:
 */

SpeechRecognitionRecognizerEvent *
speech_recognition_recognizer_event_new(SpeechEngine *source,
                        int id,
                        long old_engine_state,
                        long new_engine_state,
                        SpeechRecognitionGrammarException *grammar_exception)
{
    printf("speech_recognition_recognizer_event_new called.\n");
}


/**
 * speech_recognition_recognizer_event_get_grammar_exception:
 * @event: the #SpeechRecognitionRecognizerEvent object that this operation
 *         will be applied to.
 *
 * Returns non-null for a 
 * #SPEECH_RECOGNITION_RECOGNIZER_EVENT_CHANGES_COMMITTED event if
 * an error is found in the grammar definition. The exception
 * serves the same functional role as the 
 * #SPEECH_RECOGNITION_GRAMMAR_EXCEPTION returned with the 
 * speech_recognition_recognizer_commit_changes() function.
 *
 * See: speech_recognition_recognizer_commit_changes()
 *
 * Returns: the grammar exception value.
 */

SpeechRecognitionGrammarException *
speech_recognition_recognizer_event_get_grammar_exception(
                                SpeechRecognitionRecognizerEvent *event)
{
    printf("speech_recognition_recognizer_event_get_grammar_exception called.\n");
}
