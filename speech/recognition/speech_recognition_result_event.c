
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionResultEvent is issued by a #SpeechRecognitionResult 
 * object to indicate changes in the recognized tokens and changes in state.
 * An event is issued to all appropriate #SpeechRecognitionResultListener
 * objects. #SpeechRecognitionResultListeners can be attached in three places:
 *
 * <itemizedlist>
 *   <listitem>A #SpeechRecognitionResultListener attached to the
 *	 #SpeechRecognitionRecognizer receives all events for all results
 *	 produced by the recognizer.</listitem>
 *   <listitem>A #SpeechRecognitionResultListener attached to a 
 *       #SpeechRecognitionGrammar receives all events for all results that 
 *       have been finalized for that #SpeechRecognitionGrammar: the 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event and 
 *       following events. (Note: it never receives a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event because that 
 *       event always precedes 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED).</listitem>
 *   <listitem>A #SpeechRecognitionResultListener attached to a 
 *       #SpeechRecognitionResult receives all events for that
 *	 result starting from the time at which the listener is attached.
 *	 (Note: it never receives a 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event because a 
 *       listener can only be attached to a #SpeechRecognitionResult
 *	 once a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event has 
 *       already been issued.)</listitem>
 * </itemizedlist>
 *
 * In all three forms of listener attachment, one listener can be attached to
 * multiple objects, and multiple listeners can be attached to a single object.
 *
 * The three states of a recognition result are described in the documentation
 * for the #SpeechRecognitionResult object and can be tested by the
 * speech_recognition_result_get_result_state() function of that object. The
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED, 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED and
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED events indicate result 
 * state changes.
 *
 * In brief, the three states are:
 *
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_UNFINALIZED state: initial state 
 *       of result indicating that recognition is still taking place. A new 
 *       result is created in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 *       state with a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 *	 event.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_ACCEPTED state: the result 
 *       is finalized and the recognizer is confident it has recognized the 
 *       result correctly. The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED
 *       event indicates a state change from the 
 *       #SPEECH_RECOGNITION_RESULT_UNFINALIZED state to the 
 *       #SPEECH_RECOGNITION_RESULT_ACCEPTED state.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_REJECTED state: the result is 
 *       finalized but the recognizer is not confident it has recognized 
 *       the result correctly.  The 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event indicates 
 *       a state change from the #SPEECH_RECOGNITION_RESULT_UNFINALIZED
 *       state to the #SPEECH_RECOGNITION_RESULT_REJECTED state.</listitem>
 * </itemizedlist>
 *
 * The sequence of #SpeechRecognitionResultEvents associated with a 
 * recognition result are described in the documentation for the 
 * #SpeechRecognitionResult object.
 *
 * In brief, the events that occur depend upon the #SpeechRecognitionResult 
 * state:
 *
 * <itemizedlist>
 *   <listitem>A #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED event 
 *       creates a #SpeechRecognitionResult object. A new result is started 
 *       in the #SPEECH_RECOGNITION_RESULT_UNFINALIZED state.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_UNFINALIZED state: 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED events
 *	 indicate a change in finalized and/or unfinalized tokens; a
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED event indicates 
 *       that the #SpeechRecognitionGrammar matched by this result has been 
 *       identified.</listitem>
 *   <listitem>The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED event 
 *       finalizes a result by indicating a change in state from 
 *       #SPEECH_RECOGNITION_RESULT_UNFINALIZED to
 *	 #SPEECH_RECOGNITION_RESULT_ACCEPTED.</listitem>
 *   <listitem>The #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event 
 *       finalizes a result by indicating a change in state from 
 *       #SPEECH_RECOGNITION_RESULT_UNFINALIZED to
 *	 #SPEECH_RECOGNITION_RESULT_REJECTED.</listitem>
 *   <listitem>In the finalized states - #SPEECH_RECOGNITION_RESULT_ACCEPTED
 *       and #SPEECH_RECOGNITION_RESULT_REJECTED - the 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED and 
 *       #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED may be 
 *       issued.</listitem>
 * </itemizedlist>
 *
 * See: Recognizer
 * See: Result
 * See: ResultListener
 * See: FinalResult
 * See: FinalRuleResult
 * See: FinalDictationResult
 */

#include <speech/recognition/speech_recognition_result_event.h>


GType
speech_recognition_result_event_get_type(void)
{
    printf("speech_recognition_result_event_get_type called.\n");
}


/**
 * speech_recognition_result_event_new:
 * @source: the object that issued the event.
 * @id: the identifier for the event type.
 *
 * Initializes a #SpeechRecognitionResultEvent with an event type
 * identifier.	The is_token_finalized and is_unfinalized_tokens_changed 
 * flags are set to %FALSE.
 */

SpeechRecognitionResultEvent *
speech_recognition_result_event_new(SpeechRecognitionResult *source, int id)
{
    printf("speech_recognition_result_event_new called.\n");
}


/**
 * speech_recognition_result_event_new_with_state:
 * @source: the #SpeechRecognitionResult object that issued the event.
 * @id: the identifier for the event type.
 * @is_token_finalized: %TRUE if any token is finalized with this event.
 * @is_unfinalized_tokens_changed: %TRUE if the unfinalized text is changed 
 *                                 with this event.
 *
 * Initializes a #SpeechRecognitionResultEvent for a specified source
 * #SpeechRecognitionResult and result event id. The two boolean flags
 * indicating change in tokens should be set appropriately for
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED, 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED,
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED and 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED events.
 * (For other event types these flags should be %FALSE).
 *
 * Returns:
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_GRAMMAR_FINALIZED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED
 */

SpeechRecognitionResultEvent *
speech_recognition_result_event_new_with_state(
                                     SpeechRecognitionResult *source,
                                     int id,
                                     gboolean is_token_finalized,
                                     gboolean is_unfinalized_tokens_changed)
{
    printf("speech_recognition_result_event_new_with_state called.\n");
}


/**
 * speech_recognition_result_event_is_token_finalized:
 * @event: the #SpeechRecognitionResultEvent object that this operaion will
 *         be applied to.
 *
 * For #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED, 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED,
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED and 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED events returns %TRUE 
 * if any tokens were finalized. For other events, return %FALSE. If %TRUE,
 * the number of tokens returned by speech_recognition_result_num_tokens()
 * and speech_recognition_result_get_best_tokens() has increased.
 *
 * See: speech_recognition_result_num_tokens()
 * See: speech_recognition_result_get_best_tokens()
 *
 * Returns: an indication of whether this token is finalized.
 */

gboolean
speech_recognition_result_event_is_token_finalized(
                                           SpeechRecognitionResultEvent *event)
{
    printf("speech_recognition_result_event_is_token_finalized called.\n");
}


/**
 * speech_recognition_result_event_is_unfinalized_tokens_changed:
 * @event: the #SpeechRecognitionResultEvent object that this operaion will
 *         be applied to.
 *
 * For #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_CREATED, 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_UPDATED,
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED and 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED events returns %TRUE 
 * if the unfinalized tokens changed. For other events, return %FALSE. 
 * If %TRUE, the value returned by 
 * speech_recognition_result_get_unfinalized_tokens() has changed.
 *
 * Note that both #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED and
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED events implicitly 
 * set the unfinalized text to %NULL. The
 * speech_recognition_result_event_is_unfinalized_tokens_changed() function
 * should return %TRUE only if the unfinalized text was non-null prior to
 * finalization.
 *
 * See: speech_recognition_result_event_is_unfinalized_tokens_changed()
 *
 * Returns: an indication on whether the unfinalized tokens changed.
 */

gboolean
speech_recognition_result_event_is_unfinalized_tokens_changed(
                                 SpeechRecognitionResultEvent *event)
{
    printf("speech_recognition_result_event_is_unfinalized_tokens_changed called.\n");
}
