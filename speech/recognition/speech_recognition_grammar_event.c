
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionGrammarEvent is issued to each 
 * #SpeechRecognitionGrammarListener attached to a 
 * #SpeechRecognitionGrammar when major events associated 
 * with that #SpeechRecognitionGrammar occur.
 *
 * The source for a #SpeechRecognitionGrammarEvent is 
 * always a #SpeechRecognitionGrammar object.
 *
 * See: #SpeechRecognitionGrammar
 * See: #SpeechRecognitionGrammarListener
 */

#include <speech/recognition/speech_recognition_grammar_event.h>


GType
speech_audio_event_get_type(void)
{
    printf("speech_audio_event_get_type called.\n");
}


/**
 * speech_recognition_grammar_event_new:
 * @source: the object that issued the event.
 * @id: the identifier for the event type.
 *
 * Initializes a #SpeechRecognitionGrammarEvent event with a specified
 * event identifier. The #enabled_changed and #definition_changed fields 
 * are set to false.  The #grammar_exception field is set to %NULL.
 *
 * Returns:
 */

SpeechRecognitionGrammarEvent *
speech_recognition_grammar_event_new(SpeechRecognitionGrammar *source, int id)
{
    printf("speech_recognition_grammar_event_new called.\n");
}


/**
 * speech_recognition_grammar_event_new_with_state:
 * @source: the object that issued the event.
 * @id: the identifier for the event type.
 * @enabled_changed: %TRUE if the grammar's #enabled property changed.
 * @definition_changed: true if the grammar's definition has changed.
 * @grammar_exception: non-null if an error is detected in a grammar's 
 *                     definition.
 *
 * Initializes a #SpeechRecognitionGrammarEvent event with a specified
 * event identifier plus state change and exception values. For a
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event, the
 * @enabled_changed and @definition_changed parameters should indicate 
 * what properties of the #SpeechRecognitionGrammar has changed, otherwise 
 * they should be false. For a 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED, the
 * @grammar_exception parameter should be non-null only if an error is 
 * encountered in the grammar definition.
 *
 * Returns:
 */

SpeechRecognitionGrammarEvent *
speech_recognition_grammar_event_new_with_state(
                         SpeechRecognitionGrammar *source,
                         int id,
                         gboolean enabled_changed,
                         gboolean definition_changed,
                         SpeechRecognitionGrammarException *grammar_exception)
{
    printf("speech_recognition_grammar_event_new_with_state called.\n");
}


/**
 * speech_recognition_grammar_event_get_definition_changed:
 * @event: the #SpeechRecognitionGrammarEvent object that this operation will
 *         be applied to.
 *
 * Returns %TRUE for a 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event
 * if the definition of the source #SpeechRecognitionGrammar has changed.
 *
 * Returns: %TRUE for a 
 *          #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event
 *	    if the definition of the source #SpeechRecognitionGrammar has
 *	    changed.
 */

gboolean
speech_recognition_grammar_event_get_definition_changed(
                                        SpeechRecognitionGrammarEvent *event)
{
    printf("speech_recognition_grammar_event_get_definition_changed called.\n");
}


/**
 * speech_recognition_grammar_event_get_enabled_changed:
 * @event: the #SpeechRecognitionGrammarEvent object that this operation will
 *         be applied to.
 *
 * Returns %TRUE for a 
 * #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event if 
 * the #enabled property of the #SpeechRecognitionGrammar changed.
 *
 * Returns: %TRUE for a 
 *          #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED event
 *	    if the #enabled property of the #SpeechRecognitionGrammar changed.
 */

gboolean
speech_recognition_grammar_event_get_enabled_changed(
                                        SpeechRecognitionGrammarEvent *event)
{
    printf("speech_recognition_grammar_event_get_enabled_changed called.\n");
}


/**
 * speech_recognition_grammar_event_get_grammar_exception:
 * @event: the #SpeechRecognitionGrammarEvent object that this operation will
 *         be applied to.
 *
 * Returns: non-null for a 
 *          #SPEECH_RECOGNITION_GRAMMAR_EVENT_GRAMMAR_CHANGES_COMMITTED
 *	    event if an error is found in the grammar definition.
 */

SpeechRecognitionGrammarException *
speech_recognition_grammar_event_get_grammar_exception(
                                        SpeechRecognitionGrammarEvent *event)
{
    printf("speech_recognition_grammar_event_get_grammar_exception called.\n");
}
