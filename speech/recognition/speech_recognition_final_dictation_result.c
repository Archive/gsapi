
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Provides information on a finalized result for an utterance that matches
 * a #SpeechRecognitionDictationGrammar. A finalized result is a result that is
 * in either the #SPEECH_RECOGNITION_RESULT_ACCEPTED or 
 * #SPEECH_RECOGNITION_RESULT_REJECTED state
 * (tested by the speech_recognition_result_get_result_state() function of 
 * the #SpeechRecognitionResult object).
 *
 * The #SpeechRecognitionFinalDictationResult object extends the
 * #SpeechRecognitionResult and #SpeechRecognitionFinalResult objects with a
 * single function. The 
 * speech_recognition_final_rule_result_get_alternative_tokens() function 
 * provides access to alternative guesses for tokens in a dictation result.
 *
 * Every #SpeechRecognitionResult object provided by a 
 * #SpeechRecognitionRecognizer implements the 
 * #SpeechRecognitionFinalDictationResult and the
 * #SpeechRecognitionFinalRuleResult objects (and by inheritence the
 * #SpeechRecognitionFinalResult and #SpeechRecognitionResult objects).
 * However, the functions of #SpeechRecognitionFinalDictationResult should 
 * only be called if the speech_recognition_result_get_grammar() function
 * returns a #SpeechRecognitionDictationGrammar and once the 
 * #SpeechRecognitionResult has been finalized with either a 
 * #SPEECH_RECOGNITION_RESULT_ACCEPTED or
 * #SPEECH_RECOGNITION_RESULT_REJECTED event. Inappropriate calls will cause a
 * #SPEECH_RECOGNITION_RESULT_STATE_ERROR.
 *
 * See: #SPEECH_RECOGNITION_RESULT_ACCEPTED
 * See: #SPEECH_RECOGNITION_RESULT_REJECTED
 * See: speech_recognition_result_get_result_state()
 * See: #SpeechRecognitionDictationGrammar
 * See: #SpeechRecognitionResult
 * See: #SpeechRecognitionFinalResult
 * See: #SpeechRecognitionFinalRuleResult
 * See: #SPEECH_RECOGNITION_RESULT_STATE_ERROR
 */

#include <speech/recognition/speech_recognition_final_dictation_result.h>


GType
speech_recognition_final_dictation_result_get_type(void)
{
    printf("speech_recognition_final_dictation_result_get_type called.\n");
}


SpeechRecognitionFinalDictationResult *
speech_recognition_final_dictation_result_new(void)
{
    printf("speech_recognition_final_dictation_result_new called.\n");
}


/**
 * speech_recognition_final_dictation_result_get_alternative_tokens:
 * @result: the #SpeechRecognitionFinalDictationResult object that this
 *          operation will be applied to.
 * @from_token: start of the inclusive set of tokens.
 * @to_token: end of the inclusive set of tokens.
 * @max: the maximum number of alternative guesses to be returned.
 * @results: return a set of alternative token guesses for a single known 
 *           token or sequence of tokens.
 *
 * Return a set of alternative token guesses for a single known token
 * or sequence of tokens. In a dictation application the alternative
 * guesses are typically provided to a user to facilitate efficient
 * correction of dictated text. The assumption is that if a recognizer
 * does not correctly hear a user (a mis-recognition) the correct
 * tokens are likely to be amongst the top alternatives guesses.
 *
 * Typically when a user selects one of the alternative guesses as the
 * correct token sequence, the 
 * speech_recognition_final_result_token_correction() function
 * of the #SpeechRecognitionFinalResult object is called with those
 * tokens. That call allows the #SpeechRecognitionRecognizer to learn
 * from recognition errors and can improve future recognition accuarcy.
 *
 * The @from_token and @to_token parameters define an inclusive set of 
 * tokens for which alternatives are required. @from_token and @to_token are
 * typically included in the set of alternative guesses.
 * If @to_token is %NULL or if @from_token and @to_token are the same, 
 * then alternatives are provided for the single @to_token token.
 *
 * The tokens passed to this function must be #SpeechRecognitionResultToken 
 * objects provided by this result through previous calls to 
 * speech_recognition_final_rule_result_get_alternative_tokens() 
 * or the speech_recognition_result_get_best_token() and
 * speech_recognition_result_get_best_tokens() functions of the 
 * #SpeechRecognitionResult object.
 *
 * <title>Returned Array Structure</title>
 *
 * The return value is a ragged two-dimension array with indices being 
 * altTokens[guessNumber][tokenNumber]. The guesses are ordered by from 
 * best guess to least likely (determined by the recognizer):
 * altTokens[0] is the best guess for the span of @from_token to
 * @to_token, altTokens[1] is the first alternative guess and so on.
 *
 * The number of tokens may be different in each alternative guess.
 * This means that the length of altTokens[0] may be different from the 
 * length of altTokens[1] and so on. The length is never zero or less.
 *
 * The max parameter indicates the maximum number of alternative
 * guesses to be returned. The number of guesses returned may be
 * less than or equal to max. The number of alternatives returned
 * is also less than or equal to the #NumResultAlternatives
 * property set in the #SpeechRecognitionRecognizerProperties at the
 * time of recognition.
 *
 * The number of alternative guesses and the number of tokens in each
 * guess can vary between results. The numbers can vary for different
 * values of @from_token and @to_token for the calls to the same result.
 *
 * The returned alternative guess is always an array of length one or
 * greater. If there is only one guess, it may be the sequence of
 * @from_token to @to_token tokens. Each guess always contains one or 
 * more tokens. If the result is #SPEECH_RECOGNITION_RESULT_ACCEPTED then 
 * the recognizer is confident that all the alternatives guesses are 
 * reasonable guesses of what the user said.
 *
 * See: #SpeechRecognitionResult
 * See: speech_recognition_result_num_tokens()
 * See: speech_recognition_result_get_best_token()
 * See: speech_recognition_result_get_best_tokens()
 * See: speech_recognition_final_result_token_correction()
 * See: speech_recognition_recognizer_properties_set_num_result_alternatives()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if passed a
 *	#SpeechRecognitionResultToken not obtained from this 
 *      result.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before 
 *       a result is finalized or if the matched grammar is not a
 *	 #SpeechRecognitionDictationGrammar.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_dictation_result_get_alternative_tokens(
                                 SpeechRecognitionFinalDictationResult *result,
                                 SpeechRecognitionResultToken *from_token,
                                 SpeechRecognitionResultToken *to_token,
                                 int max,
                                 SpeechRecognitionResultToken *results[])
{
    printf("speech_recognition_final_dictation_result_get_alternative_tokens called.\n");
}
