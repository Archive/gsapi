
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A #SpeechRecognitionSpeakerProfile object is used to identify each 
 * enrollment by a user to a #SpeechRecognitionRecognizer. 
 * #SpeechRecognitionSpeakerProfile objects are used in management of 
 * speaker data through the #SpeechRecognitionSpeakerManager object 
 * for a #SpeechRecognitionRecognizer and in selection of recognizers 
 * through the #SpeechRecognitionRecognizerModeDesc class.
 *
 * A user may have a single or multiple profiles stored in recognizer. Examples
 * of multiple profiles include a user who enrolls and trains the recognizer
 * separately for different microphones or for different application domains
 * (e.g. romance novels and business email).
 *
 * Each #SpeechRecognitionSpeakerProfile object has a unique identifier (unique
 * to the #SpeechRecognitionRecognizer), plus a user name and optionally a
 * variant name that identifies each separate profile for a user (per-user
 * unique). All three identifying properties should be human-readable strings.
 * (The identifier is often the concatenation of the user name and variant.)
 *
 * Appropriate naming of profiles is the joint responsibility of users and
 * recognizers.
 *
 * Calls to the speech_recognition_speaker_profile_set_XXX functions of a 
 * #SpeechRecognitionSpeakerProfile make persistent changes to the speaker 
 * data stored by the recognizer. These changes are persistent across 
 * sessions with the recognizer.
 *
 * #SpeechRecognitionSpeakerProfiles are created and managed by the
 * #SpeechRecognitionSpeakerManager for a #SpeechRecognitionRecognizer.
 *
 * <title>Speaker Data</title>
 *
 * A #SpeechRecognitionSpeakerProfile object identifies all the stored 
 * data the recognizer has about a speaker in a particular enrollment. 
 * The contents of the profile are controlled by the recognizer. Except  
 * for the properties of the #SpeechRecognitionSpeakerProfile, this data 
 * is not accessibile to an application. The profile may include:
 *
 * <itemizedlist>
 *   <listitem>Speaker data: full name, age, gender etc.</listitem>
 *   <listitem>Speaker preferences: including settings of the
 *	 #SpeechRecognitionRecognizerProperties.</listitem>
 *   <listitem>Language models: data about the words and word patterns 
 *       of the speaker.</listitem>
 *   <listitem>Word models: data about the pronunciation of words by the 
 *       speaker.</listitem>
 *   <listitem>Acoustic models: data about the speaker's voice.</listitem>
 *   <listitem>Training information and usage history.</listitem>
 * </itemizedlist>
 *
 * The key role of stored profiles maintaining information that enables a
 * recognition to adapt to characteristics of the speaker. The goal of this
 * adaptation is to improve the performance and accuracy of speech recognition.
 *
 * <title>Speakers in Recognizer Selection</title>
 *
 * When selecting a #SpeechRecognitionRecognizer (through #SpeechCentral) a
 * user will generally prefer to select an engine which they have already
 * trained. The speech_recognition_recognizer_mode_desc_get_speaker_profiles()
 * function of a #SpeechRecognitionRecognizerModeDesc should return the list 
 * of speaker profiles known to a recognizer (or %NULL for speaker-independent
 * recognizers and new recognizers). For that selection process to be 
 * effective, a recognizer is responsible for ensuring that the 
 * speech_recognition_recognizer_mode_desc_get_speaker_profiles() function
 * of its RecognizerModeDesc includes its complete list of known speakers.
 *
 * See: #SpeechRecognitionSpeakerManager
 * See: speech_recognition_speaker_manager_new_speaker_profile()
 * See: speech_recognition_recognizer_get_speaker_manager()
 * See: speech_recognition_recognizer_mode_desc_get_speaker_profiles()
 */

#include <speech/recognition/speech_recognition_speaker_profile.h>


GType
speech_recognition_speaker_profile_get_type(void)
{
    printf("speech_recognition_speaker_profile_get_type called.\n");
}


SpeechRecognitionSpeakerProfile *
speech_recognition_speaker_profile_new(void)
{
    printf("speech_recognition_speaker_profile_new called.\n");
}


/**
 * speech_recognition_speaker_profile_equals:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 *
 * %TRUE if and only if the input parameter is not null and is a
 * #SpeechRecognitionSpeakerProfile with equal values of all properties.
 *
 * Returns: %TRUE if and only if the input parameter is not %NULL and
 *	   is a #SpeechRecognitionSpeakerProfile with equal values of
 *	   all properties.
 */

gboolean
speech_recognition_speaker_profile_equals(
                               SpeechRecognitionSpeakerProfile *profile,
                               SpeechRecognitionSpeakerProfile *an_object)
{
    printf("speech_recognition_speaker_profile_equals called.\n");
}


/**
 * speech_recognition_speaker_profile_get_id:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 *
 * Returns: the #SpeechRecognitionSpeakerProfile identifier.
 */

gchar *
speech_recognition_speaker_profile_get_id(
                                    SpeechRecognitionSpeakerProfile *profile)
{
    printf("speech_recognition_speaker_profile_get_id called.\n");
}


/**
 * speech_recognition_speaker_profile_set_id:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 * @identifier: the #SpeechRecognitionSpeakerProfile identifier.
 *
 * Set the #SpeechRecognitionSpeakerProfile identifier. The identifier
 * should be a human-readable string. The identifier string must
 * be unique for a recognizer. The identifier is sometimes the
 * concatenation of the user name and variants strings.
 *
 * If the #SpeechRecognitionSpeakerProfile object is one returned from
 * a recognizer's #SpeechRecognitionSpeakerManager, setting the
 * identifier changes the persistent speaker data of the recognizer.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the speaker id is
 *		already being used by this recognizer.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_speaker_profile_set_id(
                               SpeechRecognitionSpeakerProfile *profile,
                               gchar *identifier)
{
    printf("speech_recognition_speaker_profile_set_id called.\n");
}


/**
 * speech_recognition_speaker_profile_get_name:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 *
 * Return the speaker name.
 *
 * Returns: the speaker name.
 */

gchar *
speech_recognition_speaker_profile_get_name(
                               SpeechRecognitionSpeakerProfile *profile)
{
    printf("speech_recognition_speaker_profile_get_name called.\n");
}


/**
 * speech_recognition_speaker_profile_set_name:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 * @name: the speaker name.
 *
 * Set the speaker name. The speaker name should be a human-readable
 * string. The speaker name does not need to be unique for a
 * recognizer. (A speaker with more than one profile must have a
 * separate variant for each).
 *
 * If the #SpeechRecognitionSpeakerProfile object is one returned from
 * a recognizer's #SpeechRecognitionSpeakerManager, setting the name
 * changes the persistent speaker data of the recognizer.
 */

void
speech_recognition_speaker_profile_set_name(
                               SpeechRecognitionSpeakerProfile *profile,
                               gchar *name)
{
    printf("speech_recognition_speaker_profile_set_name called.\n");
}


/**
 * speech_recognition_speaker_profile_get_variant:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 *
 * Get the variant description.
 *
 * Returns: the variant description.
 */

gchar *
speech_recognition_speaker_profile_get_variant(
                               SpeechRecognitionSpeakerProfile *profile)
{
    printf("speech_recognition_speaker_profile_get_variant called.\n");
}


/**
 * speech_recognition_speaker_profile_set_variant:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 * @variant: the variant description.
 *
 * Set the variant description. The variant should be a human-readable
 * string. A speaker with more than one #SpeechRecognitionSpeakerProfile
 * should have a different variant description for each profile.
 * If a speaker has only one profile, the variant description may
 * be %NULL.
 *
 * If the #SpeechRecognitionSpeakerProfile object is one returned from
 * a recognizer's #SpeechRecognitionSpeakerManager, setting the variant
 * changes the persistent speaker data of the recognizer.
 */

void
speech_recognition_speaker_profile_set_variant(
                               SpeechRecognitionSpeakerProfile *profile,
                               gchar *variant)
{
    printf("speech_recognition_speaker_profile_set_variant called.\n");
}


/**
 * speech_recognition_speaker_profile_match:
 * @profile: the #SpeechRecognitionSpeakerProfile object that this operation
 *           will be applied to.
 * @require: the #SpeechRecognitionSpeakerProfile to be matched against.
 *
 * Returns %TRUE if this object matches the @require object. A match
 * requires that each non-null or non-zero-length string property of
 * the required object be an exact string match to the properties of
 * this object.
 *
 * Returns: %TRUE if this object matches the @require object.
 */

gboolean
speech_recognition_speaker_profile_match(
                               SpeechRecognitionSpeakerProfile *profile,
                               SpeechRecognitionSpeakerProfile *require)
{
    printf("speech_recognition_speaker_profile_match called.\n");
}
