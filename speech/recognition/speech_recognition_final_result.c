
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechRecognitionFinalResult is an extension to the 
 * #SpeechRecognitionResult object that provides information about a 
 * result that has been finalized - that is, recognition is complete. 
 * A finalized result is a #SpeechRecognitionResult that has received 
 * either a #SPEECH_RECOGNITION_RESULT_ACCEPTED or 
 * #SPEECH_RECOGNITION_RESULT_REJECTED #SpeechRecognitionResultEvent 
 * that puts it in either the #SPEECH_RECOGNITION_RESULT_ACCEPTED
 * or #SPEECH_RECOGNITION_RESULT_REJECTED state (indicated by the
 * speech_recognition_result_get_result_state() function of the 
 * #SpeechRecognitionResult object).
 *
 * The #SpeechRecognitionFinalResult object provides information for 
 * finalized results that match either a #SpeechRecognitionDictationGrammar
 * or a #SpeechRecognitionRuleGrammar.
 *
 * Any result object provided by a recognizer implements both the
 * #SpeechRecognitionFinalRuleResult and #SpeechRecognitionFinalDictationResult
 * objects. Because both these objects extend the 
 * #SpeechRecognitionFinalResult object, which in turn extends the
 * #SpeechRecognitionResult object, all results implement
 * #SpeechRecognitionFinalResult.
 *
 * The functions of the #SpeechRecognitionFinalResult object provide
 * information about a finalized result (SPEECH_RECOGNITION_RESULT_ACCEPTED or
 * SPEECH_RECOGNITION_RESULT_REJECTED state). If any function of the 
 * #SpeechRecognitionFinalResult object is called on a result in the 
 * #SPEECH_RECOGNITION_RESULT_UNFINALIZED state, a 
 * #SPEECH_RECOGNITION_RESULT_STATE_ERROR is thrown.
 *
 * Three capabilities can be provided by a finalized result:
 * training/correction, access to audio data, and access to alternative
 * guesses. All three capabilities are optional because they are not all
 * relevant to all results or all recognition environments, and they are
 * not universally supported by speech recognizers. Training and access
 * to audio data are provided by the #SpeechRecognitionFinalResult object.
 * Access to alternative guesses is provided by the
 * #SpeechRecognitionFinalDictationResult and 
 * #SpeechRecognitionFinalRuleResult objects (depending upon the type of 
 * grammar matched).
 *
 * <title>Training / Correction</title>
 *
 * Because speech recognizers are not always correct, applications need
 * to consider the possibility that a recognition error has occurred.
 * When an application detects an error (e.g. a user updates a result),
 * the application should inform the recognizer so that it can learn from the
 * mistake and try to improve future performance.
 * speech_recognition_final_result_token_correction() is provided for an 
 * application to provide feedback from user correction to the recognizer.
 *
 * Sometimes, but certainly not always, the correct result is selected by a
 * user from amongst the N-best alternatives for a result obtained through
 * either the #SpeechRecognitionFinalRuleResult or
 * #SpeechRecognitionFinalDictationResult objects. In other cases, a user may
 * type the correct result or the application may infer a correction from
 * following user input.
 *
 * Recognizers must store considerable information to support training from
 * results. Applications need to be involved in the management of that
 * information so that it is not stored unnecessarily. The
 * speech_recognition_final_result_is_training_info_available() function 
 * tests whether training information is available for a finalized result.
 * When an application/user has finished correction/training for a result 
 * it should call speech_recognition_final_result_release_training_info() 
 * to free up system resources. Also, a recognizer may choose at any time 
 * to free up training information. In both cases, the application is 
 * notified of the the release with a
 * #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event to 
 * #SpeechRecognitionResultListeners.
 *
 * <title>Audio Data</title>
 *
 * Audio data for a finalized result is optionally provided by recognizers.
 * In dictation systems, audio feedback to users can remind them of what they
 * said and is useful in correcting and proof-reading documents. Audio data
 * can be stored for future use by an application or user and in certain
 * circumstances can be provided by one recognizer to another.
 *
 * Since storing audio requires substantial system resources, audio data
 * requires special treatment. If an application wants to use audio data,
 * it should set the #setResultAudioProvided property of the
 * #SpeechRecognitionRecognizerProperties to %TRUE.
 *
 * Not all recognizers provide access to audio data. For those recognizers,
 * speech_recognition_recognizer_properties_set_result_audio_provided()
 * has no effect, the speech_recognition_final_result_is_audio_available() 
 * always returns %FALSE, and the speech_recognition_final_result_get_audio()
 * functions always return %NULL.
 *
 * Recognizers that provide access to audio data cannot always provide audio
 * for every result. Applications should test audio availability for every
 * #SpeechRecognitionFinalResult and should always test for %NULL on the
 * speech_recognition_final_result_get_audio() functions.
 *
 * See: #SpeechRecognitionResult
 * See: speech_recognition_result_get_result_state()
 * See: #SPEECH_RECOGNITION_RESULT_ACCEPTED
 * See: #SPEECH_RECOGNITION_RESULT_REJECTED
 * See: #SpeechRecognitionFinalDictationResult
 * See: #SpeechRecognitionFinalRuleResult
 * See: #SpeechRecognitionDictationGrammar
 * See: #SpeechRecognitionRuleGrammar
 * See: speech_recognition_recognizer_properties_set_result_audio_provided()
 */

#include <speech/recognition/speech_recognition_final_result.h>


GType
speech_recognition_final_result_get_type(void)
{
    printf("speech_recognition_final_result_get_type called.\n");
}


SpeechRecognitionFinalResult *
speech_recognition_final_result_new(void)
{
    printf("speech_recognition_final_result_new called.\n");
}


/**
 * speech_recognition_final_result_get_audio:
 * @result: the #SpeechRecognitionFinalResult object that this operation will
 *          be applied to.
 * @audio return the result audio for the complete utterance of a
 *	   #SpeechRecognitionFinalResult.
 *
 * Get the result audio for the complete utterance of a
 * #SpeechRecognitionFinalResult. Returns %NULL if result audio
 * is not available or if it has been released.
 *
 * See: speech_recognition_final_result_is_audio_available()
 * See: speech_recognition_final_result_get_audio_for_token()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before 
 *       a result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_result_get_audio(SpeechRecognitionFinalResult *result,
                                          SpeechRecognitionAudioClip *audio)
{
    printf("speech_recognition_final_result_get_audio called.\n");
}


/**
 * speech_recognition_final_result_get_audio_for_token:
 * @result: the #SpeechRecognitionFinalResult object that this operation will
 *          be applied to.
 * @from_token: the start token.
 * @to_token: the end token.
 * @audio: return the audio for a token or sequence of tokens.
 *
 * Get the audio for a token or sequence of tokens.  Recognizers
 * make a best effort at determining the start and end of tokens,
 * however, it is not unusual for chunks of surrounding audio
 * to be included or for the start or end token to be chopped.
 *
 * Returns %NULL if result audio is not available or if it cannot be
 * obtained for the specified sequence of tokens.
 *
 * If @to_token is %NULL or if @from_token and @to_token are the same, 
 * the function returns audio for @from_token. If both @from_token and
 * @to_token are %NULL, it returns the audio for the entire result 
 * (same as speech_recognition_final_result_get_audio() ).
 *
 * Not all recognizers can provide per-token audio, even if they
 * can provide audio for a complete utterance.
 *
 * See: speech_recognition_final_result_get_audio()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if one of the token 
 *       parameters is not from this result.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before 
 *       a result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_result_get_audio_for_token(
                               SpeechRecognitionFinalResult *result,
                               SpeechRecognitionResultToken *from_token,
                               SpeechRecognitionResultToken *to_token,
                               SpeechRecognitionAudioClip *audio)
{
    printf("speech_recognition_final_result_get_audio_for_token called.\n");
}


/**
 * speech_recognition_final_result_is_audio_available:
 * @result: the #SpeechRecognitionFinalResult object that this operation will
 *          be applied to.
 * @available: return an indication of whether result audio data is available
 *	   for this result.
 *
 * Test whether result audio data is available for this result.
 * Result audio is only available if:
 *
 * <itemizedlist>
 *   <listitem>The #ResultAudioProvided property of
 *	 #SpeechRecognitionRecognizerProperties was set to true when
 *	 the result was recognized.</listitem>
 *   <listitem>The #SpeechRecognitionRecognizer was able to collect result
 *	 audio for the current type of #SpeechRecognitionFinalResult
 *	 (#SpeechRecognitionFinalRuleResult or
 *	 #SpeechRecognitionFinalDictationResult).</listitem>
 *   <listitem>The result audio has not yet been released.</listitem>
 * </itemizedlist>
 *
 * The availability of audio for a result does not mean that all
 * speech_recognition_final_result_get_audio() calls will return a 
 * #SpeechRecognitionAudioClip.
 * For example, some recognizers might provide audio data only for the 
 * entire result or only for individual tokens, or not for sequences of 
 * more than one token.
 *
 * See: speech_recognition_final_result_get_audio()
 * See: speech_recognition_recognizer_properties_set_result_audio_provided()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before a 
 *       result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_result_is_audio_available(
                                          SpeechRecognitionFinalResult *result,
                                          gboolean *available)
{
    printf("speech_recognition_final_result_is_audio_available called.\n");
}


/**
 * speech_recognition_final_result_release_audio:
 * @result: the #SpeechRecognitionFinalResult object that this operation will
 *          be applied to.
 *
 * Release the result audio for the result. After audio is released,
 * speech_recognition_final_result_is_audio_available() will return %FALSE.
 * This call is ignored if result audio is not available or has already been
 * released.
 *
 * This function is asynchronous - audio data is not necessarily
 * released immediately. A #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED
 * event is issued to the #SpeechRecognitionResultListener when the audio is
 * released by a call to this function. A 
 * #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED event is also issued if 
 * the recognizer releases the audio for some other reason (e.g. to reclaim 
 * memory).
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_AUDIO_RELEASED
 * See: speech_recognition_result_listener_audio_released()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before a 
 *       result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_result_release_audio(
                                          SpeechRecognitionFinalResult *result)
{
    printf("speech_recognition_final_result_release_audio called.\n");
}


/**
 * speech_recognition_final_result_is_training_info_available:
 * @result: the #SpeechRecognitionFinalResult object that this operation will
 *          be applied to.
 * @available: return a boolean indication of whether training information is
 *	   available.
 *
 * Returns %TRUE if the #SpeechRecognitionRecognizer has training
 * information available for this result. Training is available
 * if the following conditions are met:
 *
 * <itemizedlist>
 *   <listitem>The #isTrainingProvided property of the
 *	 #SpeechRecognitionRecognizerProperties is set to %TRUE.</listitem>
 *   <listitem>And, the training information for this result has not been
 *	 released by the application or by the recognizer. (The
 *	 #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event has 
 *       not been issued.)</listitem>
 * </itemizedlist>
 *
 * Calls to speech_recognition_final_result_token_correction() have no 
 * effect if the training information is not available.
 *
 * See: speech_recognition_recognizer_properties_set_training_provided()
 * See: speech_recognition_final_result_release_training_info()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before a 
 *       result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_result_is_training_info_available(
                                         SpeechRecognitionFinalResult *result,
                                         gboolean *available)
{
    printf("speech_recognition_final_result_is_training_info_available called.\n");
}


/**
 * speech_recognition_final_result_release_training_info:
 * @result: the #SpeechRecognitionFinalResult object that this operation will
 *          be applied to.
 *
 * Release training information for this #SpeechRecognitionFinalResult.
 * The release frees memory used for the training information --
 * this information can be substantial.
 *
 * It is not an error to call the function when training information
 * is not available or has already been released.
 *
 * This function is asynchronous - the training info is not necessarily
 * released when the call returns. A
 * #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event is issued 
 * to the #SpeechRecognitionResultListener once the information is released.
 * The #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED event is 
 * also issued if the recognizer releases the training information for 
 * any other reason (e.g. to reclaim memory).
 *
 * See: #SPEECH_RECOGNITION_RESULT_EVENT_TRAINING_INFO_RELEASED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before a 
 *       result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_result_release_training_info(
                                        SpeechRecognitionFinalResult *result)
{
    printf("speech_recognition_final_result_release_training_info called.\n");
}


/**
 * speech_recognition_final_result_token_correction:
 * @result: the #SpeechRecognitionFinalResult object that this operation will
 *          be applied to.
 * @correct_tokens: sequence of correct tokens to replace @from_token to 
 *                  @to_token.
 * @from_token: first token in the sequence being corrected.
 * @to_token: last token in the sequence being corrected.
 * @correction_type: type of correction:
 *			 #SPEECH_RECOGNITION_FINAL_RESULT_MISRECOGNITION,
 *			 #SPEECH_RECOGNITION_FINAL_RESULT_USER_CHANGE,
 *			 #SPEECH_RECOGNITION_FINAL_RESULT_DONT_KNOW.
 *
 * Inform the recognizer of a correction to one of more tokens in
 * a finalized result so that the recognizer can re-train itself.
 * Training the recognizer from its mistakes allows it to improve
 * its performance and accuracy in future recognition.
 *
 * The @from_token and @to_token parameters indicate the inclusive 
 * sequence of best-guess or alternative tokens that are being trained 
 * or corrected. If @to_token is %NULL or if @from_token and @to_token 
 * are the same, the training applies to a single recognized token.
 *
 * The @correct_tokens token sequence may have the same of a different 
 * length than the token sequence being corrected. Setting @correct_tokens
 * to %NULL indicates the deletion of tokens. The @correction_type parameter
 * must be one of #SPEECH_RECOGNITION_FINAL_RESULT_MISRECOGNITION,
 * #SPEECH_RECOGNITION_FINAL_RESULT_USER_CHANGE, 
 * #SPEECH_RECOGNITION_FINAL_RESULT_DONT_KNOW.
 *
 * <emphasis>Note:</emphasis> 
 * speech_recognition_final_result_token_correction() does not change the
 * result object. So, future calls to the 
 * speech_recognition_result_get_best_token(),
 * speech_recognition_result_get_best_tokens() and 
 * speech_recognition_final_rule_result_get_alternative_tokens() 
 * functions return exactly the same values as before the call to
 * speech_recognition_final_result_token_correction().
 *
 * See: #SPEECH_RECOGNITION_FINAL_RESULT_MISRECOGNITION
 * See: #SPEECH_RECOGNITION_FINAL_RESULT_USER_CHANGE
 * See: #SPEECH_RECOGNITION_FINAL_RESULT_DONT_KNOW
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if either token is not 
 *       from this #SpeechRecognitionFinalResult.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_STATE_ERROR if called before 
 *       a result is finalized.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_final_result_token_correction(
                               SpeechRecognitionFinalResult *result,
                               gchar *correct_tokens[],
                               SpeechRecognitionResultToken *from_token,
                               SpeechRecognitionResultToken *to_token,
                               int correction_type)
{
    printf("speech_recognition_final_result_token_correction called.\n");
}
