
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_GRAMMAR_H__
#define __SPEECH_RECOGNITION_GRAMMAR_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/recognition/speech_recognition_recognizer.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_GRAMMAR              (speech_recognition_grammar_get_type())
#define SPEECH_RECOGNITION_GRAMMAR(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR, SpeechRecognitionGrammar))
#define SPEECH_RECOGNITION_GRAMMAR_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR, SpeechRecognitionGrammarClass))
#define SPEECH_IS_RECOGNITION_GRAMMAR(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR))
#define SPEECH_IS_RECOGNITION_GRAMMAR_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_GRAMMAR))
#define SPEECH_RECOGNITION_GRAMMAR_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_GRAMMAR, SpeechRecognitionGrammarClass))

#include <speech/recognition/speech_recognition_grammar_listener.h>
#include <speech/recognition/speech_recognition_result_listener.h>
#include <speech/recognition/speech_recognition_recognizer.h>

/*
 * Default value of activation mode that requires the
 * #SpeechRecognitionRecognizer have focus and that there be no enabled
 * grammars with #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL mode for 
 * the grammar to be activated. This is the lowest priority activation 
 * mode and should be used unless there is a user interface design reason
 * to use another mode.
 *
 * The activation conditions for the 
 * #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS mode are describe above.
 *
 * See: speech_recognition_grammar_set_activation_mode()
 * See: #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL
 * See: #SPEECH_RECOGNITION_GRAMMAR_GLOBAL
 */

static int SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS = 900;

/*
 * Value of activation mode that requires the #SpeechRecognitionRecognizer
 * have focus for the grammar to be activated.
 *
 * The activation conditions for the 
 * SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL mode are describe above.
 *
 * See: speech_recognition_grammar_set_activation_mode()
 * See: #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS
 * See: #SPEECH_RECOGNITION_GRAMMAR_GLOBAL
 */

static int SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL = 901;

/*
 * Value of activation mode in which the #SpeechRecognitionGrammar is
 * active for recognition irrespective of the focus state of the
 * #SpeechRecognitionRecognizer.
 *
 * The activation conditions for the 
 * #SPEECH_RECOGNITION_GRAMMAR_GLOBAL mode are describe above.
 *
 * See: speech_recognition_grammar_set_activation_mode()
 * See: #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_FOCUS
 * See: #SPEECH_RECOGNITION_GRAMMAR_RECOGNIZER_MODAL
 */

static int SPEECH_RECOGNITION_GRAMMAR_GLOBAL = 902;


GType 
speech_recognition_grammar_get_type(void);

SpeechRecognitionGrammar * 
speech_recognition_grammar_new(void);

void 
speech_recognition_grammar_add_grammar_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionGrammarListener *listener);

void 
speech_recognition_grammar_remove_grammar_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionGrammarListener *listener);

void 
speech_recognition_grammar_add_result_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionResultListener *listener);

void 
speech_recognition_grammar_remove_result_listener(
                             SpeechRecognitionGrammar *grammar,
                             SpeechRecognitionResultListener *listener);

int 
speech_recognition_grammar_get_activation_mode(
                             SpeechRecognitionGrammar *grammar);

gchar * 
speech_recognition_grammar_get_name(
                             SpeechRecognitionGrammar *grammar);

SpeechRecognitionRecognizer * 
speech_recognition_grammar_get_recognizer(SpeechRecognitionGrammar *grammar);

gboolean 
speech_recognition_grammar_is_active(SpeechRecognitionGrammar *grammar);

gboolean 
speech_recognition_grammar_is_enabled(SpeechRecognitionGrammar *grammar);

SpeechStatus 
speech_recognition_grammar_set_activation_mode(
                             SpeechRecognitionGrammar *grammar,
                             int mode);

void 
speech_recognition_grammar_set_enabled(SpeechRecognitionGrammar *grammar,
                                       gboolean enabled);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_GRAMMAR_H__ */
