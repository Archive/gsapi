
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechRecognitionRuleAlternatives represents a #SpeechRecognitionRule 
 * composed of a set of alternative sub-rules. 
 * #SpeechRecognitionRuleAlternatives are used to construct 
 * #SpeechRecognitionRuleGrammar objects. A #SpeechRecognitionRuleAlternatives
 * object is spoken by saying one, and only one, of its sub-rules.
 *
 * A #SpeechRecognitionRuleAlternatives object contains a set of zero or
 * more #SpeechRecognitionRule objects. A set of zero alternatives is
 * equivalent to #SPEECH_RECOGNITION_RULE_NAME_VOID; (it is unspeakable).
 *
 * Weights may be (optionally) assigned to each alternative rule. The
 * weights indicate the chance of each #SpeechRecognitionRule being spoken.
 * The speech_recognition_rule_alternatives_set_weights() function defines 
 * the constraints upon weights. If no weights are defined, then all 
 * alternatives are considered equally likely.
 *
 * See: #SpeechRecognitionRule
 * See: #SpeechRecognitionRuleCount
 * See: #SpeechRecognitionRuleGrammar
 * See: #SpeechRecognitionRuleName
 * See: #SpeechRecognitionRuleParse
 * See: #SpeechRecognitionRuleSequence
 * See: #SpeechRecognitionRuleTag
 * See: #SpeechRecognitionRuleToken
 * See: #SPEECH_RECOGNITION_RULE_NAME_VOID
 */

#include <speech/recognition/speech_recognition_rule_alternatives.h>


GType
speech_recognition_rule_alternatives_get_type(void)
{
    printf("speech_recognition_rule_alternatives_get_type called.\n");
}


SpeechRecognitionRuleAlternatives *
speech_recognition_rule_alternatives_new(void)
{
    printf("speech_recognition_rule_alternatives_new called.\n");
}


/**
 * speech_recognition_rule_alternatives_new_with_subrule:
 * @rule: the sub-rule.
 *
 * Initializes a #SpeechRecognitionRuleAlternatives object containing
 * a single sub-rule. The weights array is set to %NULL.
 *
 * Returns:
 */

SpeechRecognitionRuleAlternatives *
speech_recognition_rule_alternatives_new_with_subrule(
                                              SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_alternatives_new_with_subrule called.\n");
}


/**
 * speech_recognition_rule_alternatives_new_with_tokens:
 * @tokens: a set of alternative tokens.
 *
 * Initializes for #SpeechRecognitionRuleAlternatives that produces a
 * phrase list from an array of strings. Each string is used to
 * create a single #SpeechRecognitionRuleToken object.
 *
 * A string containing multiple words (e.g. "san francisco") is treated
 * as a single token. If appropriate, an application should parse such
 * strings to produce separate tokens.
 *
 * The phrase list may be zero-length or %NULL. This will produce an
 * empty set of alternatives which is equivalent to 
 * #SPEECH_RECOGNITION_RULE_NAME_VOID; (i.e. unspeakable).
 *
 * Returns:
 *
 * See: #SPEECH_RECOGNITION_RULE_NAME_VOID
 */

SpeechRecognitionRuleAlternatives *
speech_recognition_rule_alternatives_new_with_tokens(gchar *tokens[])
{
    printf("speech_recognition_rule_alternatives_new_with_tokens called.\n");
}


/**
 * speech_recognition_rule_alternatives_new_with_subrules:
 * @rules: the set of alternative sub-rules.
 *
 * Initializes a #SpeechRecognitionRuleAlternatives object with an
 * array of sub-rules.	The weights are set to %NULL.
 *
 * Returns: a new #SpeechRecognitionRuleAlternatives object.
 */

SpeechRecognitionRuleAlternatives *
speech_recognition_rule_alternatives_new_with_subrules(
                                              SpeechRecognitionRule *rules[])
{
    printf("speech_recognition_rule_alternatives_new_with_subrules called.\n");
}


/**
 * speech_recognition_rule_alternatives_new_with_subrules_and_weights:
 * @rules: the set of alternative sub-rules.
 * @weights: set of weights for each rule or %NULL.
 *
 * Initializes a #SpeechRecognitionRuleAlternatives object with an array
 * of sub-rules and an array of weights. The rules array and weights
 * array may be null. If the weights array is non-null, it must have
 * identical length to the rules array.
 *
 * See: speech_recognition_rule_alternatives_set_weights()
 *
 * Returns: a new #SpeechRecognitionRuleAlternatives object or %NULL
 *          if there is an error in length of array, or the weight values
 *          (see speech_recognition_rule_alternatives_set_weights()).
 */

SpeechRecognitionRuleAlternatives *
speech_recognition_rule_alternatives_new_with_subrules_and_weights(
                                              SpeechRecognitionRule *rules[],
                                              float weights[])
{
    printf("speech_recognition_rule_alternatives_new_with_subrules_and_weights called.\n");
}


/**
 * speech_recognition_rule_alternatives_append:
 * @rule_alternatives: the #SpeechRecognitionRuleAlternatives object that this
 *                     operation will be applied to.
 * @rule: the rule to append.
 *
 * Append a single rule to the set of alternatives.
 * The weights are set to %NULL.
 */

void
speech_recognition_rule_alternatives_append(
                         SpeechRecognitionRuleAlternatives *rule_alternatives,
                         SpeechRecognitionRule rule)
{
    printf("speech_recognition_rule_alternatives_append called.\n");
}


/**
 * speech_recognition_rule_alternatives_get_rules:
 * @rule_alternatives: the #SpeechRecognitionRuleAlternatives object that this
 *                     operation will be applied to.
 *
 * Returns: the array of alternative sub-rules.
 */

SpeechRecognitionRule **
speech_recognition_rule_alternatives_get_rules(
                         SpeechRecognitionRuleAlternatives *rule_alternatives)
{
    printf("speech_recognition_rule_alternatives_get_rules called.\n");
}


/**
 * speech_recognition_rule_alternatives_get_weights:
 * @rule_alternatives: the #SpeechRecognitionRuleAlternatives object that this
 *                     operation will be applied to.
 *
 * Return the array of weights.	 May return %NULL. If non-null, the
 * length of the weights array is guaranteed to be the same length
 * as the array of rules.
 *
 * Returns: the array of weights.
 */

float **
speech_recognition_rule_alternatives_get_weights(
                         SpeechRecognitionRuleAlternatives *rule_alternatives)
{
    printf("speech_recognition_rule_alternatives_get_weights called.\n");
}


/**
 * speech_recognition_rule_alternatives_set_rules:
 * @rule_alternatives: the #SpeechRecognitionRuleAlternatives object that this
 *                     operation will be applied to.
 *
 * Set the array of alternative sub-rules.
 *
 * If the weights are non-null and the number of rules is
 * not equal to the number of weights, the weights are set to %NULL.
 * To change the number of rules and weights, call
 * speech_recognition_rule_alternatives_set_rules() before 
 * speech_recognition_rule_alternatives_set_weights().
 *
 * See: speech_recognition_rule_alternatives_set_weights()
 */

void
speech_recognition_rule_alternatives_set_rules(
                         SpeechRecognitionRuleAlternatives *rule_alternatives,
                         SpeechRecognitionRule *rules[])
{
    printf("speech_recognition_rule_alternatives_set_rules called.\n");
}


/**
 * speech_recognition_rule_alternatives_set_weights:
 * @rule_alternatives: the #SpeechRecognitionRuleAlternatives object that this
 *                     operation will be applied to.
 * @weights: the array of weights.
 *
 * Set the array of weights for the rules. The @weights array may be %NULL. 
 * If the weights are %NULL, then all alternatives are assumed to be equally
 * likely.
 *
 * The length of the weights array must be the same length as the
 * array of rules. The weights must all be greater than or equal
 * to 0.0 and at least one must be non-zero.
 *
 * To change the number of rules and weights, first call
 * speech_recognition_rule_alternatives_set_rules().
 *
 * See: speech_recognition_rule_alternatives_set_rules()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if there is an error in
 *              length of array, or values of weights.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_rule_alternatives_set_weights(
                         SpeechRecognitionRuleAlternatives *rule_alternatives,
                         float weights[])
{
    printf("speech_recognition_rule_alternatives_set_weights called.\n");
}
