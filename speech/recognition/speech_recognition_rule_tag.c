
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechRecognitionRuleTag attaches a tag to a contained 
 * #SpeechRecognitionRule object. A tag is a string attached to any  
 * #SpeechRecognitionRule entity. The tag does not affect the recognition 
 * of a #SpeechRecognitionRuleGrammar in which it is used. Instead tags 
 * are used to embed information into a grammar that helps with processing 
 * of recognition results.
 *
 * Tags are
 *
 * <itemizedlist>
 *   <listitem>Used in the definition of a #SpeechRecognitionRuleGrammar,
 *       </listitem>
 *   <listitem>Included in parse output (#SpeechRecognitionRuleParse 
 *       objects).</listitem>
 * </itemizedlist>
 *
 * The tag string in the Java Speech Grammar Format allows the backslash
 * character to escape the curly brace character '}' or backslash. The
 * #SpeechRecognitionRuleTag class assumes that all such string handling is
 * handled separately. The exception is 
 * speech_recognition_rule_tag_to_string() which is required to produce a 
 * grammar-compliant string, and so escapes special characters as required.
 *
 * Note that an empty tag in JSGF is "{}". This tag is defined to be the
 * zero-length string, "". A null tag is converted to a zero-length string.
 *
 * See: #SpeechRecognitionRule
 * See: #SpeechRecognitionRuleAlternatives
 * See: #SpeechRecognitionRuleCount
 * See: #SpeechRecognitionRuleGrammar
 * See: speech_recognition_rule_grammar_parse()
 * See: #SpeechRecognitionRuleName
 * See: #SpeechRecognitionRuleParse
 * See: #SpeechRecognitionRuleSequence
 * See: #SpeechRecognitionRuleToken
 */

#include <speech/recognition/speech_recognition_rule_tag.h>


GType
speech_recognition_rule_tag_get_type(void)
{
    printf("speech_recognition_rule_tag_get_type called.\n");
}


SpeechRecognitionRuleTag *
speech_recognition_rule_tag_new(void)
{
    printf("speech_recognition_rule_tag_new called.\n");
}


/**
 * speech_recognition_rule_tag_new_with_rule_and_tag:
 * @rule: the rule being tagged.
 * @tag: the tag string.
 *
 * Initialize a #SpeechRecognitionRuleTag with for #SpeechRecognitionRule
 * object with a tag string. The function assumes that pre-processing
 * of grammar tags is complete (the leading and trailing curly
 * braces are removed, escape characters are removed).
 *
 * Returns:
 */

SpeechRecognitionRuleTag *
speech_recognition_rule_tag_new_with_rule_and_tag(SpeechRecognitionRule *rule,
                                                  gchar *tag)
{
    printf("speech_recognition_rule_tag_new_with_rule_and_tag called.\n");
}


/**
 * speech_recognition_rule_tag_get_rule:
 * @rule_tag: the #SpeechRecognitionRuleTag object that this operation will
 *            be applied to.
 *
 * Returns the #SpeechRecognitionRule object being tagged.
 *
 * Returns: the #SpeechRecognitionRule object being tagged.
 */

SpeechRecognitionRule *
speech_recognition_rule_tag_get_rule(SpeechRecognitionRuleTag *rule_tag)
{
    printf("speech_recognition_rule_tag_get_rule called.\n");
}


/**
 * speech_recognition_rule_tag_set_rule:
 * @rule_tag: the #SpeechRecognitionRuleTag object that this operation will
 *            be applied to.
 * @rule: the rule.
 *
 * Set the #SpeechRecognitionRule object to be tagged.
 */

void
speech_recognition_rule_tag_set_rule(SpeechRecognitionRuleTag *rule_tag,
                                     SpeechRecognitionRule *rule)
{
    printf("speech_recognition_rule_tag_set_rule called.\n");
}


/**
 * speech_recognition_rule_tag_get_tag:
 * @rule_tag: the #SpeechRecognitionRuleTag object that this operation will
 *            be applied to.
 *
 * Returns the tag string.
 *
 * Returns: the tag string.
 */

gchar *
speech_recognition_rule_tag_get_tag(SpeechRecognitionRuleTag *rule_tag)
{
    printf("speech_recognition_rule_tag_get_tag called.\n");
}


/**
 * speech_recognition_rule_tag_set_tag:
 * @rule_tag: the #SpeechRecognitionRuleTag object that this operation will
 *            be applied to.
 * @tag: the tag string for the #SpeechRecognitionRule.
 *
 * Set the tag string for the Rule. A zero-length string is legal.
 * A null tag is converted to "".
 */

void
speech_recognition_rule_tag_set_tag(SpeechRecognitionRuleTag *rule_tag,
                                    gchar *tag)
{
    printf("speech_recognition_rule_tag_set_tag called.\n");
}


gchar *
speech_recognition_rule_tag_to_string(SpeechRecognitionRuleTag *rule_tag)
{
    printf("speech_recognition_rule_tag_to_string called.\n");
}
