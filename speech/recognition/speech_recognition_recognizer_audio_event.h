
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_H__
#define __SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_H__

#include <speech/recognition/speech_recognition_types.h>
#include <speech/speech_audio_event.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_EVENT              (speech_recognition_recognizer_audio_event_get_type())
#define SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_EVENT, SpeechRecognitionRecognizerAudioEvent))
#define SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_EVENT, SpeechRecognitionRecognizerAudioEventClass))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_AUDIO_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_EVENT))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_AUDIO_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_EVENT))
#define SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_AUDIO_EVENT, SpeechRecognitionRecognizerAudioEventClass))

typedef struct _SpeechRecognitionRecognizerAudioEvent            SpeechRecognitionRecognizerAudioEvent;
typedef struct _SpeechRecognitionRecognizerAudioEventClass       SpeechRecognitionRecognizerAudioEventClass;

struct _SpeechRecognitionRecognizerAudioEvent {
    SpeechAudioEvent event;
};

struct _SpeechRecognitionRecognizerAudioEventClass {
    SpeechAudioEventClass parent_class;
};


/*
 * The recognizer has detected the possible start of speech in the
 * incoming audio. Applications may use this event to display visual
 * feedback to a user indicating that the recognizer is listening.
 *
 * A detailed description of
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED and
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED events is 
 * provided above.
 *
 * See: speech_recognition_recognizer_audio_listener_speech_started()
 * See: speech_event_get_id()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED
 */

static int SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED = 1100;

/*
 * The recognizer has detected the end of speech or noise in the
 * incoming audio that it previously indicated by a
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED event.
 * This event always follows a
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED event.
 *
 * A detailed description of
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED and
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED events 
 * is provided above.
 *
 * See: speech_recognition_recognizer_audio_listener_speech_stopped()
 * See: #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED
 * See: speech_event_get_id()
 */

static int SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED = 1101;

/*
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL event indicates 
 * a change in the volume level of the incoming audio.
 *
 * A detailed description of the 
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL event is 
 * provided above.
 *
 * See: speech_recognition_recognizer_audio_listener_audio_level()
 */

static int SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL = 1102;


GType 
speech_recognition_recognizer_audio_event_get_type(void);

SpeechRecognitionRecognizerAudioEvent * 
speech_recognition_recognizer_audio_event_new(
                                    SpeechRecognitionRecognizer *source,
                                    int id);

SpeechRecognitionRecognizerAudioEvent * 
speech_recognition_recognizer_audio_event_new_with_level(
                                    SpeechRecognitionRecognizer *source,
                                    int id,
                                    float audio_level);

float 
speech_recognition_recognizer_audio_event_get_audio_level(
                               SpeechRecognitionRecognizerAudioEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_AUDIO_EVENT_H__ */
