
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechRecognitionRecognizerModeDesc extends the #SpeechEngineModeDesc 
 * with properties that are specific to speech recognizers. A 
 * #SpeechRecognitionRecognizerModeDesc inherits engine name, mode name  
 * and locale from #SpeechEngineModeDesc. 
 * 
 * #SpeechRecognitionRecognizerModeDesc adds three features:
 *
 * <itemizedlist>
 *   <listitem>A #SpeechTristate indicating whether dictation is 
 *       supported.</listitem>
 *   <listitem>List of the recognizer's speaker profiles.</listitem>
 *   <listitem>Speaker profile that  will be loaded when a recognizer 
 *       is started (not used in selection).</listitem>
 * </itemizedlist>
 *
 * Like #SpeechEngineModeDesc, there are two types of 
 * #SpeechRecognitionRecognizerModeDesc: those created by an application for
 * use in engine selection, and those created by an engine which descibe
 * a particular mode of operation of the engine. Descriptors provided by
 * engines are obtained through the 
 * speech_central_available_recognizers() function of the #SpeechCentral
 * object. These descriptors must have all their features defined. A 
 * descriptor created by an application may make any or all of the features 
 * %NULL to indictae a "don't care" value (%NULL features are ignored 
 * in engine selection).
 *
 * Note: the #SpeechTristate "is running" feature is a full object
 *	 rather than a boolean primitive so that it can represent true,
 *	 false and "don't care".
 *
 * Applications can modify application-created descriptors in any way.
 * Applications should never modify the features of a
 * #SpeechRecognitionRecognizerModeDesc provided by an engine (i.e. returned
 * by the speech_central_available_recognizers() function).
 *
 * Engine creation is described in the documentation for the
 * #SpeechCentral object.
 *
 * See: #SpeechCentral
 * See: #SpeechRecognitionSpeakerManager
 * See: #SpeechRecognitionSpeakerProfile
 */

#include <speech/recognition/speech_recognition_recognizer_mode_desc.h>


GType
speech_recognition_recognizer_mode_desc_get_type(void)
{
    printf("speech_recognition_recognizer_mode_desc_get_type called.\n");
}


SpeechRecognitionRecognizerModeDesc *
speech_recognition_recognizer_mode_desc_new(void)
{
    printf("speech_recognition_recognizer_mode_desc_new called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_new_with_locale:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 * @locale: the language locale.
 * @dictation_grammar_supported: indicates whether dictation is supported.
 *
 * Initialize a #SpeechRecognitionRecognizerModeDesc given a locale and
 * the dictation flag. The speaker profiles array and other features
 * are all null.
 *
 * Returns:
 */

SpeechRecognitionRecognizerModeDesc *
speech_recognition_recognizer_mode_desc_new_with_locale(
                                 gchar *locale,
                                 SpeechTristate dictation_grammar_supported)
{
    printf("speech_recognition_recognizer_mode_desc_new_with_locale called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_new_with_state:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 * @engine_name: the recognizer's engine name.
 * @mode_name: the recognizer's mode name.
 * @locale: the language locale.
 * @running: indicates if this engine is running.
 * @dictation_grammar_supported: indicates whether dictation is supported.
 * @profiles: array of the recognizer's speaker profiles.
 *
 * Initialize a fully-specified descriptor.
 *
 * Returns:
 */

SpeechRecognitionRecognizerModeDesc *
speech_recognition_recognizer_mode_desc_new_with_state(
                                 gchar *engine_name,
                                 gchar *mode_name,
                                 gchar *locale,
                                 SpeechTristate running,
                                 SpeechTristate dictation_grammar_supported,
                                 SpeechRecognitionSpeakerProfile *profiles[])
{
    printf("speech_recognition_recognizer_mode_desc_new_with_state called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_add_speaker_profile:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 * @profile: the speaker profile to add.
 *
 * Add a speaker profile to the #SpeechRecognitionSpeakerProfile array.
 */

void
speech_recognition_recognizer_mode_desc_add_speaker_profile(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechRecognitionSpeakerProfile *profile)
{
    printf("speech_recognition_recognizer_mode_desc_add_speaker_profile called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_get_speaker_profiles:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 * @profiles: return the list of speaker profiles known to this mode of this 
 *            recognition engine.
 *
 * Returns the list of speaker profiles known to this mode of this
 * recognition engine. Returns null if speaker training is not
 * supported (#SpeechRecognitionSpeakerManager not implemented). Returns
 * zero-length array if speaker training is supported but no
 * speaker profiles have been constructed yet.
 *
 * The list of speaker profiles is the same as returned by the the
 * speech_recognition_speaker_manager_list_known_speakers() function of
 * #SpeechRecognitionSpeakerManager if this engine is running.
 *
 * See: speech_recognition_speaker_manager_list_known_speakers()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the application does not have
 *		#accessSpeakerProfiles permission.</listitem>
 * </itemizedlist>
 */

SpeechStatus 
speech_recognition_recognizer_mode_desc_get_speaker_profiles(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechRecognitionSpeakerProfile *profiles[])
{
    printf("speech_recognition_recognizer_mode_desc_get_speaker_profiles called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_get_speaker_profiles_impl:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 *
 * Version of speech_recognition_recognizer_mode_desc_get_speaker_profiles()
 * that performs the operation. This function can be overridden in 
 * sub-classes. However, application can only call the 
 * speech_recognition_recognizer_mode_desc_get_speaker_profiles() function
 * which does a security check.
 *
 * See: speech_recognition_recognizer_mode_desc_get_speaker_profiles()
 *
 * Returns: the list of speaker profiles known to this mode of this 
 *          recognition engine.
 */

SpeechRecognitionSpeakerProfile **
speech_recognition_recognizer_mode_desc_get_speaker_profiles_impl(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc)
{
    printf("speech_recognition_recognizer_mode_desc_get_speaker_profiles_impl called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 *
 * Test whether this engine mode provides a
 * #SpeechRecognitionDictationGrammar. The value may be %TRUE, %FALSE or 
 * %NULL. A %NULL value means "don't care".
 *
 * Returns: an indication of whether this engine mode provides a
 *	   #SpeechRecognitionDictationGrammar.
 */

SpeechTristate
speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc)
{
    printf("speech_recognition_recognizer_mode_desc_is_dictation_grammar_supported called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_set_dictation_grammar_supported:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 * @dictation_grammar_supported: indicates whether dictation is supported.
 *
 * Set the #dictation_grammar_supported parameter. The value may be %TRUE,
 * %FALSE or %NULL. A %NULL value means "don't care".
 */

void
speech_recognition_recognizer_mode_desc_set_dictation_grammar_supported(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechTristate dictation_grammar_supported)
{
    printf("speech_recognition_recognizer_mode_desc_set_dictation_grammar_supported called.\n");
}


/**
 * speech_recognition_recognizer_mode_desc_set_speaker_profiles:
 * @mode_desc: the #SpeechRecognitionRecognizerModeDesc object that this
 *             operation will be applied to.
 * @speakers: array of the recognizer's speaker profiles.
 *
 * Set the list of speaker profiles. May be %NULL.
 */

void
speech_recognition_recognizer_mode_desc_set_speaker_profiles(
                                 SpeechRecognitionRecognizerModeDesc *mode_desc,
                                 SpeechRecognitionSpeakerProfile *speakers[])
{
    printf("speech_recognition_recognizer_mode_desc_set_speaker_profiles called.\n");
}
