
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Enables control of the properties of a #SpeechRecognitionRecognizer. The
 * #SpeechRecognitionRecognizerProperties object is obtained by calling the
 * speech_engine_get_engine_properties() function of the 
 * #SpeechRecognitionRecognizer (inherited from the #SpeechEngine object).
 *
 * Because #SpeechRecognitionRecognizerProperties extends the 
 * #SpeechEngineProperties object, it inherits the following behavior 
 * (described in detail in the #SpeechEngineProperties documentation):
 *
 * <itemizedlist>
 *   <listitem>Each property has a get and set function.</listitem>
 *   <listitem>Engines may ignore changes to properties or apply maximum 
 *       and minimum limits. If an engine does not apply a property change 
 *       request, the set function returns a 
 *       #SPEECH_ILLEGAL_ARGUMENT_EXCEPTION.</listitem>
 *   <listitem>Calls to set functions may be asynchronous (they may return 
 *       before the property change takes effect). The #SpeechEngine will 
 *       apply a change as soon as possible.</listitem>
 *   <listitem>The get functions return the current setting - not a pending 
 *       value.</listitem>
 *   <listitem>A #SpeechPropertyChangeListener may be attached to receive
 *	 property change events.</listitem>
 *   <listitem>Where appropriate, property settings are persistent across 
 *       sessions.</listitem>
 * </itemizedlist>
 *
 * <title>Per-Speaker Properties</title>
 *
 * For recognizers that maintain speaker data (recognizers that implement 
 * the #SpeechRecognitionSpeakerManager object) the 
 * #SpeechRecognitionRecognizerProperties should be stored and loaded as 
 * part of the speaker data. Thus, when the current speaker is changed 
 * through the #SpeechRecognitionSpeakerManager object, the properties of 
 * the new speaker should be loaded.
 *
 * See: #SpeechEngineProperties
 * See: #SpeechRecognitionSpeakerManager
 */

#include <speech/recognition/speech_recognition_recognizer_properties.h>


GType
speech_recognition_recognizer_properties_get_type(void)
{
    printf("speech_recognition_recognizer_properties_get_type called.\n");
}


SpeechRecognitionRecognizerProperties *
speech_recognition_recognizer_properties_new(void)
{
    printf("speech_recognition_recognizer_properties_new called.\n");
}


/**
 * speech_recognition_recognizer_properties_get_complete_timeout:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the "CompleteTimeout" property.
 *
 * See: speech_recognition_recognizer_properties_set_complete_timeout()
 *
 * Returns: the "CompleteTimeout" property.
 */

float
speech_recognition_recognizer_properties_get_complete_timeout(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_get_complete_timeout called.\n");
}


/**
 * speech_recognition_recognizer_properties_get_confidence_level:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the recognizer's "ConfidenceLevel" property.
 *
 * See: speech_recognition_recognizer_properties_set_confidence_level()
 *
 * Returns: the recognizer's "ConfidenceLevel" property.
 */

float
speech_recognition_recognizer_properties_get_confidence_level(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_get_confidence_level called.\n");
}


/**
 * speech_recognition_recognizer_properties_get_incomplete_timeout:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the "IncompleteTimeout" property.
 *
 * See: speech_recognition_recognizer_properties_set_incomplete_timeout()
 *
 * Returns: the "IncompleteTimeout" property.
 */

float
speech_recognition_recognizer_properties_get_incomplete_timeout(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_get_incomplete_timeout called.\n");
}


/**
 * speech_recognition_recognizer_properties_get_num_result_alternatives:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the "NumResultAlternatives" property.
 *
 * See: speech_recognition_recognizer_properties_set_num_result_alternatives()
 *
 * Returns: the "NumResultAlternatives" property.
 */

int
speech_recognition_recognizer_properties_get_num_result_alternatives(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_get_num_result_alternatives called.\n");
}


/**
 * speech_recognition_recognizer_properties_get_sensitivity:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the "Sensitivity" property.
 *
 * See: speech_recognition_recognizer_properties_set_sensitivity()
 *
 * Returns: the "Sensitivity" property.
 */

float
speech_recognition_recognizer_properties_get_sensitivity(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_get_sensitivity called.\n");
}


/**
 * speech_recognition_recognizer_properties_get_speed_vs_accuracy:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the "SpeedVsAccuracy" property.
 *
 * See: speech_recognition_recognizer_properties_set_speed_vs_accuracy()
 *
 * Returns: the "SpeedVsAccuracy" property.
 */

float
speech_recognition_recognizer_properties_get_speed_vs_accuracy(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_get_speed_vs_accuracy called.\n");
}


/**
 * speech_recognition_recognizer_properties_is_result_audio_provided:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the "ResultAudioProvided" property.
 *
 * See: speech_recognition_recognizer_properties_set_result_audio_provided()
 *
 * Returns: the "ResultAudioProvided" property.
 */

gboolean
speech_recognition_recognizer_properties_is_result_audio_provided(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_is_result_audio_provided called.\n");
}


/**
 * speech_recognition_recognizer_properties_is_training_provided:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 *
 * Get the "TrainingProvided" property.
 *
 * See: speech_recognition_recognizer_properties_set_training_provided()
 *
 * Returns: the "TrainingProvided" property.
 */

gboolean
speech_recognition_recognizer_properties_is_training_provided(
                              SpeechRecognitionRecognizerProperties *properties)
{
    printf("speech_recognition_recognizer_properties_is_training_provided called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_complete_timeout:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @timeout: the #CompleteTimeout property in seconds.
 *
 * Set the #CompleteTimeout property in seconds. This timeout value 
 * determines the length of silence required following user speech 
 * before the recognizer finalizes a result (with an
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_ACCEPTED or 
 * #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event). The complete 
 * timeout is used when the speech is a complete match an active 
 * #SpeechRecognitionRuleGrammar. By contrast, the incomplete timeout 
 * is used when the speech is an incomplete match to an active grammar.
 *
 * A long complete timeout value delays the result completion and
 * therefore makes the computer's response slow. A short complete
 * timeout may lead to an utterance being broken up inappropriately.
 * Reasonable complete timeout values are typically in the range of
 * 0.3 seconds to 1.0 seconds.
 *
 * See: speech_recognition_recognizer_properties_get_complete_timeout()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *		limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_complete_timeout(
                              SpeechRecognitionRecognizerProperties *properties,
                              float timeout)
{
    printf("speech_recognition_recognizer_properties_set_complete_timeout called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_confidence_level:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @confidence_level: the recognizer's "ConfidenceLevel" property.
 *
 * Set the recognizer's "ConfidenceLevel" property. The confidence
 * level value can very between 0.0 and 1.0. A value of 0.5 is the
 * default for the recognizer. A value of 1.0 requires the recognizer
 * to have maximum confidence in its results or otherwise reject them.
 * A value of 0.0 requires only low confidence so fewer results are
 * rejected.
 *
 * See: speech_recognition_recognizer_properties_get_confidence_level()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_confidence_level(
                             SpeechRecognitionRecognizerProperties *properties,
                             float confidence_level)
{
    printf("speech_recognition_recognizer_properties_set_confidence_level called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_incomplete_timeout:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @timeout: the #IncompleteTimeout property in seconds.
 *
 * Set the #IncompleteTimeout property in seconds. The timeout value 
 * determines the required length of silence following user speech after 
 * which a recognizer finalizes a result.
 *
 * The incomplete timeout applies when the speech prior to the silence is 
 * an incomplete match of the active #SpeechRecognitionRuleGrammars. In 
 * this case, once the timeout is triggered, the partial result is rejected
 * (with a #SPEECH_RECOGNITION_RESULT_EVENT_RESULT_REJECTED event).
 *
 * The incomplete timeout also applies when the speech prior to
 * the silence is a complete match of an active grammar, but
 * where it is possible to speak further and still match the
 * grammar.  For example, in a grammar for digit sequences for
 * telephone numbers it might be legal to speak either 7 or 10 digits.
 * If the user pauses briefly after speaking 7 digits then the
 * incomplete timeout applies because the user may then continue
 * with a further 3 digits.
 *
 * By contrast, the complete timeout is used when the speech is a
 * complete match to an active #SpeechRecognitionRuleGrammar and no
 * further words can be spoken.
 *
 * A long complete timeout value delays the result completion
 * and therefore makes the computer's response slow.  A short
 * incomplete timeout may lead to an utterance being broken up
 * inappropriately.
 *
 * The incomplete timeout is usually longer than the complete timeout
 * to allow users to pause mid-utterance (for example, to breathe).
 *
 * See: speech_recognition_recognizer_properties_get_incomplete_timeout()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_incomplete_timeout(
                             SpeechRecognitionRecognizerProperties *properties,
                             float timeout)
{
    printf("speech_recognition_recognizer_properties_set_incomplete_timeout called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_num_result_alternatives:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @num: the "NumResultAlternatives" property.
 *
 * Set the "NumResultAlternatives" property. This property indicates
 * the preferred maximum number of N-best alternatives in
 * #SpeechRecognitionFinalDictationResult and 
 * #SpeechRecognitionFinalRuleResult objects. A value of 0 or 1 
 * indicates that the application does not want alternatives; that is, 
 * it only wants the best guess.
 *
 * Recognizers are not required to provide this number of
 * alternatives for every result and the number of alternatives
 * may vary from result to result.  Recognizers should only provide
 * alternative tokens which are considered reasonable guesses: that
 * is, the alternatives should be above the #ConfidenceLevel set 
 * through this object (unless the #SpeechRecognitionResult is rejected).
 *
 * Providing alternatives requires additional computing power.
 * Applications should only request the number of alternatives
 * that they are likely to use.
 *
 * See: speech_recognition_recognizer_properties_get_num_result_alternatives()
 * See: speech_recognition_final_dictation_result_get_alternative_tokens()
 * See: speech_recognition_final_rule_result_get_alternative_tokens()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_num_result_alternatives(
                             SpeechRecognitionRecognizerProperties *properties,
                             int num)
{
    printf("speech_recognition_recognizer_properties_set_num_result_alternatives called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_result_audio_provided:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @audio: the "ResultAudioProvided" property.
 *
 * Set the "ResultAudioProvided" property. If set to true, the recognizer 
 * is requested to provide audio with #SpeechRecognitionFinalResult objects.
 * If available, the audio is provided through the 
 * speech_recognition_final_result_get_audio() and
 * speech_recognition_final_result_get_audio_for_token() functions 
 * of the #SpeechRecognitionFinalResult object.
 *
 * Some recognizers that can provide audio for a
 * #SpeechRecognitionFinalResult cannot provide audio for all results.
 * Applications need test audio available individually for results.
 * (For example, a recognizer might only provide audio for dictation
 * results.)
 *
 * A #SpeechRecognitionRecognizer that does not provide audio for any
 * results throws a IllegalArgumentException when an app attempts
 * to set the value to %TRUE.
 *
 * See: speech_recognition_recognizer_properties_is_result_audio_provided()
 * See: speech_recognition_final_result_get_audio()
 * See: speech_recognition_final_result_get_audio_for_token()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_result_audio_provided(
                             SpeechRecognitionRecognizerProperties *properties,
                             gboolean audio)
{
    printf("speech_recognition_recognizer_properties_set_result_audio_provided called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_sensitivity:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @sensitivity: the "Sensitivity" property.
 *
 * Set the "Sensitivity" property. The sensitivity level can vary
 * between 0.0 and 1.0. A value of 0.5 is the default for the
 * recognizer. A value of 1.0 gives maximum sensitivity and makes the
 * recognizer sensitive to quiet input but more sensitive to noise.
 * A value of 0.0 gives minimum sensitivity requiring the user to
 * speak loudly and making the recognizer less sensitive to background
 * noise.
 *
 * <title>Note</title>: some recognizers set the gain automatically during use,
 *              or through a setup "Wizard". On these recognizers the
 *		sensitivity adjustment should be used only in extreme
 *		cases where the automatic settings are not adequate.
 *
 * See: speech_recognition_recognizer_properties_get_sensitivity()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_sensitivity(
                             SpeechRecognitionRecognizerProperties *properties,
                             float sensitivity)
{
    printf("speech_recognition_recognizer_properties_set_sensitivity called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_speed_vs_accuracy:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @speed_vs_accuracy: the "SpeedVsAccuracy" property.
 *
 * Set the "SpeedVsAccuracy" property. The default value is 0.5 and
 * is the factory-defined compromise of speed and accuracy. A value
 * of 0.0 minimizes response time. A value of 1.0 maximizes
 * recognition accuracy.
 *
 * Why are speed and accuracy a trade-off?  A recognizer determines
 * what a user says by testing different possible sequence of words
 * (with legal sequences being defined by the active grammars). If the
 * recognizer tests more sequences it is more likely to find the
 * correct sequence, but there is additional processing so it is
 * slower. Increasing grammar complexity and decreasing the computer
 * power both make this trade-off more important. Conversely, a
 * simpler grammar or more powerful computer make the trade-off less
 * important.
 *
 * See: speech_recognition_recognizer_properties_get_speed_vs_accuracy()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_speed_vs_accuracy(
                           SpeechRecognitionRecognizerProperties *properties,
                           float speed_vs_accuracy)
{
    printf("speech_recognition_recognizer_properties_set_speed_vs_accuracy called.\n");
}


/**
 * speech_recognition_recognizer_properties_set_training_provided:
 * @properties: the #SpeechRecognitionRecognizerProperties object tha this
 *              operation will be applied to.
 * @training_provided: the #TrainingProvided property.
 *
 * Set the #TrainingProvided property. If %TRUE, request a recognizer to 
 * provide training information for
 * #SpeechRecognitionFinalResult objects through the
 * speech_recognition_final_result_token_correction() function.
 *
 * Not all recognizers support training. Also, recognizers that do
 * support training are not required to support training data for all
 * results. For example, a recognizer might only produce training data
 * with dictation results. A #SpeechRecognitionRecognizer that does not
 * support training raises a IllegalArgumentException when an
 * application attempts to set the value to %TRUE.
 *
 * See: speech_recognition_recognizer_properties_is_training_provided()
 * See: speech_recognition_final_result_token_correction()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_PROPERTY_VETO_EXCEPTION if the recognizer rejects or
 *              limits the new value.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_recognition_recognizer_properties_set_training_provided(
                            SpeechRecognitionRecognizerProperties *properties,
                            gboolean training_provided)
{
    printf("speech_recognition_recognizer_properties_set_training_provided called.\n");
}
