
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * A token (usually a word) contained by a #SpeechRecognitionResult 
 * representing something heard by a recognizer. For a result of a 
 * #SpeechRecognitionRuleGrammar a #SpeechRecognitionResultToken
 * contains the same string as the defining #SpeechRecognitionRuleToken 
 * in the #SpeechRecognitionRuleGrammar. For a result of a 
 * #SpeechRecognitionDictationGrammar, the tokens are defined by the
 * recognizer.
 *
 * For any result, best guess finalized tokens are obtained from the
 * speech_recognition_result_get_best_token() and 
 * speech_recognition_result_get_best_tokens() functions of 
 * the #SpeechRecognitionResult object. For a finalized result matching a
 * #SpeechRecognitionRuleGrammar or a finalized result matching a
 * #SpeechRecognitionDictationGrammar alternative guesses are available 
 * through the speech_recognition_final_rule_result_get_alternative_tokens()
 * and speech_recognition_final_dictation_result_get_alternative_tokens()
 * functions of the #SpeechRecognitionFinalRuleResult and 
 * #SpeechRecognitionFinalDictationResult objects respectively.
 *
 * The #SpeechRecognitionResultToken provides the following information:
 *
 * <itemizedlist>
 *   <listitem>Required: Spoken-form text</listitem>
 *   <listitem>Required: reference to the #SpeechRecognitionResult that 
 *       contains this token</listitem>
 *   <listitem>Optional: Start and end time</listitem>
 *   <listitem>Dictation only: Written-form text</listitem>
 *   <listitem>Dictation only: Presentation hints (capitalization and 
 *       spacing)</listitem>
 * </itemizedlist>
 *
 * <title>Spoken vs. Written Form</title>
 *
 * The distinction between spoken and written forms of a
 * #SpeechRecognitionResultToken applies only to results for a
 * #SpeechRecognitionDictationGrammar. For a result matching a
 * #SpeechRecognitionRuleGrammar, the written and spoken forms are identical.
 *
 * The spoken form is a printable string that indicates what the recognizer
 * heard the user say. In a dictation application, the spoken form is
 * typically used when displaying unfinalized tokens.
 *
 * The written form is a printable string that indicates how the recognizer
 * thinks the token should be printed in text. In a dictation application,
 * the written form is typically used when displaying finalized tokens.
 *
 * For example, in English, a recognizer might return "twenty" for the
 * spoken form and "20" for the written form.
 *
 * Recognizers perform the conversion from spoken to written form on a
 * per-token basis. For example, "nineteen thirty eight" is three tokens.
 * The written form would also be three tokens: "19 30 8". Applications may
 * use additional processing to convert such token sequences to "1938".
 *
 * For some #SpeechRecognitionResultTokens, the written form is a single UTF8
 * character. Amongst these are the following important formatting
 * characters (shown here as spoken form for US English, written form as
 * a UTF8 character number):
 *
 * <itemizedlist>
 *   <listitem>New line character is "\u005Cu000A" and equals the static 
 *       string NEW_LINE.
 *	 In English, it might be spoken as "new line", "line break" or 
 *       similar.</listitem>
 *   <listitem>New paragraph character is "\u005Cu2029" and equals the 
 *       static string NEW_PARAGRAPH.
 *	 In English, it might be spoken as "new paragraph", "start paragraph"
 *	 or something similar.</listitem>
 * </itemizedlist>
 *
 * The following are examples of punctuation characters given with sample
 * spoken forms and the corresponding written form. The spoken form may vary
 * between recognizers and one recognizer may even allow one punctuation
 * character to be spoken multiple ways. Also the set of characters may be
 * engine-specific and language-specific.
 *
 * <itemizedlist>
 *   <listitem>"space bar" -&gt; "\u0020" (u0020)</listitem>
 *   <listitem>"exclamation mark", "exclamation point" -&gt; "\u0021" (u0021)
 *       </listitem>
 *   <listitem>"open quote", "begin quote", "open-\"" -&gt; "\u005C\u0022" 
 *       (u0022) (single quote char)</listitem>
 *   <listitem>"dash", "hyphen", "minus" -&gt; "\u002D" (u002D)</listitem>
 *   <listitem>"pound sign" -&gt; "\u00A3" (u00A3)</listitem>
 *   <listitem>"yen sign" -&gt; "\u00A5" (u00A5)</listitem>
 * </itemizedlist>
 *
 *
 * <title>Presentation Hints</title>
 *
 * Note: results for rule grammars do not provide presentation hints. Default
 * values are returns for both SpacingHint and CapitalizationHint.
 *
 * Applications use the presentation hints as a guide to rendering the
 * written-form tokens into complete strings. The two hints are SpacingHint
 * and CapitalizationHint.
 *
 * SpacingHint is a long with several flags indicating how the token should
 * be spaced.
 *
 * <itemizedlist>
 *   <listitem>SpacingHint==#SPEECH_RECOGNITION_RESULT_TOKEN_SEPARATE
 *	 (value of 0) when all the flags are false.
 *	 The token should be surrounding by preceding and following spaces.
 *       </listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS:
 *	 Flag is %TRUE if the token should be attached to the previous token:
 *	 i.e. no space between this token and the previous token.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_FOLLOWING:
 *	 Flag is %TRUE if the token should be attached to the following token:
 *	 i.e. no space between this token and the following token.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP:
 *	 If this flag is %TRUE and if the 
 *       #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP flag for a previous 
 *       and/or following token is true, then override the other spacing 
 *       flags and put no space between the tokens in the group.</listitem>
 * </itemizedlist>
 *
 * The #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP is the least obvious 
 * flag. In English, a common use of this flag is for sequence of digits. 
 * If the user says four tokens "3" "point" "1" "4" (point='.'), and these 
 * tokens all have the #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP flag 
 * set, then they are joined. The presentation string is "3.14". The name 
 * of the flag implies that tokens in the same "group" should have no 
 * spaces between them.
 *
 * "previous" means the previously spoken token in the sequence of tokens -
 * that is, previous in time.  For left-to-right languages (e.g. English,
 * German) #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS means left 
 * attachment - no space to the left. For right-to-left languages (e.g. 
 * Arabic, Hebrew) #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS means 
 * right attachment - no space to the right. The converse is true for 
 * #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_FOLLOWING.
 *
 * The spacing flags are OR'ed. A legal value might be
 * (#SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS | 
 *  #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_FOLLOWING). The 
 *  #SPEECH_RECOGNITION_RESULT_TOKEN_SEPARATE value is 0
 * (zero). A flag can be tested by the following code:
 *
 *   // if attach previous ...
 *   if ((speech_recognition_result_token_get_spacing_hint(token) & 
 *                                      ResultToken.ATTACH_PREVIOUS) != 0)
 *	...
 *
 * capitalizationHint indicates how the written form of the following token 
 * should be capitalized. The options are
 *
 * <itemizedlist>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_TOKEN_CAP_AS_IS:
 *	As-is indicates the capitalization of the spoken form of the following
 *	should not be changed.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_TOKEN_CAP_FIRST:
 *	Capitalize first character of the spoken form of the following 
 *      token.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_TOKEN_UPPERCASE: All uppercase 
 *       following token.</listitem>
 *   <listitem>#SPEECH_RECOGNITION_RESULT_TOKEN_LOWERCASE: All lowercase 
 *       following token.</listitem>
 * </itemizedlist>
 *
 * <title>Null Written Form</title>
 *
 * Some spoken directives to recognizers produce tokens that have no printable
 * form. These tokens return null for the written form. Typically, these
 * directives give explicit capitalization or spacing hints. The spoken forms
 * of these tokens should be non-null (to allow the application to provide
 * appropriate feedback to a user. Example directives for English might
 * include:
 *
 * <itemizedlist>
 *   <listitem>"Capitalize next", "Cap next", "Upper case"</listitem>
 *   <listitem>"Lowercase"</listitem>
 *   <listitem>"Uppercase"</listitem>
 *   <listitem>"No space"</listitem>
 * </itemizedlist>
 *
 * For these tokens, the interpretation of the capitalization and spacing
 * hints is specialized. If the spacing hint differs from the default,
 * #SPEECH_RECOGNITION_RESULT_TOKEN_SEPARATE, it overrides the spacing hint
 * of the next non-null token. If the capitalization hint differs from the 
 * default, #SPEECH_RECOGNITION_RESULT_TOKEN_CAP_AS_IS, it overrides the 
 * capitalization hints of previous non-null token (which in fact applies 
 * to the following token also).
 *
 * <title>Example</title>
 *
 * This example shows how a string of result tokens should be processed to
 * produce a single printable string. The following is a sequence of tokens
 * in a #SpeechRecognitionFinalDictationResult shown as spoken form, written
 * form, and spacing and capitalization hints.
 *
 * <orderedlist>
 *   <listitem>"NEW_LINE", "\u005Cu000A", SEPARATE, CAP_FIRST</listitem>
 *   <listitem>"the", "the", SEPARATE, CAP_AS_IS</listitem>
 *   <listitem>"UPPERCASE_NEXT", "", SEPARATE, UPPERCASE</listitem>
 *   <listitem>"index", "index", SEPARATE, CAP_AS_IS</listitem>
 *   <listitem>"is", "is" SEPARATE, CAP_AS_IS</listitem>
 *   <listitem>"seven", "7", ATTACH_GROUP, CAP_AS_IS</listitem>
 *   <listitem>"-", "-", ATTACH_GROUP, CAP_AS_IS</listitem>
 *   <listitem>"two", "2", ATTACH_GROUP, CAP_AS_IS</listitem>
 *   <listitem>"period", ".", ATTACH_PREVIOUS, CAP_FIRST</listitem>
 * </orderedlist>
 *
 * that could be converted to "\nThe INDEX is 7-2."
 *
 * See: #SpeechRecognitionResult
 * See: #SpeechRecognitionFinalResult
 * See: #SpeechRecognitionFinalDictationResult
 * See: #SpeechRecognitionFinalRuleResult
 */

#include <speech/recognition/speech_recognition_result_token.h>


GType
speech_recognition_result_token_get_type(void)
{
    printf("speech_recognition_result_token_get_type called.\n");
}


SpeechRecognitionResultToken *
speech_recognition_result_token_new(void)
{
    printf("speech_recognition_result_token_new called.\n");
}


/**
 * speech_recognition_result_token_get_capitalization_hint:
 * @token: the #SpeechRecognitionResultToken object that this operation will
 *         be applied to.
 *
 * Get the capitalization hint. (See description above).
 * Values are: #SPEECH_RECOGNITION_RESULT_TOKEN_CAP_AS_IS (the default),
 * #SPEECH_RECOGNITION_RESULT_TOKEN_CAP_FIRST,
 * #SPEECH_RECOGNITION_RESULT_TOKEN_UPPERCASE,
 * #SPEECH_RECOGNITION_RESULT_TOKEN_LOWERCASE.
 * Tokens from a #SpeechRecognitionRuleGrammar result always return
 * #SPEECH_RECOGNITION_RESULT_TOKEN_CAP_AS_IS.
 */

int
speech_recognition_result_token_get_capitalization_hint(
                                         SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_token_get_capitalization_hint called.\n");
}


/**
 * speech_recognition_result_token_get_result:
 * @token: the #SpeechRecognitionResultToken object that this operation will
 *         be applied to.
 *
 * Returns: a reference to the result that contains this token.
 */

SpeechRecognitionResult *
speech_recognition_result_token_get_result(SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_token_get_result called.\n");
}


/**
 * speech_recognition_result_token_get_spacing_hint:
 * @token: the #SpeechRecognitionResultToken object that this operation will
 *         be applied to.
 *
 * Get the spacing hints. (See description above). The value equals
 * #SPEECH_RECOGNITION_RESULT_TOKEN_SEPARATE (the default) if the 
 * token should be presented with surrounding spaces. Otherwise any 
 * or all of the following flags can be %TRUE: 
 * #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_FOLLOWING,
 * #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_PREVIOUS, 
 * #SPEECH_RECOGNITION_RESULT_TOKEN_ATTACH_GROUP.
 * Tokens from a #SpeechRecognitionRuleGrammar result always return
 * #SPEECH_RECOGNITION_RESULT_TOKEN_SEPARATE.
 *
 * Returns: the spacing hints.
 */

int
speech_recognition_result_token_get_spacing_hint(
                                         SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_token_get_spacing_hint called.\n");
}


/**
 * speech_recognition_result_token_get_spoken_text:
 * @token: the #SpeechRecognitionResultToken object that this operation will
 *         be applied to.
 *
 * Get the spoken text of a token. In dictation, the spoken form
 * is typically used when displaying unfinalized tokens.
 * The difference between spoken and written forms is discussed above.
 *
 * Returns: the spoken text of a token.
 */

gchar *
speech_recognition_result_token_get_spoken_text(
                                         SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_token_get_spoken_text called.\n");
}


/**
 * speech_recognition_result_token_get_start_time:
 * @token: the #SpeechRecognitionResultToken object that this operation will
 *         be applied to.
 *
 * Get the approximate start time for the token.
 * The value is matched against the current system time (in
 * milliseconds).
 *
 * The start time of a token is always greater than or equal to the
 * the end time of a preceding token. The values will be different
 * if the tokens are separated by a pause.
 *
 * Returns -1 if timing information is not available. Not all
 * recognizers provide timing information. Timing information is not
 * usually available for unfinalized or finalized tokens in a
 * #SpeechRecognitionResult that is not yet finalized. Even if timing
 * information is available for the best-guess tokens, it might not
 * be available for alternative tokens.
 *
 * Returns: the approximate start time for the token.
 */

long
speech_recognition_result_token_get_start_time(
                                         SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_token_get_start_time called.\n");
}


/**
 * speech_recognition_result_token_get_end_time:
 * @token: the #SpeechRecognitionResultToken object that this operation will
 *         be applied to.
 *
 * Get the approximate end time for the token.
 * The value is matched against the current system time (in
 * milliseconds).
 *
 * The end time of a token is always less than or equal to the
 * the end time of a preceding token. The values will be different
 * if the tokens are separated by a pause.
 *
 * Returns -1 if timing information is not available. Not all
 * recognizers provide timing information. Timing information is not
 * usually available for unfinalized or finalized tokens in a
 * #SpeechRecognitionResult that is not yet finalized. Even if timing
 * information is available for the best-guess tokens, it might not
 * be available for alternative tokens.
 *
 * Returns: the approximate end time for the token.
 */

long
speech_recognition_result_token_get_end_time(
                                         SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_token_get_end_time called.\n");
}


/**
 * speech_recognition_result_token_get_written_text:
 * @token: the #SpeechRecognitionResultToken object that this operation will
 *         be applied to.
 *
 * Get the written form of a spoken token. Spoken and written forms
 * are discussed above.
 */

gchar *
speech_recognition_result_token_get_written_text(
                                         SpeechRecognitionResultToken *token)
{
    printf("speech_recognition_result_token_get_written_text called.\n");
}
