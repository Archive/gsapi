
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Extends the set of audio event of an engine for a recognizer by adding a
 * audio level event.
 */

#include <speech/recognition/speech_recognition_recognizer_audio_listener.h>


GType
speech_recognition_recognizer_audio_listener_get_type(void)
{
    printf("speech_recognition_recognizer_audio_listener_get_type called.\n");
}


SpeechRecognitionRecognizerAudioListener *
speech_recognition_recognizer_audio_listener_new(void)
{
    printf("speech_recognition_recognizer_audio_listener_new called.\n");
}


/**
 * speech_recognition_recognizer_audio_listener_audio_level:
 * @listener: the #SpeechRecognitionRecognizerAudioListener object that this
 *            operation will be applied to.
 * @e: the audio event.
 *
 * #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL event has 
 * occurred indicating a change in the current volume of the audio 
 * input to a recognizer.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_AUDIO_LEVEL
 */

void
speech_recognition_recognizer_audio_listener_audio_level(
                            SpeechRecognitionRecognizerAudioListener *listener,
                            SpeechRecognitionRecognizerAudioEvent *e)
{
    printf("speech_recognition_recognizer_audio_listener_audio_level called.\n");
}


/**
 * speech_recognition_recognizer_audio_listener_speech_started:
 * @listener: the #SpeechRecognitionRecognizerAudioListener object that this
 *            operation will be applied to.
 * @e: the audio event.
 *
 * The recognizer has detected a possible start of speech.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STARTED
 */

void
speech_recognition_recognizer_audio_listener_speech_started(
                            SpeechRecognitionRecognizerAudioListener *listener,
                            SpeechRecognitionRecognizerAudioEvent *e)
{
    printf("speech_recognition_recognizer_audio_listener_speech_started called.\n");
}


/**
 * speech_recognition_recognizer_audio_listener_speech_stopped:
 * @listener: the #SpeechRecognitionRecognizerAudioListener object that this
 *            operation will be applied to.
 * @e: the audio event.
 *
 * The recognizer has detected a possible end of speech.
 *
 * See: #SPEECH_RECOGNITION_RECOGNIZER_AUDIO_EVENT_SPEECH_STOPPED
 */

void
speech_recognition_recognizer_audio_listener_speech_stopped(
                            SpeechRecognitionRecognizerAudioListener *listener,
                            SpeechRecognitionRecognizerAudioEvent *e)
{
    printf("speech_recognition_recognizer_audio_listener_speech_stopped called.\n");
}
