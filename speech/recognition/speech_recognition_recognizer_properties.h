
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_RECOGNITION_RECOGNIZER_PROPERTIES_H__
#define __SPEECH_RECOGNITION_RECOGNIZER_PROPERTIES_H__

#include <speech/speech_engine_properties.h>
#include <speech/recognition/speech_recognition_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_RECOGNITION_RECOGNIZER_PROPERTIES              (speech_recognition_recognizer_properties_get_type())
#define SPEECH_RECOGNITION_RECOGNIZER_PROPERTIES(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_PROPERTIES, SpeechRecognitionRecognizerProperties))
#define SPEECH_RECOGNITION_RECOGNIZER_PROPERTIES_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_PROPERTIES, SpeechRecognitionRecognizerPropertiesClass))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_PROPERTIES(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_PROPERTIES))
#define SPEECH_IS_RECOGNITION_RECOGNIZER_PROPERTIES_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_RECOGNITION_RECOGNIZER_PROPERTIES))
#define SPEECH_RECOGNITION_RECOGNIZER_PROPERTIES_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_RECOGNITION_RECOGNIZER_PROPERTIES, SpeechRecognitionRecognizerPropertiesClass))

typedef struct _SpeechRecognitionRecognizerProperties            SpeechRecognitionRecognizerProperties;
typedef struct _SpeechRecognitionRecognizerPropertiesClass       SpeechRecognitionRecognizerPropertiesClass;

struct _SpeechRecognitionRecognizerProperties {
    SpeechEngineProperties properties;
};

struct _SpeechRecognitionRecognizerPropertiesClass {
    SpeechEnginePropertiesClass parent_class;
};


GType 
speech_recognition_recognizer_properties_get_type(void);

SpeechRecognitionRecognizerProperties * 
speech_recognition_recognizer_properties_new(void);

float 
speech_recognition_recognizer_properties_get_complete_timeout(
                     SpeechRecognitionRecognizerProperties *properties);

float 
speech_recognition_recognizer_properties_get_confidence_level(
                     SpeechRecognitionRecognizerProperties *properties);

float 
speech_recognition_recognizer_properties_get_incomplete_timeout(
                     SpeechRecognitionRecognizerProperties *properties);

int 
speech_recognition_recognizer_properties_get_num_result_alternatives(
                     SpeechRecognitionRecognizerProperties *properties);

float 
speech_recognition_recognizer_properties_get_sensitivity(
                     SpeechRecognitionRecognizerProperties *properties);

float 
speech_recognition_recognizer_properties_get_speed_vs_accuracy(
                     SpeechRecognitionRecognizerProperties *properties);

gboolean 
speech_recognition_recognizer_properties_is_result_audio_provided(
                     SpeechRecognitionRecognizerProperties *properties);

gboolean 
speech_recognition_recognizer_properties_is_training_provided(
                     SpeechRecognitionRecognizerProperties *properties);

SpeechStatus 
speech_recognition_recognizer_properties_set_complete_timeout(
                     SpeechRecognitionRecognizerProperties *properties,
                     float timeout);

SpeechStatus 
speech_recognition_recognizer_properties_set_confidence_level(
                     SpeechRecognitionRecognizerProperties *properties,
                     float confidence_level);

SpeechStatus 
speech_recognition_recognizer_properties_set_incomplete_timeout(
                     SpeechRecognitionRecognizerProperties *properties,
                     float timeout);

SpeechStatus 
speech_recognition_recognizer_properties_set_num_result_alternatives(
                     SpeechRecognitionRecognizerProperties *properties,
                     int num);

SpeechStatus 
speech_recognition_recognizer_properties_set_result_audio_provided(
                     SpeechRecognitionRecognizerProperties *properties,
                     gboolean audio);

SpeechStatus 
speech_recognition_recognizer_properties_set_sensitivity(
                     SpeechRecognitionRecognizerProperties *properties,
                     float sensitivity);

SpeechStatus 
speech_recognition_recognizer_properties_set_speed_vs_accuracy(
                     SpeechRecognitionRecognizerProperties *properties,
                     float speed_vs_accuracy);

SpeechStatus 
speech_recognition_recognizer_properties_set_training_provided(
                     SpeechRecognitionRecognizerProperties *properties,
                     gboolean training_provided);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_RECOGNITION_RECOGNIZER_PROPERTIES_H__ */
