
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The #SpeechEngine object is the parent object for all speech
 * engines including #SpeechRecognitionRecognizer and 
 * #SpeechSynthesisSynthesizer.
 * A speech engine is a generic entity that either processes speech input
 * or produces speech output. Engines - recognizers and synthesizers - derive
 * the following functionality from the #SpeechEngine object:
 *
 * <itemizedlist>
 *   <listitem>speech_engine_allocate() and speech_engine_deallocate()
 *       functions.</listitem>
 *   <listitem>speech_engine_pause() and speech_engine_resume() 
 *       functions.</listitem>
 *   <listitem>Access to a #SpeechAudioManager and #SpeechVocabManager.
 *       </listitem>
 *   <listitem>Access to #SpeechEngineProperties.</listitem>
 *   <listitem>Access to the engine's #SpeechEngineModeDesc.</listitem>
 *   <listitem>Methods to add and remove #SpeechEngineListener objects.
 *       </listitem>
 * </itemizedlist>
 *
 * Engines are located, selected and created through functions of the
 * #SpeechCentral object.
 *
 *
 * <title>Engine State System: Allocation</title>
 *
 * Each type of speech engine has a well-defined set of states of operation,
 * and well-defined behavior for moving between states. These states are
 * defined by constants of the #SpeechEngine, #SpeechRecognitionRecognizer
 * and #SpeechSynthesisSynthesizer objects.
 *
 * The #SpeechEngine object defines three functions for viewing and
 * monitoring states: speech_engine_get_engine_state(), 
 * speech_engine_wait_engine_state() and speech_engine_test_engine_state().
 * A #SpeechEngineEvent is issued to #SpeechEngineListeners each time a 
 * #SpeechEngine changes state.
 *
 * The basic states of any speech engine (#SpeechRecognitionRecognizer or
 * #SpeechSynthesisSynthesizer) are #SPEECH_ENGINE_DEALLOCATED,
 * #SPEECH_ENGINE_ALLOCATED, #SPEECH_ENGINE_ALLOCATING_RESOURCES and
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES. An engine in the 
 * #SPEECH_ENGINE_ALLOCATED state has acquired all the resources it 
 * requires to perform its core functions.
 *
 * Engines are created in the #SPEECH_ENGINE_DEALLOCATED state and a call to
 * speech_engine_allocate() is required to prepare them for usage. The
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state is an intermediate state between
 * #SPEECH_ENGINE_DEALLOCATED and #SPEECH_ENGINE_ALLOCATED which an engine
 * occupies during the resource allocation process (which may be a very short
 * period or takes 10s of seconds).
 *
 * Once an application finishes using a speech engine it should always
 * explicitly free system resources by calling the speech_engine_deallocate()
 * function. This call transitions the engine to the #SPEECH_ENGINE_DEALLOCATED
 * state via some period in the #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * The functions of #SpeechEngine, #SpeechRecognitionRecognizer and
 * #SpeechSynthesisSynthesizer perform differently according to the engine's
 * allocation state. Many functions cannot be performed when an engine is in
 * either the #SPEECH_ENGINE_DEALLOCATED or 
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES state. Many functions block (wait)
 * for an engine in the #SPEECH_ENGINE_ALLOCATING_RESOURCES state until the 
 * engine reaches the #SPEECH_ENGINE_ALLOCATED state. This blocking/exception
 * behavior is defined separately for each function of #SpeechEngine,
 * #SpeechRecognitionRecognizer and #SpeechSynthesisSynthesizer.
 *
 *
 * <title>Engine State System: Sub-states of #SPEECH_ENGINE_ALLOCATED</title>
 *
 * The #SPEECH_ENGINE_ALLOCATED states has sub-states.
 * (The #SPEECH_ENGINE_DEALLOCATED, #SPEECH_ENGINE_ALLOCATING_RESOURCES and
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states do not have any sub-states.)
 *
 * <itemizedlist>
 *   <listitem>Any #SPEECH_ENGINE_ALLOCATED engine 
 *       (#SpeechRecognitionRecognizer or #SpeechSynthesisSynthesizer) is 
 *       either #SPEECH_ENGINE_PAUSED or #SPEECH_ENGINE_RESUMED. These states
 *       indicates whether audio input/output is stopped or running.</listitem>
 *
 *   <listitem>A #SPEECH_ENGINE_ALLOCATED #SpeechSynthesisSynthesizer has 
 *       additional sub-states for #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY 
 *       and #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY that indicate the 
 *       status of its speech output queue. These two states are independent 
 *       of the #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED states.
 *       </listitem>
 *
 *   <listitem>A #SPEECH_ENGINE_ALLOCATED #SpeechRecognitionRecognizer has 
 *       additional sub-states for #SPEECH_RECOGNITION_RECOGNIZER_LISTENING, 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING and
 *	 SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED that indicate the status of 
 *       the recognition process. These three states are independent of the 
 *       #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED states (with the 
 *       exception of minor interactions documented with 
 *       #SpeechRecognitionRecognizer).</listitem>
 *
 *   <listitem>A #SPEECH_ENGINE_ALLOCATED #SpeechRecognitionRecognizer also has 
 *       additional sub-states for #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_ON
 *       and #SPEECH_RECOGNITION_RECOGNIZER_FOCUS_OFF.
 *	 Focus determines when most of an application's grammars are active
 *	 or deactive for recognition. The focus states are independent of the
 *	 #SPEECH_ENGINE_PAUSED and #SPEECH_ENGINE_RESUMED states and of the
 *	 #SPEECH_RECOGNITION_RECOGNIZER_LISTENING / 
 *       #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING /
 *	 #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED states. (Limited exceptions
 *       are discussed in the documentation for #SpeechRecognitionRecognizer).
 *       </listitem>
 * </itemizedlist>
 *
 * The speech_engine_pause() and speech_engine_resume() functions are used to
 * transition an engine between the #SPEECH_ENGINE_PAUSED and
 * #SPEECH_ENGINE_RESUMED states. The #SPEECH_ENGINE_PAUSED and
 * ##SPEECH_ENGINE_RESUMED states are shared by all applications that use
 * the underlying engine. For instance, pausing a recognizer pauses all
 * applications that use that engine.
 *
 *
 * <title>Engine State System: get/test/wait</title>
 *
 * The current state of a #SpeechEngine is returned by the
 * speech_engine_get_engine_state() function. The 
 * speech_engine_wait_engine_state() function blocks the calling thread 
 * until the #SpeechEngine reaches a specified state. The 
 * speech_engine_test_engine_state() function tests whether a #SpeechEngine
 * is in a specified state.
 *
 * The state values can be bitwise OR'ed
 * For example, for an allocated, resumed synthesizer with items in its
 * speech output queue, the state is
 *
 *    #SPEECH_ENGINE_ALLOCATED | SPEECH_ENGINE_RESUMED | 
 *    #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 *
 * The states and sub-states defined above put constraints upon the state of
 * an engine. The following are examples of illegal states:
 *
 * <itemizedlist>
 *   <listitem>Illegal #SpeechEngine states:
 *	#SPEECH_ENGINE_DEALLOCATED | #SPEECH_ENGINE_RESUMED
 *	#SPEECH_ENGINE_ALLOCATED | #SPEECH_ENGINE_DEALLOCATED</listitem>
 *
 *   <listitem>Illegal #SpeechSynthesisSynthesizer states:
 *	#SPEECH_ENGINE_DEALLOCATED | #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY
 *	#SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY | #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_NOT_EMPTY</listitem>
 *
 *   <listitem>Illegal #SpeechRecognitionRecognizer states:
 *	#SPEECH_ENGINE_DEALLOCATED | #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING
 *	#SPEECH_RECOGNITION_RECOGNIZER_PROCESSING | #SPEECH_RECOGNITION_RECOGNIZER_SUSPENDED</listitem>
 * </itemizedlist>
 *
 * Calls to the speech_engine_test_engine_state() and 
 * speech_engine_wait_engine_state() functions with illegal state values 
 * will cause exception values to be returned.
 *
 * See: #SpeechCentral
 * See: #SpeechSynthesisSynthesizer
 * See: #SpeechRecognitionRecognizer
 */

#include <speech/speech_engine.h>


/**
 * speech_engine_get_type:
 *
 * Returns: the type ID for #SpeechEngine.
 */

GType
speech_engine_get_type(void)
{
    printf("speech_engine_get_type called.\n");
}


SpeechEngine *
speech_engine_new(void)
{
    printf("speech_engine_new called.\n");
}


/**
 * speech_engine_add_engine_listener:
 * @engine: the speech engine this operation will be applied to.
 * @listener: the listener that will receive #SpeechEngineEvents.
 *
 * Request notifications of events of related to the #SpeechEngine. An 
 * application can attach multiple listeners to a #SpeechEngine. A single 
 * listener can be attached to multiple engines.
 *
 * The #SpeechEngineListener is extended for both recognition and synthesis.
 * Typically, a #SpeechRecognitionRecognizerListener is attached to a 
 * #SpeechRecognitionRecognizer and a #SpeechSynthesisSynthesizerListener is 
 * attached to a #SpeechSynthesisSynthesizer.
 *
 * A #SpeechEngineListener can be attached or removed in any state of an 
 * #SpeechEngine.
 *
 * See: #SpeechRecognitionRecognizer
 * See: #SpeechRecognitionRecognizerListener
 * See: #SpeechSynthesisSynthesizer
 * See: #SpeechSynthesisSynthesizerListener
 */

void
speech_engine_add_engine_listener(SpeechEngine *engine,
                                  SpeechEngineListener *listener)
{
    printf("speech_engine_add_engine_listener called.\n");
}


/**
 * speech_engine_remove_engine_listener:
 * @engine: the speech engine this operation will be applied to.
 * @listener: the listener to be removed.
 *
 * Remove a listener from this #SpeechEngine. A #SpeechEngineListener can 
 * be attached or removed in any state of a #SpeechEngine.
 */

void
speech_engine_remove_engine_listener(SpeechEngine *engine,
                                     SpeechEngineListener *listener)
{
    printf("speech_engine_remove_engine_listener called.\n");
}


/**
 * speech_engine_allocate:
 * @engine: the speech engine this operation will be applied to.
 *
 * Allocate the resources required for the #SpeechEngine and
 * put it into the #SPEECH_ENGINE_ALLOCATED state. When this function
 * returns successfully the #SPEECH_ENGINE_ALLOCATED bit of engine
 * state is set, and speech_engine_test_engine_state(#SPEECH_ENGINE_ALLOCATED)
 * returns true. During the processing of the function, the
 * #SpeechEngine is temporarily in the #SPEECH_ENGINE_ALLOCATING_RESOURCES 
 * state.
 *
 * When the #SpeechEngine reaches the #SPEECH_ENGINE_ALLOCATED state, other 
 * engine states are determined:
 *
 * States #SPEECH_ENGINE_PAUSED or #SPEECH_ENGINE_RESUMED: the pause
 * state depends upon the existing state of the engine. In a multi-app 
 * environment, the pause/resume state of the engine is shared by all apps.
 *
 * A #SpeechRecognitionRecognizer always starts in the 
 * #SPEECH_RECOGNITION_RECOGNIZER_LISTENING state when newly allocated but 
 * may transition immediately to another state.
 *
 * A #SpeechRecognitionRecognizer may be allocated in either the
 * #SPEECH_RECOGNITION_RECOGNIZER_HAS_FOCUS state or 
 * #SPEECH_RECOGNITION_RECOGNIZER_LOST_FOCUS state depending upon the 
 * activity of other applications.
 *
 * A #SpeechSynthesisSynthesizer always starts in the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state when newly allocated.
 *
 * While this function is being processed events are issued to any
 * #SpeechEngineListeners attached to the #SpeechEngine to indicate state 
 * changes. First, as the #SpeechEngine changes from the 
 * #SPEECH_ENGINE_DEALLOCATED to the #SPEECH_ENGINE_ALLOCATING_RESOURCES 
 * state, an #SPEECH_ENGINE_ENGINE_ALLOCATING_RESOURCES event is issued. 
 * As the allocation process completes, the engine moves from the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state to the
 * #SPEECH_ENGINE_ALLOCATED state and a #SPEECH_ENGINE_ENGINE_ALLOCATED
 * event is issued.
 *
 * The speech_engine_allocate() function should be called for a
 * #SpeechEngine in the #SPEECH_ENGINE_DEALLOCATED state. The
 * function has no effect for a #SpeechEngine is either in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES or #SPEECH_ENGINE_ALLOCATED states.
 * The function returns an exception in the
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES state.
 *
 * If any problems are encountered during the allocation process
 * so that the engine cannot be allocated, the engine returns
 * to the #SPEECH_ENGINE_DEALLOCATED state (with an
 * #SPEECH_ENGINE_EVENT_DEALLOCATED event), and a
 * #SPEECH_ENGINE_EXCEPTION value is returned.
 *
 * Allocating the resources for an engine may be fast (less than
 * a second) or slow (several 10s of seconds) depending upon a range
 * of factors. Since the speech_engine_allocate() function does not
 * return until allocation is completed applications may want to
 * perform allocation in a background thread and proceed with other
 * activities.
 *
 * See: speech_engine_get_engine_state()
 * See: speech_engine_deallocate()
 * See: #SPEECH_ENGINE_ALLOCATED
 * See: #SPEECH_ENGINE_EVENT_ALLOCATED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_EXCEPTION if an allocation error occurred or the
 *		engine is not operational.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for an engine in the
 *	#SPEECH_ENGINE_DEALLOCATING_RESOURCES state.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_allocate(SpeechEngine *engine)
{
    printf("speech_engine_allocate called.\n");
}


/**
 * speech_engine_deallocate:
 * @engine: the speech engine this operation will be applied to.
 *
 * Free the resources of the engine that were acquired during
 * allocation and during operation and return the engine
 * to the #SPEECH_ENGINE_DEALLOCATED state. When this function returns
 * the #SPEECH_ENGINE_DEALLOCATED bit of engine state is set so
 * speech_engine_test_engine_state(#SPEECH_ENGINE_DEALLOCATED) returns
 * true.  During the processing of the function, the
 * #SpeechEngine is temporarily in the #SPEECH_ENGINE_DEALLOCATING_RESOURCES 
 * state.
 *
 * A deallocated engine can be re-started with a subsequent call to 
 * speech_engine_allocate().
 *
 * Engines need to clean up current activities before being deallocated. 
 * A #SpeechSynthesisSynthesizer must be in the
 * #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY state before being deallocated.
 * If the queue is not empty, any objects on the speech output queue must
 * be cancelled with appropriate events issued.	 A
 * #SpeechRecognitionRecognizer cannot be in the 
 * #SPEECH_RECOGNITION_RECOGNIZER_PROCESSING state when being deallocated.
 * If necessary, there must be a speech_recognition_recognizer_force_finalize()
 * of any unfinalized result.
 *
 * While this function is being processed, events are issued to any
 * #SpeechEngineListeners attached to the #SpeechEngine to indicate state 
 * changes. First, as the #SpeechEngine changes from the 
 * #SPEECH_ENGINE_ALLOCATED to the #SPEECH_ENGINE_DEALLOCATING_RESOURCES 
 * state, a #SPEECH_ENGINE_EVENT_DEALLOCATING_RESOURCES event is issued.
 * As the deallocation process completes, the engine moves from the
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES state to the
 * #SPEECH_ENGINE_DEALLOCATED state and an
 * #SPEECH_ENGINE_EVENT_DEALLOCATED event is issued.
 *
 * The speech_engine_deallocate() function should only be called for
 * a #SpeechEngine in the #SPEECH_ENGINE_ALLOCATED state.
 * The function has no effect for a #SpeechEngine in either
 * the #SPEECH_ENGINE_DEALLOCATING_RESOURCES or #SPEECH_ENGINE_DEALLOCATED
 * states. The function returns an exception in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES state.
 *
 * Deallocating resources for an engine is not always immediate.
 * Since the speech_engine_deallocate() function does not return until 
 * complete, applications may want to perform deallocation in a separate 
 * thread.
 *
 * See: speech_engine_allocate()
 * See: #SPEECH_ENGINE_EVENT_DEALLOCATED
 * See: #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_EXCEPTION if a deallocation error 
 *       occurs.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for an engine in the
 *	#SPEECH_ENGINE_ALLOCATING_RESOURCES state.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_deallocate(SpeechEngine *engine)
{
    printf("speech_engine_deallocate called.\n");
}


/**
 * speech_engine_get_audio_manager:
 * @engine: the speech engine this operation will be applied to.
 *
 * Return an object which provides management of the audio input or
 * output for the #SpeechEngine.
 *
 * The #SpeechAudioManager is available in any state of a #SpeechEngine.
 *
 * Returns: the #SpeechAudioManager for the engine.
 */

SpeechAudioManager *
speech_engine_get_audio_manager(SpeechEngine *engine)
{
    printf("speech_engine_get_audio_manager called.\n");
}


/**
 * speech_engine_get_engine_mode_desc:
 * @engine: the speech engine this operation will be applied to.
 * @mode_desc: return a #SpeechEngineModeDesc for the engine.
 *
 * Return a mode descriptor that defines the operating properties
 * of the engine. For a #SpeechRecognitionRecognizer the return value is a
 * #SpeechRecognitionRecognizerModeDesc. For a #SpeechSynthesisSynthesizer
 * the return value is a #SpeechSynthesisSynthesizerModeDesc.
 *
 * The #SpeechEngineModeDesc is available in any state of a #SpeechEngine.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the application does not have
 *	       speech_engine_get_engine_mode_desc() permission.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_get_engine_mode_desc(SpeechEngine *engine,
                                   SpeechEngineModeDesc *mode_desc)
{
    printf("speech_engine_get_engine_mode_desc called.\n");
}


/**
 * speech_engine_get_engine_properties:
 * @engine: the speech engine this operation will be applied to.
 *
 * Return the #SpeechEngineProperties object.
 *
 * A #SpeechRecognitionRecognizer returns a
 * #SpeechRecognitionRecognizerProperties object. The
 * #SpeechRecognitionRecognizer object also defines a
 * speech_recognition_recognizer_get_recognizer_properties() function that 
 * returns the same object as speech_engine_get_engine_properties(),
 * but without requiring a cast to be useful.
 *
 * A #SpeechSynthesisSynthesizer returns a
 * #SpeechSynthesisSynthesizerProperties object. The
 * #SpeechSynthesisSynthesizer object also defines a
 * speech_synthesis_synthesizer_get_synthesizer_properties() function that
 * returns the same object as speech_engine_get_engine_properties(), but
 * without requiring a cast to be useful.
 *
 * The #SpeechEngineProperties are available in any state of
 * a #SpeechEngine. However, changes only take effect once
 * an engine reaches the #SPEECH_ENGINE_ALLOCATED state.
 *
 * See: speech_recognition_recognizer_get_recognizer_properties()
 * See: #SpeechRecognitionRecognizerProperties
 * See: speech_synthesis_synthesizer_get_synthesizer_properties()
 * See: #SpeechSynthesisSynthesizerProperties
 *
 * Returns: the #SpeechEngineProperties object for this engine.
 */

SpeechEngineProperties *
speech_engine_get_engine_properties(SpeechEngine *engine)
{
    printf("speech_engine_get_engine_properties called.\n");
}


/**
 * speech_engine_get_engine_state:
 * @engine: the speech engine this operation will be applied to.
 *
 * Returns an OR'ed set of flags indicating the current state of a
 * #SpeechEngine. The format of the returned state value is described above.
 *
 * A #SpeechEngineEvent is issued each time the #SpeechEngine changes state.
 *
 * The speech_engine_get_engine_state() function can be called successfully
 * in any #SpeechEngine state.
 *
 * See: speech_engine_test_engine_state()
 * See: speech_engine_wait_engine_state()
 * See: speech_engine_event_get_new_engine_state()
 * See: speech_engine_event_get_old_engine_state()
 *
 * Returns: an OR'ed set of flags indicating the current state of a
 * #SpeechEngine.
 */

long
speech_engine_get_engine_state(SpeechEngine *engine)
{
    printf("speech_engine_get_engine_state called.\n");
}


/**
 * speech_engine_get_vocab_manager:
 * @engine: the speech engine this operation will be applied to.
 * @manager: returns the #SpeechVocabManager for the engine or %NULL if
 *	     it does not have a #SpeechVocabManager.
 *
 * Return an object which provides management of the vocabulary for
 * the #SpeechEngine. See the #SpeechVocabManager documentation for a 
 * description of vocabularies and their use with speech engines. 
 * Returns %NULL if the #SpeechEngine does not provide vocabulary 
 * management capabilities.
 *
 * The #SpeechVocabManager is available for engines in the
 * #SPEECH_ENGINE_ALLOCATED state. The call blocks for engines in the
 * #SPEECH_ENGINE_ALLOCATING_RESOURCES. An error is returned for engines
 * in the #SPEECH_ENGINE_DEALLOCATED or
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * See: #SpeechWord
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for an engine in the
 *		#SPEECH_ENGINE_DEALLOCATED or
 *		#SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_get_vocab_manager(SpeechEngine *engine,
                                SpeechVocabManager *manager)
{
    printf("speech_engine_get_vocab_manager called.\n");
}


/**
 * speech_engine_pause:
 * @engine: the speech engine this operation will be applied to.
 *
 * Pause the audio stream for the engine and put the
 * #SpeechEngine into the #SPEECH_ENGINE_PAUSED state. Pausing
 * an engine pauses the underlying engine for all applications that
 * are connected to that engine. Engines are typically paused and
 * resumed by request from a user.
 *
 * Applications may pause an engine indefinitely. When an engine
 * moves from the #SPEECH_ENGINE_RESUMED state to the #SPEECH_ENGINE_PAUSED
 * state, a #SPEECH_ENGINE_PAUSED event is issued to each
 * #SpeechEngineListener attached to the #SpeechEngine.
 * The #SPEECH_ENGINE_PAUSED bit of the engine state is set to true
 * when paused, and can be tested by the speech_engine_get_engine_state()
 * function and other engine state functions.
 *
 * The #SPEECH_ENGINE_PAUSED state is a sub-state of the
 * #SPEECH_ENGINE_ALLOCATED state. A #SPEECH_ENGINE_ALLOCATED
 * #SpeechEngine is always in either the #SPEECH_ENGINE_PAUSED
 * or the #SPEECH_ENGINE_RESUMED state.
 *
 * It is not an error to pause a #SpeechEngine that is already paused.
 *
 * The speech_engine_pause() function operates as defined for engines
 * in the #SPEECH_ALLOCATED state. When pause is called for an engine in 
 * the #SPEECH_ENGINE_ALLOCATING_RESOURCES state, the function blocks 
 * (waits) until the #SPEECH_ALLOCATED state is reached and then operates 
 * normally. An error is returned when speech_engine_pause() is called 
 * for an engine is either the #SPEECH_ENGINE_DEALLOCATED is 
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * The speech_engine_pause() function does not always return immediately.
 * Some applications need to execute pause in a separate thread.
 *
 * <title>Pausing a #SpeechSynthesisSynthesizer</title>
 *
 * The pause/resume mechanism for a synthesizer is analogous to pause/resume 
 * on a tape player or CD player. The audio output stream is paused. The 
 * speaking queue is left intact and a subsequent resume continues output 
 * from the point at which the pause took effect.
 *
 * <title>Pausing a #SpeechRecognitionRecognizer</title>
 *
 * Pause and resume for a recognizer are analogous to turning a microphone 
 * off and on. Pausing stops the input audio input stream as close as 
 * possible to the time of the call to pause. The incoming audio between 
 * the pause and the resume calls is ignored.
 *
 * Anything a user says while the recognizer is paused will not be
 * heard by the recognizer. Pausing a recognizer during the middle
 * of user speech forces the recognizer to finalize or reject
 * processing of that incoming speech - a recognition result cannot
 * cross a pause/resume boundary.
 *
 * Most recognizers have some amount of internal audio buffering.
 * This means that some recognizer processing may continue after the
 * pause. For example, results can be created and finalized.
 *
 * Note: recognizers add a special speech_recognition_recognizer_suspend()
 * function that allows applications to temporarily stop the recognizer to 
 * modify grammars and grammar activation.  Unlike a paused recognizer,
 * a suspended recognizer buffers incoming audio input to be processed once 
 * it returns to a listening state, so no audio is lost.
 *
 * See: speech_engine_resume()
 * See: speech_engine_get_engine_state()
 * See: #SPEECH_ENGINE_EVENT_PAUSED
 * See: speech_recognition_recognizer_suspend()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for an engine in the
 *	       #SPEECH_ENGINE_DEALLOCATED or
 *	       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_pause(SpeechEngine *engine)
{
    printf("speech_engine_pause called.\n");
}


/**
 * speech_engine_resume:
 * @engine: the speech engine this operation will be applied to.
 *
 * Put the #SpeechEngine in the #SPEECH_ENGINE_RESUMED state to
 * resume audio streaming to or from a paused engine. Resuming an
 * engine resuming the underlying engine for all applications that
 * are connected to that engine. Engines are typically paused and
 * resumed by request from a user.
 *
 * The specific pause/resume behavior of recognizers and synthesizers
 * is defined in the documentation for the speech_engine_pause() function.
 *
 * When an engine moves from the #SPEECH_ENGINE_PAUSED state to the
 * #SPEECH_ENGINE_RESUMED state, a #SPEECH_ENGINE_EVENT_RESUMED event
 * is issued to each #SpeechEngineListener attached to the
 * #SpeechEngine. The #SPEECH_ENGINE_RESUMED bit of the engine
 * state is set to true when resumed, and can be tested by the
 * getEngineState function and other engine state functions.
 *
 * The #SPEECH_ENGINE_RESUMED state is a sub-state of the
 * #SPEECH_ENGINE_ALLOCATED state. A #SPEECH_ENGINE_ALLOCATED
 * #SpeechEngine is always in either the #SPEECH_ENGINE_PAUSED
 * or the #SPEECH_ENGINE_RESUMED state.
 *
 * It is not an exception to resume an engine that is already in the 
 * #SPEECH_ENGINE_RESUMED state. An exception may be returned if the 
 * audio resource required by the engine (audio input or output) is 
 * not available.
 *
 * The speech_engine_resume() function operates as defined for engines in
 * the #SPEECH_ENGINE_ALLOCATED state. When resume is called for an
 * engine in the #SPEECH_ENGINE_ALLOCATING_RESOURCES state, the function
 * blocks (waits) until the #SPEECH_ENGINE_ALLOCATED state is reached
 * and then operates normally. An error is returned when resume is
 * called for an engine is either the #SPEECH_ENGINE_DEALLOCATED is
 * #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.
 *
 * The speech_engine_resume() function does not always return immediately.
 * Some applications need to execute resume in a separate thread.
 *
 * See: speech_engine_pause()
 * See: speech_engine_get_engine_state()
 * See: #SPEECH_ENGINE_EVENT_RESUMED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_AUDIO_EXCEPTION if unable to gain access to the 
 *       audio channel.</listitem>
 *   <listitem>#SPEECH_ENGINE_STATE_ERROR if called for an engine in the
 *	       #SPEECH_ENGINE_DEALLOCATED or
 *	       #SPEECH_ENGINE_DEALLOCATING_RESOURCES states.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_resume(SpeechEngine *engine)
{
    printf("speech_engine_resume called.\n");
}


/**
 * speech_engine_test_engine_state:
 * @engine: the speech engine this operation will be applied to.
 * @state: the engine state to test against.
 * @result: a boolean indication of whether the current engine state
 *          matches the specified state.
 *
 * Returns true if the current engine state matches the specified
 * state. The format of the @state value is described above.
 *
 * The test performed is not an exact match to the current state.
 * Only the specified states are tested. For example the following
 * returns true only if the #SpeechSynthesisSynthesizer queue is empty,
 * irrespective of the pause/resume and allocation states.
 *
 *    if (speech_engine_test_engine_state(engine, #SPEECH_SYNTHESIS_SYNTHESIZER_QUEUE_EMPTY, &amp;result)) ...
 *
 * The speech_engine_test_engine_state() function is equivalent to:
 *
 *	if ((speech_engine_get_engine_state(engine) & state) == state)
 *
 * The speech_engine_test_engine_state() function can be called successfully
 * in any #SpeechEngine state.
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the specified state is
 *	       unreachable.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_test_engine_state(SpeechEngine *engine,
                                long state,
                                gboolean *result)
{
    printf("speech_engine_test_engine_state called.\n");
}


/**
 * speech_engine_wait_engine_state:
 * @engine: the speech engine this operation will be applied to.
 * @state: the state required of the #SpeechEngine.
 *
 * Blocks the calling thread until the #SpeechEngine is in a
 * specified state. The format of the state value is described above.
 *
 * All state bits specified in the state parameter must be set in
 * order for the function to return, as defined for the
 * speech_engine_test_engine_state() function. If the state parameter
 * defines an unreachable state
 * (e.g. #SPEECH_ENGINE_PAUSED | #SPEECH_ENGINE_RESUMED)
 * an exception is returned.
 *
 * The speech_engine_wait_engine_state() function can be called successfully
 * in any #SpeechEngine state.
 *
 * See: speech_engine_test_engine_state()
 * See: speech_engine_get_engine_state()
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_INTERRUPTED_EXCEPTION if another thread has interrupted
 *		this thread.</listitem>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the specified state is
 *              unreachable.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_wait_engine_state(SpeechEngine *engine,
                                long state)
{
    printf("speech_engine_wait_engine_state called.\n");
}
