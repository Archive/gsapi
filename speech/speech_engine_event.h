
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Notify changes in state of a speech synthesis or recognition engine. */

#ifndef __SPEECH_ENGINE_EVENT_H__
#define __SPEECH_ENGINE_EVENT_H__

#include <speech/speech_types.h>
#include <speech/speech_event.h>
#include <speech/speech_engine.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_EVENT              (speech_engine_event_get_type())
#define SPEECH_ENGINE_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_EVENT, SpeechEngineEvent))
#define SPEECH_ENGINE_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_EVENT, SpeechEngineEventClass))
#define SPEECH_IS_ENGINE_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_EVENT))
#define SPEECH_IS_ENGINE_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_EVENT))
#define SPEECH_ENGINE_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_EVENT, SpeechEngineEventClass))

/*
 * Identifier for event issued when engine allocation is complete.
 * The #SPEECH_ENGINE_ALLOCATED flag of the 
 * speech_engine_event_get_new_engine_state() is set.
 *
 * See: speech_engine_event_get_new_engine_state()
 * See: speech_event_get_id()
 * See: speech_engine_allocate()
 * See: speech_engine_listener_engine_allocated()
 */

static int SPEECH_ENGINE_EVENT_ALLOCATED = 501;

/*
 * Identifier for event issued when engine deallocation is complete.
 * The #SPEECH_ENGINE_DEALLOCATED flag of the
 * speech_engine_event_get_new_engine_state() is set.
 *
 * See: speech_engine_event_get_new_engine_state()
 * See: speech_event_get_id()
 * See: speech_engine_allocate()
 * See: speech_engine_listener_engine_deallocated()
 */

static int SPEECH_ENGINE_EVENT_DEALLOCATED = 502;

/*
 * Identifier for event issued when engine allocation has commenced.
 * The #SPEECH_ENGINE_ALLOCATING_RESOURCES flag of the
 * speech_engine_event_get_new_engine_state() is set.
 *
 * See: speech_engine_event_get_new_engine_state()
 * See: speech_event_get_id()
 * See: speech_engine_allocate()
 * See: speech_engine_listener_engine_allocating_resources()
 */

static int SPEECH_ENGINE_EVENT_ALLOCATING_RESOURCES = 503;

/*
 * Identifier for event issued when engine deallocation has commenced.
 * The #SPEECH_ENGINE_DEALLOCATING_RESOURCES flag of the
 * speech_engine_event_get_new_engine_state() is set.
 *
 * See: speech_engine_event_get_new_engine_state()
 * See: speech_event_get_id()
 * See: speech_engine_allocate()
 * See: speech_engine_listener_engine_deallocating_resources()
 */

static int SPEECH_ENGINE_EVENT_DEALLOCATING_RESOURCES = 504;

/*
 * Identifier for event issued when engine is paused.
 * The #SPEECH_ENGINE_PAUSED flag of the 
 * speech_engine_event_get_new_engine_state() is set.
 *
 * See: speech_engine_event_get_new_engine_state()
 * See: speech_event_get_id()
 * See: speech_engine_pause()
 * See: speech_engine_listener_engine_paused()
 */

static int SPEECH_ENGINE_EVENT_PAUSED = 505;

/*
 * Identifier for event issued when engine is resumed.
 * The #SPEECH_ENGINE_RESUMED flag of the 
 * speech_engine_event_get_new_engine_state() is set.
 *
 * See: speech_engine_event_get_new_engine_state()
 * See: speech_event_get_id()
 * See: speech_engine_resume()
 * See: speech_engine_listener_engine_resumed()
 */

static int SPEECH_ENGINE_EVENT_RESUMED = 506;


GType 
speech_engine_event_get_type(void);

SpeechEngineEvent * 
speech_engine_event_new(SpeechEngine *source,
                        int id,
                        long old_engine_state,
                        long new_engine_state);

long 
speech_engine_event_get_new_engine_state(SpeechEngineEvent *event);

long 
speech_engine_event_get_old_engine_state(SpeechEngineEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_EVENT_H__ */
