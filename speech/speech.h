
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		   See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef __SPEECH_H__
#define __SPEECH_H__

#include <speech/speech_audio_event.h>
#include <speech/speech_audio_listener.h>
#include <speech/speech_audio_manager.h>
#include <speech/speech_central.h>
#include <speech/speech_control_component.h>
#include <speech/speech_engine.h>
#include <speech/speech_engine_central.h>
#include <speech/speech_engine_create.h>
#include <speech/speech_engine_error_event.h>
#include <speech/speech_engine_event.h>
#include <speech/speech_engine_list.h>
#include <speech/speech_engine_listener.h>
#include <speech/speech_engine_mode_desc.h>
#include <speech/speech_engine_properties.h>
#include <speech/speech_error.h>
#include <speech/speech_event.h>
#include <speech/speech_event_listener.h>
#include <speech/speech_exception.h>
#include <speech/speech_input_stream.h>
#include <speech/speech_object.h>
#include <speech/speech_output_stream.h>
#include <speech/speech_property_veto_exception.h>
#include <speech/speech_vocab_manager.h>
#include <speech/speech_word.h>

#endif /* __SPEECH_H__ */
