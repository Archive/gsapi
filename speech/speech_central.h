
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* The initial access point to all speech input and output capabilities. */

#ifndef __SPEECH_CENTRAL_H__
#define __SPEECH_CENTRAL_H__

#include <speech/speech_types.h>
#include <speech/speech_engine_list.h>
#include <speech/speech_engine_mode_desc.h>
#include <speech/recognition/speech_recognition_recognizer.h>
#include <speech/synthesis/speech_synthesis_synthesizer.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_CENTRAL              (speech_central_get_type())
#define SPEECH_CENTRAL(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_CENTRAL, SpeechCentral
#define SPEECH_CENTRAL_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_CENTRAL, SpeechCentralClass))
#define SPEECH_IS_CENTRAL(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_CENTRAL))
#define SPEECH_IS_CENTRAL_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_CENTRAL))
#define SPEECH_CENTRAL_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_CENTRAL, SpeechCentralClass))

typedef struct _SpeechCentral            SpeechCentral;
typedef struct _SpeechCentralClass       SpeechCentralClass;

struct _SpeechCentral {
    SpeechObject object;
};

struct _SpeechCentralClass {
    SpeechObjectClass parent_class;
};


GType 
speech_central_get_type(void);

SpeechCentral * 
speech_central_new(void);

SpeechStatus 
speech_central_available_recognizers(SpeechCentral *central,
                                     SpeechEngineModeDesc *require,
                                     SpeechEngineList *list);

SpeechStatus 
speech_central_available_synthesizers(SpeechCentral *central,
                                      SpeechEngineModeDesc *require,
                                      SpeechEngineList *list);

SpeechStatus 
speech_central_create_recognizer(SpeechCentral *central,
                                 SpeechEngineModeDesc *require,
                                 SpeechRecognitionRecognizer *recognizer);

SpeechStatus 
speech_central_create_synthesizer(SpeechCentral *central,
                                  SpeechEngineModeDesc *require,
                                  SpeechSynthesisSynthesizer *synthesizer);

SpeechStatus 
speech_central_register_engine_central(SpeechCentral *central,
                                       gchar *class_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_CENTRAL_H__ */
