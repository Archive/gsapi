
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Provides a list of #SpeechEngineModeDesc objects that define the
 * available operating modes of a speech engine.
 *
 * Each speech engine registers a #SpeechEngineCentral object with the
 * #SpeechCentral object. When requested by the
 * #SpeechCentral object, each registered #SpeechEngineCentral
 * object provides a list with a #SpeechEngineModeDesc object that
 * describes each available operating mode of the engine.
 *
 * The #SpeechEngineModeDesc objects returned by
 * #SpeechEngineCentral in its list must implement the
 * #SpeechEngineCreate object and be a sub-class of either
 * #SpeechRecognitionRecognizerModeDesc or #SpeechSynthesisSynthesizerModeDesc.
 * The #SpeechCentral object calls the speech_engine_create_create_engine()
 * function of the #SpeechEngineCentral object when it is requested to create
 * an engine. (See #SpeechEngineCreate for more details.)
 *
 * The engine must perform the same security checks on access to speech
 * engines as the #SpeechCentral class.
 *
 * Note: Application developers do not need to use this object.
 * #SpeechEngineCentral is used internally by #SpeechCentral and
 * speech engines.
 *
 * See: #SpeechCentral
 * See: #SpeechEngineCreate
 * See: #SpeechEngineModeDesc
 */

#include <speech/speech_engine_central.h>


/**
 * speech_engine_central_get_type:
 *
 * Returns: the type ID for #SpeechEngineCentral.
 */

GType
speech_engine_central_get_type(void)
{
    printf("speech_engine_central_get_type called.\n");
}


SpeechEngineCentral *
speech_engine_central_new(void)
{
    printf("speech_engine_central_new called.\n");
}


/**
 * speech_engine_central_create_engine_list:
 * @engine_central: the #SpeechEngineCentral object this operation will be
 *                  applied to.
 * @require: a #SpeechEngineModeDesc containing the feature set required.
 *
 * Create a #SpeechEngineList containing a #SpeechEngineModeDesc for each 
 * mode of operation of a speech engine that matches a set of required 
 * features. Each object in the list must be a sub-class of either
 * #SpeechRecognitionRecognizerModeDesc or #SpeechSynthesisSynthesizerModeDesc
 * and must implement the #SpeechEngineCreate object.
 *
 * The #SpeechCentral class ensures that the @require parameter is an 
 * instance of either #SpeechRecognitionRecognizerModeDesc or 
 * #SpeechSynthesisSynthesizerModeDesc.
 * This enables the #SpeechEngineCentral to optimize its search
 * for either recognizers or synthesizers.
 *
 * Returns %NULL if no engines are available or if none meet the specified 
 * requirements.
 *
 * The returned list should indicate the list of modes available
 * at the time of the call (the list may change over time). The
 * engine can create the list at the time of the call or it may
 * be pre-stored.
 *
 * See: #SpeechEngineCreate
 * See: #SpeechEngineModeDesc
 * See: #SpeechRecognitionRecognizerModeDesc
 * See: #SpeechSynthesisSynthesizerModeDesc
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the caller does not have
 *		#accessEngineModeDesc permission.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_central_create_engine_list(SpeechEngineCentral *engine_central,
                                         SpeechEngineModeDesc *require,
                                         SpeechEngineList *list)
{
    printf("speech_engine_central_create_engine_list called.\n");
}
