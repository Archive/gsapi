
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The root event class for all speech events.
 * All events from a speech engine (recognizer or synthesizer)
 * are synchronized with the GTK+ event queue.	This allows an
 * application to mix speech and GTK+ events without being concerned
 * with multi-threading problems.
 */

#include <speech/speech_event.h>


/**
 * speech_event_get_type:
 *
 * Returns: the type ID for SpeechEvent.
 */

GType
speech_event_get_type(void)
{
    printf("speech_event_get_type called.\n");
}


/**
 * speech_event_new:
 * @source: the object that issued the event. Must must be non-null.
 *
 * Creates a #SpeechEvent with a specified source.
 *
 * Returns:
 */

SpeechEvent *
speech_event_new(SpeechObject *source)
{
    printf("speech_event_new called.\n");
}


/**
 * speech_event_new_with_id:
 * @source: the object that issued the event.
 * @id: the identifier for the event type.
 *
 * Creates a #SpeechEvent with a given source and identifier.
 *
 * Returns:
 */

SpeechEvent *
speech_event_new_with_id(SpeechObject *source, int id)
{
    printf("speech_event_new_with_id called.\n");
}


/**
 * speech_event_get_id:
 * @event: the #SpeechEvent object that this operation will be applied to.
 *
 * Get the event identifier.
 *
 * Returns: the event identifier. Id values are defined for each sub-class 
 *          of #SpeechEvent.
 */

int
speech_event_get_id(SpeechEvent *event)
{
    printf("speech_event_get_id called.\n");
}


/**
 * speech_event_get_source:
 * @event: the #SpeechEvent object that this operation will be applied to.
 *
 * Get the object on which the event initially occurred.
 *
 * Returns: The object on which the event initially occurred.
 */

SpeechObject *
speech_event_get_source(SpeechEvent *event)
{
    printf("speech_event_get_source called.\n");
}


/**
 * speech_event_param_string:
 * @event: the #SpeechEvent object that this operation will be applied to.
 *
 * Returns a parameter string identifying this event.
 * This function is useful for event-logging and for debugging.
 *
 * Returns: a string identifying the event.
 */

gchar *
speech_event_param_string(SpeechEvent *event)
{
    printf("speech_event_param_string called.\n");
}


/**
 * speech_event_to_string:
 * @event: the #SpeechEvent object that this operation will be applied to.
 *
 * Returns a printable string representation of this event. Useful
 * for event-logging and debugging.
 *
 * Returns: A string representation of this event.
 */

gchar *
speech_event_to_string(SpeechEvent *event)
{
    printf("speech_event_to_string called.\n");
}
