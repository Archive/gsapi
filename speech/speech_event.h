
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* The root object for all speech events. */

#ifndef __SPEECH_EVENT_H__
#define __SPEECH_EVENT_H__

#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_EVENT              (speech_event_get_type())
#define SPEECH_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_EVENT, SpeechEvent))
#define SPEECH_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_EVENT, SpeechEventClass))
#define SPEECH_IS_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_EVENT))
#define SPEECH_IS_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_EVENT))
#define SPEECH_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_EVENT, SpeechEventClass))


GType 
speech_event_get_type(void);

SpeechEvent * 
speech_event_new(SpeechObject *source);

SpeechEvent * 
speech_event_new_with_id(SpeechObject *source,
                         int id);

int 
speech_event_get_id(SpeechEvent *event);

SpeechObject * 
speech_event_get_source(SpeechEvent *event);

gchar * 
speech_event_param_string(SpeechEvent *event);

gchar * 
speech_event_to_string(SpeechEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_EVENT_H__ */
