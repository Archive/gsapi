
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Base speech object that all others are derived from. */

#ifndef __SPEECH_INPUT_STREAM_H__
#define __SPEECH_INPUT_STREAM_H__

#include <glib-object.h>
#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_INPUT_STREAM              (speech_input_stream_get_type())
#define SPEECH_INPUT_STREAM(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_INPUT_STREAM, SpeechInputStream))
#define SPEECH_INPUT_STREAM_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_INPUT_STREAM, SpeechInputStreamClass))
#define SPEECH_IS_INPUT_STREAM(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_INPUT_STREAM))
#define SPEECH_IS_INPUT_STREAM_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_INPUT_STREAM))
#define SPEECH_INPUT_STREAM_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_INPUT_STREAM, SpeechInputStreamClass))

typedef struct _SpeechInputStream            SpeechInputStream;
typedef struct _SpeechInputStreamClass       SpeechInputStreamClass;

struct _SpeechInputStream {
    GObject object;
};

struct _SpeechInputStreamClass {
    GObjectClass parent_class;
};


GType 
speech_input_stream_get_type(void);

SpeechInputStream * 
speech_input_stream_new(void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_INPUT_STREAM_H__ */
