
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Container for a set of EngineModeDesc objects. */

#ifndef __SPEECH_ENGINE_LIST_H__
#define __SPEECH_ENGINE_LIST_H__

#include <speech/speech_types.h>
#include <speech/speech_engine_mode_desc.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_LIST              (speech_engine_list_get_type())
#define SPEECH_ENGINE_LIST(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_LIST, SpeechEngineList))
#define SPEECH_ENGINE_LIST_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_LIST, SpeechEngineListClass))
#define SPEECH_IS_ENGINE_LIST(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_LIST))
#define SPEECH_IS_ENGINE_LIST_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_LIST))
#define SPEECH_ENGINE_LIST_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_LIST, SpeechEngineListClass))

typedef struct _SpeechEngineList            SpeechEngineList;
typedef struct _SpeechEngineListClass       SpeechEngineListClass;

struct _SpeechEngineList {
    SpeechObject object;
};

struct _SpeechEngineListClass {
    SpeechObjectClass parent_class;
};


GType 
speech_engine_list_get_type(void);

SpeechEngineList * 
speech_engine_list_new(void);

SpeechEngineList ** 
speech_engine_list_get_engine_list(SpeechEngineList *engine_list);

gboolean 
speech_engine_list_any_match(SpeechEngineList *engine_list,
                             SpeechEngineModeDesc *require);

void 
speech_engine_list_order_by_match(SpeechEngineList *engine_list,
                                  SpeechEngineModeDesc *require);

void 
speech_engine_list_reject_match(SpeechEngineList *engine_list,
                                SpeechEngineModeDesc *reject);

void 
speech_engine_list_require_match(SpeechEngineList *engine_list,
                                 SpeechEngineModeDesc *require);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_LIST_H__ */
