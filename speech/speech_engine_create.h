
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Used to create speech engines with specific properties. */

#ifndef __SPEECH_ENGINE_CREATE_H__
#define __SPEECH_ENGINE_CREATE_H__

#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_CREATE              (speech_engine_create_get_type())
#define SPEECH_ENGINE_CREATE(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_CREATE, SpeechEngineCreate))
#define SPEECH_ENGINE_CREATE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_CREATE, SpeechEngineCreateClass))
#define SPEECH_IS_ENGINE_CREATE(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_CREATE))
#define SPEECH_IS_ENGINE_CREATE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_CREATE))
#define SPEECH_ENGINE_CREATE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_CREATE, SpeechEngineCreateClass))

typedef struct _SpeechEngineCreate            SpeechEngineCreate;
typedef struct _SpeechEngineCreateClass       SpeechEngineCreateClass;

struct _SpeechEngineCreate {
    SpeechObject object;
};

struct _SpeechEngineCreateClass {
    SpeechObjectClass parent_class;
};


GType 
speech_engine_create_get_type(void);

SpeechEngineCreate * 
speech_engine_create_new(void);

SpeechStatus 
speech_engine_create_create_engine(SpeechEngineCreate *create,
                                   SpeechEngine *engine);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_CREATE_H__ */
