
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechEngineErrorEvent is an asynchronous notification of an
 * internal error in the engine which prevents normal behavior of that
 * engine. The event encapsulates a string that provides details about
 * the error.
 *
 * See: speech_engine_listener_engine_error()
 */

#include <speech/speech_engine_error_event.h>


/**
 * speech_engine_error_event_get_type:
 *
 * Returns: the type ID for #SpeechEngineErrorEvent.
 */

GType
speech_engine_error_event_get_type(void)
{
    printf("speech_engine_error_event_get_type called.\n");
}


/**
 * speech_engine_error_event_new:
 * @source: the object that issued the event.
 * @id: the identifier for the event type.
 * @error: description of the detected error.
 * @old_engine_state: engine state prior to this event.
 * @new_engine_state: engine state following this event.
 *
 * Initializes a #SpeechEngineErrorEvent with an event identifier, error 
 * string, old engine state and new engine state. The old and new states 
 * are zero if the engine states are unknown or undefined.
 *
 * Returns:
 *
 * See: speech_engine_get_engine_state()
 */

SpeechEngineErrorEvent *
speech_engine_error_event_new(SpeechEngine *source,
                              int id,
                              gchar *error,
                              long old_engine_state,
                              long new_engine_state)
{
    printf("speech_engine_error_event_new called.\n");
}


/**
 * speech_engine_error_event_get_engine_error:
 * @event: the #SpeechEngineErrorEvent that this operation will be applied to.
 *
 * Return the string (#SpeechException or #SpeechError)
 * that describes the engine problem.
 *
 * Returns: the error description.
 */

gchar *
speech_engine_error_event_get_engine_error(SpeechEngineErrorEvent *event)
{
    printf("speech_engine_error_event_get_engine_error called.\n");
}
