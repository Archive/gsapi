
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * Object defining functions to be called when state-change events
 * for a speech engine occur. To receive engine events an application
 * attaches a listener by calling the speech_engine_add_engine_listener()
 * function of a #SpeechEngine. A listener is removed by a call
 * to the speech_engine_remove_engine_listener() function.
 *
 * The event dispatch policy is defined in the documentation for the
 * #SpeechEvent object.
 *
 * This object is extended by the #SpeechRecognitionRecognizerListener and
 * #SpeechSynthesisSynthesizerListener objects to handle the specialized
 * events of speech recognizers and synthesizers.
 *
 * See: #SpeechSpeechEvent
 * See: #SpeechRecognitionRecognizerListener
 * See: #SpeechSynthesisSynthesizerListener
 */

#include <speech/speech_engine_listener.h>


/**
 * speech_engine_listener_get_type:
 *
 * Returns: the type ID for #SpeechEngineListener.
 */

GType
speech_engine_listener_get_type(void)
{
    printf("speech_engine_listener_get_type called.\n");
}


SpeechEngineListener *
speech_engine_listener_new(void)
{
    printf("speech_engine_listener_new called.\n");
}


/**
 * speech_engine_listener_engine_allocated:
 * @listener: the #SpeechEngineListener object that this operation will be 
 *            applied to.
 * @e: the #SpeechEngineEvent causing this state change.
 *
 * The #SpeechEngine has been allocated.
 *
 * See: #SPEECH_ENGINE_EVENT_ALLOCATED
 */

void
speech_engine_listener_engine_allocated(SpeechEngineListener *listener,
                                        SpeechEngineEvent *e)
{
    printf("speech_engine_listener_engine_allocated called.\n");
}


/**
 * speech_engine_listener_engine_allocating_resources:
 * @listener: the #SpeechEngineListener object that this operation will be 
 *            applied to.
 * @e: the #SpeechEngineEvent causing this state change.
 *
 * The #SpeechEngine is being allocated.
 *
 * See: #SPEECH_ENGINE_EVENT_ALLOCATING_RESOURCES
 */

void
speech_engine_listener_engine_allocating_resources(
                                        SpeechEngineListener *listener,
                                        SpeechEngineEvent *e)
{
    printf("speech_engine_listener_engine_allocating_resources called.\n");
}


/**
 * speech_engine_listener_engine_deallocated:
 * @listener: the #SpeechEngineListener object that this operation will be 
 *            applied to.
 * @e: the #SpeechEngineEvent causing this state change.
 *
 * The #SpeechEngine has been deallocated.
 *
 * See: #SPEECH_ENGINE_EVENT_DEALLOCATED
 */

void
speech_engine_listener_engine_deallocated(SpeechEngineListener *listener,
                                          SpeechEngineEvent *e)
{
    printf("speech_engine_listener_engine_deallocated called.\n");
}


/**
 * speech_engine_listener_engine_deallocating_resources:
 * @listener: the #SpeechEngineListener object that this operation will be 
 *            applied to.
 * @e: the #SpeechEngineEvent causing this state change.
 *
 * The #SpeechEngine is being deallocated.
 *
 * See: #SPEECH_ENGINE_EVENT_DEALLOCATING_RESOURCES
 */

void
speech_engine_listener_engine_deallocating_resources(
                                        SpeechEngineListener *listener,
                                        SpeechEngineEvent *e)
{
    printf("speech_engine_listener_engine_deallocating_resources called.\n");
}


/**
 * speech_engine_listener_engine_error:
 * @listener: the #SpeechEngineListener object that this operation will be 
 *            applied to.
 * @e: the #SpeechEngineEvent causing this state change.
 *
 * A #SpeechEngineErrorEvent has occurred and the
 * #SpeechEngine is unable to continue normal operation.
 *
 * See: #SpeechEngineErrorEvent
 */

void
speech_engine_listener_engine_error(SpeechEngineListener *listener,
                                    SpeechEngineErrorEvent *e)
{
    printf("speech_engine_listener_engine_error called.\n");
}


/**
 * speech_engine_listener_engine_paused:
 * @listener: the #SpeechEngineListener object that this operation will be 
 *            applied to.
 * @e: the #SpeechEngineEvent causing this state change.
 *
 * The #SpeechEngine has been paused.
 *
 * See: #SPEECH_ENGINE_EVENT_PAUSED
 */

void
speech_engine_listener_engine_paused(SpeechEngineListener *listener,
                                     SpeechEngineEvent *e)
{
    printf("speech_engine_listener_engine_paused called.\n");
}


/**
 * speech_engine_listener_engine_resumed:
 * @listener: the #SpeechEngineListener object that this operation will be 
 *            applied to.
 * @e: the #SpeechEngineEvent causing this state change.
 *
 * The #SpeechEngine has been resumed.
 *
 * See: #SPEECH_ENGINE_EVENT_RESUMED
 */

void
speech_engine_listener_engine_resumed(SpeechEngineListener *listener,
                                      SpeechEngineEvent *e)
{
    printf("speech_engine_listener_engine_resumed called.\n");
}
