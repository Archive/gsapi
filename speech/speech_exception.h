
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Signals that a Speech API exception has occurred. */

#ifndef __SPEECH_EXCEPTION_H__
#define __SPEECH_EXCEPTION_H__

#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_EXCEPTION              (speech_exception_get_type())
#define SPEECH_EXCEPTION(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_EXCEPTION, SpeechException))
#define SPEECH_EXCEPTION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_EXCEPTION, SpeechExceptionClass))
#define SPEECH_IS_EXCEPTION(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_EXCEPTION))
#define SPEECH_IS_EXCEPTION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_EXCEPTION))
#define SPEECH_EXCEPTION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_EXCEPTION, SpeechExceptionClass))

typedef struct _SpeechException            SpeechException;
typedef struct _SpeechExceptionClass       SpeechExceptionClass;

struct _SpeechException {
    SpeechObject object;
};

struct _SpeechExceptionClass {
    SpeechObjectClass parent_class;
};


GType 
speech_exception_get_type(void);

SpeechException * 
speech_exception_new(void);

SpeechException * 
speech_exception_new_with_message(gchar *message);

gchar * 
speech_exception_get_message(SpeechException *exception);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_EXCEPTION_H__ */
