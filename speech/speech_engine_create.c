
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The #SpeechEngineCreate object is implemented by #SpeechEngineModeDesc 
 * objects obtained through calls to the #SpeechEngineCentral objects of 
 * each speech engine registered with the #SpeechCentral class.
 *
 * <title>Note:</title> most applications do not need to use this object.
 *
 * Each engine implementation must sub-class either
 * #SpeechRecognitionRecognizerModeDesc or #SpeechSynthesisSynthesizerModeDesc
 * to implement the #SpeechEngineCreate object.
 *
 * This implementation mechanism allows the engine to embed additional
 * mode information (engine-specific mode identifiers, GUIDs etc)
 * that simplify creation of the engine if requested by the
 * #SpeechCentral class. The engine-specific mode descriptor may
 * need to override equals and other functions if engine-specific features
 * are defined.
 *
 * The engine must perform the same security checks on access to
 * speech engines as the #SpeechCentral object.
 *
 * See: #SpeechCentral
 * See: #SpeechEngineCentral
 * See: #SpeechEngineModeDesc
 * See: #SpeechRecognitionRecognizerModeDesc
 * See: #SpeechSynthesisSynthesizerModeDesc
 */

#include <speech/speech_engine_create.h>


/**
 * speech_engine_create_get_type:
 *
 * Returns: the type ID for #SpeechEngineCreate.
 */

GType
speech_engine_create_get_type(void)
{
    printf("speech_engine_create_get_type called.\n");
}


SpeechEngineCreate *
speech_engine_create_new(void)
{
    printf("speech_engine_create_new called.\n");
}


/**
 * speech_engine_create_create_engine:
 * @create: the #SpeechEngineCreate object this operation will be applied to.
 * @engine: return an engine with the properties specified by this object.
 *
 * Create an engine with the properties specified by this object.
 * A new engine should be created in the #SPEECH_ENGINE_DEALLOCATED
 * state.
 *
 * See: #SPEECH_ENGINE_DEALLOCATED
 *
 * Returns: the operation status.
 * <itemizedlist>
 *   <listitem>#SPEECH_SECURITY_EXCEPTION if the caller does not have
 *	       speech_central_create_recognize() permission but is
 *	       attempting to create a #SpeechRecognitionRecognizer.
 *             </listitem>
 *   <listitem>#SPEECH_ILLEGAL_ARGUMENT_EXCEPTION if the properties of the
 *	       #SpeechEngineModeDesc do not refer to a known engine or 
 *             engine mode.</listitem>
 *   <listitem>#SPEECH_ENGINE_EXCEPTION if the engine defined by this
 *	       #SpeechEngineModeDesc could not be properly created.</listitem>
 * </itemizedlist>
 */

SpeechStatus
speech_engine_create_create_engine(SpeechEngineCreate *create,
                                   SpeechEngine *engine)
{
    printf("speech_engine_create_create_engine called.\n");
}
