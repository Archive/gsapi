
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * The listener object for receiving events associated with the audio
 * input or output of a #SpeechEngine. A #SpeechAudioListener is attached 
 * to a #SpeechEngine by the speech_audio_manager_add_audio_listener()
 * funtion of the engine's #SpeechAudioManager.
 *
 * #SpeechRecognitionRecognizerAudioListener extends this object to support.
 * #SpeechRecognitionRecognizerAudioEvents provided by a 
 * #SpeechRecognitionRecognizer.
 *
 * See: speech_audio_manager_add_audio_listener()
 * See: #SpeechRecognitionRecognizerAudioEvent
 * See: #SpeechRecognitionRecognizerAudioListener
 */

#include <speech/speech_audio_listener.h>


/**
 * speech_audio_listener_get_type:
 *
 * Returns: the type ID for #SpeechAudioListener.
 */

GType
speech_audio_listener_get_type(void)
{
    printf("speech_audio_listener_get_type called.\n");
}


SpeechAudioListener *
speech_audio_listener_new(void)
{
    printf("speech_audio_listener_new called.\n");
}
