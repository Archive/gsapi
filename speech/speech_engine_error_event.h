
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* An asynchronous notification of an internal error in the engine which 
 * prevents normal behavior of that engine.
 */


#ifndef __SPEECH_ENGINE_ERROR_EVENT_H__
#define __SPEECH_ENGINE_ERROR_EVENT_H__

#include <speech/speech_types.h>
#include <speech/speech_engine_event.h>
#include <speech/speech_engine.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ENGINE_ERROR_EVENT              (speech_engine_error_event_get_type())
#define SPEECH_ENGINE_ERROR_EVENT(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ENGINE_ERROR_EVENT, SpeechEngineErrorEvent))
#define SPEECH_ENGINE_ERROR_EVENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ENGINE_ERROR_EVENT, SpeechEngineErrorEventClass))
#define SPEECH_IS_ENGINE_ERROR_EVENT(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ENGINE_ERROR_EVENT))
#define SPEECH_IS_ENGINE_ERROR_EVENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ENGINE_ERROR_EVENT))
#define SPEECH_ENGINE_ERROR_EVENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ENGINE_ERROR_EVENT, SpeechEngineErrorEventClass))


/**
 * Identifier for event issued when engine error occurs.
 *
 * See: speech_engine_listener_engine_error()
 */

static int SPEECH_ENGINE_ERROR = 550;


GType 
speech_engine_error_event_get_type(void);

SpeechEngineErrorEvent * 
speech_engine_error_event_new(SpeechEngine *source,
                              int id,
                              gchar *error,
                              long old_engine_state,
                              long new_engine_state);

gchar * 
speech_engine_error_event_get_engine_error(SpeechEngineErrorEvent *event);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ENGINE_ERROR_EVENT_H__ */
