
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/*
 * #SpeechEngineModeDesc provides information about a specific
 * operating mode of a speech engine. The 
 * speech_central_available_recognizers() and 
 * speech_central_available_synthesizers() functions of the #SpeechCentral
 * object provide a list of mode descriptors for all operating modes of
 * registered engines. Applications may also create
 * #SpeechEngineModeDescs for use in selecting and creating engines.
 * Examples of uses mode descriptors are provided in the documentation for
 * the #SpeechCentral object.
 *
 * The properties defined in the #SpeechEngineModeDesc class apply to
 * all speech engines including speech recognizers and speech synthesizers.
 * The #SpeechRecognitionRecognizerModeDesc and 
 * #SpeechSynthesisSynthesizerModeDesc objects extend the 
 * #SpeechEngineModeDesc object to define specialized properties for 
 * recognizers and synthesizers.
 *
 * The #SpeechEngineModeDesc and its sub-classes use set/get property
 * patterns. The list of properties is outlined below.
 *
 * The properties of #SpeechEngineModeDesc and its sub-classes are all
 * object references.
 * A #SpeechTristate value for a property means that its three values are one 
 * of #SPEECH_IS_TRUE, #SPEECH_IS_FALSE or #SPEECH_DONT_CARE.
 *
 * The basic properties of an engine defined by #SpeechEngineModeDesc
 * are:
 *
 * <itemizedlist>
 *   <title>engine name</title>
 *   <listitem>A string that uniquely identifies a speech engine.
 *	 e.g. "Acme Recognizer"</listitem>
 *
 *   <title>mode name</title>
 *   <listitem>A string that uniquely identifies a mode of operation of the
 *       speech engine.
 *	 e.g. "Spanish Dictator"</listitem>
 *
 *   <title>Locale</title>
 *   <listitem>A string representing the language supported by the engine 
 *       mode. The country code may be optionally defined for an engine. 
 *       The Locale variant is typically ignored.</listitem>
 *
 *   <title>Running</title>
 *   <listitem>A #SpeechTristate value indicating whether a speech engine is
 *	 already running. This allows for the selection of engines that
 *	 already running so that system resources are conserved.</listitem>
 * </itemizedlist>
 *
 * <title>Selection</title>
 *
 * There are two types of #SpeechEngineModeDesc object (and its
 * sub-classes): those created by a speech engine and those created by an
 * application. Engine-created descriptors are obtained through the
 * speech_central_available_recognizers() and 
 * speech_central_available_synthesizers() functions of the #SpeechCentral 
 * object and must have all features set to non-null values.
 *
 * Applications can create descriptors using the constructors of
 * the descriptor classes.
 *
 * Typically, application-created descriptors are used to test the
 * engine-created descriptors to select an appropriate engine for creation.
 *
 * An application can create a descriptor and pass it to the
 * speech_central_create_recognizer() or speech_central_create_synthesizer()
 * functions of #SpeechCentral. In this common approach, the #SpeechCentral
 * performs the engine selection.
 *
 * Applications that need advanced selection criterion will
 *
 * <orderedlist>
 *   <listitem>Request a list of engine mode descriptors from
 *	 speech_central_available_recognizers() or 
 *	 speech_central_available_synthesizers(),</listitem>
 *   <listitem>Select one of the descriptors using the functions of
 *	 #SpeechEngineList and #SpeechEngineModeDesc and its
 *	 sub-classes,</listitem>
 *   <listitem>Pass the selected descriptor to the 
 *       speech_central_create_recognizer() or
 *	 speech_central_create_synthesizer() function of #SpeechCentral.
         </listitem>
 * </orderedlist>
 *
 * See: #SpeechRecognitionRecognizerModeDesc
 * See: #SpeechSynthesisSynthesizerModeDesc
 * See: #SpeechCentral
 */

#include <speech/speech_engine_mode_desc.h>


/**
 * speech_engine_mode_desc_get_type:
 *
 * Returns: the type ID for #SpeechEngineModeDesc.
 */

GType
speech_engine_mode_desc_get_type(void)
{
    printf("speech_engine_mode_desc_get_type called.\n");
}


SpeechEngineModeDesc *
speech_engine_mode_desc_new(void)
{
    printf("speech_engine_mode_desc_new called.\n");
}


/**
 * speech_engine_mode_desc_new_with_locale:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will 
 *             be applied to.
 * @locale: the locale. The locale for an engine mode must have
 *	    the language defined but the country may be undefined.
 *
 * Create  a #SpeechEngineModeDesc for a locale.
 * The engine name, mode name and running are set to %NULL.
 *
 * Returns:
 */

SpeechEngineModeDesc *
speech_engine_mode_desc_new_with_locale(gchar *locale)
{
    printf("speech_engine_mode_desc_new_with_locale called.\n");
}


/**
 * speech_engine_mode_desc_new_with_name_and_state:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 * @engine_name: the engine name. The engine name should be a unique string 
 *               across the provider company and across companies.
 * @mode_name: the mode name that should uniquely identify a single mode of 
 *             operation of a speech engine (per-engine unique).
 * @locale: the locale. The locale for an engine mode must have	the language 
 *          defined but the country may be undefined. The locale variant is 
 *          typically ignored.
 * @running: the running feature. Values may be #SPEECH_IS_TRUE, 
 *           #SPEECH_IS_FALSE or #SPEECH_DONT_CARE.
 *
 * Create with engine name, mode name, locale and running. Any parameter 
 * may be %NULL.
 *
 * Returns:
 */

SpeechEngineModeDesc *
speech_engine_mode_desc_new_with_name_and_state(gchar *engine_name,
                                                gchar * mode_name,
                                                gchar * locale,
                                                SpeechTristate running)
{
    printf("speech_engine_mode_desc_new_with_name_and_state called.\n");
}


/**
 * speech_engine_mode_desc_equals:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 * @an_object: a #SpeechEngineModeDesc to test for equality against.
 *
 * True if and only if the parameter is not %NULL and is a
 * #SpeechEngineModeDesc with equal values of Locale, engineName and modeName.
 *
 * Returns: a boolean indication of equality between this
 *	   #SpeechEngineModeDesc and anObject
 */

gboolean
speech_engine_mode_desc_equals(SpeechEngineModeDesc *mode_desc,
                               SpeechEngineModeDesc *an_object)
{
    printf("speech_engine_mode_desc_equals called.\n");
}


/**
 * speech_engine_mode_desc_get_engine_name:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 *
 * Get the engine name. The engine name should be a unique string
 * across the provider company and across companies.
 *
 * Returns: the engine name.
 */

gchar *
speech_engine_mode_desc_get_engine_name(SpeechEngineModeDesc *mode_desc)
{
    printf("speech_engine_mode_desc_get_engine_name called.\n");
}


/**
 * speech_engine_mode_desc_get_locale:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 *
 * Get the locale. The locale for an engine mode must have
 * the language defined but the country may be undefined.
 * The locale variant is typically ignored.
 *
 * Returns: the locale.
 */

gchar *
speech_engine_mode_desc_get_locale(SpeechEngineModeDesc *mode_desc)
{
    printf("speech_engine_mode_desc_get_locale called.\n");
}


/**
 * speech_engine_mode_desc_get_mode_name:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 *
 * Get the mode name. The mode that should uniquely identify a
 * single mode of operation of a speech engine (per-engine unique).
 *
 * Returns: the mode name.
 */

gchar *
speech_engine_mode_desc_get_mode_name(SpeechEngineModeDesc *mode_desc)
{
    printf("speech_engine_mode_desc_get_mode_name called.\n");
}


/**
 * speech_engine_mode_desc_get_running:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 *
 * Get the running feature. Values may be #SPEECH_IS_TRUE, #SPEECH_IS_FALSE or
 * #SPEECH_DONT_CARE.
 *
 * Returns: the running feature.
 */

SpeechTristate
speech_engine_mode_desc_get_running(SpeechEngineModeDesc *mode_desc)
{
    printf("speech_engine_mode_desc_get_running called.\n");
}


/**
 * speech_engine_mode_desc_match:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 * @require: a #SpeechEngineModeDesc containing the feature set required.
 *
 * Determine whether a #SpeechEngineModeDesc has all the features defined 
 * in the @require object. Strings in @require which are either %NULL or
 * zero-length ("") are not tested, including those in the locale.
 * All string comparisons are exact (case-sensitive).
 *
 * Returns: a boolean indication of whether this #SpeechEngineModeDesc has 
 *          the feature set given in @require.
 */

gboolean
speech_engine_mode_desc_match(SpeechEngineModeDesc *mode_desc,
                              SpeechEngineModeDesc *require)
{
    printf("speech_engine_mode_desc_match called.\n");
}


/**
 * speech_engine_mode_desc_set_engine_name:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 * @engine_name: the engine name.
 *
 * Set the engine name. The engine name should be a unique string
 * across the provider company and across companies. May be %NULL.
 */

void
speech_engine_mode_desc_set_engine_name(SpeechEngineModeDesc *mode_desc,
                                        gchar *engine_name)
{
    printf("speech_engine_mode_desc_setEngineName called.\n");
}


/**
 * speech_engine_mode_desc_set_locale:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 * @locale: the locale.
 *
 * Set the locale. The locale for an engine mode must have
 * the language defined but the country may be undefined.
 * The locale variant is typically ignored. May be %NULL.
 */

void
speech_engine_mode_desc_set_locale(SpeechEngineModeDesc *mode_desc,
                                   gchar *locale)
{
    printf("speech_engine_mode_desc_setLocale called.\n");
}


/**
 * speech_engine_mode_desc_set_mode_name:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 * @mode_name: the mode name.
 *
 * Set the mode name. The mode name should uniquely identify a
 * single mode of operation of a speech engine (per-engine unique).
 * May be %NULL.
 */

void
speech_engine_mode_desc_set_mode_name(SpeechEngineModeDesc *mode_desc,
                                      gchar *mode_name)
{
    printf("speech_engine_mode_desc_setModeName called.\n");
}


/**
 * speech_engine_mode_desc_set_running:
 * @mode_desc: the #SpeechEngineModeDesc object that this operation will be 
 *             applied to.
 * @running: the running feature.
 *
 * Set the running feature. Values may be #SPEECH_IS_TRUE, #SPEECH_IS_FALSE
 * or #SPEECH_DONT_CARE.
 */

void
speech_engine_mode_desc_set_running(SpeechEngineModeDesc *mode_desc,
                                    SpeechTristate running)
{
    printf("speech_engine_mode_desc_setRunning called.\n");
}
