
/*
 * $Header$
 *
 * Copyright (c) 2002-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.            See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

/* Signals that a speech error has occurred. */

#ifndef __SPEECH_ERROR_H__
#define __SPEECH_ERROR_H__

#include <speech/speech_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPEECH_TYPE_ERROR              (speech_error_get_type())
#define SPEECH_ERROR(obj)              (G_TYPE_CHECK_INSTANCE_CAST((obj), SPEECH_TYPE_ERROR, SpeechError))
#define SPEECH_ERROR_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), SPEECH_TYPE_ERROR, SpeechErrorClass))
#define SPEECH_IS_ERROR(obj)           (G_TYPE_CHECK_INSTANCE_TYPE((obj), SPEECH_TYPE_ERROR))
#define SPEECH_IS_ERROR_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), SPEECH_TYPE_ERROR))
#define SPEECH_ERROR_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS((obj), SPEECH_TYPE_ERROR, SpeechErrorClass))

typedef struct _SpeechError            SpeechError;
typedef struct _SpeechErrorClass       SpeechErrorClass;

struct _SpeechError {
    SpeechObject object;
};

struct _SpeechErrorClass {
    SpeechObjectClass parent_class;
};


GType 
speech_error_get_type(void);

SpeechError * 
speech_error_new(void);

SpeechError * 
speech_error_new_with_message(gchar *message);

gchar * 
speech_error_get_message(SpeechError *error);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPEECH_ERROR_H__ */
