#!/bin/sh -x
# Script to compile the GNOME Speech IDL files.
#
# Adjust as needed for your distribution.
BASE_DIR=/opt/gnome-2.0

BASE_IDL_DIR=$BASE_DIR/share/idl
BONOBO_ACTIVATION_IDL_DIR=$BASE_IDL_DIR/bonobo-activation-2.0
LIBBONOBO_IDL_DIR=$BASE_IDL_DIR/bonobo-2.0
IDLFLAGS="-I$BONOBO_ACTIVATION_IDL_DIR -I$LIBBONOBO_IDL_DIR -I. --add-imodule"

$BASE_DIR/bin/orbit-idl-2 $IDLFLAGS GNOME_Speech.idl
