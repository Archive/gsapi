#!/bin/sh -x
#
# Script to generate the HTML web pages from the GNOME Speech 0.6 IDL files.
#
# Adjust the following definitions if files are not in their default
# locations.
#
PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.2/site-packages
export PYTHONPATH
SYNOPSIS=/usr/local/bin/synopsis

$SYNOPSIS \
	-c config.py \
       	-I. \
       	-Ignome-IDL-for-doc-gen/bonobo-2.0 \
	-Ignome-IDL-for-doc-gen/bonobo-activation-2.0 \
       	-Wc,parser=IDL,linker=Linker,formatter=HTML \
	-o ../doc GNOME_Speech.idl
